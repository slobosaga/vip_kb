import { AfterViewInit, Component, HostListener, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppConfigService } from 'src/app/base/configuration.service';
import { GuidedHelpAnswer, GuidedHelpAnswerType, GuidedHelpQuestion, GuidedHelpQuestionType } from 'src/app/models/guided-help.model';
import { GuidedHelpService } from 'src/app/services/guided-help.service';
import { MediaService } from 'src/app/services/media.service';
import { TinyMce as tinyConfig } from 'src/app/models/enums/tiny';
declare var tinymce: any;

@Component({
  selector: 'app-question-modal',
  templateUrl: './question-modal.component.html',
  styleUrls: ['./question-modal.component.scss']
})
export class QuestionModalComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('questionForm', { static: true }) questionForm: NgForm;

  question = new GuidedHelpQuestion();
  questionTypes = new Array<GuidedHelpQuestionType>();
  answerTypes = new Array<GuidedHelpAnswerType>();
  answers = new Array<GuidedHelpAnswer>();
  formData: FormData;
  questionBodyEditorId = 'questionBody';
  questionFooterEditorId = 'questionFooter';

  constructor(@Inject(MAT_DIALOG_DATA) public data: {
    question: GuidedHelpQuestion,
    questionTypes: Array<GuidedHelpQuestionType>,
    answerTypes: Array<GuidedHelpAnswerType>
  },
    private guidedHelpService: GuidedHelpService, private mediaService: MediaService, private appConfigServie: AppConfigService, private mdDialogRef: MatDialogRef<QuestionModalComponent>) { }

  ngOnInit(): void {
    this.question = this.data.question;
    if (this.data.question.answers !== null) {
      this.answers = this.question.answers;
    }
    this.questionTypes = this.data.questionTypes;
    this.answerTypes = this.data.answerTypes;
  }

  ngAfterViewInit(): void {
    this.initTiny(this.questionBodyEditorId, tinyConfig.pluginsExternal, tinyConfig.toolbarExternal);
    this.initTiny(this.questionFooterEditorId, tinyConfig.pluginsExternal, tinyConfig.toolbarExternal);
    tinymce.get(this.questionBodyEditorId).setContent(this.question.body);
    tinymce.get(this.questionFooterEditorId).setContent(this.question.footer);
  }

  ngOnDestroy(): void {
    tinymce.remove('#' + this.questionBodyEditorId);
    tinymce.remove('#' + this.questionFooterEditorId);
  }

  public cancel() {
    this.close(false);
  }

  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close();
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

  addAnswer() {
    this.question.answers.push(new GuidedHelpAnswer());
  }

  deleteAnswer(i) {

  }

  saveQuestion() {
    this.question.footer = tinymce.get(this.questionFooterEditorId).getContent();
    this.question.body = tinymce.get(this.questionBodyEditorId).getContent();
  }

  private initTiny(elementId, tinyPlugins, tinyToolbar) {
    tinymce.init({
      selector: '#' + elementId,
      suffix: '.min',
      element_format: 'html',
      plugins: tinyPlugins,
      toolbar: tinyToolbar,
      menubar: 'file edit view insert format tools table tc help',
      statusbar: true,
      image_caption: true,
      table_grid: true,
      toolbar_mode: 'sliding',
      // templates: this.editorTemplates,
      height: '300',
      width: '1090',
      paste_data_images: true,
      image_title: true,
      images_file_types: 'jpg, jpeg, jfif, gif, png, svg',
      image_advtab: true,
      fullscreen_native: true,
      // images_upload_url: 'postAcceptor.php',
      automatic_uploads: true,
      font_formats: tinyConfig.fontFormats,
      noneditable_noneditable_class: 'snippets',
      file_browser_callback_types: 'image media',
      file_picker_types: 'image media',
      file_picker_callback: (cb, value, meta) => {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = async () => {
          const file = input.files[0];
          const reader = new FileReader();
          reader.onload = async () => {
            const location: string = await this.uploadImage(file);
            cb(location, { title: file.name });
          };
          reader.readAsDataURL(file);
        };
        input.click();
      },
      images_upload_handler: async (blobInfo, success, failure) => {
        const data = await this.uploadImage(blobInfo.blob());
        success(data);
      },
      init_instance_callback: (editor) => {
        editor.on('CloseWindow', (e) => {
          const dialog = e.dialog.getData();
          // this.saveSnippTemplate();
          if (dialog.snippets) {
            // this.addSnippetToList(dialog.snippets);
          }
        });
      }
    });
  }

  async uploadImage(file) {
    this.formData = new FormData();
    this.formData.append('file', file, file.filename);
    const result = await this.mediaService.uploadImage(this.formData);
    return result.location;
  }

}
