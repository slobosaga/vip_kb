import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { WebPortal } from 'src/app/models/webPortal';
import { ConfigService } from 'src/app/services/config.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-delete-portal-modal',
  templateUrl: './delete-portal-modal.component.html',
  styleUrls: ['./delete-portal-modal.component.scss']
})
export class DeletePortalModalComponent implements OnInit {

  selectedPortal: number;
  confirmDialog: MatDialogRef<ConfirmDialogComponent>;
  portalDeleted = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: { webPortals: Array<WebPortal> }, private mdDialogRef: MatDialogRef<DeletePortalModalComponent>, private configService: ConfigService, private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  public cancel() {
    this.close(null);
  }

  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close(this.portalDeleted);
  }

  deletePortal() {
    this.confirmDialogOpen({
      title: 'Delete Portal',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.configService.deleteWebPortal(this.selectedPortal).then(data => {
          this.portalDeleted = true;
          this.close(this.portalDeleted);
        });
      }
    });
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(this.portalDeleted);
  }

  public confirmDialogOpen(options) {
    this.confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: options.title,
        message: options.message,
        cancelText: options.cancelText,
        confirmText: options.confirmText
      }
    });
  }

  public confirmDialogConfirmed(): Observable<any> {
    return this.confirmDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

}
