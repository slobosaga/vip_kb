import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-queryconfig-modal',
  templateUrl: './add-queryconfig-modal.component.html',
  styleUrls: ['./add-queryconfig-modal.component.scss']
})
export class AddQueryconfigModalComponent implements OnInit {
  @ViewChild('typeForm') typeForm: NgForm;

  selectedType: number;

  constructor(private mdDialogRef: MatDialogRef<AddQueryconfigModalComponent>, @Inject(MAT_DIALOG_DATA) public data: {
    queryTypes
  }) { }

  ngOnInit(): void {
  }

  public cancel() {
    this.close(null);
  }

  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close(this.selectedType);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(null);
  }
}
