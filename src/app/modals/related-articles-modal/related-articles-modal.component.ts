import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { RelatedArticle, RelatedArticleObj, RelatedArticlePaging } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-related-articles-modal',
  templateUrl: './related-articles-modal.component.html',
  styleUrls: ['./related-articles-modal.component.scss']
})
export class RelatedArticlesModalComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;


  paging = new RelatedArticlePaging();
  articleList = new RelatedArticleObj();
  dataSource: MatTableDataSource<RelatedArticle>;
  observable: Observable<any>;
  pageSize = 10;
  tableLength = 0;
  pageIndex = 0;
  skip = 0;
  take = 0;
  includeCategories = false;
  selectedArticles = new Array<RelatedArticle>();


  constructor(@Inject(MAT_DIALOG_DATA) public data: {
    categories: number[],
    languageId: number,
    maxNo: number,
    articleId: number
  }, private mdDialogRef: MatDialogRef<RelatedArticlesModalComponent>, private articleService: ArticleService, private snackBar: MatSnackBar) {
    this.dataSource = new MatTableDataSource<RelatedArticle>(this.articleList.articles);
    this.observable = this.dataSource.connect();
  }

  ngOnInit(): void {
    this.getArticles();
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close(this.selectedArticles);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

  async getArticles() {
    this.paging.categories = this.data.categories;
    this.paging.languageId = this.data.languageId;
    this.articleList = await this.articleService.searchRelatedArticles(this.paging, this.skip, this.pageSize);
    this.dataSource.data = this.articleList.articles;
    if (this.articleList != null) {
      this.tableLength = this.articleList.total;
    } else {
      this.tableLength = 0;
    }
  }

  keywordInput() {
    this.pageIndex = 0;
    setTimeout(() => {
      this.getArticles();
    }, 900);
  }

  pageChange(event: any) {
    this.skip = event.pageSize * (event.pageIndex);
    this.pageSize = event.pageSize;
    this.getArticles();
  }

  addArticle(i) {
    if (this.data.maxNo > this.selectedArticles.length) {
      if (!this.selectedArticles.find(s => s.id === i)) {
        this.selectedArticles.push(this.articleList.articles.find(a => a.id === i));
      } else {
        this.snackBar.open('You have already selected that article', 'OK', { duration: 2000 });
      }
    } else {
      this.snackBar.open('You have selected the maximum number of articles', 'OK', { duration: 2000 });
    }
  }

  removeArticle(i) {
    this.selectedArticles.splice(i, 1);
  }

}
