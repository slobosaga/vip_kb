import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-preview-img-modal',
  templateUrl: './preview-img-modal.component.html',
  styleUrls: ['./preview-img-modal.component.scss']
})
export class PreviewImgModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string, private mdDialogRef: MatDialogRef<PreviewImgModalComponent>) { }

  ngOnInit(): void {
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close(true);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

}
