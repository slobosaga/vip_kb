import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ArticleGet } from 'src/app/models/article';
import { Comment, CommentStatus } from 'src/app/models/comment';
import { ArticleService } from 'src/app/services/article.service';
import { CommentService } from 'src/app/services/comment.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-comment-modal',
  templateUrl: './comment-modal.component.html',
  styleUrls: ['./comment-modal.component.scss']
})
export class CommentModalComponent implements OnInit {

  commentStatus: string;
  article = new ArticleGet();

  constructor(@Inject(MAT_DIALOG_DATA) public data: { comment: Comment, statuses: Array<CommentStatus> }, private mdDialogRef: MatDialogRef<CommentModalComponent>, private commentService: CommentService, private articleService: ArticleService, private snackBar: MatSnackBar, private eventEmitter: EventEmitterService, private router: Router) { }

  async ngOnInit() {
    this.article = null;
    this.commentStatus = this.data.statuses.find(s => s.name === this.data.comment.status).name;
    if (this.data.comment.articleId !== null || this.data.comment.type.toLowerCase() !== 'recommendation') {
      this.article = await this.articleService.getArticleContent(this.data.comment.articleId, this.data.comment.languageId);
    }
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close(true);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

  async updateCommentStatus() {
    this.commentService.updateCommentStatus(this.data.comment.id, this.commentStatus).then(data => {
      if (data) {
        this.snackBar.open('Comment Status Updated Successfully', 'OK', { duration: 3500 });
        this.eventEmitter.emitCommentChange.emit(true);
        this.close(false);
      }
    });
  }

  navigateToEditArticle(articleId: string, languageId) {
    this.articleService.lockArticle(articleId).then(data => {
      if (data !== undefined) {
        this.router.navigate([`/articles/edit-article/${articleId}/${languageId}`]);
      }
    });
    this.close(false);
  }

}
