import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Category } from 'src/app/models/category';
import { Language } from 'src/app/models/enums/language';
import { Translations } from 'src/app/models/link-group';
import { ArticleService } from 'src/app/services/article.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-category-modal',
  templateUrl: './category-modal.component.html',
  styleUrls: ['./category-modal.component.scss']
})
export class CategoryModalComponent implements OnInit {
  @ViewChild('categoryEdit') categoryEdit: NgForm;

  languages = new Array<Language>();

  constructor(@Inject(MAT_DIALOG_DATA) public data: {
    cancelText: string,
    confirmText: string,
    message: string,
    title: string,
    category: Category;
  }, private mdDialogRef: MatDialogRef<CategoryModalComponent>, private eventEmiter: EventEmitterService, private articleService: ArticleService) { }

  async ngOnInit() {
    this.languages = await this.articleService.getAllLanguages();
    if (this.data.category.translations != null) {
      this.data.category.translations.forEach(t => {
        t.language = this.languages.find(l => l.id === t.languageId).name;
      });
    }
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }
  public confirm() {
    this.categoryEdit.form.markAllAsTouched();
    if (this.categoryEdit.form.valid) {
      this.close(this.data.category);
    }
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

}
