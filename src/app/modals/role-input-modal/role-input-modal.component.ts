import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-role-input-modal',
  templateUrl: './role-input-modal.component.html',
  styleUrls: ['./role-input-modal.component.scss']
})
export class RoleInputModalComponent implements OnInit {
  @ViewChild('inputForm') inputForm: NgForm;

  constructor(@Inject(MAT_DIALOG_DATA) public data: {
    cancelText: string,
    confirmText: string,
    message: string,
    title: string,
    role: {
      name: string,
      is2FA: boolean,
      color: string
    },
  }, private mdDialogRef: MatDialogRef<RoleInputModalComponent>, private eventEmiter: EventEmitterService) { }

  ngOnInit(): void {
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }
  public confirm() {
    if (this.inputForm.form.valid === true) {
      this.close(this.data.role);
    }
  }
  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

}
