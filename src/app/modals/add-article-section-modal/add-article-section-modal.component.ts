import { HostListener } from '@angular/core';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ArticleSection, Section } from 'src/app/models/section';
import { SectionService } from 'src/app/services/section.service';

@Component({
  selector: 'app-add-article-section-modal',
  templateUrl: './add-article-section-modal.component.html',
  styleUrls: ['./add-article-section-modal.component.scss']
})
export class AddArticleSectionModalComponent implements OnInit {

  selectedSections: Array<number>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: { article, sections: Array<Section>, articleSectin: ArticleSection }, private mdDialogRef: MatDialogRef<AddArticleSectionModalComponent>, private sectionService: SectionService) { }

  ngOnInit(): void {
  }

  public cancel() {
    this.close(null);
  }

  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close(this.selectedSections);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(null);
  }

}
