import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-forgotpass-modal',
  templateUrl: './forgotpass-modal.component.html',
  styleUrls: ['./forgotpass-modal.component.scss']
})
export class ForgotpassModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {
    cancelText: string,
    confirmText: string,
    message: string,
    title: string,
    input: string
  },          private mdDialogRef: MatDialogRef<ForgotpassModalComponent>, private eventEmiter: EventEmitterService) { }

  ngOnInit(): void {
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }
  public confirm() {
    this.close(this.data.input);
  }
  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

}
