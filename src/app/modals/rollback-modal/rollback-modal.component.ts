import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ArticleRevision, ArticleRevisions } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-rollback-modal',
  templateUrl: './rollback-modal.component.html',
  styleUrls: ['./rollback-modal.component.scss']
})
export class RollbackModalComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  internalEditor = 'internal-editor';
  externalEditor = 'external-editor';
  articleRevision = new ArticleRevision();
  revisions = new Array<ArticleRevisions>();
  dataSource: MatTableDataSource<ArticleRevisions>;
  displayedColumns = ['subject', 'articleStatus', 'updatedBy', 'updatedDate'];
  showInternal = false;
  showExternal = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data: { articleId: number, languageId: number, revisions: Array<ArticleRevisions> }, private articleService: ArticleService, private mdDialogRef: MatDialogRef<RollbackModalComponent>) {
    this.articleRevision = null;
  }

  ngOnInit(): void {
    this.getArticleRevisions();
  }

  async getArticleRevisions() {
    // this.revisions = await this.articleService.getArticleRevisions(this.data.articleId, this.data.languageId);
    this.dataSource = new MatTableDataSource<ArticleRevisions>(this.data.revisions);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  async getArticleRevisionContent(revisionId) {
    this.articleRevision = await this.articleService.getArticleRevisionContent(this.data.articleId, revisionId);
    if (this.articleRevision.seo !== null) {
      this.articleRevision.seo = this.articleRevision.seo.split(',');
    }
  }

  showInternalExternal() {
    this.showExternal = !this.showExternal;
    this.showInternal = !this.showInternal;
  }

  public cancel() {
    this.close(null);
  }

  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close(this.articleRevision);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(null);
  }

}
