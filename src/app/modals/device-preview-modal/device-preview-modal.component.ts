import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-device-preview-modal',
  templateUrl: './device-preview-modal.component.html',
  styleUrls: ['./device-preview-modal.component.scss']
})
export class DevicePreviewModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: { articleBody: string }) { }

  ngOnInit(): void {
  }

}
