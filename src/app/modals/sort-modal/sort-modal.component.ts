import { AfterViewInit, Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ArticleAttachment } from 'src/app/models/article';
import { CdkDragDrop, CdkDropList } from '@angular/cdk/drag-drop';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Component({
  selector: 'app-sort-modal',
  templateUrl: './sort-modal.component.html',
  styleUrls: ['./sort-modal.component.scss']
})
export class SortModalComponent implements OnInit, AfterViewInit {
  @ViewChild('scrollableParent') scrollableParent: ElementRef;
  @ViewChild(CdkDropList) dropList: CdkDropList;

  attachments = new Array<ArticleAttachment>();
  confirmDialog: MatDialogRef<ConfirmDialogComponent>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Array<ArticleAttachment>, private mdDialogRef: MatDialogRef<SortModalComponent>, private dialog: MatDialog) {
    this.attachments = this.data;
  }

  ngOnInit(): void {
    this.attachments = this.data;
    this.attachments = this.attachments.sort((a, b) => a.order - b.order);
  }

  ngAfterViewInit() {
    this.dropList._dropListRef.withScrollableParents([this.scrollableParent.nativeElement]);
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.data = this.attachments;
    this.mdDialogRef.close(this.data);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }

  dropAttachment(event: CdkDragDrop<string[]>) {
    this.moveItemInArray(this.attachments, event.previousIndex, event.currentIndex);
    this.attachments.forEach(a => {
      a.order = this.attachments.indexOf(a) + 1;
    });
  }

  removeAttachment(index) {
    this.confirmDialogOpen({
      title: 'Delete Attachment',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.attachments.splice(index, 1);
        for (let i = 0; i < this.attachments.length; i++) {
          const att = this.attachments[i];
          att.order = i + 1;
        }
      }
    });
  }

  moveItemInArray(attachments: Array<ArticleAttachment>, previousIndex, currentIndex) {
    const attachment = attachments[currentIndex];
    attachments[currentIndex] = attachments[previousIndex];
    attachments[previousIndex] = attachment;
  }

  public confirmDialogOpen(options) {
    this.confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: options.title,
        message: options.message,
        cancelText: options.cancelText,
        confirmText: options.confirmText
      }
    });
  }
  public confirmDialogConfirmed(): Observable<any> {
    return this.confirmDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }


}
