import { Component, OnInit, Inject, HostListener, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EventEmitter } from 'protractor';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-input-modal',
  templateUrl: './input-modal.component.html',
  styleUrls: ['./input-modal.component.scss']
})
export class InputModalComponent implements OnInit {
  @ViewChild('inputForm') inputForm: NgForm;

  constructor(@Inject(MAT_DIALOG_DATA) public data: {
    cancelText: string,
    confirmText: string,
    message: string,
    title: string,
    input: string
  }, private mdDialogRef: MatDialogRef<InputModalComponent>, private eventEmiter: EventEmitterService) { }

  ngOnInit(): void {
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }
  public confirm() {
    if (this.inputForm.form.valid === true) {
      this.close(this.data.input);
    }
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }
}
