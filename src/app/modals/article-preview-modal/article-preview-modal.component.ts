import { AfterViewInit, Component, HostListener, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-article-preview-modal',
  templateUrl: './article-preview-modal.component.html',
  styleUrls: ['./article-preview-modal.component.scss']
})
export class ArticlePreviewModalComponent implements OnInit, AfterViewInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: string, private mdDialogRef: MatDialogRef<ArticlePreviewModalComponent>) { }

  ngOnInit(): void {
  }

  public cancel() {
    this.close(false);
  }
  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.mdDialogRef.close(true);
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }
  ngAfterViewInit(): void {
    const elements = document.getElementsByName('highlight');
    for (let i = 0; i < elements.length; i++) {
      const ele = elements[i];
      ele.style.background = 'yellow';
      ele.style.backgroundColor = 'yellow';
    }
  }

}
