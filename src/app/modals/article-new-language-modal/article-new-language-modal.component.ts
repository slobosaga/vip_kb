import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Language } from 'src/app/models/enums/language';
import { ArticleService } from 'src/app/services/article.service';
import { ArticlePreviewModalComponent } from '../article-preview-modal/article-preview-modal.component';

@Component({
  selector: 'app-article-new-language-modal',
  templateUrl: './article-new-language-modal.component.html',
  styleUrls: ['./article-new-language-modal.component.scss']
})
export class ArticleNewLanguageModalComponent implements OnInit {
  @ViewChild('languageForm') languageForm: NgForm;

  languages: Array<Language> = new Array<Language>();
  articleLangs: Array<number>;

  constructor(private articleService: ArticleService, private router: Router, @Inject(MAT_DIALOG_DATA) public data: {
    articleId: string,
    languageId: number
  }, private mdDialogRef: MatDialogRef<ArticlePreviewModalComponent>) { }

  async ngOnInit() {
    await this.getArticleLanguages();
    await this.getLanguages();
    const filter = this.articleLangs.map(a => a);
    this.languages = this.languages.filter(lang => !this.articleLangs.includes(lang.id));
  }

  async getLanguages() {
    this.languages = await this.articleService.getAllLanguages();
  }

  async getArticleLanguages() {
    this.articleLangs = await this.articleService.getArticleLanguages(this.data.articleId);
  }

  navigate() {
  }

  public cancel() {
    this.close(false);
  }

  public close(value) {
    this.mdDialogRef.close(value);
  }

  public confirm() {
    this.languageForm.form.markAllAsTouched();
    if (this.languageForm.form.valid === true) {
      this.router.navigate([`/articles/create-new-article-new-language/${this.data.articleId}/${this.data.languageId}`]);
      this.mdDialogRef.close();
    }
  }

  @HostListener('keydown.esc')
  public onEsc() {
    this.close(false);
  }


}
