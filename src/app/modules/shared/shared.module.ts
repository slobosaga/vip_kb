import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryMenuComponent } from 'src/app/components/category-menu/category-menu.component';
import { MatMenuModule } from '@angular/material/menu';



@NgModule({
  declarations: [ CategoryMenuComponent
  ],
  imports: [
    CommonModule,
    MatMenuModule
  ], exports: [ CategoryMenuComponent
  ]
})
export class SharedModule { }
