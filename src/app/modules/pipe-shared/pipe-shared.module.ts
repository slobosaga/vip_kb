import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionPipe } from 'src/app/base/permissionPipe';
import { TransformNumberPipe } from 'src/app/base/numberpipe';



@NgModule({
  imports: [

  ],
  declarations: [
    TransformNumberPipe,
    PermissionPipe
  ],
  exports: [
    TransformNumberPipe,
    PermissionPipe
  ]
})
export class PipeSharedModule { }
