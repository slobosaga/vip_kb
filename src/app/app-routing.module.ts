import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'activate-account/:token',
    component: LoginComponent
  },
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: 'login',
  },
  {
    path: '',
    component: LayoutComponent,
    // canActivate: [AuthGuard],
    data: {
      title: ''
    },
    children: [
      {
        path: 'articles',
        loadChildren: 'src/app/components/article/article.module#ArticleModule',
      },
      {
        path: 'application-settings',
        loadChildren: 'src/app/components/app-settings/app-settings.module#AppSettingsModule'
      },
      {
        path: 'site-settings',
        loadChildren: 'src/app/components/site-settings/site-settings.module#SiteSettingsModule'
      },
      {
        path: 'reports',
        loadChildren: 'src/app/components/reports/report.module#ReportModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top', onSameUrlNavigation: 'reload', preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
