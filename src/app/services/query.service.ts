import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientHelper } from '../base/httpHelper';
import { Article } from '../models/article';
import { QueryArticleView, QueryConfig, QueryFilter, QueryRequest, Relevance } from '../models/query';

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  url = 'Query/';
  constructor(private http: HttpClientHelper) { }

  // GET

  getAllFields(): Promise<Array<string>> {
    return this.http.get(this.url + `fields`);
  }

  getQueryConfigs(queryConfigId, showActive, queryTypeId): Promise<Array<QueryConfig>> {
    const params = new HttpParams().set('queryConfigId', queryConfigId)
      .set('showActive', showActive)
      .set('queryTypeId', queryTypeId);
    return this.http.get(this.url, params);
  }

  queryTester(queryFilter: QueryFilter): Promise<Array<QueryArticleView>> {
    queryFilter.languageId = JSON.parse(localStorage.getItem('A1_Admin_language'));
    return this.http.post(this.url + 'querytester', queryFilter, null);
  }

  getAllQueryTypes(): Promise<any> {
    return this.http.get(this.url + 'types');
  }

  getRelevanceFields(): Promise<Array<Relevance>> {
    return this.http.get(this.url + `relevance`);
  }

  // POST

  createQueryConfig(queryConfig: QueryRequest): Promise<any> {
    return this.http.post(this.url, queryConfig);
  }

  // PUT

  updateQueryConfig(queryConfigId, queryRequest: QueryRequest) {
    return this.http.put(this.url + `${queryConfigId}`, queryRequest);
  }

  markQuertConfigAsActive(queryConfigId) {
    return this.http.put(this.url + `${queryConfigId}/activate`, null);
  }

  // DELETE
}
