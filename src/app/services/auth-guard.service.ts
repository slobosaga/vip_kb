import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree, NavigationEnd, NavigationCancel } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { EditArticleComponent } from '../components/article/article/edit-article/edit-article.component';
import { AuthService } from './auth.service';
import { ModalService } from './modal.service';
@Injectable()
export class AuthGuardService implements CanActivate, CanDeactivate<EditArticleComponent> {
  currentRoles = JSON.parse(localStorage.getItem('A1_Admin_a1kbRoles')) as Array<string>;

  constructor(public auth: AuthService, public router: Router, private modalService: ModalService) {
    this.currentRoles = JSON.parse(localStorage.getItem('A1_Admin_a1kbRoles')) as Array<string>;
    this.router.events.pipe(filter(event => event instanceof NavigationCancel)).subscribe((event: NavigationCancel) => {
      localStorage.setItem('A1_Admin_redirectUrl', event.url);
    });
  }
  canDeactivate(component: EditArticleComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if (component.articleRequest.htmlBodyInternal === null) {
      if (component.contentForm.form.dirty === true || component.datesForm.form.dirty === true) {
        this.modalService.confirmDialogOpen({
          title: 'Are you sure?',
          message: 'You have unsaved changes',
          cancelText: 'Cancel',
          confirmText: 'Yes'
        });
        return this.modalService.confirmDialogConfirmed() ? this.modalService.confirmDialogConfirmed() : true;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    } else {

      if (!route.data.isSystemAdmin) {
        if (JSON.parse(localStorage.getItem('A1_Admin_isSystemAdmin')) === true) {
          this.router.navigate(['/application-settings/tenant']);
          return false;
        }
      }
      if (route.data.roles) {
        this.currentRoles = JSON.parse(localStorage.getItem('A1_Admin_a1kbRoles')) as Array<string>;
        return this.compareArrays(this.currentRoles, route.data.roles);
      } else {
        return true;
      }
    }
  }

  compareArrays(arr1: Array<any>, arr2: Array<any>) {
    return arr1.some(item => arr2.includes(item));
  }
}
