import { CollectionViewer } from '@angular/cdk/collections';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { ArticlePaging, ArticleViewResponse } from '../models/article';
import { ArticleService } from './article.service';

export class ArticleDataSource implements DataSource<ArticleViewResponse> {

  private articleSubject = new BehaviorSubject<ArticleViewResponse[]>(null);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private articleService: ArticleService) { }

  loadArticles(paging: ArticlePaging) {
    this.loadingSubject.next(true);

    this.articleService.getAllArticlesObs(paging).pipe(catchError(() => of(null)), finalize(() => this.loadingSubject.next(false))).subscribe(data => this.articleSubject.next(data));
  }

  connect(collectionViewer: CollectionViewer): Observable<ArticleViewResponse[]> {
    return this.articleSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.articleSubject.complete();
    this.loadingSubject.complete();
  }

}
