import { Injectable } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Injectable()
export class AuthService {
  constructor(private notification: NotificationsService, private loader: NgxUiLoaderService) { }

  public isAuthenticated(): boolean {
    const expireDate = localStorage.getItem('A1_Admin_expiresIn');
    const date = new Date();
    const newDate = new Date(expireDate);
    if (newDate) {
      if (newDate < date) {
        this.notification.error('Token Expired', 'Please Login');
        this.loader.stopAllLoader('login');
        this.loader.stopLoader('l-1');
        return false;
      } else {
        return true;
      }
    }
  }
}
