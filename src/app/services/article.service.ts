import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClientHelper } from '../base/httpHelper';
import { ArticleGet, ArticlePaging, ArticleRequest, ArticleRevision, ArticleRevisions, ArticleSort, ArticleView, ArticleViewResponse, FindAndReplaceSnippetReslut, FindReplaceArticleResult, GenerateRelatedArticles, RelatedArticle, RelatedArticleObj, RelatedArticlePaging } from '../models/article';
import { CategoryRequest, CategorySortRequest } from '../models/category';
import { Role } from '../models/role';
import { Snippet } from '../models/snippet';
import { Template } from '../models/template';
import { WebPortal } from '../models/webPortal';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  url = 'Article/';
  headers: any;

  constructor(private httpHelper: HttpClientHelper, private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  // GET

  getArticleComments(articleId: string) {
    return this.httpHelper.get(this.url + `${articleId} + '/comments'`);
  }

  getAllArticles(articlePaging: ArticlePaging): Promise<ArticleViewResponse> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.post(this.url + 'all', articlePaging, params);
  }

  getAllArticlesObs(articlePaging: ArticlePaging): Observable<ArticleViewResponse> {
    const parameters = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpClient.post('https://10.1.132.35/api/Article/all', articlePaging, { headers: this.headers.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), params: parameters }).pipe(map(res => res["payload"]));
  }

  getArticlesForLanguage(languageId: number): Promise<Array<ArticleView>> {
    return this.httpHelper.get(this.url + `language/${languageId}`);
  }

  getArticlesForPortalType(portalTypeId: number): Promise<Array<ArticleView>> {
    return this.httpHelper.get(this.url + `portal/${portalTypeId}`);
  }

  getArticlesForPortalTypeAndLanguage(portalTypeId: number, languageId: number) {
    return this.httpHelper.get(this.url + `portal/${portalTypeId}/language/${languageId}`);
  }

  getArticleVersionStatus(articleId, articleVersionId): Promise<Array<string>> {
    return this.httpHelper.get(this.url + `${articleId}/versions/${articleVersionId}/status`);
  }

  getAllArticleVersionRevisions(articleId, articleVersionId): Promise<Array<string>> {
    return this.httpHelper.get(this.url + `${articleId}/versions/${articleVersionId}`);
  }

  getArticleContent(articleId, languageId): Promise<ArticleGet> {
    const params = new HttpParams().set('languageId', languageId);
    return this.httpHelper.get(this.url + `${articleId}`, params);
  }

  getArticleContentMeta(articleId, languageId): Promise<ArticleGet> {
    const params = new HttpParams().set('languageId', languageId);
    return this.httpHelper.get(this.url + `${articleId}/meta`, params);
  }

  getAllArticleStatuses(): Promise<Array<any>> {
    return this.httpHelper.get(this.url + `articlestatuses`);
  }

  getAllLanguages(all = 'false'): Promise<Array<any>> {
    const params = new HttpParams().set('all', all);
    return this.httpHelper.get(this.url + `languages`, params);
  }

  getAllProtalTypes(): Promise<Array<any>> {
    return this.httpHelper.get(this.url + 'portaltypes');
  }

  getAllCategories(filter: string = ''): Promise<Array<any>> {
    const params = new HttpParams().set('filter', filter);
    return this.httpHelper.get(this.url + 'categories', params );
  }

  getAllCategoriesForWebPortal(webPortalId: Array<string>, filter: string = ''): Promise<Array<any>> {
    let params = new HttpParams();
    webPortalId.forEach(wp => {
      params = params.append('webPortals', wp);
    });
    params = params.append('filter', filter);
    return this.httpHelper.get(this.url + 'categories', params);
  }

  getAllKeywords(): Promise<Array<any>> {
    return this.httpHelper.get(this.url + 'keywords');
  }

  getAllSnippets(from, numOfRows, filter, onlyMine): Promise<any> {
    const params = new HttpParams().set('fromIndex', JSON.stringify(from))
      .set('numOfRows', JSON.stringify(numOfRows))
      .set('filter', filter)
      .set('onlyAuthors', JSON.stringify(onlyMine));
    return this.httpHelper.get(this.url + 'snippets', params);
  }

  getSnippet(id): Promise<Snippet> {
    return this.httpHelper.get(this.url + `snippets/${id}`);
  }

  getAllTemplates(from, numOfRows, filter, onlyMine): Promise<any> {
    const params = new HttpParams().set('fromIndex', JSON.stringify(from))
      .set('numOfRows', JSON.stringify(numOfRows))
      .set('filter', filter)
      .set('onlyAuthors', JSON.stringify(onlyMine));
    return this.httpHelper.get(this.url + 'templates', params);
  }

  getTemplate(id): Promise<Template> {
    return this.httpHelper.get(this.url + `templates/${id}`);
  }

  lockArticle(articleId: string): Promise<string> {
    return this.httpHelper.put(this.url + `${articleId}/lock`, null);
  }

  lockArticleObservable(articleId: string): Observable<string> {
    return this.httpHelper.putObsv(this.url + `${articleId}/lock`, null);
  }

  unlockArticle(articleId: string, isManual = false): Promise<string> {
    const params = new HttpParams().set('isManual', JSON.stringify(isManual));
    return this.httpHelper.put(this.url + `${articleId}/unlock`, null, params);
  }

  unpublishArticle(articleId: string, language): Promise<string> {
    const params = new HttpParams().set('languageId', language);
    return this.httpHelper.put(this.url + `${articleId}/unpublished`, null, params);
  }

  unscheduleArticle(articleId: string, language): Promise<string> {
    const params = new HttpParams().set('languageId', language);
    return this.httpHelper.put(this.url + `${articleId}/unscheduled`, null, params);
  }

  getAllRoles(all: string): Promise<Array<Role>> {
    const params = new HttpParams().set('all', all);
    return this.httpHelper.get('Config/roles', params);
  }

  getMaxNoRelatedArticles(): Promise<number> {
    return this.httpHelper.get(this.url + 'relatedArticles/count');
  }

  getAllWebPortals(): Promise<Array<WebPortal>> {
    return this.httpHelper.get('Config/webportals');
  }

  getArticleCountPerStatus(languageId, categoryId, webPortalId): Promise<Array<{ id: number, name: string, conut: 0 }>> {
    let params = new HttpParams().set('languageId', languageId.toString());
    if (categoryId !== null) {
      params = params.set('categoryId', categoryId);
    }
    if (webPortalId !== null) {
      params = params.set('webPortalId', webPortalId);
    }
    return this.httpHelper.get(this.url + 'count', params);
  }

  getSnippetComfirm(id): Promise<any> {
    return this.httpHelper.get(this.url + `snippets/${id}/confirm`);
  }

  getFindAndReplaceSnippetConfirm(find: string): Promise<FindAndReplaceSnippetReslut> {
    return this.httpHelper.get(this.url + `snippets/find/${find}`);
  }

  getFindAndreplaceArticleConfirm(find: string): Promise<FindReplaceArticleResult> {
    return this.httpHelper.get(this.url + `find/${find}`);
  }

  getArticleLanguages(articleId): Promise<number[]> {
    return this.httpHelper.get(this.url + `${articleId}/languages`);
  }

  getArticleRevisions(articleId, languageId): Promise<Array<ArticleRevisions>> {
    const params = new HttpParams().set('languageId', languageId);
    return this.httpHelper.get(this.url + `${articleId}/revisions`, params);
  }

  getArticleRevisionContent(articleId, revisionId): Promise<ArticleRevision> {
    return this.httpHelper.get(this.url + `${articleId}/revisions/${revisionId}`);
  }


  // POST

  createArticle(articleRequest: ArticleRequest, language): Promise<any> {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.post(this.url, articleRequest, params);
  }

  createArticleForAnotherLanguage(articleRequest: ArticleRequest, articleId, language): Promise<any> {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.post(this.url + `${articleId}`, articleRequest, params);
  }

  createCategory(category: CategoryRequest): Promise<any> {
    return this.httpHelper.post(this.url + 'category', category);
  }

  createCategoryWebPortal(category: CategoryRequest, webPortalId: string): Promise<any> {
    const params = new HttpParams().set('webPortalId', webPortalId);
    return this.httpHelper.post(this.url + 'category', category, params);
  }

  createKeywords(keywords: Array<string>): Promise<any> {
    return this.httpHelper.post(this.url + 'keywords', keywords);
  }

  createSnippet(snippet: ({ name: string, text: string })): Promise<any> {
    return this.httpHelper.post(this.url + 'snippets', snippet);
  }

  createTemplate(template: ({ name: string, text: string })): Promise<any> {
    return this.httpHelper.post(this.url + 'templates', template);
  }

  changeArticleLanguage(articleId: string, articleRequest: ArticleRequest): Promise<any> {
    return this.httpHelper.post(this.url + `${articleId}`, articleRequest);
  }

  getRelatedArticles(relatedArticleRequest: GenerateRelatedArticles): Promise<Array<RelatedArticle>> {
    return this.httpHelper.post(this.url + 'relatedArticles', relatedArticleRequest);
  }

  searchRelatedArticles(paging: RelatedArticlePaging, skip, take): Promise<RelatedArticleObj> {
    const params = new HttpParams().set('fromArticle', skip.toString()).set('numArticles', take.toString());
    return this.httpHelper.post(this.url + `searchArticles`, paging, params);
  }

  // PUT

  updateArticelVersion(articleId: string, articleVersionId: number, article: ArticleRequest): Promise<any> {
    return this.httpHelper.put(this.url + `${articleId}/versions/{${articleVersionId}`, article);
  }

  updateArticleVersionStatus(article: string, articleVersionId: number): Promise<any> {
    return this.httpHelper.put(this.url + `${article}/versions/${articleVersionId}/status`, null);
  }

  updateCategoty(category: CategoryRequest, categoryId: number): Promise<any> {
    return this.httpHelper.put(this.url + `categories/${categoryId}`, category);
  }

  sortCategories(categories: Array<CategorySortRequest>): Promise<any> {
    return this.httpHelper.put(this.url + `categories/sort`, categories);
  }

  sortCategoriesByPortal(categories: Array<CategorySortRequest>, webPortalId: string): Promise<any> {
    return this.httpHelper.put(this.url + `webportals/${webPortalId}/categories/sort`, categories);
  }

  markAsNeedChanges(articleId: string, article: ArticleRequest, language): Promise<any> {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.put(this.url + `${articleId}/needchanges`, article, params);
  }

  markAsApproved(articleId: string, article: ArticleRequest, language): Promise<any> {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.put(this.url + `${articleId}/approved`, article, params);
  }

  markAsSubmited(articleId: string, article: ArticleRequest, language): Promise<any> {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.put(this.url + `${articleId}/submitted`, article, params);
  }

  markAsDraft(articleId: string, article: ArticleRequest, language): Promise<any> {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.put(this.url + `${articleId}/draft`, article, params);
  }

  markAsRejected(articleId: string, article: ArticleRequest, language): Promise<any> {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.put(this.url + `${articleId}/rejected`, null, params);
  }

  publishArticle(articleId: string, article: ArticleRequest, language) {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.put(this.url + `${articleId}/published`, article, params);
  }

  scheduleArticle(articleId: string, article: ArticleRequest, language): Promise<any> {
    const params = new HttpParams().set('languageId', language.toString());
    return this.httpHelper.put(this.url + `${articleId}/scheduled`, article, params);
  }

  updateSnippet(id, snippet: ({ name: string, text: string })): Promise<any> {
    return this.httpHelper.put(this.url + `snippets/${id}`, snippet);
  }

  updateTemplate(id, template: ({ name: string, text: string })): Promise<any> {
    return this.httpHelper.put(this.url + `templates/${id}`, template);
  }

  findAndReplaceSnippet(request: ({ find: string, replace: string })): Promise<any> {
    return this.httpHelper.put(this.url + `findandreplacesnippet`, request);
  }

  findAndReplaceArticle(request: ({ find: string, replace: string })): Promise<any> {
    return this.httpHelper.put(this.url + `findandreplace`, request);
  }

  rollback(articleId, languageId) {
    const param = new HttpParams().set('languageId', languageId);
    return this.httpHelper.put(this.url + articleId + '/rollback', {}, param);
  }

  saveArticleSortOrder(articleSort: ArticleSort) {
    return this.httpHelper.put(this.url + 'sort', articleSort);
  }


  // DELETE

  deleteArticle(articleId: string, language): Promise<any> {
    const params = new HttpParams().set('languageId', language);
    return this.httpHelper.delete(this.url + `${articleId}`, params);
  }

  deleteArticleVersion(articleId: string, articleVersionId: number): Promise<any> {
    return this.httpHelper.delete(this.url + `${articleId}/versions/${articleVersionId}`);
  }

  deleteArticleRevision(articleId: string, articleVersionId: number, articleRevisionsId: number): Promise<any> {
    return this.httpHelper.delete(this.url + `${articleId}/versions/${articleVersionId}/revisions/${articleRevisionsId}`);
  }

  deleteCategory(categoryId: number): Promise<any> {
    return this.httpHelper.delete(this.url + `categories/${categoryId}`);
  }

  deleteTemplate(id): Promise<any> {
    return this.httpHelper.delete(this.url + `templates/${id}`);
  }

  deleteSnippet(id): Promise<any> {
    return this.httpHelper.delete(this.url + `snippets/${id}`);
  }
}
