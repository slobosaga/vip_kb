import { Injectable } from '@angular/core';
import { HttpClientHelper } from '../base/httpHelper';
import { GuidedHelpQuestion } from '../models/guided-help.model';

@Injectable({
  providedIn: 'root'
})
export class GuidedHelpService {

  constructor(private http: HttpClientHelper) { }

  getAll(webPortalId: string, languageId: number) {

  }

  getQuestionTypes() {

  }

  getAnswerTypes() {

  }

  createQuestion(question: GuidedHelpQuestion) {

  }

  updateQuestion(question: GuidedHelpQuestion) {

  }

  deleteQuestion(questionId) {

  }

}
