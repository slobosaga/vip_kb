import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfigService } from '../base/configuration.service';
import { HttpClientHelper } from '../base/httpHelper';
import { Media, MediaFilter, MediaResponse } from '../models/media';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  blobHeaders: HttpHeaders;
  url = 'Media/';

  constructor(private httpHelper: HttpClientHelper, private http: HttpClient, private appConfigService: AppConfigService) {
    this.blobHeaders = new HttpHeaders({
      'Content-Dispo': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
  }

  uploadImage(file: FormData): Promise<any> {
    return this.httpHelper.file(this.url, file);
  }

  getImage(fileName: string): Observable<any> {
    // return this.httpHelper.blob(this.url + `${fileName}`);
    return this.http.get(this.appConfigService.mediaUrl + fileName, { headers: this.blobHeaders.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), withCredentials: false, responseType: 'blob', params: null });
  }

  getAllMedia(filter: MediaFilter): Promise<MediaResponse> {
    const params = new HttpParams().set('filter', filter.filter)
      .set('fromIndex', filter.fromIndex.toString())
      .set('numOfRows', filter.numOfRows.toString())
      .set('onlyAuthors', filter.onlyAuthors ? 'true' : 'false');
    return this.httpHelper.get(this.url, params);
  }

  deleteMedia(fileName): Promise<any> {
    return this.httpHelper.delete(this.url + `${fileName}`);
  }

}
