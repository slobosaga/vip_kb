import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientHelper } from '../base/httpHelper';
import { ArticleSection, ArticleSectionGet, Section, SectionPaging } from '../models/section';


@Injectable({
  providedIn: 'root'
})

export class SectionService {

  url = 'Section/';

  constructor(private http: HttpClientHelper) { }

  // GET

  getAllSections(): Promise<Array<Section>> {
    return this.http.get(this.url);
  }

  getAllSectionArticles(paging: SectionPaging): Promise<Array<ArticleSectionGet>> {
    const params = new HttpParams()
      .set('sectionId', paging.sectionId.toString())
      .set('webPortalId', paging.webPortalId.toString())
      .set('languageId', JSON.parse(localStorage.getItem('A1_Admin_language')));
    return this.http.get(this.url + `${paging.sectionId}`, params);
  }

  // POST

  addArticleToSection(sectionId: number, article: ArticleSection): Promise<any> {
    return this.http.post(this.url + `${sectionId}`, article);
  }

  // PUT

  sortSectionArticles(sectionId: number, sections: Array<ArticleSection>): Promise<any> {
    return this.http.put(this.url + `${sectionId}`, { sections });
  }

  // DELETE

  removeArticleFromSection(sectionId: number, webPortalId: string, articleId: string): Promise<any> {
    const params = new HttpParams()
      .set('webPortalId', webPortalId)
      .set('articleid', articleId);
    return this.http.delete(this.url + `${sectionId}`, params);
  }

}
