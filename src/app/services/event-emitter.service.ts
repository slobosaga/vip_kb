import { EventEmitter, Injectable, Output } from '@angular/core';
import { TreeNode } from '../base/models/treeNode';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {
  @Output() emmitTreeNodes: EventEmitter<Array<TreeNode>> = new EventEmitter<Array<TreeNode>>();
  @Output() emitTreeNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  @Output() emitNodeName: EventEmitter<string> = new EventEmitter<string>();
  @Output() emitNestedTree: EventEmitter<Array<number>> = new EventEmitter<Array<number>>();
  @Output() emitCategories: EventEmitter<Array<{ id: number, name: string }>> = new EventEmitter<Array<{ id: number, name: string }>>();
  @Output() emitExpirationCategories: EventEmitter<Array<{ id: number, name: string }>> = new EventEmitter<Array<{ id: number, name: string }>>();
  @Output() emitLanguageId: EventEmitter<number> = new EventEmitter<number>();
  @Output() emitCommentChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() emitSaveArticleOrder: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

}
