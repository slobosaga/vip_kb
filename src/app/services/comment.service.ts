import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientHelper } from '../base/httpHelper';
import { Comment, CommentFilter, CommentResponse, CommentStatus, CommentType } from '../models/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  url = 'Comment/';

  constructor(private httpHelper: HttpClientHelper) { }

  // GET

  getAllCommentTypes(): Promise<Array<CommentType>> {
    return this.httpHelper.get(this.url + `types`);
  }

  getAllCommentStatuses(): Promise<Array<CommentStatus>> {
    return this.httpHelper.get(this.url + `statuses`);
  }

  // POST

  getAllComments(commentFilter: CommentFilter): Promise<CommentResponse> {
    return this.httpHelper.post(this.url, commentFilter);
  }

  // PUT

  updateCommentStatus(commentId, status): Promise<any> {
    status = new HttpParams().set('commentStatus', status);
    return this.httpHelper.put(this.url + `${commentId}`, null, status);
  }

  // DELETE
}
