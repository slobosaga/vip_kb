import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { HttpClientHelper } from '../base/httpHelper';
import { ArticleCategoryKpiTableRequest, ArticleCategoryKpiTableResponse, ArticleRatingsKpiTableRequest, ArticleRatingsKpiTableResponse, ArticleStatusesKpiTableRequest, ArticleStatusesKpiTableResponse, BookmarkStatisticsKpiTableRequest, BookmarkStatisticsKpiTableResponse, KeywordsAndPhrasesKpiTableRequest, KeywordsAndPhrasesKpiTableResponse, OpenedReadArticlesKpiChartRequest, OpenedReadArticlesKpiChartResponse, OpenedReadArticlesKpiTableRequest, OpenedReadArticlesKpiTableResponse, ReportFilters, ReportTypesResponse } from '../models/reports';

@Injectable({
  providedIn: 'root'
})
export class ReportingService {
  url = 'report';
  filters = new BehaviorSubject<ReportFilters>(new ReportFilters());

  constructor(private httpHelper: HttpClientHelper) { }

  getReportDatePeriods(): Promise<number> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.get(this.url + '/dateperiod',  params);
  }

  getReportTypes(): Promise<Array<ReportTypesResponse>> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.get(this.url + '/types',  params);
  }

  getReportLabels(reportId: number): Promise<any> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.get(this.url + `/${reportId}/labels`,  params);
  }

  getOpenedReadArticlesKpiTableData(data: OpenedReadArticlesKpiTableRequest): Promise<OpenedReadArticlesKpiTableResponse> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.post(this.url + '/articleopenings/table', data, params);
  }

  getOpenedReadArticlesKpiChartData(data: OpenedReadArticlesKpiChartRequest): Promise<OpenedReadArticlesKpiChartResponse> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.post(this.url + '/articleopenings/chart', data, params);
  }

  getArticleRatingsKpiTableData(data: ArticleRatingsKpiTableRequest): Promise<ArticleRatingsKpiTableResponse> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.post(this.url + '/articlegrades', data, params);
  }

  getArticleStatusesKpiTableData(data: ArticleStatusesKpiTableRequest): Promise<ArticleStatusesKpiTableResponse> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.post(this.url + '/statuses', data, params);
  }

  getBookmarkStatisticsKpiTableData(data: BookmarkStatisticsKpiTableRequest): Promise<BookmarkStatisticsKpiTableResponse> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.post(this.url + '/bookmarks', data, params);
  }

  getKeywordsAndPhrasesKpiTableData(data: KeywordsAndPhrasesKpiTableRequest): Promise<KeywordsAndPhrasesKpiTableResponse> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.post(this.url + '/searchphrases', data, params);
  }

  getArticleCategoryKpiTableData(data: ArticleCategoryKpiTableRequest): Promise<ArticleCategoryKpiTableResponse> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpHelper.post(this.url + '/categories', data, params);
  }

}
