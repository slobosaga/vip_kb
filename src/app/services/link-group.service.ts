import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientHelper } from '../base/httpHelper';
import { LinkGroupRequest, LinkGroupResponse, LinkGroupSort, LinkResponse } from '../models/link-group';

@Injectable({
  providedIn: 'root'
})
export class LinkGroupService {
  url = 'Article/';

  constructor(private http: HttpClientHelper) { }

  // GET

  getLinkGroups(webPortalId): Promise<Array<LinkGroupResponse>> {
    const params = new HttpParams().set('webPortalId', webPortalId);
    return this.http.get(this.url + `linkgroups`, params);
  }

  // POST

  createLinkGroups(linkGroups: LinkGroupRequest): Promise<any> {
    return this.http.post(this.url + `linkgroups`, linkGroups);
  }

  addLinkToGroup(link: LinkResponse, linkGroups: string, linkGroupId: number): Promise<any> {
    return this.http.post(this.url + `${linkGroups}/${linkGroupId}/link`, link);
  }

  // PUT

  updateLinkGroup(linkGroup: LinkGroupResponse, linkGroupId: number): Promise<any> {
    return this.http.put(this.url + `linkgroups/${linkGroupId}`, linkGroup);
  }

  updateLink(link: LinkResponse): Promise<any> {
    return this.http.put(this.url + `link${link.id}`, link);
  }

  sortLinkGroups(linkgroups: LinkGroupSort): Promise<any> {
    return this.http.put(this.url + `linkgroups`, linkgroups);
  }

  // DELETE

  removeLinkFromGroup(linkGroupId: number, linkId: number): Promise<any> {
    return this.http.delete(this.url + `linkGroups/${linkGroupId}/links/${linkId}`);
  }

  deleteLinkGroup(linkGroups: string, linkGroupId: number): Promise<any> {
    return this.http.delete(this.url + `linkgroups/${linkGroupId}`);
  }
}
