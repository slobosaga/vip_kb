import { Injectable } from '@angular/core';
import { HttpClientHelper } from '../base/httpHelper';

@Injectable({
  providedIn: 'root'
})
export class SynonymService {

  url = 'Synonym';

  constructor(private httpHelper: HttpClientHelper) { }

  // GET

  getAllSynonyms(): Promise<Array<string>> {
    return this.httpHelper.get(this.url);
  }

  // POST

  createSynonym(synonyms: string[]): Promise<any> {
    return this.httpHelper.post(this.url, { synonyms });
  }

  // PUT


  // DELETE


}
