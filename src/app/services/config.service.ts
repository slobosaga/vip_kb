import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientHelper } from '../base/httpHelper';
import { EmailTemplate, EmailTemplatePlaceholder } from '../models/emailTemplate';
import { Role, RoleUpdate } from '../models/role';
import { Tenant, TenantRequest, TenantView } from '../models/tenant';
import { CreateWebPortalRequest, UpdateWebPortalRequest } from '../models/webPortal';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  url = 'Config/';

  constructor(private httpClient: HttpClientHelper) { }

  // GET

  getTenant(id): Promise<TenantView> {
    return this.httpClient.get(this.url + `tenants/${id}`);
  }

  getAllTenants(): Promise<Array<Tenant>> {
    return this.httpClient.get(this.url + 'tenants');
  }

  getAllEmailTemplates(): Promise<any> {
    const param = new HttpParams().set('languageId', JSON.parse(localStorage.getItem('A1_Admin_language')));
    return this.httpClient.get(this.url + 'emailtemplates', param);
  }

  getAllEmailTemplatePlaceholders(): Promise<Array<EmailTemplatePlaceholder>> {
    return this.httpClient.get(this.url + 'emailtemplates/placeholders');
  }

  getEmailTemplate(emailTemplateId: number): Promise<EmailTemplate> {
    return this.httpClient.get(this.url + `emailtemplates/${emailTemplateId}`);
  }

  getAllEmailTypes(): Promise<any> {
    const param = new HttpParams().set('languageId', JSON.parse(localStorage.getItem('A1_Admin_language')));
    return this.httpClient.get(this.url + `emailtypes`, param);
  }

  getWebPortal(webPortalId): Promise<any> {
    return this.httpClient.get(this.url + `webportals/${webPortalId}`);
  }

  getAllRoles(all = 'true'): Promise<Array<Role>> {
    const params = new HttpParams().set('all', all);
    return this.httpClient.get(this.url + `roles`, params);
  }

  getAllEmailCategoryViewTypes() {
    return this.httpClient.get(this.url + 'emailcategoryviewtypes');
  }

  // POST

  createTenant(tenant: TenantView): Promise<any> {
    return this.httpClient.post(this.url + `tenants`, tenant);
  }

  createEmailTemplate(emailTemplate: EmailTemplate): Promise<any> {
    const params = new HttpParams().set('languageId', localStorage.getItem('A1_Admin_language'));
    return this.httpClient.post(this.url + 'emailtemplates', emailTemplate, params);
  }

  createWebPortal(webPortal: CreateWebPortalRequest): Promise<any> {
    return this.httpClient.post(this.url + 'webportals', webPortal);
  }

  addCategoryToWebPortal(categoryId, webPortalId): Promise<any> {
    const params = new HttpParams().set('categoryId', categoryId.toString()).set('webPortalId', webPortalId);
    return this.httpClient.post(this.url + `categories/addcategory`, null, params);
  }

  addRoleToWebPortal(roleId, webPortalId): Promise<any> {
    const params = new HttpParams().set('categoryId', roleId.toString()).set('webPortalId', webPortalId);
    return this.httpClient.post(this.url + `webportals/addrole`, null, params);
  }

  addRoleToUser(roleId, userId): Promise<any> {
    const params = new HttpParams().set('categoryId', roleId.toString()).set('webPortalId', userId.toString());
    return this.httpClient.post(this.url + `users/addrole`, null, params);
  }

  createRole(role: RoleUpdate): Promise<string> {
    return this.httpClient.post(this.url + `roles`, role);
  }


  // PUT

  updateTenat(tenant: TenantView, id): Promise<any> {
    return this.httpClient.put(this.url + `tenants/${id}`, tenant);
  }

  updateWebPortal(webPortalId, webPortalRequest: UpdateWebPortalRequest): Promise<any> {
    return this.httpClient.put(this.url + `webportals/${webPortalId}`, webPortalRequest);
  }

  updateEmailTemplate(emailTemplate: EmailTemplate, emailId): Promise<any> {
    return this.httpClient.put(this.url + `emailtemplates/${emailId}`, emailTemplate);
  }

  updateRole(roleId: number, role: RoleUpdate) {
    return this.httpClient.put(this.url + `roles/${roleId}`, role);
  }

  // DELETE

  deleteWebPortal(webPortalId): Promise<any> {
    return this.httpClient.delete(this.url + `webportals/${webPortalId}`);
  }

  removeCategoryFromWebPortal(categoryId, webPortalId): Promise<any> {
    const params = new HttpParams().set('categoryId', categoryId.toString()).set('webPortalId', webPortalId);
    return this.httpClient.delete(this.url + `categories/removecategory`, params);
  }

  removeRoleFromWebPortal(roleId, webPortalId): Promise<any> {
    const params = new HttpParams().set('categoryId', roleId.toString()).set('webPortalId', webPortalId);
    return this.httpClient.delete(this.url + `categories/removerole`, params);
  }

  removeRoleFromUser(roleId, userId): Promise<any> {
    const params = new HttpParams().set('categoryId', roleId.toString()).set('webPortalId', userId.toString());
    return this.httpClient.delete(this.url + `users/removerole`, params);
  }

  deleteEmailTemplate(emailId): Promise<any> {
    return this.httpClient.delete(this.url + `emailtemplates/${emailId}`);
  }

  deleteRole(id) {
    return this.httpClient.delete(this.url + `roles/${id}`);
  }

}
