import { typeSourceSpan } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AddArticleSectionModalComponent } from '../modals/add-article-section-modal/add-article-section-modal.component';
import { AddQueryconfigModalComponent } from '../modals/add-queryconfig-modal/add-queryconfig-modal.component';
import { ArticleNewLanguageModalComponent } from '../modals/article-new-language-modal/article-new-language-modal.component';
import { ArticlePreviewModalComponent } from '../modals/article-preview-modal/article-preview-modal.component';
import { CategoryModalComponent } from '../modals/category-modal/category-modal.component';
import { CommentModalComponent } from '../modals/comment-modal/comment-modal.component';
import { ConfirmDialogComponent } from '../modals/confirm-dialog/confirm-dialog.component';
import { DeletePortalModalComponent } from '../modals/delete-portal-modal/delete-portal-modal.component';
import { DevicePreviewModalComponent } from '../modals/device-preview-modal/device-preview-modal.component';
import { ForgotpassModalComponent } from '../modals/forgotpass-modal/forgotpass-modal.component';
import { InputModalComponent } from '../modals/input-modal/input-modal.component';
import { PreviewImgModalComponent } from '../modals/preview-img-modal/preview-img-modal.component';
import { QuestionModalComponent } from '../modals/question-modal/question-modal.component';
import { RelatedArticlesModalComponent } from '../modals/related-articles-modal/related-articles-modal.component';
import { RoleInputModalComponent } from '../modals/role-input-modal/role-input-modal.component';
import { RollbackModalComponent } from '../modals/rollback-modal/rollback-modal.component';
import { SortModalComponent } from '../modals/sort-modal/sort-modal.component';
import { ArticleAttachment, ArticleRevision, ArticleRevisions } from '../models/article';
import { Comment, CommentStatus } from '../models/comment';
import { GuidedHelpAnswerType, GuidedHelpQuestion, GuidedHelpQuestionType } from '../models/guided-help.model';


@Injectable()
export class ModalService {
  constructor(private dialog: MatDialog) { }
  private confirmDialog: MatDialogRef<ConfirmDialogComponent>;
  private inputDialog: MatDialogRef<InputModalComponent>;
  private sortDialog: MatDialogRef<SortModalComponent>;
  private relatedArticleDialog: MatDialogRef<RelatedArticlesModalComponent>;
  private commentDialog: MatDialogRef<CommentModalComponent>;
  private forgotPassDialog: MatDialogRef<ForgotpassModalComponent>;
  private previewImg: MatDialogRef<PreviewImgModalComponent>;
  private categoryEditDialog: MatDialogRef<CategoryModalComponent>;
  private articlePreview: MatDialogRef<ArticlePreviewModalComponent>;
  private articleNewLanguage: MatDialogRef<ArticleNewLanguageModalComponent>;
  private roleInputDialog: MatDialogRef<RoleInputModalComponent>;
  private addQueryDialog: MatDialogRef<AddQueryconfigModalComponent>;
  private addArticleSectionDialog: MatDialogRef<AddArticleSectionModalComponent>;
  private deletePortalDialog: MatDialogRef<DeletePortalModalComponent>;
  private rollbackDialog: MatDialogRef<RollbackModalComponent>;
  private questionDialog: MatDialogRef<QuestionModalComponent>;
  private devicePreviewDialog: MatDialogRef<DevicePreviewModalComponent>;

  public confirmDialogOpen(options) {
    this.confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: options.title,
        message: options.message,
        cancelText: options.cancelText,
        confirmText: options.confirmText
      }
    });
  }
  public confirmDialogConfirmed(): Observable<any> {
    let result: any = null;
    if (this.confirmDialog) {
      return this.confirmDialog.afterClosed().pipe(take(1), map(res => {
        result = res;
        return result;
      }));
    } else {
      return result;
    }
  }

  inputDialogOpen(options) {
    this.inputDialog = this.dialog.open(InputModalComponent, {
      data: {
        title: options.title,
        message: options.message,
        cancelText: options.cancelText,
        confirmText: options.confirmText,
        input: options.input
      }, height: '300px'
    });
  }

  inputDialogConfirmed(): Observable<any> {
    return this.inputDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  roleDialogOpen(options) {
    this.roleInputDialog = this.dialog.open(RoleInputModalComponent, {
      data: {
        title: options.title,
        message: options.message,
        cancelText: options.cancelText,
        confirmText: options.confirmText,
        role: { name: options.role.name, color: options.role.color }
      },
      width: '300px'
    });
  }

  roleDialogConfirmed(): Observable<any> {
    return this.roleInputDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  categoryDialogOpen(options) {
    this.categoryEditDialog = this.dialog.open(CategoryModalComponent, {
      data: {
        title: options.title,
        message: options.message,
        cancelText: options.cancelText,
        confirmText: options.confirmText,
        category: options.category
      }
    });
  }

  categoryDialogConfirmed(): Observable<any> {
    return this.categoryEditDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  articleNewLanguageOpen(options) {
    this.articleNewLanguage = this.dialog.open(ArticleNewLanguageModalComponent, {
      data: {
        articleId: options.articleId,
        languageId: options.languageId,
      }
    });
  }

  articleNewLanguageConfirmed(): Observable<any> {
    return this.articleNewLanguage.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }



  forgotPassDialogOpened(options) {
    this.forgotPassDialog = this.dialog.open(ForgotpassModalComponent, {
      data: {
        title: options.title,
        message: options.message,
        cancelText: options.cancelText,
        confirmText: options.confirmText,
        input: options.input
      }
    });
  }

  forgotPassDialogConfirmed(): Observable<any> {
    return this.forgotPassDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  sortDialogOpen(attachments: Array<ArticleAttachment>) {
    this.sortDialog = this.dialog.open(SortModalComponent, { data: attachments, width: '50vw' });
  }

  sortDialogConfirmed(): Observable<any> {
    return this.sortDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  relatedArticleOpen(modalData: {
    categories: number[],
    languageId: number,
    maxNo: number,
    articleId: number,
  }) {
    this.relatedArticleDialog = this.dialog.open(RelatedArticlesModalComponent, { data: modalData, width: '160vh', height: '80vh' });
  }

  relatedArticleConfirmed() {
    return this.relatedArticleDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  commentModalOpen(modalData: { comment: Comment, statuses: Array<CommentStatus> }) {
    this.commentDialog = this.dialog.open(CommentModalComponent, { data: modalData, width: '160vh', height: '80vh' });
  }

  commentModalConfirm() {
    return this.commentDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  previewModalOpen(link) {
    this.previewImg = this.dialog.open(PreviewImgModalComponent, { data: link, width: '160vh', height: '80vh' });
  }

  previewArticleModalOpen(html) {
    this.articlePreview = this.dialog.open(ArticlePreviewModalComponent, { data: html, width: '160vh', height: '80vh' });
  }

  addQueryConfigOpen(queryTypes) {
    this.addQueryDialog = this.dialog.open(AddQueryconfigModalComponent, { data: queryTypes });
  }

  addQueryConfigConfirmed() {
    return this.addQueryDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  addArticleToSectionOpen(modalData: { article, sections }) {
    this.addArticleSectionDialog = this.dialog.open(AddArticleSectionModalComponent, { data: modalData, width: '106vh' });
  }

  addArticleToSetionConfirmed() {
    return this.addArticleSectionDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  deletePortalModalOpen(modalData: { webPortals }) {
    this.deletePortalDialog = this.dialog.open(DeletePortalModalComponent, { data: modalData });
  }

  deletePortalModalConfirmed() {
    return this.deletePortalDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  rollbackModalOpen(modalData: { articleId: number, languageId: number, revisions: Array<ArticleRevisions> }) {
    this.rollbackDialog = this.dialog.open(RollbackModalComponent, { data: modalData, width: '80vw', height: '80vh' });
  }

  rollbackModalConfirmed() {
    return this.rollbackDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  questionModalOpen(modalData: { question: GuidedHelpQuestion, questionTypes: Array<GuidedHelpQuestionType>, answerTypes: Array<GuidedHelpAnswerType> }) {
    this.questionDialog = this.dialog.open(QuestionModalComponent, { data: modalData, width: '60vw', height: '80vh' });
  }

  questionConfirmed() {
    return this.questionDialog.afterClosed().pipe(take(1), map(res => {
      return res;
    }));
  }

  devicePreviewModalOpen(modalData: { articleBody: string }, modalWidth, modalHeight) {
    this.devicePreviewDialog = this.dialog.open(DevicePreviewModalComponent, { data: modalData, width: modalWidth, height: modalHeight });
  }

}
