import { HttpHeaders } from '@angular/common/http';
import { HttpClientHelper } from '../base/httpHelper';
import { LoginRequest } from '../models/loginRequest';
import { Injectable } from '@angular/core';
import { JwToken } from '../models/jwtoken';
import { User } from '../models/user';

@Injectable()
export class LoginSerivce {
  url = 'Auth/';
  dateExpire = new Date();
  // headers = new HttpHeaders();

  constructor(private httpHelper: HttpClientHelper) { }

  // GET

  getUser(userId): Promise<User> {
    return this.httpHelper.get(this.url + `users/${userId}`);
  }

  getAllUsers(): Promise<Array<User>> {
    return this.httpHelper.get(this.url + 'users');
  }

  // POST

  login(loginRequest: LoginRequest): Promise<JwToken> {
    return this.httpHelper.post(this.url + 'authenticate', JSON.stringify(loginRequest));
  }

  registerUser(user: User): Promise<number> {
    return this.httpHelper.post(this.url + 'register', user);
  }

  resetPassword(email: string): Promise<any> {
    return this.httpHelper.post(this.url + 'password/reset', email);
  }

  activateAccount(activationToken, pass: string): Promise<string> {
    return this.httpHelper.post(this.url + `activate/${activationToken}`, { password: pass }, null);
  }

  // PUT

  updateUser(user: User) {
    return this.httpHelper.put(this.url + `users/${user.id}`, user);
  }

  // DELETE

  deleteUser(userId): Promise<any> {
    return this.httpHelper.delete(this.url + `users/${userId}`);
  }


}
