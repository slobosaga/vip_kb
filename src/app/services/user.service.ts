import { Injectable } from '@angular/core';
import { HttpClientHelper } from '../base/httpHelper';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  url = 'User/';
  constructor(private httpHelper: HttpClientHelper) { }

  getUserProfile(): Promise<any> {
    return this.httpHelper.get(this.url + 'profile');
  }

  updateUserProfile(webPortalId, languageId): Promise<any> {
    return this.httpHelper.put(this.url + 'profile', { webPortalId, languageId });
  }

  createUserProfile(webPortalId, languageId): Promise<any> {
    return this.httpHelper.post(this.url + 'profile', { webPortalId, languageId });
  }

}
