import { SelectionModel } from '@angular/cdk/collections';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TreeNode } from 'src/app/base/models/treeNode';
import { Category, CategorySortRequest } from 'src/app/models/category';
import { ArticleService } from 'src/app/services/article.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-nested-tree-view',
  templateUrl: './nested-tree-view.component.html',
  styleUrls: ['./nested-tree-view.component.scss']
})
export class NestedTreeViewComponent implements OnInit, OnDestroy, OnChanges {

  @Input('selectedNodes') selectedNodesInput = new Array<{ id: number, name: string }>();
  @Input() readonly: boolean;
  @Input() disableBox: boolean;
  @Input() webPortals: Array<string> = new Array<string>();
  @Input() disableSelectAll: boolean;
  @Output('selectCategoryEvent') selecCategoryEvent: EventEmitter<Array<{ id: number, name: string }>> = new EventEmitter<Array<{ id: number, name: string }>>();

  categories = new Array<Category>();
  nodes = new Array<TreeNode>();
  checklistSelection = new SelectionModel<TreeNode>(true);
  nestedTreeControl: NestedTreeControl<TreeNode>;
  nestedDataSource: MatTreeNestedDataSource<TreeNode>;
  sort: any;
  categoriesSortRequest: any;
  categoriesDragable: boolean;
  categoriesEditable: boolean;
  sortedNodes: any;
  categoriesSubscriber: Subscription;
  selectedNodes = new Array<{ id: number, name: string }>();
  webPortal: boolean;
  article: boolean;

  // selectedNodes = new Array<{id: number, name: string}>();

  constructor(private articleService: ArticleService, private router: Router, private eventEmitter: EventEmitterService, private modalService: ModalService) {
    this.nestedTreeControl = new NestedTreeControl<TreeNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource<TreeNode>();
    if (this.router.url.match('/articles/')) {
      this.article = true;
    } else if (this.router.url.match('/web-portals')) {
      this.webPortal = true;
    }
  }

  async ngOnChanges(changes: SimpleChanges) {
    if (this.router.url.match('/articles/edit-article') || this.router.url.match('/articles/create-new-article-new-language/')) {
      if (changes.selectedNodesInput) {
        this.preselectCategories(this.selectedNodesInput);
      }
    }
    if (this.router.url.match('/articles/view-article')) {
      if (changes.selectedNodesInput) {
        if (changes.selectedNodesInput) {
          this.preselectCategories(this.selectedNodesInput);
        }
      }
    }
    if (changes.webPortals) {
      await this.getCategories();
      // this.checklistSelection.clear();
      this.preselectCategories(this.selectedNodesInput);
    }
  }

  ngOnDestroy(): void {
    if (this.categoriesSubscriber) {
      this.categoriesSubscriber.unsubscribe();
    }
  }

  async ngOnInit() {
    await this.getCategories();
    if (!this.router.url.match('/articles/')) {
      setTimeout(() => {
        this.preselectCategories(this.selectedNodesInput);
      }, 800);
    }
  }

  preselectCategories(nodes: Array<{ id: number, name: string }>) {
    if (nodes != null) {
      nodes.forEach(node => {
        if (node.id) {
          this.checkArticleNodes(null, node.id);
        } else {
          this.checkArticleNodes(null, node);
        }
      });
    }
  }

  async getCategories() {
    if (this.webPortals !== null) {
      this.categories = await this.articleService.getAllCategoriesForWebPortal(this.webPortals);
    } else {
      this.categories = await this.articleService.getAllCategories();
    }
    // this.categories = this.categories.sort((a, b) => a.sort - b.sort);
    this.categoriesTreeView();
    this.nestedTreeControl = new NestedTreeControl<TreeNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource<TreeNode>();
    this.nestedDataSource.data = this.nodes;
    this.nestedTreeControl.dataNodes = this.nodes;
  }

  private _getChildren = (node: TreeNode): TreeNode[] => node.children;
  hasNestedChild = (_: number, nodeData: TreeNode) => nodeData.hasChildren === true;

  categoriesTreeView() {
    this.nodes = new Array<TreeNode>();
    this.categories.forEach(c => {
      const node: TreeNode = { id: c.id, name: c.name, parentId: c.parentId, hasChildren: false, children: new Array<TreeNode>() };
      if (c.parentId === null) {
        this.nodes.push(node);
      }
    });
    this.categories.forEach(c => {
      const node: TreeNode = { id: c.id, name: c.name, parentId: c.parentId, hasChildren: false, children: new Array<TreeNode>() };
      if (c.parentId > 0) {
        this.fillTree(node, null);
      }
    });
  }

  fillTree(node: TreeNode, nodes = null) {
    if (nodes === null) {
      nodes = this.nodes;
    }
    nodes.forEach(n => {
      if (n.hasChildren === true && n.id === node.parentId) {
        n.children.push(node);
      } else if (n.hasChildren === true) {
        this.fillTree(node, n.children);
      } else if (n.id === node.parentId) {
        n.hasChildren = true;
        n.children.push(node);
      }
    });
  }

  descendantsAllSelected(node: TreeNode) {
    if (this.disableSelectAll === false) {
      const descendants = this.nestedTreeControl.getDescendants(node);
      const descAllSelected = descendants.length > 0 && descendants.every(child => {
        return this.checklistSelection.isSelected(child);
      });
      return descAllSelected;
    } else {
      return this.checklistSelection.isSelected(node);
    }
  }

  descendantsPartiallySelected(node: TreeNode): boolean {
    const descendants = this.nestedTreeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  todoItemSelectionToggle(node: TreeNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.nestedTreeControl.getDescendants(node);
    if (this.disableSelectAll === true) {
      if (this.webPortal === true) {
        if (!this.checklistSelection.isSelected(node)) {
          this.checklistSelection.deselect(...descendants);
        }
      }
      this.checklistSelection.isSelected(node);
    } else {
      this.checklistSelection.isSelected(node)
        ? this.checklistSelection.select(...descendants)
        : this.checklistSelection.deselect(...descendants);
    }
    if (this.selectedNodesInput !== null && this.selectedNodesInput !== undefined) {
      if (this.selectedNodesInput.length > 0) {
        if (this.checklistSelection.isSelected(node)) {
          if (!this.selectedNodesInput.find(n => n.id === node.id)) {
            this.selectedNodesInput.push({ id: node.id, name: node.name });
          }
        } else {
          this.selectedNodesInput.splice(this.selectedNodesInput.indexOf(this.selectedNodesInput.find(n => n.id === node.id)), 1);
        }
      }
    }
    // Force update for the parent
    descendants.forEach(child => this.checklistSelection.isSelected(child));
    this.checkAllParentsSelection(node);
    this.setCategories();
  }

  checkAllParentsSelection(node: TreeNode): void {
    let parent: TreeNode | null = this.getParentNode(node);
    let parents: Array<TreeNode> | null = [];
    if (parent === undefined) {
      parents = this.getAllParentNodes(node, this.nodes);
    }
    if (this.webPortal === true) {
      if (parents.length > 0) {
        parents.pop();
        parents.forEach(p => {
          if (this.checklistSelection.isSelected(node)) {
            this.checkRootNodeSelection(p);
          }
        });
      } else {
        while (parent) {
          this.checkRootNodeSelection(parent);
          parent = this.getParentNode(parent);
        }
      }
    }
  }

  getParentNode(node: TreeNode): TreeNode | null {
    if (node === null) {
      return null;
    }
    const childNode = this.categories.find(c => c.id === node.id);
    if (childNode.parentId != null) {
      const parentNode = this.nestedDataSource.data.find(d => d.id === childNode.parentId);
      return parentNode;
    } else {
      return null;
    }
  }

  getAllParentNodes(node: TreeNode, nodes: Array<TreeNode>) {
    if (typeof nodes !== null) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < nodes.length; i++) {
        if (nodes[i].id === node.id) {
          return [nodes[i]];
        }
        const a = this.getAllParentNodes(node, nodes[i].children);
        if (a !== null) {
          a.unshift(nodes[i]);
          return a;
        }
      }
    }
    return null;
  }

  todoLeafItemSelectionToggle(node: TreeNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
    if (this.selectedNodesInput !== null && this.selectedNodesInput !== undefined) {
      if (this.selectedNodesInput.length > 0) {
        if (this.checklistSelection.isSelected(node)) {
          if (!this.selectedNodesInput.find(n => n.id === node.id)) {
            this.selectedNodesInput.push({ id: node.id, name: node.name });
          }
        } else {
          this.selectedNodesInput.splice(this.selectedNodesInput.indexOf(this.selectedNodesInput.find(n => n.id === node.id)), 1);
        }
      }
    }
    this.setCategories();
  }

  checkArticleNodes(node: TreeNode[] = null, id) {
    let nodes: TreeNode[] = null;
    if (node === null) {
      nodes = this.nestedDataSource.data;
    } else {
      nodes = node;
    }
    nodes.forEach(n => {
      if (n.id === id) {
        if (n.hasChildren === true) {
          if (!this.checklistSelection.isSelected(n)) {
            this.todoItemSelectionToggle(n);
          }
        } else if (n.hasChildren === false) {
          if (!this.checklistSelection.isSelected(n)) {
            this.todoItemSelectionToggle(n);
          }
        }
      } else if (n.children.length > 0) {
        n.children.forEach(nc => {
          if (nc.id === id) {
            this.descendantsAllSelected(nc);
            this.descendantsPartiallySelected(nc);
            if (nc.hasChildren === true) {
              if (!this.checklistSelection.isSelected(nc)) {
                this.todoItemSelectionToggle(nc);
              }
            } else {
              if (!this.checklistSelection.isSelected(nc)) {
                this.todoLeafItemSelectionToggle(nc);
              }
            }
          } else if (nc.children.length > 0) {
            this.checkArticleNodes(nc.children, id);
          }
        });
      }
    });
  }

  checkRootNodeSelection(node: TreeNode): void {
    let nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.nestedTreeControl.getDescendants(node);
    const descPartialSelected = descendants.length > 0 && descendants.some(child => {
      return this.checklistSelection.isSelected(child);
    });
    const descAllSelected = descendants.length > 0 && descendants.every(child => {
      return this.checklistSelection.isSelected(child);
    });
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
    nodeSelected = this.checklistSelection.isSelected(node);
    if (!nodeSelected && descPartialSelected) {
      this.checklistSelection.select(node);
    }
  }

  selectCheckCategories() {
    const articleCategories = new Array<Category>();
    this.sortCategories();
    this.checklistSelection.selected.forEach(c => {
      let category = new Category();
      category = this.categories.find(cr => cr.id === c.id);
      category.sort = this.categoriesSortRequest.find(csr => csr.id === c.id).sort;
      articleCategories.push(category);
    });
  }

  // METHODS FOR CATEGORIES EDIT COMPONENT

  rebuildTree(nodes: Array<TreeNode>) {
    this.setNodeParentId(nodes);
    this.setNodeChildren(nodes);
    this.nestedDataSource.data = nodes;
  }

  setNodeParentId(nodes: Array<TreeNode>) {
    nodes.forEach(n => {
      n.parentId = null;
    });
  }

  setNodeChildren(nodes: Array<TreeNode>) {
    nodes.forEach(n => {
      if (n.children.length > 0) {
        n.hasChildren = true;
        n.children.forEach(c => {
          c.parentId = n.id;
        });
        this.setNodeChildren(n.children);
      } else {
        n.hasChildren = false;
      }
    });
  }

  sortCategories(nodes: Array<TreeNode> = null) {
    if (nodes === null) {
      nodes = this.nestedDataSource.data;
    }
    nodes.forEach(n => {
      this.sort++;
      this.categoriesSortRequest.push({ id: n.id, name: n.name, parentId: n.parentId, sort: this.sort });
      if (n.children.length > 0) {
        this.sortCategories(n.children);
      }
    });
  }

  setCategories() {
    const categories = new Array<{ id: number, name: string }>();
    this.checklistSelection.selected.forEach(c => {
      const category = this.categories.find(ca => ca.id === c.id);
      if (category !== undefined) {
        categories.push({ id: category.id, name: category.name });
      }
    });
    if (this.router.url.match('/articles/edit-article') || this.router.url.match('/articles/create-new-article-new-language/') || this.router.url.match('/articles/create-article')) {
      if (this.selectedNodesInput !== null && this.selectedNodesInput?.length > 0) {
        this.selecCategoryEvent.emit(this.selectedNodesInput);
      } else {
        this.selectedNodesInput = categories;
        this.selecCategoryEvent.emit(this.selectedNodesInput);
      }
    } else {
      this.selecCategoryEvent.emit(categories);
    }
  }


  cancelSortButton() {
    if (this.categoriesDragable === true) {
      this.categoriesDragable = false;
    }
    if (this.categoriesEditable === true) {
      this.categoriesEditable = false;
    }
  }

  sortCategoriesButton() {
    const options = {
      title: 'Are you sure?',
      message: 'Sort Categories',
      cancelText: 'No',
      confirmText: 'Yes'
    };
    this.modalService.confirmDialogOpen(options);
    this.modalService.confirmDialogConfirmed().subscribe(confirm => {
      if (confirm) {
        if (this.sortedNodes) {
          this.rebuildTree(this.sortedNodes);
          this.sort = 0;
          this.categoriesSortRequest = new Array<CategorySortRequest>();
          this.sortCategories();
          if (this.categoriesSortRequest.length > 0) {
            this.articleService.sortCategories(this.categoriesSortRequest);
          }
          this.categoriesDragable = false;
        }
      }
    });
  }

  addNodeButton(node: TreeNode) {
    const options = {
      title: 'Add New Category',
      message: 'Enter Category Name',
      cancelText: 'Cancel',
      confirmText: 'Add',
      input: null
    };
    this.modalService.inputDialogOpen(options);
    this.modalService.inputDialogConfirmed().subscribe(confirmed => {
      if (confirmed) {
        this.addNode(node, confirmed);
      }
    });
  }

  addNode(node: TreeNode, nodeName) {
    const newNode: TreeNode = { id: null, name: nodeName, parentId: node.id, hasChildren: false, children: new Array<TreeNode>() };
    // this.articleService.createCategory({ name: newNode.name, parentId: newNode.parentId, sort: this.returnLastCategory() }).then(async data => {
    //   newNode.id = await data;
    //   node.children.push(newNode);
    //   this.nestedDataSource.data = null;
    //   this.nestedDataSource.data = this.nodes;
    // });
  }

  returnLastCategory(id = null) {
    if (id > 0) {
      const sort = this.categories.find(c => c.id === id).sort;
      return sort;
    } else {
      let sort = this.categories[this.categories.length - 1].sort;
      sort = sort + 1;
      return sort;
    }
  }

  editNodeButton(node: TreeNode) {
    const options = {
      title: 'Edit Category',
      message: 'Enter Category Name',
      cancelText: 'Cancel',
      confirmText: 'Confirm',
      input: node.name
    };
    this.modalService.inputDialogOpen(options);
    this.modalService.inputDialogConfirmed().subscribe(confirmed => {
      if (confirmed) {
        node.name = confirmed;
        // this.articleService.updateCategoty({ name: node.name, parentId: node.parentId, sort: this.returnLastCategory(node.id) }, node.id);
        this.nestedDataSource.data = null;
        this.nestedDataSource.data = this.nodes;
      }
    });
  }

  deleteNodeButton(node: TreeNode) {
    this.deleteNodeDialog(node);
  }

  deleteNodeDialog(node: TreeNode) {
    const options = {
      title: 'Are you sure?',
      message: 'Delete Cateogry?',
      cancelText: 'No',
      confirmText: 'Yes'
    };
    this.modalService.confirmDialogOpen(options);
    this.modalService.confirmDialogConfirmed().subscribe(confirmed => {
      if (confirmed) {
        this.articleService.deleteCategory(node.id);
        this.deleteNode(node);
        this.nestedDataSource.data = null;
        this.nestedDataSource.data = this.nodes;
      }
    });
  }

  deleteNode(node: TreeNode, nodes: Array<TreeNode> = null) {
    if (nodes === null) {
      nodes = this.nodes;
    }
    nodes.forEach(n => {
      if (n.id === node.id) {
        nodes.splice(nodes.indexOf(node), 1);
      } else {
        this.deleteNode(node, n.children);
      }
    });
  }
}


