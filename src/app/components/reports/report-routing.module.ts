import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportComponent } from './report/report.component';

const routes: Routes = [
  {
    path: 'view',
    component: ReportComponent,
    data: { title: 'Reports' },
    children: [
       {
        path: ':reportType',
        component: ReportComponent,
        data: { title: 'Reports' },
      },
    ]
  },
  // {
  //   path: ':reportType',
  //   component: ReportComponent,
  //   data: { title: 'Reports' },
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }

