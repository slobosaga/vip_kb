import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { OpenedReadArticlesKpiChartRequest, OpenedReadArticlesKpiChartResponse } from '../../../models/reports';
import { EventEmitterService } from '../../../services/event-emitter.service';
import { ReportingService } from '../../../services/reporting.service';

@Component({
  selector: 'app-open-read-articles-chart',
  templateUrl: './open-read-articles-chart.component.html',
  styleUrls: ['../report/report.component.scss']
})
export class OpenReadArticlesChartComponent implements OnInit {
  language = localStorage.getItem('A1_Admin_language');
  languageSubscriber: Subscription;
  paging = new OpenedReadArticlesKpiChartRequest();
  chartData = new OpenedReadArticlesKpiChartResponse();
  reportResponseTypes = [
    'Day',
    'Week',
    'Month',
    'Year'
    
  ];
  reportFilterTypes = [
    'WebPortal',
    'UserRole',
    'None'
  ];
  selectedReportResponseType = 'Month';
  selectedReportFilterType = 'None';
  filteredReportResponseTypes = new Array<string>();
  chartConfig;
  chart;
  showNumbersOnChart = true;
  destroy$ = new Subject<void>();

  constructor(private reportService: ReportingService, private loader: NgxUiLoaderService, private eventEmitter: EventEmitterService) {
  }

  ngOnInit(): void {
    this.paging.reportFilterType = 'None';
    this.paging.reportResponseType = 'Month';
    this.paging.languageId = +this.language;

    this.languageSubscriber = this.eventEmitter.emitLanguageId.subscribe(() => {
      this.getChartData();
      this.language = localStorage.getItem('A1_Admin_language');
      this.paging.languageId = +this.language;
    });

    this.reportService.filters.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.paging.webPortalId = data?.webPortal?.id;
      this.paging.roleId = data?.role?.id;
      this.paging.fromDate = data?.fromDate;
      this.paging.untilDate = data?.untilDate;
      this.paging.reportDateFilterType = data?.reportDateFilterType;
      // this.filteredReportResponseTypes = [];
      // if (this.paging.reportResponseType == 'range') {
      //   const dateDiffDays = (this.paging.untilDate.getTime() - this.paging.fromDate.getTime()) / (1000 * 3600 * 24);

      //   const dateDiffMonths = dateDiffDays / 30;

      //   if (dateDiffMonths > 12) {
      //     const allowedValues = ['Year', 'Total'];
      //     this.filteredReportResponseTypes = this.reportResponseTypes.filter(rt => allowedValues.includes(rt));
      //   } else {
      //     if (dateDiffDays > 31) {
      //       const allowedValues = ['Month'];
      //       this.filteredReportResponseTypes = this.reportResponseTypes.filter(rt => allowedValues.includes(rt));
      //     } else {
      //       const allowedValues = ['Day', 'Week'];
      //       this.filteredReportResponseTypes = this.reportResponseTypes.filter(rt => allowedValues.includes(rt));
      //     }
      //   }
      // } else if (this.paging.reportResponseType == 'Year') {
      //   const allowedValues = ['Month', 'Year', 'Total'];
      //   this.filteredReportResponseTypes = this.reportResponseTypes.filter(rt => allowedValues.includes(rt));
      // } else {
      //   this.filteredReportResponseTypes = this.reportResponseTypes.filter(rt => rt == this.paging.reportDateFilterType);
      // }

      this.getChartData();
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.languageSubscriber.unsubscribe();
  }

  getChartData() {
    this.loader.startLoader('chart');
    this.reportService.getOpenedReadArticlesKpiChartData(this.paging).then(data => {
      this.drawChart(data);
      this.loader.stopLoader('chart');
    }).catch(error => {
      console.log(error);
      this.loader.stopLoader('chart');
    })
  }

  drawChart(data: OpenedReadArticlesKpiChartResponse) {
    let formattedData = [];
    if (this.paging.reportFilterType != 'None') {
      const groups = [];
      const groupData = [];

      data.openings.forEach(opening => {
        opening.filters.forEach(f => {
          if (!groups.find(g => g == f.name)) {
            groups.push(f.name);
          }

          groupData.push({
            name: f.name,
            total: f.count,
            date: opening.date
          });
        });
      });

      groups.forEach(group => {
        formattedData.push({
            name: group,
            type: 'bar',
            stack: 'total',
            label: {
              show: this.showNumbersOnChart
            },
            emphasis: {
              focus: 'series'
            },
            data: groupData.filter(gd => gd.name == group).map(d => {
              return [d.date, d.total];
            })
        });
      });
    } else {
      formattedData = [
        {
          name: 'Total',
          emphasis: {
            focus: 'series'
          },
          type: 'bar',
          label: {
            show: this.showNumbersOnChart
          },
          data: data.openings.map(d => {
            return [d.date, d.total];
          })
        }
      ]
    }

    this.chartConfig = {
      tooltip: {
        trigger: 'item',
        formatter: (params) => {
          if (params.data) {
            return params.marker + ' ' + new Date(params.data[0]).toLocaleDateString('de-DE') + '<br/>' + params.seriesName + ': ' + params.data[1];
          }
          return null;
        }
      },
      toolbox: {
        feature: {
          dataZoom: {
            show: true
          },
          restore: {
            show: true
          },

        },
        left: 'center'
      },
      dataZoom: [
        {
          type: 'slider'
        },
        {
          type: 'inside'
        }
      ],
      legend: {
        show: true,
        left: 'center',
        bottom: 'bottom'
      },
      grid: {
        left: '3%',
        right: '4%',
        containLabel: true
      },
      yAxis: {
        type: 'value'
      },
      xAxis: {
        type: 'time',
        minInterval: 3600 * 1000 * 24
      },
      series: formattedData
    };
  }

  setReportResponseType(reportResponseType: string) {
    this.paging.reportResponseType = reportResponseType;
    this.getChartData();
  }

  setReportFilterType(reportFilterType: string) {
    this.paging.reportFilterType = reportFilterType;
    this.getChartData();
  }

  chartInit(chart) {
    this.chart = chart;
  }

  toggleNumbersOnChart(checked: boolean) {
    this.showNumbersOnChart = checked;
    const chartOptions = JSON.parse(JSON.stringify(this.chartConfig));

    chartOptions.series.forEach(s => {
      s.label.show = checked;
    });

    this.chart.setOption(chartOptions);
  }

}
