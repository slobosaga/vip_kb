import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxUiLoaderConfig, NgxUiLoaderModule } from 'ngx-ui-loader';
import { AngularMaterialModule } from '../../modules/angular-material/angular-material.module';
import { PipeSharedModule } from '../../modules/pipe-shared/pipe-shared.module';
import { SharedModule } from '../../modules/shared/shared.module';
import { ArticleCategoryKpiComponent } from './article-category-kpi/article-category-kpi.component';
import { ArticleRatingsKpiComponent } from './article-ratings-kpi/article-ratings-kpi.component';
import { ArticleStatusesKpiComponent } from './article-statuses-kpi/article-statuses-kpi.component';
import { BookmarkStatisticsKpiComponent } from './bookmark-statistics-kpi/bookmark-statistics-kpi.component';
import { KeywordsAndPhrasesKpiComponent } from './keywords-and-phrases-kpi/keywords-and-phrases-kpi.component';
import { OpenReadArticlesComponent } from './open-read-articles/open-read-articles.component';
import { ReportRoutingModule } from './report-routing.module';
import { ReportComponent } from './report/report.component';
import { OpenReadArticlesChartComponent } from './open-read-articles-chart/open-read-articles-chart.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { ReportFilterPipe } from '../../base/report-filter-pipe';
// import * as echarts from 'echarts';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: '#f77474',
  fgsColor: '#f77474',
  hasProgressBar: false
};

export function chartModule(): any {
  return import('echarts');
}

@NgModule({
  declarations: [OpenReadArticlesComponent, ArticleRatingsKpiComponent, ArticleStatusesKpiComponent, BookmarkStatisticsKpiComponent, KeywordsAndPhrasesKpiComponent, ArticleCategoryKpiComponent, ReportComponent, OpenReadArticlesChartComponent, ReportFilterPipe],
  imports: [
    CommonModule,
    ReportRoutingModule,
    SharedModule,
    PipeSharedModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    AngularMaterialModule,
    FormsModule,
    // NgxEchartsModule.forRoot({
    //   echarts: { init: echarts.init},
    // })
    NgxEchartsModule.forRoot({
      echarts: chartModule
    })
  ]
})
export class ReportModule { }
