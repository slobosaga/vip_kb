import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable, Subject, Subscription } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { ExportToExcelHelper } from '../../../base/export-to-excel-helper';
import { KeywordsAndPhrasesKpiTableRequest, KeywordsAndPhrasesKpiTableSearchPhrase, ReportFilters } from '../../../models/reports';
import { EventEmitterService } from '../../../services/event-emitter.service';
import { ReportingService } from '../../../services/reporting.service';

@Component({
  selector: 'app-keywords-and-phrases-kpi',
  templateUrl: './keywords-and-phrases-kpi.component.html',
  styleUrls: ['../report/report.component.scss']
})
export class KeywordsAndPhrasesKpiComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  language = localStorage.getItem('A1_Admin_language');
  languageSubscriber: Subscription;
  dataSource: MatTableDataSource<KeywordsAndPhrasesKpiTableSearchPhrase>;
  observable: Observable<any>;
  paging = new KeywordsAndPhrasesKpiTableRequest();
  displayedColumns = ['searchPhrase', 'total'];
  pageIndex = 0;
  tableLength = 0;
  destroy$ = new Subject<void>();
  reportData = new Array<KeywordsAndPhrasesKpiTableSearchPhrase>();
  filters = new ReportFilters();

  constructor(private reportService: ReportingService, private loader: NgxUiLoaderService, private eventEmitter: EventEmitterService) {
    this.paging.fromRecord = 0;
    this.paging.numRecord = 60;
    this.paging.orderType = 'desc';
  }

  ngOnInit(): void {
    this.paging.reportDateFilterType = 'none';
    this.paging.languageId = +this.language;

    this.languageSubscriber = this.eventEmitter.emitLanguageId.subscribe(() => {
      this.getTableData();
      this.language = localStorage.getItem('A1_Admin_language');
      this.paging.languageId = +this.language;
    });

    this.reportService.filters.pipe(takeUntil(this.destroy$)).subscribe(data => {
      debugger;
      this.paging.fromDate = data?.fromDate;
      this.paging.untilDate = data?.untilDate;
      this.paging.webPortalId = data?.webPortal?.id;
      this.paging.roleId = data?.role?.id;
      this.paging.reportDateFilterType = data?.reportDateFilterType;
      this.filters = data;
      var getFullYear = new Date(new Date().getFullYear(), 0, 1);
      var my_date = new Date();  
      var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
      if(data?.reportDateFilterType == "Today"){this.paging.fromDate_excel = new Date();}
      if(data?.reportDateFilterType == "Month"){this.paging.fromDate_excel = first_date ; this.paging.untilDate_excel = new Date()}
      if(data?.reportDateFilterType == "Year"){this.paging.fromDate_excel = getFullYear ; this.paging.untilDate_excel = new Date()}
      this.paginator?.firstPage();
      this.getTableData();
    });
  }

  ngAfterViewInit(): void {
    this.paginator.page.pipe(tap(event => {
      this.paging.fromRecord = (event.pageIndex * event.pageSize);
      this.paging.numRecord = event.pageSize;
      this.getTableData();
    })).subscribe();
    this.sortTableEvent();
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.languageSubscriber.unsubscribe();
  }

  getTableData() {
    this.loader.startLoader('table');
    this.reportService.getKeywordsAndPhrasesKpiTableData(this.paging).then(data => {
      this.tableLength = data.total;
      debugger;
      this.dataSource = new MatTableDataSource<KeywordsAndPhrasesKpiTableSearchPhrase>(data.searchPhrases);
      this.observable = this.dataSource.connect();
      this.loader.stopLoader('table');
    }).catch(error => {
      console.log(error);
      this.loader.stopLoader('table');
    })
  }

  pageChange(event) {
    this.paging.fromRecord = (event.pageIndex * event.pageSize);
    this.paging.numRecord = event.pageSize;
    this.getTableData();
  }

  private sortTableEvent() {
    if (this.sort) {
      this.sort.sortChange.pipe(tap(event => {
        this.paginator.pageIndex = 0;
        this.paging.fromRecord = 0;
        if (event.active !== null) {
          if (event.direction === 'asc') {
            this.paging.orderType = 'asc';
          } else if (event.direction === 'desc') {
            this.paging.orderType = 'desc';
          }
          this.getTableData();
        }
      })).subscribe();
    }
  }

  downloadExcel() {
    const paging = JSON.parse(JSON.stringify(this.paging));
    paging.numRecord = this.tableLength;
    paging.fromRecord = 0;
    this.reportService.getKeywordsAndPhrasesKpiTableData(paging).then(data => {
      this.reportData = data.searchPhrases;

      setTimeout(() => {
        ExportToExcelHelper.downloadExcel('Search phrases.xlsx');
      }, 100);
    }).catch(error => {
      console.log(error);
    })
  }

}
