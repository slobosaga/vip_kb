import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable, Subject, Subscription } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { ExportToExcelHelper } from '../../../base/export-to-excel-helper';
import { ArticleStatusesKpiTableRequest, ArticleStatusesKpiTableStatus, ReportFilters } from '../../../models/reports';
import { EventEmitterService } from '../../../services/event-emitter.service';
import { ReportingService } from '../../../services/reporting.service';

@Component({
  selector: 'app-article-statuses-kpi',
  templateUrl: './article-statuses-kpi.component.html',
  styleUrls: ['../report/report.component.scss']
})
export class ArticleStatusesKpiComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  language = localStorage.getItem('A1_Admin_language');
  languageSubscriber: Subscription;
  dataSource: MatTableDataSource<ArticleStatusesKpiTableStatus>;
  observable: Observable<any>;
  paging = new ArticleStatusesKpiTableRequest();
  displayedColumns = ['webPortal', 'draft', 'submitted', 'rejected', 'need_changes', 'approved', 'scheduled', 'published', 'unpublished', 'total'];
  pageIndex = 0;
  tableLength = 0;
  destroy$ = new Subject<void>();
  filters = new ReportFilters();
  reportData = new Array<ArticleStatusesKpiTableStatus>();

  constructor(private reportService: ReportingService, private loader: NgxUiLoaderService, private eventEmitter: EventEmitterService) {
    this.paging.fromRecord = 0;
    this.paging.numRecord = 60;
    this.paging.sortColumn = 'total';
    this.paging.orderType = 'desc';
  }

  ngOnInit(): void {
    this.paging.reportDateFilterType = 'none';
    this.paging.languageId = +this.language;

    this.languageSubscriber = this.eventEmitter.emitLanguageId.subscribe(() => {
      this.getTableData();
      this.language = localStorage.getItem('A1_Admin_language');
      this.paging.languageId = +this.language;
    });

    this.reportService.filters.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.paging.webPortalId = data?.webPortal?.id;
      this.paging.fromDate = data?.fromDate;
      this.paging.untilDate = data?.untilDate;
      this.paging.reportDateFilterType = data?.reportDateFilterType;
      this.filters = data;
      var getFullYear = new Date(new Date().getFullYear(), 0, 1);
      var my_date = new Date();  
      var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
      if(data?.reportDateFilterType == "Today"){this.paging.fromDate_excel = new Date();}
      if(data?.reportDateFilterType == "Month"){this.paging.fromDate_excel = first_date ; this.paging.untilDate_excel = new Date()}
      if(data?.reportDateFilterType == "Year"){this.paging.fromDate_excel = getFullYear ; this.paging.untilDate_excel = new Date()}
      if(data?.reportDateFilterType == "Week"){this.paging.fromDate_excel = new Date() ; this.paging.untilDate_excel = new Date()}
      this.paginator?.firstPage();
      this.getTableData();
    });
  }

  ngAfterViewInit(): void {
    this.paginator.page.pipe(tap(event => {
      this.paging.fromRecord = (event.pageIndex * event.pageSize);
      this.paging.numRecord = event.pageSize;
      this.getTableData();
    })).subscribe();
    this.sortTableEvent();
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.languageSubscriber.unsubscribe();
  }

  getTableData() {
    this.loader.startLoader('table');
    this.reportService.getArticleStatusesKpiTableData(this.paging).then(data => {
      this.tableLength = data.total;
      this.dataSource = new MatTableDataSource<ArticleStatusesKpiTableStatus>(data.statuses);
      this.observable = this.dataSource.connect();
      this.loader.stopLoader('table');
    }).catch(error => {
      console.log(error);
      this.loader.stopLoader('table');
    })
  }

  pageChange(event) {
    this.paging.fromRecord = (event.pageIndex * event.pageSize);
    this.paging.numRecord = event.pageSize;
    this.getTableData();
  }

  private sortTableEvent() {
    if (this.sort) {
      this.sort.sortChange.pipe(tap(event => {
        this.paginator.pageIndex = 0;
        this.paging.fromRecord = 0;
        if (event.active !== null) {
          this.paging.sortColumn = event.active;
          if (event.direction === 'asc') {
            this.paging.orderType = 'asc';
          } else if (event.direction === 'desc') {
            this.paging.orderType = 'desc';
          }
          this.getTableData();
        }
      })).subscribe();
    }
  }

  downloadExcel() {
    const paging = JSON.parse(JSON.stringify(this.paging));
    paging.numRecord = this.tableLength;
    paging.fromRecord = 0;
    this.reportService.getArticleStatusesKpiTableData(paging).then(data => {
      this.reportData = data.statuses;
      setTimeout(() => {
        ExportToExcelHelper.downloadExcel('Articles count per status.xlsx');
      }, 100);
    }).catch(error => {
      console.log(error);
    })
  }

}
