import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Role as Roles } from '../../../models/enums/rolesEnum';
import { ReportTypesResponse } from '../../../models/reports';
import { Role } from '../../../models/role';
import { WebPortal } from '../../../models/webPortal';
import { ArticleService } from '../../../services/article.service';
import { ConfigService } from '../../../services/config.service';
import { ReportingService } from '../../../services/reporting.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit, OnDestroy {
  title: string;
  reportTypes = new Array<ReportTypesResponse>();
  webPortals = new Array<WebPortal>();
  roles = new Array<Role>();
  datePeriodMonths: number;
  selectedReportType: ReportTypesResponse;
  selectedWebPortal: WebPortal;
  selectedRole: Role;
  selectedDateFrom: Date = new Date();
  selectedDateTo: Date = new Date();
  selectedDateView = 'Year';
  minDate: Date;
  maxDate = new Date();
  dateView = [
    'Range',
    'Today',
    'Week',
    'Month',
    'Year',
    'None'
  ];

  language = localStorage.getItem('A1_Admin_language');
  admin = [Roles.SystemAdmin, Roles.Admin];
  supervisor = [Roles.SystemAdmin, Roles.Admin, Roles.Supervisor];
  writer = [Roles.SystemAdmin, Roles.Admin, Roles.Supervisor, Roles.Writer];
  reader = [Roles.SystemAdmin, Roles.Admin, Roles.Supervisor, Roles.Writer, Roles.Reader];
  destroy$ = new Subject<void>();

  constructor(private articleService: ArticleService, private reportingService: ReportingService, private route: ActivatedRoute, private configService: ConfigService, private router: Router) {
  }

  ngOnInit(): void {
    this.title = this.route.snapshot.data.title;

    const defaultWebPortal = JSON.parse(localStorage.getItem('A1_Admin_defaultWebPortal'));
    if (defaultWebPortal !== null) {
      this.selectedWebPortal = defaultWebPortal;
    }

    Promise.all([
      this.getAllReportTypes(),
      this.getAllWebPortals(),
      this.getAllRoles(),
      this.getDatePeriod(),
    ]).then(res => {

      this.router.events.pipe(filter(event => event instanceof NavigationEnd), takeUntil(this.destroy$)).subscribe(event => {
        const navigationEvent = event as NavigationEnd;
        const reportTypeName = navigationEvent.url.split('/').reverse()[0];

        const reportType = this.reportTypes.find(r => r.name == reportTypeName);
        if (reportType) {
          this.selectedReportType = reportType;
        } else {
          this.selectedReportType = this.reportTypes[0];
        }
      })
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  async getAllReportTypes() {
    this.reportTypes = await this.reportingService.getReportTypes();

    const reportTypeIndex = this.reportTypes.findIndex(r => r.name == this.route.snapshot?.firstChild?.params.reportType);

    if (this.reportTypes.length > 0) {
      this.selectedReportType = reportTypeIndex > -1 ? this.reportTypes[reportTypeIndex] : this.reportTypes[0];
    }

  }

  async getAllWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
  }

  async getAllRoles() {
    this.roles = await this.configService.getAllRoles();
  }

  async getDatePeriod() {
    this.datePeriodMonths = await this.reportingService.getReportDatePeriods();
    const date = new Date();
    this.minDate = new Date(date.setMonth(date.getMonth() - this.datePeriodMonths));
  }

  setReportType(reportType: ReportTypesResponse) {
    this.selectedReportType = reportType;
    this.router.navigateByUrl(`/reports/view/${reportType.name}`);
  }

  setWebPortal(webPortal: WebPortal) {  
    const filter = this.reportingService.filters.value;
    filter.webPortal = webPortal;
    this.reportingService.filters.next(filter);
  }

  setRole(role: Role) {
    const filter = this.reportingService.filters.value;
    filter.role = role;
    this.reportingService.filters.next(filter);
  }

  setDateFrom(date: MatDatepickerInputEvent<Date>) {
    const filter = this.reportingService.filters.value;
    filter.fromDate = date.value;
    this.reportingService.filters.next(filter);
  }

  setDateTo(date: MatDatepickerInputEvent<Date>) {
    const filter = this.reportingService.filters.value;
    filter.untilDate = date.value;
    this.reportingService.filters.next(filter);
  }

  setDateView(dateView: string) {  
    const filter = this.reportingService.filters.value;
    filter.reportDateFilterType = dateView;
    this.reportingService.filters.next(filter);
  }

  dateFromFilter = (d: Date | null): boolean => {
    if (d == null) {
      return true;
    }
    return new Date(d)?.getTime() <= new Date(this.selectedDateTo)?.getTime();
  }

  dateToFilter = (d: Date | null): boolean => {
    if (d == null) {
      return true;
    }
    return new Date(d)?.getTime() >= new Date(this.selectedDateFrom)?.getTime();
  }

}
