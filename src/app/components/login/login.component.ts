import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginRequest } from 'src/app/models/loginRequest';
import { LoginSerivce } from 'src/app/services/login.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { NotificationsService } from 'angular2-notifications';
import { ArticleService } from 'src/app/services/article.service';
import { ModalService } from 'src/app/services/modal.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MediaService } from 'src/app/services/media.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;
  loginRequest: LoginRequest;
  error: string;
  dateExpire = new Date();
  rememberMe: boolean;
  settings = new Array<{ name: string, value: string }>();
  hidePassword = true;
  tinyRoles: any;
  activationToken: string;
  pattern: RegExp;
  activation = false;
  password;
  confirmPassword;
  languages: any;
  redirectUrl = null;

  constructor(private articleService: ArticleService, private route: ActivatedRoute, private snackBar: MatSnackBar, private modalService: ModalService, private loginService: LoginSerivce, private notification: NotificationsService, private router: Router, private cookieService: CookieService, private loader: NgxUiLoaderService, private mediaService: MediaService) {
    this.loginRequest = new LoginRequest();
    this.redirectUrl = localStorage.getItem('A1_Admin_redirectUrl');
    if (this.route.snapshot.paramMap.get('token')) {
      this.activationToken = this.route.snapshot.paramMap.get('token');
      if (this.activationToken.match('(^([0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12})$)')) {
        this.activation = true;
      } else {
        this.router.navigate(['/login']);
      }
    }
    this.cookieService.set('sameSite', 'Strict');
  }

  ngOnInit(): void {
    if (this.cookieService.get('email') && this.cookieService.get('password')) {
      this.loginRequest.email = this.cookieService.get('email');
      this.loginRequest.password = this.cookieService.get('password');
      this.rememberMe = true;
    }
  }

  login() {
    if (this.loginForm.form.valid) {
      if (this.loginRequest.password === null || this.loginRequest.email === null || this.loginRequest.password === '' || this.loginRequest.email === '') {
        this.error = 'Username and password must have values';
      } else {
        this.loader.startLoader('login');
        this.loginService.login(this.loginRequest).then(async data => {
          if (data.isSuccess === true) {
            localStorage.setItem('A1_Admin_Username', data.userFullName);
            localStorage.setItem('A1_Admin_UserId', data.userId.toString());
            localStorage.setItem('A1_Admin_accessToken', data.access_token);
            localStorage.setItem('A1_Admin_defaultLanguage', JSON.stringify(data.defaultLanguageId));
            localStorage.setItem('A1_Admin_defaultWebPortal', JSON.stringify(data.defaultWebPortal));
            localStorage.setItem('A1_Admin_isSystemAdmin', `${data.isSystemAdmin}`);
            let roles: string[] = [];
            data.roles.forEach(r => {
              const role = r.toUpperCase();
              roles.push(role);
            });
            localStorage.setItem('A1_Admin_a1kbRoles', JSON.stringify(roles));
            this.dateExpire.setSeconds(data.expires_in);
            localStorage.setItem('A1_Admin_expiresIn', this.dateExpire.toUTCString());
            this.tinyRoles = await this.articleService.getAllRoles('false');
            localStorage.setItem('A1_Admin_userRoles', JSON.stringify(this.tinyRoles));
            if (!localStorage.getItem('A1_Admin_languages')) {
              await this.getAllLanguages();
              localStorage.setItem('A1_Admin_languages', JSON.stringify(this.languages));
            } else {
              this.languages = JSON.parse(localStorage.getItem('A1_Admin_languages'));
            }
            if (data.isSystemAdmin === true) {
              this.router.navigate(['/application-settings/tenant']);
            } else if (this.redirectUrl != null) {
              this.router.navigate([this.redirectUrl]);
            } else {
              this.router.navigate(['/articles/article-management']);
            }
          } else if (data.isSuccess === false) {
            this.error = data.message.replace('Error: ', '');
            this.loader.stopLoader('login');
          }
        }).catch(async err => {
          if (err.errors) {
            for (const key in err.errors) {
              if (Object.prototype.hasOwnProperty.call(err.errors, key)) {
                const element = err.errors[key];
                this.error = element.toString() + '\n';
              }
            }
          } else if (typeof err === 'string') {
            this.error = err;
          } else {
            this.error = 'Service is not available';
          }
          this.loader.stopLoader('login');
        });
      }
    }
  }

  // getRoles() {
  //   this.roleService.getUserRoles().then(data => {
  //     this.userRoles = data;
  //     localStorage.setItem('A1_Admin_userRoles', JSON.stringify(this.userRoles));
  //   });
  //   this.roleService.getPartnerRoles().then(data => {
  //     this.partnerRoles = data;
  //     localStorage.setItem('A1_Admin_partnerRoles', JSON.stringify(this.partnerRoles));
  //   });
  // }

  validateLogin() {
    this.loginForm.form.markAllAsTouched();
    if (this.loginForm.valid === true) {
      this.login();
    }
  }

  forgotPass() {
    this.modalService.forgotPassDialogOpened({
      title: 'Password Reset',
      message: 'Enter your email address',
      cancelText: 'Cancel',
      confirmText: 'Reset password',
      input: this.loginRequest.email
    });
    this.modalService.forgotPassDialogConfirmed().subscribe(data => {
      if (data) {
        if (data !== null) {
          this.loginService.resetPassword(data).then(res => {
            if (res) {
              this.snackBar.open('Password Reset Sent', 'OK', { duration: 3500 });
            }
          });
        }
      }
    });
  }

  activateAccount() {
    if (this.password === this.confirmPassword) {
      this.loader.startLoader('login');
      this.loginService.activateAccount(this.activationToken, this.password).then(async data => {
        this.snackBar.open('Activated Successfully', 'OK', { duration: 3000 });
        this.loader.stopLoader('login');
        window.location.href = 'https://knowledgebaseapp.payment.saga.co.yu/kbinternal/index.html';
        // this.router.navigate(['/login']);
      }).catch(err => {
        // this.router.navigate(['/login']);
        window.location.href = 'https://knowledgebaseapp.payment.saga.co.yu/kbinternal/index.html';
      });
    }
  }

  private async getAllLanguages() {
    this.languages = await this.articleService.getAllLanguages();
    // this.languages.forEach(l => {
    //   l.local = l.local.toUpperCase();
    // });
  }

}
