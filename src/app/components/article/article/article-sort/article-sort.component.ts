import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ArticleSort, ArticleView } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { MatTableDataSource } from '@angular/material/table';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';
import { cloneDeep } from 'lodash';
import { NgForm } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ArticleComponent } from '../article.component';


@Component({
  selector: 'app-article-sort',
  templateUrl: './article-sort.component.html',
  styleUrls: ['./article-sort.component.scss']
})
export class ArticleSortComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('sortForm') sortForm: NgForm;

  @Input() articles: Array<ArticleView>;
  @Input() categoryId: number;
  @Output() returnValidation: EventEmitter<boolean> = new EventEmitter<boolean>();

  articleSort = new ArticleSort();
  dataSource: MatTableDataSource<ArticleView>;
  articleData = new Array<ArticleView>();
  saveOrderSub: Subscription;
  saveSortOrder = false;
  oldData: Array<ArticleView>;
  displayedColumns = ['order', 'subject', 'description', 'articleStatus', 'createdByFullName', 'createdDate', 'updatedByFullName', 'updatedDate', 'hasAttachment', 'action'];

  constructor(private articleService: ArticleService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private eventEmitter: EventEmitterService, private modalService: ModalService) { }

  ngOnInit(): void {
    this.articleData = cloneDeep(this.articles);
    this.oldData = cloneDeep(this.articleData);
    this.dataSource = new MatTableDataSource<ArticleView>(this.articleData);
    this.saveOrderSub = this.eventEmitter.emitSaveArticleOrder.subscribe(data => {
      if (data === true) {
        this.saveOrder();
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.articles) {
      this.articleData = { ... this.articles };
      this.dataSource = new MatTableDataSource<ArticleView>(this.articleData);
    }
  }

  ngOnDestroy(): void {
    this.saveOrderSub.unsubscribe();
  }

  dropArticle(event: CdkDragDrop<Array<ArticleView>>) {
    moveItemInArray(this.articleData, event.previousIndex, event.currentIndex);
    const droppedArticle = event.item.data;
    if (droppedArticle.order === null) {
      const orderArray = this.articleData.map(a => a.order);
      this.articleData.find(a => a === droppedArticle).order = Math.max(...orderArray) + 1;
    }
    this.sortArticlesByOrder();
    this.dataSource = new MatTableDataSource<ArticleView>(this.articleData);
  }

  sortArticles() {
    this.articleSort = new ArticleSort();
    this.articleData.forEach(a => {
      if (a.order !== null) {
        this.articleSort.articles.push({ id: a.id, order: a.order });
      } else {
        this.articleSort.articles.push({ id: a.id, order: null });
      }
    });
    console.log(this.articleSort);
    this.dataSource = new MatTableDataSource<ArticleView>(this.articleData);
  }

  sortArticlesByOrder() {
    this.articleSort = new ArticleSort();
    let order = 1;
    this.articleData.forEach(a => {
      if (a.order !== null) {
        this.articleSort.articles.push({ id: a.id, order: order });
        a.order = order;
        order++;
      } else {
        this.articleSort.articles.push({ id: a.id, order: null });
      }
    });
    this.dataSource = new MatTableDataSource<ArticleView>(this.articleData);
  }

  async saveOrder() {
    if (this.verifyDuplicate() === true) {
      this.loader.startLoader('l-1');
      this.sortArticles();
      this.articleSort.categoryId = this.categoryId;
      this.returnValidation.emit(true);
      this.articleService.saveArticleSortOrder(this.articleSort).then(async data => {
        this.snackBar.open('Article Sort Order Saved Successfully', 'OK', { duration: 3500 });
      });
      this.loader.stopLoader('l-1');
    } else {
      this.returnValidation.emit(false);
    }
  }

  removeOrder(id) {
    this.modalService.confirmDialogOpen({
      title: 'Remove Article Order?',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.articleData.find(a => a.id === id).order = null;
        this.sortArticlesByOrder();
      }
    });
  }

  verifyDuplicate(): boolean {
    for (var i = 0; i < this.articleData.length; i++) {
      for (var j = i; j < this.articleData.length; j++) {
        if (i != j) {
          if (this.articleData[i].order !== null) {
            if (this.articleData[i].order == this.articleData[j].order) {
              console.log(this.articleData[i]);
              const element = document.getElementById(this.articleData[i].id);
              element.focus();
              element.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
              this.snackBar.open('NO DUPLICATE VALUES', 'OK');
              return false;
            }
          }
        }
      }
    }
    return true;
  }

  updateArticleOrder(order, id) {
    if (order !== null) {
      const oldArticle = this.oldData.find(item => item.id === id);
      const article = {...this.articleData.find(item => item.id === id)}
      article.order = order;
      this.articleData = this.articleData.sort((a, b) => a.order - b.order);
      for (let i = 0; i < this.articleData.length; i++) {
        const element = this.articleData[i];
        if (element.order !== null) {
          if (element.order === article.order) {
            if (element.id !== article.id) {
              this.articleData[i].order = oldArticle.order;
            }
          }
        }
      }
    }
  }

}
