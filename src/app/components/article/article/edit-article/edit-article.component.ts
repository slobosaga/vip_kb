import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { TreeNode } from 'src/app/base/models/treeNode';
import { ArticleRequest, ArticleViewResponse, ArticleStatus, ArticleGet, ArticleAttachment, GenerateRelatedArticles, RelatedArticle, ArticleRevision, ArticleRevisions, Devices, DeviceDetails } from 'src/app/models/article';
import { Language } from 'src/app/models/enums/language';
import { Snippet } from 'src/app/models/snippet';
import { EditorTemplate, Template } from 'src/app/models/template';
import { ArticleService } from 'src/app/services/article.service';
import { MediaService } from 'src/app/services/media.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';
import { Subscription } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MatChipInputEvent } from '@angular/material/chips';
import { Role } from 'src/app/models/role';
import { Role as Roles } from 'src/app/models/enums/rolesEnum';
import { MatSnackBar } from '@angular/material/snack-bar';
import { InternalComment } from 'src/app/models/comment';
import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';
import { AppConfigService } from 'src/app/base/configuration.service';
import { CkeditorConfig, CkeditorFilter } from 'src/app/models/enums/ckeditor';
import { WebPortal } from 'src/app/models/webPortal';
import { ElementRef } from '@angular/core';
import { filter } from 'rxjs/operators';
import { environment } from './../../../../../environments/environment';
import { CKEditor4 } from 'ckeditor4-angular/ckeditor';
declare var $: any;

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.scss']
})
export class EditArticleComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('autosize') autosize: CdkTextareaAutosize;
  @ViewChild('contentForm') contentForm: NgForm;
  @ViewChild('datesForm') datesForm: NgForm;

  internalEditor = 'internal-editor';
  externalEditor = 'external-editor';
  articleId = null;
  templateData = new Array<Template>();
  snippetData = new Array<Snippet>();
  editorTemplates = new Array<EditorTemplate>();
  editorSnippets = new Array<EditorTemplate>();
  articleRequest = new ArticleRequest();
  articles = new ArticleViewResponse();
  article = new ArticleGet();
  template = new Template();
  snippet = new Snippet();
  languages = new Array<Language>();
  keywordsArray = new Array<string>();
  seoArray = new Array<string>();
  articleStatuses = new Array<any>();
  sortedNodes: Array<TreeNode>;
  treeNodeSubscription: Subscription;
  nestedTreeSubscription: Subscription;
  selectedExpirationCategories = new Array<number>();
  attachments: Array<ArticleAttachment> = [];
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  keywordRemoveable = true;
  seoRemoveable = true;
  emailToArray = new Array<string>();
  emailCcArray = new Array<string>();
  emailToRemoveable = true;
  emailCcRemoveable = true;
  userId;
  relatedArticleRequest = new GenerateRelatedArticles();
  relatedArticles = new Array<RelatedArticle>();
  maxNoRelatedArticles = 0;
  showInternal = false;
  showExternal = true;
  readonly = false;
  articleAnotherLanguage = false;
  articleLanguageId = null;
  showSpinner = false;
  featuredImageName = 'Select an Image';
  attachmentNames = 'Select Attachments';
  formData = new FormData();
  articleStatus = null;
  selectedCategories: number[] = [];
  routeSub: any;
  supervisor = [Roles.SystemAdmin, Roles.Admin, Roles.Supervisor];
  commentNgModel = '';
  minDate = new Date();
  minUnpublishDate = new Date();
  articleBodyRoles = new Array<{ role: string, id: number, content: string }>();
  customElements = '';
  roleTags: string[] = [];
  articleRoles = new Array<Role>();
  tinyArticleRoles = new Array<Role>();
  tinyRoles = new Array<Role>();
  selectedRoles: number[] = [];
  writer = [Roles.SystemAdmin, Roles.Admin, Roles.Supervisor, Roles.Writer];
  webPortalsFilter = new Array<string>();
  webPortals = new Array<WebPortal>();
  ckconfig = new CkeditorConfig();
  ckfilter = new CkeditorFilter();
  lockSubscription: Subscription;
  routerSubscription: Subscription;
  currentArticleVersion;
  showUndoBtn = false;
  revisions = null;
  displayAttachModal = false;
  relatedQuestion: string = null;
  previewDevices = new Array<Devices>();
  selectedDevice: Devices = null;
  ckEditorUrl: string;

  constructor(private articleService: ArticleService, private el: ElementRef, private router: Router, private location: Location, private modalService: ModalService, private route: ActivatedRoute, private mediaService: MediaService, private eventEmmiter: EventEmitterService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private appConfigService: AppConfigService) {
    this.articleId = this.route.snapshot.paramMap.get('id');
    this.articleLanguageId = JSON.parse(this.route.snapshot.paramMap.get('languageId'));
    this.article.language = JSON.parse(localStorage.getItem('A1_Admin_defaultLanguage'));
    this.userId = localStorage.getItem('A1_Admin_UserId');
    localStorage.removeItem('A1_Admin_articleBodyRoles');
    localStorage.removeItem('A1_Admin_bodyRoles');
    this.setReadonly();
    this.minDate.setDate(this.minDate.getDate() + 1);
    if (this.articleId !== null && this.readonly === false) {
      this.lockSubscription = this.articleService.lockArticleObservable(this.articleId).subscribe();
    }
    if (environment.production === true) {
      this.ckEditorUrl = appConfigService.ckEditorUrlProd;
    } else {
      this.ckEditorUrl = appConfigService.ckEditorUrlDev;
    }
    this.ckconfig.removePlugins = 'image2';
    this.ckconfig.extraPlugins = 'image,font,templates,snippets,html5video,widgetselection,lineutils,uploadimage,uploadwidget,filebrowser,accordionList,collapsibleItem,iframedialog,iframe,mailto,createSnippet,createTemplate,dialog,roles,tableresize';
    this.ckconfig.extraAllowedContent = 'div[data-*,id,contenteditable]; p[data-*,id,contenteditable]; p{background-*}; span[data-*,id,contenteditable]';
  }

  async ngOnInit() {
    this.routerSubscription = this.router.events.pipe(filter(event => event instanceof NavigationStart)).subscribe((event: NavigationStart) => {
      if (this.lockSubscription) {
        this.lockSubscription.unsubscribe();
      }
      this.unlockArticle();
    });
  }

  async ngAfterViewInit() {
    this.scrollIntoView();
    this.loader.startLoader('l-1');
    await this.getTemplates();
    await this.getSnippets();
    await this.getRoles();
    await this.getWebPortals();
    this.setDeviceData();
    this.setSubscriptions();
    setTimeout(async () => {
      await this.getArticle();
      this.loader.stopLoader('l-1');
    }, 2000);
  }

  private scrollIntoView() {
    const topOfPage = document.getElementById('topOfPage');
    if (topOfPage !== null) {
      topOfPage.scrollIntoView();
    }
  }

  // unlock article if tab/browser closes
  @HostListener('window:beforeunload')
  async unlockArticle() {
    if (this.articleId != null && this.readonly === false) {
      await this.articleService.unlockArticle(this.articleId);
    }
  }

  private setReadonly() {
    if (this.route.snapshot.data?.readonly) {
      this.readonly = this.route.snapshot.data?.readonly;
      if (this.readonly === true) {
        this.keywordRemoveable = false;
        this.seoRemoveable = false;
        this.emailToRemoveable = false;
        this.keywordRemoveable = false;
      }
    }
    if (this.route.snapshot.data?.articleAnotherLanguage) {
      this.articleAnotherLanguage = true;
      if (this.articleAnotherLanguage === true) {
        this.keywordRemoveable = true;
        this.seoRemoveable = true;
      }
    }
  }

  async ngOnDestroy() {
    this.removeSubscriptions();
    localStorage.removeItem('A1_Admin_articleBodyRoles');
    localStorage.removeItem('A1_Admin_bodyRoles');
    localStorage.removeItem('A1_Admin_templates');
    localStorage.removeItem('A1_Admin_snippets');
  }

  removeSubscriptions() {
    if (this.lockSubscription) {
      this.lockSubscription.unsubscribe();
    }
    if (this.treeNodeSubscription) {
      this.treeNodeSubscription.unsubscribe();
    }
    if (this.nestedTreeSubscription) {
      this.nestedTreeSubscription.unsubscribe();
    }
    if (this.routerSubscription) {
      this.routerSubscription.unsubscribe();
    }
  }

  setSubscriptions() {
    this.treeNodeSubscription = this.eventEmmiter.emmitTreeNodes.subscribe(data => {
      this.sortedNodes = data;
    });
    this.nestedTreeSubscription = this.eventEmmiter.emitNestedTree.subscribe(data => {
      this.articleRequest.expirationCategories = data;
    });
  }

  async ckFileUpload(event) {
    const fileLoader = event.data.fileLoader;
    const formData = new FormData();
    const xhr = fileLoader.xhr;
    xhr.open('POST', fileLoader.uploadUrl, true);
    xhr.setRequestHeader('Content-Disposition', 'miltipart/form-data');
    xhr.setRequestHeader('X-File-Name', fileLoader.fileName);
    xhr.setRequestHeader('X-File-Size', fileLoader.total);
    xhr.setRequestHeader('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken'));
    formData.append('file', fileLoader.file, fileLoader.fileName);
    xhr.send(formData);
    event.stop();
  }

  ckFileResponse(event) {
    event.stop();
    const data = event.data;
    const xhr = data.fileLoader.xhr;
    let response = xhr.responseText;
    response = JSON.parse(response);
    data.url = response.url;
  }

  ckOnChange(event: CKEditor4.EventInfo) {
    let snippetId = JSON.parse(sessionStorage.getItem('addedSnippetId'));
    if (snippetId) {
      snippetId = parseInt(snippetId);
      this.articleRequest.snippets.push(snippetId);
      sessionStorage.setItem('addedSnippetId', null);
    }
  }

  async getArticle() {
    await this.getArticleContent().catch(err => {
      console.error(err);
      this.router.navigate(['articles/article-management']);
      this.unlockArticle();
    });
    if (this.articleId !== null) {
      await this.getArticleRevisionContent();
    }
    this.languages = await this.articleService.getAllLanguages('false');
    this.maxNoRelatedArticles = await this.articleService.getMaxNoRelatedArticles();
  }

  async getWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
  }

  async getRoles() {
    this.tinyArticleRoles = await this.articleService.getAllRoles('false');
    this.tinyRoles = await this.articleService.getAllRoles('false');
  }

  async getTemplates() {
    this.editorTemplates = new Array<EditorTemplate>();
    this.templateData = new Array<Template>();
    const data = await this.articleService.getAllTemplates(0, 10000, '', false);
    if (data.templates != null) {
      this.templateData = data.templates;
      const editorTemplates = new Array<{ text: string, value: string }>();
      this.templateData.forEach(t => {
        editorTemplates.push({ text: t.name, value: t.text });
      });
      localStorage.setItem('A1_Admin_templates', JSON.stringify(editorTemplates));
    }
  }

  async getSnippets() {
    this.editorSnippets = new Array<EditorTemplate>();
    this.snippetData = new Array<Snippet>();
    const data = await this.articleService.getAllSnippets(0, 1000000, '', false);
    if (data.snippets != null) {
      this.snippetData = data.snippets;
      // snippets for ck plugin
      const editorSnippets = new Array<{ text: string, value: string }>();
      this.snippetData.forEach(s => {
        const element = this.setSnippetElement(s.text);
        editorSnippets.push({ text: s.name, value: `<${element} id=${s.id} data-snippet=${s.id} contentEditable=false>${s.text}</${element}>` });
      });
      localStorage.setItem('A1_Admin_snippets', JSON.stringify(editorSnippets));
    }
  }

  // determine if snippet is a span or div
  setSnippetElement(text): string {
    let element = 'div';
    const snippet = $(text);
    if (snippet.length <= 1) {
      element = 'span';
    } else {
      const snippetChildren = snippet.children();
      for (const child of snippetChildren) {
        child.setAttribute('contenteditable', 'false');
      }
    }
    return element;
  }

  // add inserted snippet to article request
  addSnippetToList(editorSnippet: string) {
    this.snippetData.forEach(s => {
      const snippet = `data-snippet=${s.id}`;
      if (editorSnippet.match(snippet)) {
        if (!this.articleRequest.snippets.find(snip => snip === s.id)) {
          this.articleRequest.snippets.push(s.id);
        }
      }
    });
  }

  // check if snippets exist in article on submit
  checkSnippets() {
    let articleInternalBody: string = this.article.htmlBodyInternal;
    let articleExternalBody: string = this.article.htmlBodyExternal;
    if (articleInternalBody === null) {
      articleInternalBody = '';
    }
    if (articleExternalBody === null) {
      articleExternalBody = '';
    }
    if (this.article.snippets !== null) {
      this.article.snippets.forEach(s => {
        const snippet = `data-snippet="${s.id}"`;
        if (!articleInternalBody.includes(snippet) && !articleExternalBody.includes(snippet)) {
          this.articleRequest.snippets.splice(this.articleRequest.snippets.indexOf(s.id), 1);
        }
      });
    }
    if (this.snippetData !== null) {
      this.snippetData.forEach(s => {
        const snippet = `data-snippet="${s.id}"`;
        if (articleInternalBody.includes(snippet) && articleExternalBody.includes(snippet)) {
          this.articleRequest.snippets.push(s.id);
        }
      });
    }
  }

  // set inital snippets on article load
  setSnippets() {
    if (this.article.snippets !== null) {
      this.article.snippets.forEach(s => {
        if (!this.articleRequest.snippets.find(snip => snip === s.id)) {
          this.articleRequest.snippets.push(s.id);
        }
      });
    }
  }

  async getArticleContent(languageId: string = null) {
    if (this.articleLanguageId !== null) {
      languageId = this.articleLanguageId;
    } else {
      languageId = localStorage.getItem('A1_Admin_language');
    }
    if (this.articleId != null) {
      if (this.articleAnotherLanguage === true) {
        this.article = await this.articleService.getArticleContentMeta(this.articleId, languageId);
        this.article.language = this.articleLanguageId;
      } else {
        this.article = await this.articleService.getArticleContent(this.articleId, languageId);
      }
      if (this.article.articleStatus === 'rejected') {
        this.readonly = true;
      }
      if (this.article.attachments != null) {
        this.attachments = this.article.attachments.sort((a, b) => a.order - b.order);
      }
      if (this.article.keywords != null) {
        this.keywordsArray = this.article.keywords;
      }
      if (this.article.relatedArticles != null) {
        this.relatedArticles = this.article.relatedArticles;
      }
      if (this.article.comments) {
        this.article.comments.forEach(c => {
          c.initials = this.generateUsernameInitials(c.userFullName);
        });
      }
      if (this.article.categories !== null) {
        this.selectedCategories = this.article.categories.map(c => c.id);
      }
      if (this.article.seo !== null) {
        this.seoArray = this.article.seo?.split(',');
      }
      if (this.article.snippets !== null) {
        this.articleRequest.snippets = this.article.snippets.map(s => s.id);
      }
      if (this.article.relatedQuestions === null) {
        this.article.relatedQuestions = [];
      }
      this.emailCcArray = this.article?.emailCc;
      this.emailToArray = this.article?.emailTo;
      if (this.article.userRoles !== null) {
        this.article.userRoles.forEach(r => {
          this.selectedRoles.push(r.id);
        });
      }
    }
    this.setArticleRoles();
    this.findBodyRoles();
    this.setSnippets();
  }

  async getArticleStatuses() {
    this.articleStatuses = await this.articleService.getAllArticleStatuses();
  }

  async getRelatedArticles() {
    if (this.selectedCategories.length > 0 && this.article.language != null) {
      this.showSpinner = true;
      this.relatedArticleRequest.alreadyRelatedArticles = this.articleRequest.relatedArticles.map(ra => ra.id);
      this.relatedArticleRequest.articleId = this.articleId;
      this.relatedArticleRequest.categories = this.selectedCategories;
      this.relatedArticleRequest.languageId = this.article.language;
      this.relatedArticles = await this.articleService.getRelatedArticles(this.relatedArticleRequest);
      if (this.relatedArticles !== null && this.relatedArticles !== undefined) {
        for (let i = 0; i < this.relatedArticles.length; i++) {
          const article = this.relatedArticles[i];
          article.order = i + 1;
        }
      } else {
        this.relatedArticles = new Array<RelatedArticle>();
      }
      this.showSpinner = false;
    } else {
      this.snackBar.open('Please select a LANGUAGE and a CATEGORY to generate related articles', 'OK', { duration: 3000 });
    }
    this.relatedArticleRequest = new GenerateRelatedArticles();
  }

  async setFeaturedImage(event) {
    const file = event.target.files[0];
    if (file) {
      this.featuredImageName = file.filename;
      this.formData.append('file', file, file.filename);
      this.mediaService.uploadImage(this.formData).then(data => {
        this.article.featuredImage = data.location;
      });
    }
  }

  openImgPreview() {
    if (this.article.featuredImage !== null) {
      this.modalService.previewModalOpen(this.article.featuredImage);
    }
  }

  onFileChange(event, attachUpload) {
    const attachOrder = this.attachments.map(a => a.order);
    for (let i = 0; i < event.target.files.length; i++) {
      let order = 0;
      if (this.attachments.length > 0) {
        order = Math.min(...attachOrder);
      } else {
        order = i + 1;
      }
      const file = event.target.files[i];
      const attachment = new ArticleAttachment();
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        attachment.attachment = reader.result;
        attachment.order = order;
        attachment.name = file.name;
        attachment.attachment = attachment.attachment.toString().split(',')[1];
        this.attachments.unshift(attachment);
        this.attachments.forEach(a => {
          a.order = this.attachments.indexOf(a) + 1;
        });
      };
    }
    attachUpload.clear();
  }

  openSortModal() {
    if (this.attachments.length > 0) {
      // this.attachments.sort((a, b) => a.order - b.order);
      if (this.attachments.length >= 10) {
        this.displayAttachModal = true;
      } else {
        this.modalService.sortDialogOpen(this.attachments);
        this.modalService.sortDialogConfirmed().subscribe(data => {
          this.articleRequest.attachments = data;
        });
      }
    }
  }

  dropAttachment(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.attachments, event.previousIndex, event.currentIndex);
    for (let i = 0; i < this.attachments.length; i++) {
      const att = this.attachments[i];
      att.order = i + 1;
    }
  }

  removeAttachment(index) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Attachment',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.attachments.splice(index, 1);
        for (let i = 0; i < this.attachments.length; i++) {
          const att = this.attachments[i];
          att.order = i + 1;
        }
      }
    });
  }

  addKeyword(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.keywordsArray.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  pasteKeyword(event: ClipboardEvent): void {
    event.preventDefault();
    event.clipboardData.getData('Text').split(/;|,|\n/).forEach(value => {
      if (value.trim()) {
        this.keywordsArray.push(value.trim());
      }
    });
  }

  removeKeyword(keyword: string): void {
    const index = this.keywordsArray.indexOf(keyword);
    if (index >= 0) {
      this.keywordsArray.splice(index, 1);
    }
  }

  addSeo(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.seoArray.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  pasteSeo(event: ClipboardEvent): void {
    event.preventDefault();
    event.clipboardData.getData('Text').split(/;|,|\n/).forEach(value => {
      if (value.trim()) {
        this.seoArray.push(value.trim());
      }
    });
  }

  removeSeo(keyword: string): void {
    const index = this.seoArray.indexOf(keyword);

    if (index >= 0) {
      this.seoArray.splice(index, 1);
    }
  }

  addEmailTo(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      if (this.emailToArray === null) {
        this.emailToArray = [];
      }
      this.emailToArray.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  removeEmailTo(keyword: string): void {
    const index = this.emailToArray.indexOf(keyword);
    if (index >= 0) {
      this.emailToArray.splice(index, 1);
    }
  }

  addemailCc(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      if (this.emailCcArray === null) {
        this.emailCcArray = [];
      }
      this.emailCcArray.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  removeemailCc(keyword: string): void {
    const index = this.emailCcArray.indexOf(keyword);
    if (index >= 0) {
      this.emailCcArray.splice(index, 1);
    }
  }

  async submitArticle(event) {
    const status = event?.submitter?.id;
    let result = null;
    if (this.contentForm.form.valid === true) {
      if (this.article.htmlBodyInternal !== '') {
        if (this.selectedCategories.length <= 0) {
          if (status === 'published' || status === 'scheduled') {
            this.snackBar.open('Please Select a Category', 'OK', { duration: 3000 });
            return;
          }
        }
        this.setArticleRequest(status);
        this.checkSnippets();
        if (this.articleId === null) {
          this.loader.startLoader('l-1');
          result = this.createNewArticle(result);
        } else if (this.articleAnotherLanguage === true) {
          this.createArticleForAnotherLanguage(this.articleRequest);
        } else {
          // this.articleStatus = this.articleRequest.articleStatus;
          result = await this.saveArticleAs(this.articleRequest.articleStatus);
        }
        this.loader.stopLoader('l-1');
      } else {
        this.snackBar.open('Internal Editor can not be empty', 'OK', { duration: 3000 });
      }
    } else {
      this.scrollIntoErrorField();
    }
  }

  createNewArticle(result: any) {
    this.articleService.createArticle(this.articleRequest, this.article.language).then(async (data) => {
      result = await data;
      this.router.navigate([`/articles/edit-article/${result}/${this.article.language}`]);
      this.snackBar.open('Article Created Successfully', 'OK', { duration: 3000 });
    }).catch(error => {
      result = error;
      this.loader.stopLoader('l-1');
    });
    return result;
  }

  scrollIntoErrorField() {
    for (const key of Object.keys(this.contentForm.controls)) {
      if (this.contentForm.controls[key].invalid) {
        const invalidControl = this.el.nativeElement.querySelector('[name="' + key + '"]');
        invalidControl.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
      }
    }
  }

  setArticleRequest(status: string) {
    this.articleRequest.articleStatus = status ? status : this.article.articleStatus;
    this.articleRequest.htmlBodyInternal = this.article.htmlBodyInternal;
    this.articleRequest.htmlBodyExternal = this.article.htmlBodyExternal;
    this.articleRequest.subject = this.article.subject;
    this.articleRequest.excerpt = this.article.excerpt;
    this.articleRequest.description = this.article.description;
    this.articleRequest.expirationDate = this.article.expirationDate;
    this.articleRequest.scheduledDate = this.article.scheduledDate;
    this.articleRequest.scheduledUnpublishedDate = this.article.scheduledUnpublishedDate;
    this.articleRequest.attachments = this.attachments;
    this.articleRequest.keywords = this.keywordsArray.length > 0 ? this.keywordsArray : null;
    this.articleRequest.seo = this.seoArray.length > 0 ? this.seoArray.join() : null;
    this.articleRequest.emailCc = this.emailCcArray;
    this.articleRequest.emailTo = this.emailToArray;
    this.articleRequest.relatedArticles = this.relatedArticles;
    this.articleRequest.comments = this.article.comments;
    this.articleRequest.categories = this.selectedCategories;
    this.articleRequest.expirationCategories = this.selectedExpirationCategories;
    this.articleRequest.featuredImage = this.article.featuredImage;
    this.articleRequest.sendSchedulePublishEmail = this.article.sendSchedulePublishEmail;
    this.articleRequest.sendScheduleUnpublishEmail = this.article.sendScheduleUnpublishEmail;
    this.articleRequest.sendExpirationEmail = this.article.sendExpirationEmail;
    this.articleRequest.userRoles = this.selectedRoles;
    this.articleRequest.relatedQuestions = this.article.relatedQuestions;
  }

  createArticleForAnotherLanguage(articleRequest) {
    this.articleService.createArticleForAnotherLanguage(articleRequest, this.articleId, this.articleLanguageId);
    this.snackBar.open('Article Created Successfully', 'OK', { duration: 3000 });
    this.router.navigate(['/articles/article-management']);
  }

  async saveArticleAs(status) {
    let result = null;
    switch (status) {
      case ArticleStatus.Approved:
        this.loader.startLoader('l-1');
        result = await this.articleService.markAsApproved(this.articleId, this.articleRequest, this.article.language).then(data => {
          if (data !== undefined) {
            result = data;
            this.snackBar.open(`Article Saved Successfully as ${this.articleRequest.articleStatus}`, 'OK', { duration: 3000 });
            this.reloadComponent(this.articleId);
            this.loader.stopLoader('l-1');
          } else {
            this.loader.stopLoader('l-1');
          }
        });
        return result;
      case ArticleStatus.Draft:
        this.loader.startLoader('l-1');
        this.articleService.markAsDraft(this.articleId, this.articleRequest, this.article.language).then(async data => {
          if (await data !== undefined) {
            result = await data;
            this.snackBar.open(`Article Saved Successfully as ${this.articleRequest.articleStatus}`, 'OK', { duration: 3000 });
            this.reloadComponent(result);
          } else {
            this.loader.stopLoader('l-1');
          }
        });
        return result;
      case ArticleStatus.NeedChanges:
        this.loader.startLoader('l-1');
        result = await this.articleService.markAsNeedChanges(this.articleId, this.articleRequest, this.article.language).then(data => {
          if (data !== undefined) {
            result = data;
            this.snackBar.open(`Article Saved Successfully as ${this.articleRequest.articleStatus}`, 'OK', { duration: 3000 });
            this.reloadComponent(this.articleId);
            this.loader.stopLoader('l-1');
          } else {
            this.loader.stopLoader('l-1');
          }
        });
        return result;
      case ArticleStatus.Published:
        this.modalService.confirmDialogOpen({
          title: 'Publish Article?',
          message: 'Are you sure?',
          cancelText: 'Cancel',
          confirmText: 'Yes'
        });
        this.modalService.confirmDialogConfirmed().subscribe(async confirm => {
          if (confirm === true) {
            this.loader.startLoader('l-1');
            result = await this.articleService.publishArticle(this.articleId, this.articleRequest, this.article.language).then(data => {
              if (data !== undefined) {
                result = data;
                this.snackBar.open(`Article Saved Successfully as ${this.articleRequest.articleStatus}`, 'OK', { duration: 3000 });
                this.reloadComponent(this.articleId);
              } else {
                this.loader.stopLoader('l-1');
              }
            });
          } else {
            return null;
          }
        });
        return result;
      case ArticleStatus.Rejected:
        this.modalService.confirmDialogOpen({
          title: 'Reject Article?',
          message: 'Are you sure?',
          cancelText: 'Cancel',
          confirmText: 'Yes'
        });
        this.modalService.confirmDialogConfirmed().subscribe(async confirm => {
          if (confirm === true) {
            this.loader.startLoader('l-1');
            result = await this.articleService.markAsRejected(this.articleId, this.articleRequest, this.article.language).then(data => {
              if (data !== undefined) {
                result = data;
                this.snackBar.open(`Article Saved Successfully as ${this.articleRequest.articleStatus}`, 'OK', { duration: 3000 });
                this.reloadComponent(this.articleId);
              } else {
                this.loader.stopLoader('l-1');
              }
            });
          }
        });
        return result;
      case ArticleStatus.Scheduled:
        if (this.articleRequest.scheduledDate !== null) {
          this.loader.startLoader('l-1');
          result = await this.articleService.scheduleArticle(this.articleId, this.articleRequest, this.article.language).then(data => {
            if (data !== undefined) {
              result = data;
              this.snackBar.open(`Article Saved Successfully as ${this.articleRequest.articleStatus}`, 'OK', { duration: 3000 });
              this.reloadComponent(this.articleId);
            } else {
              this.loader.stopLoader('l-1');
            }
          });
        } else {
          this.snackBar.open(`You must set SCHEDULED DATE to SCEUDLE an article`, 'OK', { duration: 3000 });
        }
        return result;
      case ArticleStatus.Submitted:
        this.loader.startLoader('l-1');
        this.articleService.markAsSubmited(this.articleId, this.articleRequest, this.article.language).then(async data => {
          if (await data !== undefined) {
            result = await data;
            this.snackBar.open(`Article Saved Successfully as ${this.articleRequest.articleStatus}`, 'OK', { duration: 3000 });
            this.reloadComponent(result);
          } else {
            this.loader.stopLoader('l-1');
          }
        });
        return result;
      case ArticleStatus.Unpublished:
        this.modalService.confirmDialogOpen({
          title: 'Unpublish Article?',
          message: 'Are you sure?',
          cancelText: 'Cancel',
          confirmText: 'Yes'
        });
        this.modalService.confirmDialogConfirmed().subscribe(async confirm => {
          if (confirm === true) {
            this.loader.startLoader('l-1');
            result = await this.articleService.unpublishArticle(this.articleId, this.article.language).then(data => {
              if (data !== undefined) {
                result = data;
                this.snackBar.open(`Article Saved Successfully as ${this.articleRequest.articleStatus}`, 'OK', { duration: 3000 });
                this.reloadComponent(this.articleId);
              } else {
                this.loader.stopLoader('l-1');
              }
            });
          }
        });
        return result;
      default:
        break;
    }
  }

  rollback() {
    this.modalService.confirmDialogOpen({
      title: 'Rollback Article?',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(async data => {
      if (data === true) {
        this.loader.startLoader('l-1');
        await this.articleService.rollback(this.articleId, this.article.language);
        this.getArticle();
        this.loader.stopLoader('l-1');
      }
    });
  }

  setExpirationCategories(event: Array<any>) {
    this.selectedExpirationCategories = [];
    this.selectedExpirationCategories = event.map(c => c.id);
  }

  setArticleCategories(event: Array<any>) {
    this.selectedCategories = [];
    this.selectedCategories = event.map(c => c.id);
  }

  openRelatedArticleModal() {
    if (this.article.language != null && this.selectedCategories.length > 0) {
      this.modalService.relatedArticleOpen({ categories: this.selectedCategories, languageId: this.article.language, articleId: this.articleId, maxNo: this.maxNoRelatedArticles - this.relatedArticles.length });
      this.modalService.relatedArticleConfirmed().subscribe((data: Array<RelatedArticle>) => {
        if (data !== null && data.length > 0) {
          this.relatedArticles = data.map(article => article);
          for (let i = 0; i < this.relatedArticles.length; i++) {
            const a = this.relatedArticles[i];
            a.order = i + 1;
          }
        }
      });
    } else {
      this.snackBar.open('Please select a LANGUAGE and a CATEGORY', 'OK', { duration: 3500 });
    }
  }

  moveItemInArray(relatedArticles: Array<RelatedArticle>, previousIndex, currentIndex) {
    const attachment = relatedArticles[currentIndex];
    relatedArticles[currentIndex] = relatedArticles[previousIndex];
    relatedArticles[previousIndex] = attachment;
  }

  dropRelatedArticle(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.relatedArticles, event.previousIndex, event.currentIndex);
    this.relatedArticles.forEach(ra => {
      ra.order = this.relatedArticles.indexOf(ra) + 1;
    });
  }

  removeRelatedArticle(index) {
    this.relatedArticles.splice(index, 1);
  }

  generateUsernameInitials(username: string): string {
    const initialsArray = username.split(' ');
    let initials = '';
    for (let i = 0; i < initialsArray.length; i++) {
      const name = initialsArray[i];
      if (i < 2) {
        initials += name.slice(0, 1);
      }
    }
    return initials;
  }

  insertComment() {
    let text = (document.getElementById('commentId') as HTMLInputElement).value;
    text = text.trim();
    if (text.length > 0) {
      const comment = new InternalComment();
      comment.text = text;
      comment.commentDate = new Date();
      comment.userFullName = localStorage.getItem('A1_Admin_Username');
      comment.initials = this.generateUsernameInitials(comment.userFullName);
      comment.userId = localStorage.getItem('A1_Admin_UserId');
      if (this.article.comments === null) {
        this.article.comments = new Array<InternalComment>();
      }
      this.article.comments.push(comment);
      (document.getElementById('commentId') as HTMLInputElement).value = '';
      this.commentNgModel = '';
    }
  }

  showInternalExternal() {
    this.showExternal = !this.showExternal;
    this.showInternal = !this.showInternal;
  }

  // sets roles for ckEditor plugin
  setArticleRoles() {
    const articleBodyRoles = new Array<{ value: string, text: string, color: string }>();
    this.selectedRoles.forEach(r => {
      let roleValue = 'role_';
      let roleExtension = '';
      const roleName = this.tinyArticleRoles.find(ar => ar.id === r).name.split('_');
      roleExtension = roleName[roleName.length - 1];
      roleValue = roleValue + roleExtension.toLowerCase();
      articleBodyRoles.push({ value: roleValue, text: this.tinyArticleRoles.find(ar => ar.id === r).name, color: this.tinyArticleRoles.find(ar => ar.id === r).color });
    });
    localStorage.setItem('A1_Admin_articleBodyRoles', JSON.stringify(articleBodyRoles));
  }

  // find all roles in article body
  findBodyRoles() {
    this.articleRoles = this.tinyArticleRoles;
    if (this.article.htmlBodyInternal !== null) {
      localStorage.setItem('A1_Admin_bodyRoles', JSON.stringify(this.articleBodyRoles));
      if (this.articleRoles !== null) {
        this.articleRoles.forEach(r => {
          const roleExtension = r.name.split('_');
          this.roleTags.push('role_' + roleExtension[roleExtension.length - 1].toLowerCase());
        });
      }
      const htmlBody = this.article.htmlBodyInternal;
      const parent = document.createElement('div');
      parent.innerHTML = htmlBody;
      const roleElements = parent.querySelectorAll('[data-role]');
      if (roleElements.length > 0) {
        for (let i = 0; i < roleElements.length; i++) {
          const ele = roleElements[i];
          ele.getAttribute('data-role');
          this.articleBodyRoles.push({ role: ele.getAttribute('data-role'), id: i, content: ele.innerHTML });
        }
      }
      localStorage.setItem('A1_Admin_bodyRoles', JSON.stringify(this.articleBodyRoles));
    }
  }

  // sets custom tags for tiny editor
  setTinyCustomElements() {
    this.customElements = '';
    this.tinyArticleRoles.forEach(r => {
      let roleExtension = '';
      const roleName = r.name.split('_');
      roleExtension = roleName[roleName.length - 1];
      roleExtension = `role_${roleExtension},`;
      this.customElements = this.customElements + roleExtension.toLowerCase();
    });
    this.customElements = this.customElements.slice(0, this.customElements.length - 1);
    this.customElements = this.customElements + ',snippet';
  }

  editArticle() {
    this.lockSubscription = this.articleService.lockArticleObservable(this.articleId).subscribe(data => {
      if (data) {
        this.router.navigate([`articles/edit-article/${this.articleId}/${this.articleLanguageId}`]);
      }
    });
  }

  reloadComponent(articleId) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate([`articles/edit-article/${articleId}/${this.articleLanguageId}`], { skipLocationChange: false });
  }

  async getArticleRevisionContent() {
    this.revisions = await this.articleService.getArticleRevisions(this.articleId, this.article.language);
  }

  openRevisionModal() {
    const data = {
      articleId: this.articleId,
      languageId: this.article.language,
      revisions: this.revisions
    };
    this.modalService.rollbackModalOpen(data);
    this.modalService.rollbackModalConfirmed().subscribe((article: ArticleRevision) => {
      if (article !== null) {
        localStorage.setItem('A1_Admin_oldArticle', JSON.stringify(this.article));
        this.showUndoBtn = true;
        this.article.htmlBodyExternal = article.htmlBodyExternal;
        this.article.htmlBodyInternal = article.htmlBodyInternal;
        this.article.excerpt = article.excerpt;
        this.article.subject = article.subject;
        this.article.featuredImage = article.featuredImage;
        this.article.description = article.description;
        this.article.snippets = article.snippets;
        this.article.templates = article.templates;
        this.seoArray = article.seo;
        if (this.seoArray !== null) {
          this.article.seo = this.seoArray.join();
        } else {
          this.seoArray = [];
          this.article.seo = null;
        }
        this.article.keywords = article.keywords;
        this.keywordsArray = this.article.keywords;
        if (this.keywordsArray === null) {
          this.keywordsArray = [];
        }
      }
    });
  }

  revertChanges() {
    this.article = JSON.parse(localStorage.getItem('A1_Admin_oldArticle'));
    localStorage.removeItem('A1_Admin_oldArticle');
    if (this.article.seo !== null) {
      this.seoArray = this.article.seo.split(',');
    } else {
      this.seoArray = [];
    }
    this.keywordsArray = this.article.keywords;
    if (this.keywordsArray === null) {
      this.keywordsArray = [];
    }
    this.showUndoBtn = false;
  }

  addRelatedQuestion() {
    if (this.relatedQuestion.length > 0 && this.relatedQuestion !== null) {
      this.article.relatedQuestions.push(this.relatedQuestion);
      this.relatedQuestion = null;
    }
  }

  removeRelatedQuestion(i) {
    this.article.relatedQuestions.splice(i, 1);
  }

  setDeviceData() {
    const iphoneXR = new Devices();
    iphoneXR.deviceName = 'iPhone XR, XS Max';
    iphoneXR.deviceDetails.height = '896px';
    iphoneXR.deviceDetails.width = '414px';
    const iphoneXS = new Devices();
    iphoneXS.deviceName = 'iPhone XS, X';
    iphoneXS.deviceDetails.width = '375px';
    iphoneXS.deviceDetails.height = '812px';
    const samsung = new Devices();
    samsung.deviceName = 'Galaxy S9 Plus, S8 Plus';
    samsung.deviceDetails.width = '412px';
    samsung.deviceDetails.height = '846px';
    const mediumScreen = new Devices();
    mediumScreen.deviceName = 'Medium Screen';
    mediumScreen.deviceDetails.width = '1024px';
    mediumScreen.deviceDetails.height = '812px';
    this.previewDevices.push(iphoneXR, iphoneXS, samsung, mediumScreen);
  }

  openDevicePreview() {
    let articleBody;
    if (this.showExternal === true) {
      articleBody = this.article.htmlBodyInternal;
    } else {
      articleBody = this.article.htmlBodyExternal;
    }
    this.modalService.devicePreviewModalOpen({ articleBody: articleBody }, this.selectedDevice.deviceDetails.width, this.selectedDevice.deviceDetails.height);
  }

}
