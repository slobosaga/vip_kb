import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { MatIcon } from '@angular/material/icon';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/operators';
import { TreeNode } from 'src/app/base/models/treeNode';
import { ArticlePaging, ArticleView, ArticleViewResponse } from 'src/app/models/article';
import { Category } from 'src/app/models/category';
import { Role } from 'src/app/models/enums/rolesEnum';
import { WebPortal } from 'src/app/models/webPortal';
import { ArticleService } from 'src/app/services/article.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { MediaService } from 'src/app/services/media.service';
import { ModalService } from 'src/app/services/modal.service';
import { MenuItem } from 'primeng/api';
import { FormBuilder, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChildren('button') matIcons: QueryList<MatIcon>;
  @ViewChild('orderForm') orderForm: NgForm;

  articlesView = new ArticleViewResponse();
  articles = new Array<ArticleView>();
  title: string;
  onlyMine: string;
  dataSource: MatTableDataSource<ArticleView>;
  observable: Observable<any>;
  categories: Array<Category>;
  message: string;
  categoryIdsession: string;
  nodes = new Array<MenuItem>();
  parentId: number;
  paging = new ArticlePaging();
  webPortals = new Array<WebPortal>();
  language = localStorage.getItem('A1_Admin_language');
  selectedStatusIndex = null;
  pageIndex = 0;
  tableLength = 0;
  languageSubscriber: Subscription;
  articleCountPerStatus = new Array<{ id: number, name: string, conut: 0 }>();
  displayedColumns = ['subject', 'description', 'articleStatus', 'createdByFullName', 'createdDate', 'updatedByFullName', 'updatedDate', 'hasAttachment', 'action'];
  prevButtonTrigger;
  isMatMenuOpen: boolean;
  isMatMenu2Open: boolean;
  enteredButton: boolean;
  admin = [Role.SystemAdmin, Role.Admin];
  supervisor = [Role.SystemAdmin, Role.Admin, Role.Supervisor];
  writer = [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer];
  reader = [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer, Role.Reader];
  timedOutCloser: any;
  enableSorting = false;

  constructor(private articleService: ArticleService, private ren: Renderer2, private modalService: ModalService, private eventEmitter: EventEmitterService, private snackBar: MatSnackBar, private router: Router, private route: ActivatedRoute, private mediaService: MediaService, private loader: NgxUiLoaderService, private fb: FormBuilder) {
    this.title = this.route.snapshot.data.title;
    this.paging.fromArticle = 0;
    this.paging.numArticles = 60;
    this.categoryIdsession = localStorage.getItem('A1_Admin_categoryId');
    this.categories = new Array<Category>();
    this.paging.columnName = 'CreatedDate';
    this.paging.orderType = 'desc';
    // this.orderForm.form = fb.group({
    //   articleOrder: ['', Validators.required],
    // }, { validator: this.areEqual });
  }

  ngOnInit(): void {
    const defaultWebPortal = JSON.parse(localStorage.getItem('A1_Admin_defaultWebPortal'));
    if (defaultWebPortal !== null) {
      this.paging.webPortalId = defaultWebPortal;
    }
    this.message = 'Please select a category.';
    this.getData();
    this.languageSubscriber = this.eventEmitter.emitLanguageId.subscribe(() => {
      this.getData();
      this.language = localStorage.getItem('A1_Admin_language');
    });
  }

  ngAfterViewInit(): void {
    this.paginator.page.pipe(tap(event => {
      this.paging.fromArticle = (event.pageIndex * event.pageSize);
      this.paging.numArticles = event.pageSize;
      this.getAllArticles();
    })).subscribe();
    this.sortTableEvent();
  }

  private sortTableEvent() {
    if (this.sort) {
      this.sort.sortChange.pipe(tap(event => {
        this.paginator.pageIndex = 0;
        this.paging.fromArticle = 0;
        if (event.active !== null) {
          this.paging.columnName = event.active[0].toUpperCase() + event.active.slice(1, event.active.length);
          if (event.direction === 'asc') {
            this.paging.orderType = 'asc';
          } else if (event.direction === 'desc') {
            this.paging.orderType = 'desc';
          } else if (event.direction === '') {
            this.paging.columnName = null;
          }
          this.getData();
        }
      })).subscribe();
    }
  }

  ngOnDestroy(): void {
    this.languageSubscriber.unsubscribe();
  }

  async getData() {
    this.loader.startLoader('l-1');
    await this.getCategories();
    await this.getAllArticles();
    await this.getAllWebPortals();
    await this.getArticleCountPerStatus();
    setTimeout(() => {
      if (this.selectedStatusIndex != null) {
        const span = document.getElementById('articleStatusSpan' + this.selectedStatusIndex);
        span.style.fontWeight = 'bold';
        span.style.textDecoration = 'underline';
        span.style.backgroundColor = 'darkgray';
      }
    }, 100);
    this.loader.stopLoader('l-1');
  }


  async getAllArticles() {
    this.loader.startLoader('article');
    await this.articleService.getAllArticles(this.paging).then(async data => {
      this.articlesView = await data;
      this.tableLength = this.articlesView.numOfArticles;
    }).catch(err => { });
    if (this.articlesView.articles != null) {
      this.articlesView.articles.forEach(a => {
        a.articleStatus = a.articleStatus.charAt(0).toUpperCase() + a.articleStatus.slice(1);
        if (a.lockedBy !== null) {
          a.lockedDetails = a.lockedBy + ' @ ' + new DatePipe('en-US').transform(a.lockedDate, 'dd-MM-yyyy HH:mm');
        }
      });
    } else {
      this.snackBar.open('There are no articles for this criteria', 'OK', { duration: 3500 });
    }
    this.articles = this.articlesView.articles;
    this.dataSource = new MatTableDataSource<ArticleView>(this.articles);
    this.observable = this.dataSource.connect();
    this.loader.stopAllLoader('article');
  }

  async getAllWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
  }

  async getArticleCountPerStatus() {
    this.articleCountPerStatus = await this.articleService.getArticleCountPerStatus(localStorage.getItem('A1_Admin_language'), this.paging.categoryId, this.paging.webPortalId);
    this.articleCountPerStatus.forEach(a => {
      a.name = a.name.charAt(0).toUpperCase() + a.name.slice(1);
      a.name = a.name.replace('_', ' ');
    });
    setTimeout(() => {
      if (this.paging.articleStatusId !== null) {
        const span = document.getElementById('articleStatusSpan' + this.paging.articleStatusId);
        if (span !== null) {
          span.style.fontWeight = 'bold';
          span.style.textDecoration = 'underline';
          span.style.backgroundColor = 'darkgray';
        }
      }
    }, 0);
  }

  getArticlesByStatusBtn(id) {
    if (this.paging.articleStatusId === 7) {
      if (this.enableSorting === true) {
        this.modalService.confirmDialogOpen({
          title: 'Cancel Sorting?',
          message: 'Are you sure?',
          cancelText: 'Cancel',
          confirmText: 'Yes'
        });
        this.modalService.confirmDialogConfirmed().subscribe(data => {
          if (data === true) {
            this.enableSorting = false;
            this.getArticlesByStatusId(id);
          }
        });
      } else {
        this.getArticlesByStatusId(id);
      }
    } else {
      this.getArticlesByStatusId(id);
    }
  }

  private getArticlesByStatusId(id: number) {
    if (this.paging.articleStatusId === id) {
      this.paging.articleStatusId = null;
    } else {
      this.paging.articleStatusId = id;
    }
    this.getAllArticles();
  }

  getOnlyMineArticles() {
    this.paging.onlyMine = !this.paging.onlyMine;
    this.getAllArticles();
  }

  onArticleStatusBtnFocus(i) {
    if (this.selectedStatusIndex === null) {
      this.selectedStatusIndex = i;
      const span = document.getElementById('articleStatusSpan' + i);
      if (span !== null) {
        span.style.fontWeight = 'bold';
        span.style.textDecoration = 'underline';
        span.style.backgroundColor = 'darkgray';
      }
    } else if (this.selectedStatusIndex === i) {
      this.selectedStatusIndex = null;
      const span = document.getElementById('articleStatusSpan' + i);
      if (span !== null) {
        span.style.fontWeight = '';
        span.style.textDecoration = 'none';
        span.style.backgroundColor = 'white';
      }
    } else if (this.selectedStatusIndex !== i) {
      const span = document.getElementById('articleStatusSpan' + i);
      if (span !== null) {
        span.style.fontWeight = 'bold';
        span.style.textDecoration = 'underline';
        span.style.backgroundColor = 'darkgray';
      }
      const spanOld = document.getElementById('articleStatusSpan' + this.selectedStatusIndex);
      if (spanOld !== null) {
        spanOld.style.fontWeight = '';
        spanOld.style.textDecoration = 'none';
        spanOld.style.backgroundColor = 'white';
        this.selectedStatusIndex = i;
      }
    }
  }

  lockArticle(id) {
    this.articleService.lockArticle(id);
  }

  unlockArticle(id) {
    this.loader.startLoader('l-1');
    this.articleService.unlockArticle(id, true).then(data => {
      if (data) {
        this.getAllArticles();
        this.snackBar.open('Article Unlocked Successfully', 'OK', { duration: 3500 });
        this.loader.stopLoader('l-1');
      }
    }).catch(err => {
      this.loader.stopLoader('l-1');
    });
  }

  viewArticle(uuId: string) {
    this.router.navigate(['/articles/view-article/' + uuId + '/' + this.language]);
  }

  editArticle(uuId: string) {
    this.lockArticle(uuId);
    this.router.navigate(['/articles/edit-article/' + uuId + '/' + this.language]);
  }

  createArticleForAnotherLanguage(article) {
    this.modalService.articleNewLanguageOpen({
      articleId: article,
      languageId: null
    });
  }

  deleteArticle(uuId: string) {
    const language = JSON.parse(localStorage.getItem('A1_Admin_language'));
    this.modalService.confirmDialogOpen({
      title: 'Delete Article?',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(async data => {
      if (data === true) {
        await this.articleService.deleteArticle(uuId, language);
        this.getData();
      }
    });
  }

  async getCategories() {
    this.categories = new Array<Category>();
    if (this.paging.webPortalId === null) {
      this.categories = await this.articleService.getAllCategories();
    } else {
      this.categories = await this.articleService.getAllCategoriesForWebPortal([this.paging.webPortalId]);
    }
    this.categoriesTreeView();
    this.removeEmptyItems();
  }

  categoriesTreeView() {
    this.nodes = new Array<MenuItem>();
    this.categories.forEach(c => {
      const node: MenuItem = { id: c.id.toString(), label: c.name, items: [], styleClass: c.parentId?.toString(), title: c.name, command: (event) => { this.selectedCategoryEvent(c.id); } };
      if (c.parentId === null) {
        this.nodes.push(node);
      }
    });
    this.categories.forEach(c => {
      const node: MenuItem = { id: c.id.toString(), label: c.name, items: [], styleClass: c.parentId?.toString(), title: c.name, command: (event) => { this.selectedCategoryEvent(c.id); } };
      if (c.parentId > 0) {
        this.fillTree(node, null);
      }
    });
  }

  fillTree(node: MenuItem, nodes: Array<MenuItem> = null) {
    if (nodes === null) {
      nodes = this.nodes;
    }
    nodes.forEach(n => {
      if (n.id === node.styleClass) {
        n.items.push(node);
      } else if (n.id === node.styleClass) {
        n.items.push(node);
      } else if (n.items !== null) {
        this.fillTree(node, n.items);
      }
    });
  }

  removeEmptyItems(nodes: MenuItem[] = null) {
    if (nodes === null) {
      nodes = this.nodes;
    }
    nodes.forEach(n => {
      if (n.items.length === 0) {
        n.items = null;
      } else {
        this.removeEmptyItems(n.items);
      }
    });
  }

  selectedCategoryEvent(id) {
    this.paging.categoryId = id;
    this.paging.columnName = null;
    this.sort.sort({ id: null, start: null, disableClear: false });
    this.sort.active = null;
    this.sort.direction = null;
    this.getAllArticles();
    this.getArticleCountPerStatus();
  }

  selectCategory(event) {
    this.paging.categoryId = event;
    this.getAllArticles();
  }

  pageChange(event) {
    this.paging.fromArticle = (event.pageIndex * event.pageSize);
    this.paging.numArticles = event.pageSize;
    this.getAllArticles();
  }

  searchArticles() {
    this.paging.search = this.paging.search.trim();
    this.getAllArticles();
  }

  selectedCrumb(event) {
    this.paging.categoryId = event;
    this.getAllArticles();
  }

  async removeBreadcrumbs() {
    if (this.enableSorting === true) {
      this.modalService.confirmDialogOpen({
        title: 'Cancel Sorting?',
        message: 'Are you sure?',
        cancelText: 'Cancel',
        confirmText: 'Yes'
      });
      this.modalService.confirmDialogConfirmed().subscribe(data => {
        if (data === true) {
          this.enableSorting = !this.enableSorting;
          this.articlesView.breadcrumbs = null;
          this.paging.categoryId = null;
          this.getAllArticles();
          this.getArticleCountPerStatus();
        }
      });
    } else {
      this.articlesView.breadcrumbs = null;
      this.paging.categoryId = null;
      this.getAllArticles();
      this.getArticleCountPerStatus();
    }
  }

  async enableSortingBtn() {
    if (this.enableSorting === true) {
      this.modalService.confirmDialogOpen({
        title: 'Cancel Sorting?',
        message: 'Are you sure?',
        cancelText: 'Cancel',
        confirmText: 'Yes'
      });
      this.modalService.confirmDialogConfirmed().subscribe(data => {
        if (data === true) {
          this.enableSorting = !this.enableSorting;
          this.paging.numArticles = 60;
          this.sortTableEvent();
        }
        return this.enableSorting;
      });
    } else {
      this.paging.fromArticle = 0;
      this.paging.numArticles = this.articlesView.numOfArticles;
      await this.getAllArticles();
      this.enableSorting = !this.enableSorting;
      return this.enableSorting;
    }
  }

  findParentNode(nodes: Array<TreeNode>, id: number) {
    nodes.forEach(node => {
      if (node.id === id) {
        this.parentId = node.parentId;
      } else if (node.hasChildren === true) {
        this.findParentNode(node.children, id);
      } else if (node.hasChildren === false) {
        this.parentId = id;
      }
    });
  }

  saveOrder() {
    this.eventEmitter.emitSaveArticleOrder.emit(true);
  }

  saveOrderBtn(event) {
    if (event === true) {
      this.enableSorting = false;
      setTimeout(() => {
        this.paging.numArticles = 60;
        this.getAllArticles();
      }, 0);
    }
  }

  removeActiveClass(event: MouseEvent) {
    const element = document.getElementById('article-categories');
    element.click();
  }

  // areEqual(group: any) {
  //   var valid = false;

  //   for (group.controls['articleOrder'] in group.controls) {
  //     var val = group.controls['articleOrder'].value

  //     (...)
  //   }

  //   if (valid) {
  //     return null;
  //   }

  //   return {
  //     areEqual: true
  //   };
  // }

}
