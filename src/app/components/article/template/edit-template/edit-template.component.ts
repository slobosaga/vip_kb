import { Location } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AppConfigService } from 'src/app/base/configuration.service';
import { CkeditorConfig } from 'src/app/models/enums/ckeditor';
import { Template } from 'src/app/models/template';
import { ArticleService } from 'src/app/services/article.service';
import { environment } from './../../../../../environments/environment';

@Component({
  selector: 'app-edit-template',
  templateUrl: './edit-template.component.html',
  styleUrls: ['./edit-template.component.scss']
})
export class EditTemplateComponent implements OnInit, OnDestroy, AfterViewInit {

  templateId = null;
  readonly = false;
  template = new Template();
  ckEditorUrl: string = null;
  ckconfig = new CkeditorConfig();

  constructor(private articleService: ArticleService, private appConfigService: AppConfigService, private route: ActivatedRoute, private location: Location, private loader: NgxUiLoaderService, private snackBar: MatSnackBar) {
    if (this.route.snapshot.data?.readonly) {
      this.readonly = this.route.snapshot.data?.readonly;
    }
    this.templateId = this.route.snapshot.paramMap.get('id');
    if (environment.production === true) {
      this.ckEditorUrl = appConfigService.ckEditorUrlProd;
    } else {
      this.ckEditorUrl = appConfigService.ckEditorUrlDev;
    }
    this.ckconfig.extraPlugins = 'font,image2,templates,snippets,html5video,widgetselection,lineutils,uploadimage,uploadwidget,filebrowser,accordionList,collapsibleItem,iframedialog,iframe,mailto,createSnippet,createTemplate,dialog,tableresize';
    this.ckconfig.width = '100%';
  }

  ngOnInit(): void {
    this.loader.startLoader('l-1');
  }

  ngAfterViewInit(): void {
    if (this.templateId != null) {
      setTimeout(() => {
        this.getTemplate();
        this.loader.stopLoader('l-1');
      }, 1500);
    }
    this.loader.stopLoader('l-1');
  }

  ngOnDestroy(): void {
  }

  async getTemplate() {
    this.template = await this.articleService.getTemplate(this.templateId);
  }

  async updateTemplate() {
    if (this.template.text) {
      const result = await this.articleService.updateTemplate(this.template.id, { name: this.template.name, text: this.template.text });
      if (result) {
        this.snackBar.open('Template Updated Successfully', 'OK', { duration: 3500 });
        this.location.back();
      } else {
        this.snackBar.open('Template Editor cannot be empty', 'OK', { duration: 3500 });
      }
    }
  }

  async createTemplate() {
    if (this.template.text) {
      const result = await this.articleService.createTemplate({ name: this.template.name, text: this.template.text });
      if (result) {
        this.snackBar.open('Template Created Successfully', 'OK', { duration: 3500 });
        this.location.back();
      }
    } else {
      this.snackBar.open('Template Editor cannot be empty', 'OK', { duration: 3500 });
    }
  }

  saveTemplate() {
    if (this.templateId != null) {
      this.updateTemplate();
    } else {
      this.createTemplate();
    }
  }

  async ckFileUpload(event) {
    const fileLoader = event.data.fileLoader;
    const formData = new FormData();
    const xhr = fileLoader.xhr;
    xhr.open('POST', fileLoader.uploadUrl, true);
    xhr.setRequestHeader('Content-Disposition', 'miltipart/form-data');
    xhr.setRequestHeader('X-File-Name', fileLoader.fileName);
    xhr.setRequestHeader('X-File-Size', fileLoader.total);
    xhr.setRequestHeader('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken'));
    formData.append('file', fileLoader.file, fileLoader.fileName);
    xhr.send(formData);
    event.stop();
  }

  ckFileResponse(event) {
    event.stop();
    const data = event.data;
    const xhr = data.fileLoader.xhr;
    let response = xhr.responseText;
    response = JSON.parse(response);
    data.url = location.origin + response.url;
  }

}
