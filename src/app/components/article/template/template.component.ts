import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable } from 'rxjs';
import { Template } from 'src/app/models/template';
import { ArticleService } from 'src/app/services/article.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  from = 0;
  numOfRows = 10;
  filter = '';
  pageIndex = 0;
  tableLength = 0;
  onlyMine = false;
  templates = new Array<Template>();
  dataSource: MatTableDataSource<Template>;
  observable: Observable<any>;
  displayedColumns = ['name', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'action'];

  constructor(private articleService: ArticleService, private modalService: ModalService, private loader: NgxUiLoaderService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getAllTemplates();
  }

  async getAllTemplates() {
    this.loader.startLoader('template');
    const data = await this.articleService.getAllTemplates(this.from, this.numOfRows, this.filter, this.onlyMine);
    this.templates = data.templates;
    this.tableLength = data.total;
    this.dataSource = new MatTableDataSource<Template>(this.templates);
    this.observable = this.dataSource.connect();
    this.loader.stopLoader('template');
  }

  pageChange(event) {
    this.from = (event.pageIndex * event.pageSize);
    this.numOfRows = event.pageSize;
    this.getAllTemplates();
  }

  searchTempaltes() {
    this.getAllTemplates();
    this.pageIndex = 0;
  }

  editTemplate(id) {
    this.router.navigate(['/articles/templates/edit-template/' + id]);
  }

  viewTemplate(id) {
    this.router.navigate(['/articles/templates/view-template/' + id]);
  }

  createTemplate() {
    this.router.navigate(['/articles/templates/create-template/']);
  }

  deleteTemplate(id) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Template',
      message: 'Are you sure?',
      cancelText: 'No',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(async data => {
      if (data === true) {
        const result = await this.articleService.deleteTemplate(id);
        if (result) {
          this.snackBar.open('Template deleted successfully', 'OK', { duration: 3500 });
          this.getAllTemplates();
        }
      }
    });
  }

}
