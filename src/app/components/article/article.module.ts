import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditArticleComponent } from './article/edit-article/edit-article.component';
import { ArticleRoutingModule } from './article-routing.module';
import { AngularMaterialModule } from 'src/app/modules/angular-material/angular-material.module';
import { FormsModule } from '@angular/forms';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatTreeModule } from '@angular/material/tree';
import { TreeViewComponent } from '../tree-view/tree-view.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RelevanceTuningComponent } from '../relevance-tuning/relevance-tuning.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { NestedTreeViewComponent } from '../nested-tree-view/nested-tree-view.component';
import { MatTabsModule } from '@angular/material/tabs';
import { PipeSharedModule } from 'src/app/modules/pipe-shared/pipe-shared.module';
import { ArticleComponent } from './article/article.component';
import { NgxUiLoaderConfig, NgxUiLoaderModule } from 'ngx-ui-loader';
import { CategoriesComponent } from './categories/categories.component';
import { CommentsComponent } from './comments/comments.component';
import { SnippetComponent } from './snippet/snippet.component';
import { TemplateComponent } from './template/template.component';
import { FindAndReplaceComponent } from './find-and-replace/find-and-replace.component';
import { EditSnippetComponent } from './snippet/edit-snippet/edit-snippet.component';
import { EditTemplateComponent } from './template/edit-template/edit-template.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { DialogModule } from 'primeng/dialog';
import { CKEditorModule } from 'ckeditor4-angular';
import { ArticleSortComponent } from './article/article-sort/article-sort.component';


const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: '#f77474',
  fgsColor: '#f77474',
  hasProgressBar: false
};


@NgModule({
  declarations: [
    EditArticleComponent,
    TreeViewComponent,
    RelevanceTuningComponent,
    NestedTreeViewComponent,
    ArticleComponent,
    CategoriesComponent,
    CommentsComponent,
    SnippetComponent,
    TemplateComponent,
    FindAndReplaceComponent,
    EditSnippetComponent,
    EditTemplateComponent,
    ArticleSortComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    ArticleRoutingModule,
    AngularMaterialModule,
    FormsModule,
    MaterialFileInputModule,
    MatTreeModule,
    EditorModule,
    MatTabsModule,
    DragDropModule,
    PipeSharedModule,
    TieredMenuModule,
    DialogModule,
    CKEditorModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
  ],
  exports: [NestedTreeViewComponent],
  providers: []
})
export class ArticleModule { }
