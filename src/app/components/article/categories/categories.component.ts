import { SelectionModel } from '@angular/cdk/collections';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subscription } from 'rxjs';
import { TreeNode } from 'src/app/base/models/treeNode';
import { Category, CategorySortRequest } from 'src/app/models/category';
import { Language } from 'src/app/models/enums/language';
import { WebPortal } from 'src/app/models/webPortal';
import { ArticleService } from 'src/app/services/article.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories = new Array<Category>();
  treeCategories = new Array<Category>();
  nodes = new Array<TreeNode>();
  checklistSelection = new SelectionModel<TreeNode>(true);
  nestedTreeControl: NestedTreeControl<TreeNode>;
  nestedDataSource: MatTreeNestedDataSource<TreeNode>;
  sort: any;
  categoriesSortRequest: any;
  sortedNodes: any;
  categoriesSubscriber: Subscription;
  selectedNodes = new Array<{ id: number, name: string }>();
  webPortals = new Array<WebPortal>();
  webPortalId: string = null;
  languages = new Array<Language>();
  filter: string = '';

  constructor(private articleService: ArticleService, private modalService: ModalService, private eventEmitter: EventEmitterService, private loader: NgxUiLoaderService) {
    this.nestedTreeControl = new NestedTreeControl<TreeNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource<TreeNode>();
  }

  private _getChildren = (node: TreeNode): TreeNode[] => node.children;
  hasNestedChild = (_: number, nodeData: TreeNode) => nodeData.hasChildren === true;


  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    await this.getWebPortals();
    await this.getAllLanguages();
    await this.getCategories();
  }

  async getCategories(filter = '') {
    this.loader.startLoader('l-1');
    this.categories = new Array<Category>();
    if (this.webPortalId === null) {
      this.categories = await this.articleService.getAllCategories(filter);
      this.treeCategories = await this.articleService.getAllCategories(filter);
    } else {
      this.categories = await this.articleService.getAllCategoriesForWebPortal([this.webPortalId], filter);
      this.treeCategories = await this.articleService.getAllCategoriesForWebPortal([this.webPortalId], filter);
    }
    this.categories.forEach(c => {
      this.languages.forEach(l => {
        if (c.translations === null) {
          c.translations.push({ language: l.name, languageId: l.id, name: null });
        } else {
          if (!c.translations.find(t => t.languageId === l.id)) {
            c.translations.push({ language: l.name, languageId: l.id, name: null });
          }
        }
      });
    });
    this.categoriesTreeView();
    this.nestedTreeControl = new NestedTreeControl<TreeNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource<TreeNode>();
    this.nestedDataSource.data = this.nodes;
    this.nestedTreeControl.dataNodes = this.nodes;
    // expand all parents if filtered
    if (filter !== '') {
      this.nestedTreeControl.expandAll();
    }
    this.eventEmitter.emmitTreeNodes.emit(this.nodes);
    this.loader.stopLoader('l-1');
  }

  filterCategoies(event) {
    this.getCategories(this.filter);
  }

  async getWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
  }

  async getAllLanguages() {
    this.languages = await this.articleService.getAllLanguages('false');
  }

  categoriesTreeView() {
    this.nodes = new Array<TreeNode>();
    for (let i = 0; i < this.treeCategories.length; i++) {
      const c = this.treeCategories[i];
      const node: TreeNode = { id: c.id, name: c.name, parentId: c.parentId, hasChildren: false, children: new Array<TreeNode>() };
      if (c.parentId === null) {
        this.nodes.push(node);
      }
    }
    for (let i = 0; i < this.treeCategories.length; i++) {
      const c = this.treeCategories[i];
      const node: TreeNode = { id: c.id, name: c.name, parentId: c.parentId, hasChildren: false, children: new Array<TreeNode>() };
      if (c.parentId > 0) {
        this.fillTree(node, null);
      }
    }
  }

  fillTree(node: TreeNode, nodes = null) {
    if (nodes === null) {
      nodes = this.nodes;
    }
    nodes.forEach(n => {
      if (n.hasChildren === true && n.id === node.parentId) {
        n.children.push(node);
      } else if (n.id === node.parentId) {
        n.hasChildren = true;
        n.children.push(node);
      } else if (n.hasChildren === true) {
        this.fillTree(node, n.children);
      }
    });
  }

  rebuildTree(nodes: Array<TreeNode>) {
    this.setNodeParentId(nodes);
    this.setNodeChildren(nodes);
    this.nestedDataSource.data = nodes;
  }

  setNodeParentId(nodes: Array<TreeNode>) {
    nodes.forEach(n => {
      n.parentId = null;
    });
  }

  setNodeChildren(nodes: Array<TreeNode>) {
    nodes.forEach(n => {
      if (n.children.length > 0) {
        n.hasChildren = true;
        n.children.forEach(c => {
          c.parentId = n.id;
        });
        this.setNodeChildren(n.children);
      } else {
        n.hasChildren = false;
      }
    });
  }

  sortCategories(nodes: Array<TreeNode> = null) {
    if (nodes === null) {
      nodes = this.nestedDataSource.data;
    }
    nodes.forEach(n => {
      this.sort++;
      this.categoriesSortRequest.push({ id: n.id, name: n.name, parentId: n.parentId, sort: this.sort });
      if (n.children.length > 0) {
        this.sortCategories(n.children);
      }
    });
  }

  sortCategoriesButton() {
    const options = {
      title: 'Are you sure?',
      message: 'Sort Categories',
      cancelText: 'No',
      confirmText: 'Yes'
    };
    this.modalService.confirmDialogOpen(options);
    this.modalService.confirmDialogConfirmed().subscribe(confirm => {
      if (confirm === true) {
        this.sort = 0;
        this.categoriesSortRequest = new Array<CategorySortRequest>();
        this.sortCategories();
        this.categoriesSortRequest.sort((a: CategorySortRequest, b: CategorySortRequest) => a.order - b.order);
        if (this.categoriesSortRequest.length > 0) {
          this.articleService.sortCategories(this.categoriesSortRequest);
        }
      }
    });
  }

  addNodeButton(node: TreeNode) {
    const category = new Category();
    this.languages.forEach(l => {
      category.translations.push({ language: l.name, languageId: l.id, name: null });
    });
    const options = {
      title: 'Add New Category',
      message: 'Enter Category Details',
      cancelText: 'Cancel',
      confirmText: 'Add',
      category: category
    };
    this.modalService.categoryDialogOpen(options);
    this.modalService.categoryDialogConfirmed().subscribe(data => {
      if (data) {
        this.addNode(node, data);
      }
    });
  }

  addNode(node: TreeNode, category: Category) {
    let newNode: TreeNode;
    if (node === null) {
      newNode = { id: null, name: category.name, parentId: null, hasChildren: false, children: new Array<TreeNode>() };
    } else {
      newNode = { id: null, name: category.name, parentId: node.id, hasChildren: false, children: new Array<TreeNode>() };
    }
    if (this.webPortalId !== null) {
      this.articleService.createCategoryWebPortal({ name: category.name, parentId: node === null ? null : node.id, order: this.returnLastCategory(), translations: category.translations }, this.webPortalId).then(async data => {
        newNode.id = await data;
        if (node != null) {
          node.children.push(newNode);
        } else {
          this.nodes.push(newNode);
        }
        await this.getCategories();
      });
    } else {
      this.articleService.createCategory({ name: category.name, parentId: node === null ? null : node.id, order: this.returnLastCategory(), translations: category.translations }).then(async data => {
        newNode.id = await data;
        if (node != null) {
          node.children.push(newNode);
        } else {
          this.nodes.push(newNode);
        }
        await this.getCategories();
      });
    }
  }

  returnLastCategory(id = null) {
    if (id > 0) {
      const sort = this.categories.find(c => c.id === id).sort;
      return sort;
    } else {
      let sort = 1;
      if (this.categories.length > 0) {
        const order = this.categories.map(c => c.sort);
        sort = Math.max(...order) + 1;
      }
      return sort;
    }
  }

  editNodeButton(node: TreeNode) {
    const options = {
      title: 'Edit Category',
      message: 'Enter Category Name',
      cancelText: 'Cancel',
      confirmText: 'Confirm',
      category: this.categories.find(c => c.id === node.id)
    };
    this.modalService.categoryDialogOpen(options);
    this.modalService.categoryDialogConfirmed().subscribe(async (data: Category) => {
      if (data) {
        node.name = data.name;
        this.articleService.updateCategoty({ name: node.name, parentId: node.parentId, order: this.returnLastCategory(node.id), translations: data.translations }, node.id);
        setTimeout(async () => {
          await this.getCategories();
        }, 100);
      }
    });
  }

  deleteNodeButton(node: TreeNode) {
    this.deleteNodeDialog(node);
  }

  deleteNodeDialog(node: TreeNode) {
    const options = {
      title: 'Are you sure?',
      message: 'Delete Cateogry?',
      cancelText: 'No',
      confirmText: 'Yes'
    };
    this.modalService.confirmDialogOpen(options);
    this.modalService.confirmDialogConfirmed().subscribe(async confirmed => {
      if (confirmed) {
        this.articleService.deleteCategory(node.id);
        this.deleteNode(node);
        setTimeout(async () => {
          await this.getCategories();
        }, 100);
      }
    });
  }

  deleteNode(node: TreeNode, nodes: Array<TreeNode> = null) {
    if (nodes === null) {
      nodes = this.nodes;
    }
    nodes.forEach(n => {
      if (n.id === node.id) {
        nodes.splice(nodes.indexOf(node), 1);
      } else {
        this.deleteNode(node, n.children);
      }
    });
  }

}

