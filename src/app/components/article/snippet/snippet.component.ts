import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable } from 'rxjs';
import { Role } from 'src/app/models/enums/rolesEnum';
import { Snippet } from 'src/app/models/snippet';
import { ArticleService } from 'src/app/services/article.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-snippet',
  templateUrl: './snippet.component.html',
  styleUrls: ['./snippet.component.scss']
})
export class SnippetComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  from = 0;
  numOfRows = 10;
  filter = '';
  pageIndex = 0;
  tableLength = 0;
  onlyMine = false;
  snippets = new Array<Snippet>();
  dataSource: MatTableDataSource<Snippet>;
  observable: Observable<any>;
  supervisor = [Role.SystemAdmin, Role.Admin, Role.Supervisor];
  displayedColumns = ['name', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'action'];

  constructor(private articleService: ArticleService, private snackBar: MatSnackBar, private loader: NgxUiLoaderService, private router: Router, private modalService: ModalService) {
  }

  ngOnInit(): void {
    this.getAllSnippets();
  }

  async getAllSnippets() {
    this.loader.startLoader('snippet');
    const data = await this.articleService.getAllSnippets(this.from, this.numOfRows, this.filter, this.onlyMine);
    this.snippets = data.snippets;
    this.tableLength = data.total;
    this.dataSource = new MatTableDataSource<Snippet>(this.snippets);
    this.observable = this.dataSource.connect();
    this.loader.stopLoader('snippet');
  }

  searchSnippets() {
    this.getAllSnippets();
    this.pageIndex = 0;
  }

  pageChange(event) {
    this.from = (event.pageIndex * event.pageSize);
    this.numOfRows = event.pageSize;
    this.getAllSnippets();
  }

  editSnippet(id) {
    this.router.navigate(['/articles/snippets/edit-snippet/' + id]);
  }

  viewSnippet(id) {
    this.router.navigate(['/articles/snippets/view-snippet/' + id]);
  }

  createSnippet() {
    this.router.navigate(['/articles/snippets/create-snippet/']);
  }

  deleteSnippet(id) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Snippet',
      message: 'Are you sure?',
      cancelText: 'No',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(async data => {
      if (data === true) {
        const result = await this.articleService.deleteSnippet(id);
        if (result) {
          this.snackBar.open('Snippet deleted successfully', 'OK', { duration: 3500 });
          this.getAllSnippets();
        }
      }
    });
  }



}
