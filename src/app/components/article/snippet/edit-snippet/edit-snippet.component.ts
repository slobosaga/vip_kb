import { Location } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AppConfigService } from 'src/app/base/configuration.service';
import { CkeditorConfig } from 'src/app/models/enums/ckeditor';
import { Snippet } from 'src/app/models/snippet';
import { ArticleService } from 'src/app/services/article.service';
import { ModalService } from 'src/app/services/modal.service';
import { environment } from './../../../../../environments/environment';

@Component({
  selector: 'app-edit-snippet',
  templateUrl: './edit-snippet.component.html',
  styleUrls: ['./edit-snippet.component.scss']
})
export class EditSnippetComponent implements OnInit, OnDestroy, AfterViewInit {

  snippetId = null;
  readonly = false;
  snippet = new Snippet();
  ckEditorUrl: string = null;
  ckconfig = new CkeditorConfig();

  constructor(private articleService: ArticleService, private appConfigService: AppConfigService, private route: ActivatedRoute, private location: Location, private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private modalService: ModalService) {
    this.snippetId = this.route.snapshot.paramMap.get('id');
    if (this.route.snapshot.data?.readonly) {
      this.readonly = this.route.snapshot.data?.readonly;
    }
    if (environment.production === true) {
      this.ckEditorUrl = appConfigService.ckEditorUrlProd;
    } else {
      this.ckEditorUrl = appConfigService.ckEditorUrlDev;
    }
    this.ckconfig.extraPlugins = 'font,image2,templates,snippets,html5video,widgetselection,lineutils,uploadimage,uploadwidget,filebrowser,accordionList,collapsibleItem,iframedialog,iframe,mailto,createSnippet,createTemplate,dialog,tableresize';
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.loader.startLoader('l-1');
    if (this.snippetId != null) {
      setTimeout(() => {
        this.getSnippet();
        this.loader.stopLoader('l-1');
      }, 1000);
    } else {
      this.loader.stopLoader('l-1');
    }
  }

  ngOnDestroy(): void {
  }

  async getSnippet() {
    this.snippet = await this.articleService.getSnippet(this.snippetId);
  }

  async updateSnippet() {
    const count = await this.articleService.getSnippetComfirm(this.snippetId);
    const numOfArticles = count.locked + count.unlocked;
    this.modalService.confirmDialogOpen({
      title: 'Update Snippet',
      message: `The number of articles that will be updated is: ${numOfArticles}. Are you sure?`,
      cancelText: 'No',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(async data => {
      if (data === true) {
        const result = await this.articleService.updateSnippet(this.snippet.id, { name: this.snippet.name, text: this.snippet.text });
        if (result) {
          this.snackBar.open('Snippet Updated Successfully', 'OK', { duration: 3500 });
          this.location.back();
        }
      }
    });
  }

  async createSnippet() {
    const result = await this.articleService.createSnippet({ name: this.snippet.name, text: this.snippet.text });
    if (result) {
      this.snackBar.open('Snippet Created Successfully', 'OK', { duration: 3500 });
      this.location.back();
    }
  }

  saveSnippet() {
    if (this.snippetId != null) {
      this.updateSnippet();
    } else {
      this.createSnippet();
    }
  }

  ckFileUpload(event) {
    const fileLoader = event.data.fileLoader;
    const formData = new FormData();
    const xhr = fileLoader.xhr;
    xhr.open('POST', fileLoader.uploadUrl, true);
    xhr.setRequestHeader('Content-Disposition', 'miltipart/form-data');
    xhr.setRequestHeader('X-File-Name', fileLoader.fileName);
    xhr.setRequestHeader('X-File-Size', fileLoader.total);
    xhr.setRequestHeader('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken'));
    formData.append('file', fileLoader.file, fileLoader.fileName);
    xhr.send(formData);
    event.stop();
  }

  ckFileResponse(event) {
    event.stop();
    const data = event.data;
    const xhr = data.fileLoader.xhr;
    let response = xhr.responseText;
    response = JSON.parse(response);
    data.url = location.origin + response.url;
  }

}
