import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Role } from 'src/app/models/enums/rolesEnum';
import { AuthGuardService as AuthGuard } from 'src/app/services/auth-guard.service';
import { ArticleComponent } from './article/article.component';
import { CategoriesComponent } from './categories/categories.component';
import { CommentsComponent } from './comments/comments.component';
import { EditArticleComponent } from './article/edit-article/edit-article.component';
import { FindAndReplaceComponent } from './find-and-replace/find-and-replace.component';
import { EditSnippetComponent } from './snippet/edit-snippet/edit-snippet.component';
import { SnippetComponent } from './snippet/snippet.component';
import { EditTemplateComponent } from './template/edit-template/edit-template.component';
import { TemplateComponent } from './template/template.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ArticleComponent,
    data: { title: 'Articles' }

  },
  {
    path: 'article-management',
    canActivate: [AuthGuard],
    component: ArticleComponent,
    data: { title: 'Article Management' }
  },
  {
    path: 'edit-article/:id/:languageId',
    canActivate: [AuthGuard],
    canDeactivate: [AuthGuard],
    component: EditArticleComponent,
    data: { readonly: false, role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'view-article/:id/:languageId',
    canActivate: [AuthGuard],
    component: EditArticleComponent,
    data: { readonly: true }
  },
  {
    path: 'create-new-article-new-language/:id/:languageId',
    canActivate: [AuthGuard],
    component: EditArticleComponent,
    data: { readonly: false, articleAnotherLanguage: true }
  },
  {
    path: 'create-article',
    canActivate: [AuthGuard],
    canDeactivate: [AuthGuard],
    component: EditArticleComponent,
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'categories',
    component: CategoriesComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'comments',
    component: CommentsComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'templates',
    component: TemplateComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'templates/edit-template/:id',
    component: EditTemplateComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'templates/view-template/:id',
    component: EditTemplateComponent,
    canActivate: [AuthGuard],
    data: { readonly: true, role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'templates/create-template',
    component: EditTemplateComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'snippets',
    component: SnippetComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'snippets/edit-snippet/:id',
    component: EditSnippetComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'snippets/view-snippet/:id',
    component: EditSnippetComponent,
    canActivate: [AuthGuard],
    data: { readonly: true, role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'snippets/create-snippet',
    component: EditSnippetComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer] }
  },
  {
    path: 'find-and-replace',
    component: FindAndReplaceComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleRoutingModule { }
