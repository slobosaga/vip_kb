import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FindAndRepalceSnippets, FindAndReplaceArticles, FindAndReplaceSnippetReslut, FindReplaceArticleResult } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-find-and-replace',
  templateUrl: './find-and-replace.component.html',
  styleUrls: ['./find-and-replace.component.scss']
})
export class FindAndReplaceComponent implements OnInit, OnDestroy {
  @ViewChild('findReplace') findReplace: NgForm;
  @ViewChild('articlePaginator') articlePaginator: MatPaginator;
  @ViewChild('articleSort', { static: true }) articleSort: MatSort;
  @ViewChild('snippetPaginator') snippetPaginator: MatPaginator;
  @ViewChild('snippetSort', { static: true }) snippetSort: MatSort;

  find: string = null;
  replace = null;
  isSnippet = false;
  isArticle = false;
  showSnippets = false;
  showArticles = false;
  articlesCount = new FindReplaceArticleResult();
  snippetsCount = new FindAndReplaceSnippetReslut();
  totalArticles = null;
  spinner = false;
  dataSourceArticles: MatTableDataSource<FindAndReplaceArticles>;
  dataSourceSnippets: MatTableDataSource<FindAndRepalceSnippets>;
  snippetResult: Array<{ id: number, name: string }> = null;
  articlesResult: Array<{ articleId: string, subject: string }> = null;
  displayedColumnsArticles = ['subject', 'articleStatus', 'webPortals', 'locked'];
  displayedColumnsSnippets = ['name', 'hasLockedArticles'];
  disableButton = false;

  constructor(private articleService: ArticleService, private modalService: ModalService, private snackBar: MatSnackBar, private router: Router, private loader: NgxUiLoaderService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  onChangeArticle() {
    if (this.showSnippets === true) {
      this.showSnippets = false;
    }
    this.showArticles = true;
    if (this.isSnippet === true) {
      this.isSnippet = false;
      this.snippetsCount = new FindAndReplaceSnippetReslut();
      this.dataSourceSnippets = new MatTableDataSource<FindAndRepalceSnippets>(null);
    }
    if (this.isArticle === false) {
      this.articlesCount.articles = null;
      this.articlesCount.locked = null;
      this.articlesCount.unlocked = null;
      this.totalArticles = null;
      this.dataSourceArticles = new MatTableDataSource<FindAndReplaceArticles>(null);
    }
    this.findArticleSnippet();
  }

  onChangeSnippet() {
    if (this.showArticles === true) {
      this.showArticles = false;
    }
    this.showSnippets = true;
    if (this.isArticle === true) {
      this.isArticle = false;
      this.articlesCount.locked = null;
      this.articlesCount.unlocked = null;
      this.articlesCount.articles = null;
      this.totalArticles = null;
      this.dataSourceArticles = new MatTableDataSource<FindAndReplaceArticles>(null);
    }
    if (this.isSnippet === false) {
      this.snippetsCount = new FindAndReplaceSnippetReslut();
      this.dataSourceSnippets = new MatTableDataSource<FindAndRepalceSnippets>(null);
    }
    this.findArticleSnippet();
  }

  async findArticleSnippet() {
    if (this.isArticle === true && this.find !== null) {
      this.find = this.find.trim();
      this.loader.startLoader('l-1');
      this.disableButton = true;
      this.articlesCount = await this.articleService.getFindAndreplaceArticleConfirm(this.find);
      if (this.articlesCount.articles !== null) {
        this.articlesCount.articles.forEach(a => {
          if (a.lockedBy != null) {
            a.lockedDetails = a.lockedBy + ' @ ' + new DatePipe('en-US').transform(a.lockedDate, 'dd-MM-yyyy HH:mm');
          }
        });
      }
      this.dataSourceArticles = new MatTableDataSource<FindAndReplaceArticles>(this.articlesCount.articles);
      this.dataSourceArticles.paginator = this.articlePaginator;
      this.dataSourceArticles.sort = this.articleSort;
      this.totalArticles = this.articlesCount.locked + this.articlesCount.unlocked;
      this.disableButton = false;
      this.loader.stopLoader('l-1');
    } else if (this.isSnippet === true && this.find !== null) {
      this.find = this.find.trim();
      this.articleService.getFindAndReplaceSnippetConfirm(this.find).then(async data => {
        this.loader.startLoader('l-1');
        this.disableButton = true;
        this.snippetsCount = await data;
        this.dataSourceSnippets = new MatTableDataSource<FindAndRepalceSnippets>(this.snippetsCount.snippets);
        if (this.snippetsCount.snippets !== null) {
          this.dataSourceSnippets.paginator = this.snippetPaginator;
          this.dataSourceSnippets.sort = this.snippetSort;
        }
        this.disableButton = false;
        this.loader.stopLoader('l-1');
      });
    }
  }

  async replaceArticleSnippet() {
    this.loader.startLoader('l-1');
    if (this.isArticle) {
      this.articlesResult = await this.articleService.findAndReplaceArticle({ find: this.find, replace: this.replace });
      if (this.articlesResult !== null) {
        const articleFilter = this.articlesResult.map(a => a.articleId);
        this.articlesCount.articles = this.articlesCount.articles.filter(ar => articleFilter.includes(ar.articleId));
        this.dataSourceArticles = new MatTableDataSource<FindAndReplaceArticles>(this.articlesCount.articles);
        this.dataSourceArticles.paginator = this.articlePaginator;
        this.dataSourceArticles.sort = this.articleSort;
      } else {
        this.dataSourceArticles = new MatTableDataSource<FindAndReplaceArticles>(null);
        this.dataSourceArticles.paginator = this.articlePaginator;
        this.dataSourceArticles.sort = this.articleSort;
      }
      this.resetFields();
      this.snackBar.open('Updates Successfully', 'OK', { duration: 3500 });
      this.loader.stopLoader('l-1');
      this.findReplace.reset();
      this.findReplace.resetForm();
    }
    if (this.isSnippet) {
      this.snippetResult = await this.articleService.findAndReplaceSnippet({ find: this.find, replace: this.replace });
      if (this.snippetResult != null) {
        this.snippetsCount.snippets = this.snippetResult.map(s => ({ id: s.id, name: s.name, createdBy: null, createdDate: null, text: null, updatedBy: null, updatedDate: null }));
        this.dataSourceSnippets = new MatTableDataSource<FindAndRepalceSnippets>(this.snippetsCount.snippets);
        this.dataSourceSnippets.paginator = this.snippetPaginator;
        this.dataSourceSnippets.sort = this.snippetSort;

      } else {
        this.dataSourceSnippets = new MatTableDataSource<FindAndRepalceSnippets>(null);
        this.dataSourceSnippets.paginator = this.snippetPaginator;
        this.dataSourceSnippets.sort = this.snippetSort;
      }
      this.resetFields();
      this.snackBar.open('Updates Successfully', 'OK', { duration: 3500 });
      this.loader.stopLoader('l-1');
      this.findReplace.form.markAsPristine();
      this.findReplace.reset();
      this.findReplace.resetForm();
    }
  }

  private resetFields() {
    this.find = null;
    this.replace = null;
    this.isArticle = false;
    this.isSnippet = false;
  }

  replaceModal() {
    if (this.findReplace.valid === true) {
      this.modalService.confirmDialogOpen({
        title: this.isArticle ? 'Update Articles' : 'Update Snippets',
        message: 'Are you sure?',
        cancelText: 'No',
        confirmText: 'Yes'
      });
      this.modalService.confirmDialogConfirmed().subscribe(data => {
        if (data === true) {
          this.replaceArticleSnippet();
        }
      });
    }
  }

  viewArticle(id, languageId) {
    this.router.navigate([`/articles/view-article/${id}/${languageId}`]);
  }

  viewSnippet(id) {
    this.router.navigate(['/articles/snippets/view-snippet/' + id]);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (this.articlesCount.articles != null) {
      this.dataSourceArticles.filter = filterValue.trim().toLowerCase();
    }

    if (this.dataSourceArticles.paginator) {
      this.dataSourceArticles.paginator.firstPage();
    }
  }

  applyFilterSnippets(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (this.snippetsCount.snippets != null) {
      this.dataSourceSnippets.filter = filterValue.trim().toLowerCase();
    }

    if (this.dataSourceSnippets.paginator) {
      this.dataSourceSnippets.paginator.firstPage();
    }
  }

}
