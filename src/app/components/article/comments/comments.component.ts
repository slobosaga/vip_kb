import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Comment, CommentFilter, CommentResponse, CommentStatus, CommentType } from 'src/app/models/comment';
import { WebPortal } from 'src/app/models/webPortal';
import { ArticleService } from 'src/app/services/article.service';
import { CommentService } from 'src/app/services/comment.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  articleId = '';
  webPortalId = '';
  commentType = null;
  selectedCommentStatuses: string[] = [];
  commentResponse = new CommentResponse();
  comments = new Array<Comment>();
  commentTypes = new Array<CommentType>();
  commentStatuses = new Array<CommentStatus>();
  webPortals = new Array<WebPortal>();
  dataSource: MatTableDataSource<Comment>;
  observable: Observable<any>;
  languages = new Array<{ id: number, name: string }>();
  commentSubscriber: Subscription;
  numRows = 10;
  fromNum = 0;
  total = 0;
  commentFilter = new CommentFilter();
  displayedColumns = ['webPortalName', 'subject', 'status', 'type', 'language', 'portalType', 'userEmail', 'createdByFullName', 'createdDate', 'updatedByFullName', 'updatedDate'];

  constructor(private commentService: CommentService, private eventEmitter: EventEmitterService, private articleService: ArticleService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private modalService: ModalService) {
    this.commentFilter.numComments = 10;
    this.commentFilter.fromComment = 0;
  }

  ngAfterViewInit(): void {
    this.paginator.page.pipe(tap(event => {
      this.commentFilter.fromComment = (event.pageIndex * event.pageSize);
      this.commentFilter.numComments = event.pageSize;
      this.getAllComments();
    })).subscribe();
  }

  ngOnInit(): void {
    this.commentSubscriber = this.eventEmitter.emitCommentChange.subscribe(data => {
      if (data) {
        this.getAllComments();
      }
    });
    const defaultWebPortal = JSON.parse(localStorage.getItem('A1_Admin_defaultWebPortal'));
    if (defaultWebPortal !== null) {
      this.webPortalId = defaultWebPortal;
    }
    this.getData();
  }

  ngOnDestroy(): void {
    this.commentSubscriber.unsubscribe();
  }

  async getData() {
    this.loader.startLoader('l-1');
    await this.getWebPortals();
    await this.getAllLanguages();
    await this.getCommentTypes();
    await this.getCommentStatuses();
    await this.getAllComments();
    this.loader.stopLoader('l-1');
  }

  async getAllComments() {
    this.commentResponse = await this.commentService.getAllComments(this.commentFilter);
    this.comments = this.commentResponse.comments;
    this.total = this.commentResponse.total;
    this.comments.forEach(c => {
      if (c.webPortalId != null) {
        if (this.webPortals.find(w => w.id === c.webPortalId)) {
          c.webPortalName = this.webPortals.find(w => w.id === c.webPortalId).name;
        } else {
          c.webPortalName = 'WebPortal Deleted';
        }
      }
      if (c.languageId != null) {
        if (this.languages.find(l => l.id === c.languageId)) {
          c.language = this.languages.find(l => l.id === c.languageId).name;
        }
      }
    });
    this.dataSource = new MatTableDataSource<Comment>(this.comments);
    this.observable = this.dataSource.connect();
  }

  async getCommentTypes() {
    this.commentTypes = await this.commentService.getAllCommentTypes();
  }

  async getCommentStatuses() {
    this.commentStatuses = await this.commentService.getAllCommentStatuses();
  }

  async getWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
  }

  async getAllLanguages() {
    this.languages = await this.articleService.getAllLanguages('false');
  }

  async updateCommentStatus(commentId) {
    await this.commentService.updateCommentStatus(commentId, this.comments.find(id => commentId === id).status);
    this.snackBar.open('Comment Status Updated Successfully', 'OK', { duration: 3500 });
  }

  openCommentModal(id: string) {
    const data: Comment = this.comments.find(c => c.id === id);
    this.modalService.commentModalOpen({ comment: data, statuses: this.commentStatuses });
  }

}
