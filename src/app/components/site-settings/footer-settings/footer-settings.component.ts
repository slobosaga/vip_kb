import { AfterViewInit, Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { LinkGroupRequest, LinkGroupResponse, LinkGroupSort, LinkResponse, Translations } from 'src/app/models/link-group';
import { WebPortal } from 'src/app/models/webPortal';
import { ArticleService } from 'src/app/services/article.service';
import { LinkGroupService } from 'src/app/services/link-group.service';
import { ModalService } from 'src/app/services/modal.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { NgForm } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-footer-settings',
  templateUrl: './footer-settings.component.html',
  styleUrls: ['./footer-settings.component.scss']
})

export class FooterSettingsComponent implements OnInit, AfterViewInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  @ViewChildren('footerForm') footerForm: QueryList<NgForm>;

  webPortals = new Array<WebPortal>();
  groups = new Array<LinkGroupResponse>();
  links = new Array<LinkResponse>();
  webPortalId: string = null;
  languages: any[];
  linkGroupSort = new LinkGroupSort();

  constructor(private linkGroupService: LinkGroupService, private articleService: ArticleService, private modalService: ModalService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar) { }

  ngAfterViewInit(): void {

  }

  async ngOnInit() {
    this.loader.startLoader('l-1');
    await this.getWebPortals();
    await this.getLanguages();
    await this.getLinkGroups();
    this.loader.stopLoader('l-1');
  }

  private async getLinkGroups() {
    this.groups = await this.linkGroupService.getLinkGroups(this.webPortalId);
    if (this.groups !== null) {
      this.groups = this.groups.sort((a, b) => a.order - b.order);
      this.groups.forEach(g => {
        if (g.links !== null) {
          g.links = g.links.sort((a, b) => a.order - b.order);
        }
      });
    }
  }

  async getWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
    if (this.webPortals.length > 0) {
      const defaultWebPortal = JSON.parse(localStorage.getItem('A1_Admin_defaultWebPortal'));
      if (defaultWebPortal !== null) {
        this.webPortalId = defaultWebPortal;
      } else {
        this.webPortalId = this.webPortals[0].id;
      }
    }
  }

  async getLanguages() {
    this.languages = await this.articleService.getAllLanguages();
  }

  onWebPortalChange() {
    this.loader.startLoader('l-1');
    this.getLinkGroups();
    this.loader.stopLoader('l-1');
  }

  addNewGroupModal() {
    this.modalService.inputDialogOpen({
      title: 'New Link Group',
      message: 'Enter Link Group Name',
      cancelText: 'Cancel',
      confirmText: 'Add',
      input: null
    });
    this.modalService.inputDialogConfirmed().subscribe(data => {
      if (data != null) {
        if (data.length > 0) {
          this.addNewGroup(data);
          setTimeout(() => {
            const newGroup = document.getElementById('groupName' + this.groups.indexOf(this.groups.find(g => g.name === data)));
            if (newGroup !== null) {
              newGroup.scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'});
            }
          }, 0);
        }
      }
    });
  }

  addNewGroup(name) {
    let order = 1;
    if (this.groups != null) {
      order = this.groups.length + 1;
    }
    const group = new LinkGroupResponse();
    group.order = order;
    group.name = name;
    group.webPortalId = this.webPortalId;
    if (this.groups === null) {
      this.groups = [];
    }
    this.groups.push(group);
  }

  addNewLinkToGroup(groupIndex) {
    const link = new LinkResponse();
    let order = 1;
    if (this.groups[groupIndex].links !== null) {
      if (this.groups[groupIndex].links.length > 0) {
        order = this.groups[groupIndex].links.length + 1;
      }
    } else {
      this.groups[groupIndex].links = [];
    }
    link.order = order;
    this.groups[groupIndex].links.push(link);
  }

  saveNewLinkToGroup(groupIndex, linkIndex) {
    const group = this.groups[groupIndex];
    const link = group.links[linkIndex];
    this.linkGroupService.addLinkToGroup(link, group.name, group.id);
  }

  removeLinkGroup(i) {
  }

  removeLinkGroupModal(i) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Link Group',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        if (this.groups[i].id != null) {
          this.linkGroupService.deleteLinkGroup(this.groups[i].name, this.groups[i].id).then(data => {
            this.groups.splice(i, 1);
            this.snackBar.open('Link Group Deleted Successfully', 'OK', { duration: 3000 });
          }).catch(err => { });
        } else {
          this.groups.splice(i, 1);
          this.snackBar.open('Link Group Deleted Successfully', 'OK', { duration: 3000 });
        }
      }
    });
  }

  removeLink(groupIndex, linkIndex) {
    this.modalService.confirmDialogOpen({
      title: 'Remove Link from Group',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        if (this.groups[groupIndex].links[linkIndex].id != null) {
          this.linkGroupService.removeLinkFromGroup(this.groups[groupIndex].id, this.groups[groupIndex].links[linkIndex].id).then(data => {
            this.groups[groupIndex].links.splice(linkIndex, 1);
            this.snackBar.open('Link Deleted Successfully', 'OK', { duration: 3000 });
          }).catch(err => { });
        } else {
          this.groups[groupIndex].links.splice(linkIndex, 1);
          this.snackBar.open('Link Deleted Successfully', 'OK', { duration: 3000 });
        }
      }
    });
  }

  dropLinkGroup(event: CdkDragDrop<Array<LinkGroupResponse>>) {
    moveItemInArray(this.groups, event.previousIndex, event.currentIndex);
    for (let i = 0; i < this.groups.length; i++) {
      const g = this.groups[i];
      g.order = i + 1;
    }
  }

  dropLinks(event: CdkDragDrop<Array<LinkResponse>>, gropIndex) {
    moveItemInArray(this.groups[gropIndex].links, event.previousIndex, event.currentIndex);
    for (let i = 0; i < this.groups[gropIndex].links.length; i++) {
      const link = this.groups[gropIndex].links[i];
      link.order = i + 1;
    }
  }

  addLinkGroupTranslation(groupIndex) {
    let hasMaxLang = false;
    if (this.languages.every(l => this.groups[groupIndex].translations.find(t => t.languageId === l.id))) {
      hasMaxLang = true;
    } else if (this.languages.length === this.groups[groupIndex].translations.length) {
      hasMaxLang = true;
    }
    hasMaxLang ? this.snackBar.open('Maximum number of translations', 'OK', { duration: 3000 }) : this.groups[groupIndex].translations.push(new Translations());
  }

  addLinkTranslation(groupIndex, linkIndex) {
    let hasMaxLang = false;
    if (this.languages.every(l => this.groups[groupIndex].links[linkIndex].translations.find(t => t.languageId === l.id))) {
      hasMaxLang = true;
    } else if (this.languages.length === this.groups[groupIndex].links[linkIndex].translations.length) {
      hasMaxLang = true;
    }
    hasMaxLang ? this.snackBar.open('Maximum number of translations', 'OK', { duration: 3000 }) : this.groups[groupIndex].links[linkIndex].translations.push(new Translations());
  }

  removeGroupTranslation(groupIndex, langIndex) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Group Translation',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.groups[groupIndex].translations.splice(langIndex, 1);
      }
    });
  }

  removeLinkTranslation(groupIndex, linkIndex, langIndex) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Link Translation',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.groups[groupIndex].links[linkIndex].translations.splice(langIndex, 1);
      }
    });
  }

  async saveLinkGroups(index) {
    const forms = this.footerForm.toArray();
    const form = forms[index];
    if (form.submitted === true) {
      if (form.form.valid === true) {
        this.loader.startLoader('l-1');
        const linkGroup = new LinkGroupRequest();
        linkGroup.linkGroup = this.groups[index];
        linkGroup.webPortalId = this.webPortalId;
        if (linkGroup.linkGroup.id === null) {
          await this.linkGroupService.createLinkGroups(linkGroup);
        } else {
          await this.linkGroupService.updateLinkGroup(this.groups[index], this.groups[index].id);
        }
        this.loader.stopLoader('l-1');
        this.snackBar.open('Footer Settings saved successfully', 'OK', { duration: 3000 });
        this.getLinkGroups();
      }
    }
  }

  onSideMenuCheckChange(event: MatCheckboxChange, groupId) {
    this.groups.forEach(g => {
      if (g.isSideGroup === true && g.id !== groupId) {
        g.isSideGroup = false;
      }
    });
  }

  async sortLinkGroups() {
    this.loader.startLoader('l-1');
    this.linkGroupSort = new LinkGroupSort();
    this.groups.forEach(g => {
      this.linkGroupSort.linkGroups.push({id: g.id, order: g.order});
    });
    this.linkGroupSort.webPortalId = this.webPortalId;
    this.linkGroupService.sortLinkGroups(this.linkGroupSort).then(data => {
      this.loader.stopLoader('l-1');
      if (data) {
        this.snackBar.open('Order Saved Successfully', 'OK', {duration: 3500});
      }
    });
  }

}
