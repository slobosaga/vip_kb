import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabGroup } from '@angular/material/tabs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MenuItem } from 'primeng/api/menuitem';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Category } from 'src/app/models/category';
import { BoostValue, QueryArticleView, QueryConfig, QueryFilter, QueryRequest, Relevance } from 'src/app/models/query';
import { WebPortal } from 'src/app/models/webPortal';
import { ArticleService } from 'src/app/services/article.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';
import { QueryService } from 'src/app/services/query.service';


@Component({
  selector: 'app-query-settings',
  templateUrl: './query-settings.component.html',
  styleUrls: ['./query-settings.component.scss']
})
export class QuerySettingsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  fields: string[];
  field: string;
  queryConfigs = new Array<QueryConfig>();
  queryTypes = new Array<{ id: number, name: string }>();
  queryType = 0;
  queryRequest = new QueryRequest();
  selectedTabIndex = 0;
  articles = new Array<QueryArticleView>();
  dataSource: MatTableDataSource<QueryArticleView>;
  observable: Observable<any>;
  categories: Array<Category>;
  nodes = new Array<MenuItem>();
  paging = new QueryFilter();
  webPortals = new Array<WebPortal>();
  webPortalId: string;
  pageIndex = 0;
  tableLength = 0;
  languageSubscriber: Subscription;
  relevanceFields = new Array<Relevance>();
  displayedColumns = ['subject', 'description', 'publishedDate', 'isLocked', 'hasAttachment'];

  constructor(private queryService: QueryService, private eventEmitter: EventEmitterService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private modalService: ModalService, private articleService: ArticleService) {
    this.queryTypes = [{ id: 1, name: 'Default' }, { id: 2, name: 'Temporary' }, { id: 3, name: 'Main' }];
    this.paging.fromArticle = 0;
    this.paging.numArticles = 10;
    this.paging.queryTypeId = 1;
    this.categories = new Array<Category>();
  }

  ngAfterViewInit(): void {
    this.languageSubscriber = this.eventEmitter.emitLanguageId.subscribe(() => {
      this.getData();
    });
    setTimeout(() => {
      this.paginator.page.pipe(tap(event => {
        this.paging.fromArticle = (event.pageIndex * event.pageSize);
        this.paging.numArticles = event.pageSize;
        this.getAllArticles();
      })).subscribe();
      this.tabGroup.selectedIndex = 0;
    }, 500);
  }

  ngOnInit(): void {
    const defaultWebPortal = JSON.parse(localStorage.getItem('A1_Admin_defaultWebPortal'));
    if (defaultWebPortal !== null) {
      this.webPortalId = defaultWebPortal;
    }
    this.getData();
  }

  ngOnDestroy(): void {
    this.languageSubscriber.unsubscribe();
  }


  async getData() {
    this.loader.startLoader('l-1');
    // await this.getAllFields();
    await this.getRelevanceFields();
    await this.getAllQueryConfigTypes();
    await this.getQueryConfigs();
    await this.getAllWebPortals();
    await this.getCategories();
    await this.getAllArticles();
    this.selectedTabIndex = 0;
    this.loader.stopLoader('l-1');
  }


  async onPortalSelectionChange() {
    await this.getCategories();
    await this.getAllArticles();
  }

  async getAllFields() {
    this.fields = await this.queryService.getAllFields();
  }

  async getRelevanceFields() {
    this.relevanceFields = await this.queryService.getRelevanceFields();
  }

  async getQueryConfigs() {
    this.queryConfigs = await this.queryService.getQueryConfigs('', '', '');
    if (this.queryConfigs != null) {
      this.queryConfigs.forEach(q => {
        q.queryTypeName = q.queryTypeName.charAt(0).toUpperCase() + q.queryTypeName.slice(1);
      });
      this.createRelevances();
    }
  }

  createRelevances() {
    this.queryConfigs.forEach(config => {
      if (config.relevanceSerialized === null) {
        config.relevanceSerialized = this.relevanceFields;
      }
    });
  }

  async getAllWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
    this.webPortalId = this.webPortals[0].id;
    this.paging.webPortalId = this.webPortalId;
  }

  async getAllQueryConfigTypes() {
    this.queryTypes = await this.queryService.getAllQueryTypes();
  }

  async getCategories() {
    if (this.paging.webPortalId !== null) {
      this.categories = await this.articleService.getAllCategoriesForWebPortal([this.paging.webPortalId]);
    } else {
      this.categories = await this.articleService.getAllCategories();
    }
    // this.categories.sort((a, b) => a.sort - b.sort);
    this.categoriesTreeView();
    this.removeEmptyItems();
  }

  categoriesTreeView() {
    this.nodes = new Array<MenuItem>();
    this.categories.forEach(c => {
      const node: MenuItem = { id: c.id.toString(), label: c.name, items: [], styleClass: c.parentId?.toString(), title: c.name, command: (event) => { this.selectedCategoryEvent(c.id); } };
      if (c.parentId === null) {
        this.nodes.push(node);
      }
    });
    this.categories.forEach(c => {
      const node: MenuItem = { id: c.id.toString(), label: c.name, items: [], styleClass: c.parentId?.toString(), title: c.name, command: (event) => { this.selectedCategoryEvent(c.id); } };
      if (c.parentId > 0) {
        this.fillTree(node, null);
      }
    });
  }

  fillTree(node: MenuItem, nodes: Array<MenuItem> = null) {
    if (nodes === null) {
      nodes = this.nodes;
    }
    nodes.forEach(n => {
      if (n.id === node.styleClass) {
        n.items.push(node);
      } else if (n.id === node.styleClass) {
        n.items.push(node);
      } else if (n.items !== null) {
        this.fillTree(node, n.items);
      }
    });
  }

  removeEmptyItems(nodes: MenuItem[] = null) {
    if (nodes === null) {
      nodes = this.nodes;
    }
    nodes.forEach(n => {
      if (n.items.length === 0) {
        n.items = null;
      } else {
        this.removeEmptyItems(n.items);
      }
    });
  }

  selectedCategoryEvent(id) {
    this.paging.categoryId = id;
    this.getAllArticles();
  }

  selectCategory(event) {
    this.paging.categoryId = event;
    this.getAllArticles();
  }

  async updateQueryConfig(i) {
    this.loader.startLoader('l-1');
    this.queryRequest.fuzziness = this.queryConfigs[i].fuzziness;
    this.queryRequest.postTags = this.queryConfigs[i].postTags;
    this.queryRequest.preTags = this.queryConfigs[i].preTags;
    this.queryRequest.queryTypeId = this.queryConfigs[i].queryTypeId;
    this.queryRequest.relevance = this.queryConfigs[i].relevanceSerialized;
    const result = await this.queryService.updateQueryConfig(this.queryConfigs[i].id, this.queryRequest);
    if (result) {
      this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
      this.getData();
    } else {
      this.loader.stopLoader('l-1');
    }
  }

  searchArticles() {
    this.paging.search = this.paging.search.trim();
    this.getAllArticles();
  }


  async markAsActive(id) {
    this.loader.startLoader('l-1');
    this.queryService.markQuertConfigAsActive(id);
    this.getData();
    this.loader.stopLoader('l-1');
  }

  async addRelevance(index) {
    const relevance = new Relevance();
    relevance.field = this.field;
    if (this.queryConfigs[index].relevanceSerialized === null) {
      this.queryConfigs[index].relevanceSerialized = new Array<Relevance>();
    }
    this.queryConfigs[index].relevanceSerialized.push(relevance);
  }

  removeRelevance(queryIndex, relevanceIndex) {
    this.modalService.confirmDialogOpen({
      title: 'Remove Relevance',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.queryConfigs[queryIndex].relevanceSerialized.splice(relevanceIndex, 1);
      }
    });
  }

  addBoost(queryIndex, relevanceIndex) {
    const boost = new BoostValue();
    if (this.queryConfigs[queryIndex].relevanceSerialized[relevanceIndex].boostValues === null) {
      this.queryConfigs[queryIndex].relevanceSerialized[relevanceIndex].boostValues = new Array<BoostValue>();
    }
    this.queryConfigs[queryIndex].relevanceSerialized[relevanceIndex].boostValues.push(boost);
  }

  removeBoost(queryIndex, relevanceIndex, boostIndex) {
    this.modalService.confirmDialogOpen({
      title: 'Remove Boost',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.queryConfigs[queryIndex].relevanceSerialized[relevanceIndex].boostValues.splice(boostIndex, 1);
      }
    });
  }

  async createQueryConfig() {
    const query = new QueryConfig();
    this.queryConfigs.push(query);
  }

  async getAllArticles() {
    this.loader.startLoader('l-1');
    await this.queryService.queryTester(this.paging).then(async data => {
      this.articles = await data;
      this.tableLength = 100;
    }).catch(err => { });
    if (this.articles != null) {
      this.dataSource = new MatTableDataSource<QueryArticleView>(this.articles);
      this.observable = this.dataSource.connect();
      setTimeout(() => {
        const elements = document.getElementsByName('highlight');
        for (let i = 0; i < elements.length; i++) {
          const ele: HTMLElement = elements[i];
          ele.style.background = 'yellow';
          ele.style.backgroundColor = 'yellow';
        }
      }, 1000);
    } else {
      this.snackBar.open('There are no articles in this category', 'OK', { duration: 3500 });
    }
    this.loader.stopLoader('l-1');
  }

  previewArticleBody(htmlBody) {
    this.modalService.previewArticleModalOpen(htmlBody);
  }

  createQueryConfigModal() {
    let queryTypes = [];
    if (this.queryConfigs !== null) {
      queryTypes = this.queryConfigs.map(q => q.queryTypeId);
    }
    const newQueryTypes = this.queryTypes.filter(q => !queryTypes.includes(q.id));
    if (newQueryTypes.length > 0) {
      this.modalService.addQueryConfigOpen(newQueryTypes);
      this.modalService.addQueryConfigConfirmed().subscribe(async data => {
        if (data != null) {
          this.loader.startLoader('l-1');
          const queryConfig = new QueryRequest();
          queryConfig.queryTypeId = data;
          queryConfig.fuzziness = 1;
          queryConfig.relevance = this.relevanceFields;
          await this.queryService.createQueryConfig(queryConfig);
          await this.getData();
          this.loader.stopLoader('l-1');
        }
      });
    } else {
      this.snackBar.open('Maximum number of Query Configurations is created', 'OK', { duration: 3500 });
    }
  }

}
