import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ArticlePaging, ArticleViewResponse, ArticleView } from 'src/app/models/article';
import { ArticleSection, ArticleSectionGet, Section, SectionPaging } from 'src/app/models/section';
import { WebPortal } from 'src/app/models/webPortal';
import { ArticleService } from 'src/app/services/article.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';
import { SectionService } from 'src/app/services/section.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('articlePaginator', { static: true }) articlePaginator: MatPaginator;
  @ViewChild('sectionPaginator', { static: true }) sectionPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) articleSort: MatSort;
  @ViewChild('sectionSort', { static: true }) sectionSort: MatSort;

  sections: Array<Section>;
  webPortals: Array<WebPortal>;
  webPortalId: string;
  sectionPaging = new SectionPaging();
  articlePaging = new ArticlePaging();
  sectionArticles = new Array<ArticleSectionGet>();
  articles = new Array<ArticleView>();
  sectionId: number;
  articlesView = new ArticleViewResponse();
  articleDataSource: MatTableDataSource<ArticleView>;
  sectionDataSource: MatTableDataSource<ArticleSectionGet>;
  observable: Observable<any>;
  tableLength = 0;
  pageIndex = 0;
  languageSubscriber: Subscription;
  languages;
  displayedColumnsArticle = ['subject', 'description', 'articleStatus', 'createdByFullName', 'createdDate', 'updatedByFullName', 'updatedDate', 'info', 'action'];
  displayedColumnsSection = ['subject', 'description', 'language', 'webPortalName', 'publishedDate', 'hasAttachment', 'action', 'sort'];

  constructor(private sectionService: SectionService, private modalService: ModalService, private eventEmitter: EventEmitterService, private articleService: ArticleService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar) {
    this.sections = new Array<Section>();
    this.webPortals = new Array<WebPortal>();
    this.articlePaging.fromArticle = 0;
    this.articlePaging.numArticles = 10;
    this.articlePaging.articleStatusId = 7;
    this.languages = JSON.stringify(localStorage.getItem('A1_Admin_languages'));
  }

  ngOnInit(): void {
    const defaultWebPortal = JSON.parse(localStorage.getItem('A1_Admin_defaultWebPortal'));
    if (defaultWebPortal !== null) {
      this.webPortalId = defaultWebPortal;
    }
    this.getData();
    this.languageSubscribe();
  }

  ngAfterViewInit(): void {
    this.articlePaginator.page.pipe(tap(event => {
      this.articlePaging.fromArticle = (event.pageIndex * event.pageSize);
      this.articlePaging.numArticles = event.pageSize;
      this.getAllArticles();
    })).subscribe();
  }

  ngOnDestroy(): void {
    this.languageSubscriber.unsubscribe();
  }

  async getData() {
    await this.getWebPortals();
    await this.getAllSections();
    this.languageSubscribe();
    await this.getAllArticles();
    await this.getAllSectionArticles();
  }

  languageSubscribe() {
    this.languageSubscriber = this.eventEmitter.emitLanguageId.subscribe(() => {
      this.loader.startLoader('l-1');
      this.getAllSectionArticles();
      this.getAllArticles();
      this.loader.stopLoader('l-1');
    });
  }

  async getAllSections() {
    this.loader.startLoader('l-1');
    this.sections = await this.sectionService.getAllSections();
    this.sectionId = this.sections[0].id;
    this.loader.stopLoader('l-1');
  }

  async getWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
    this.webPortalId = this.webPortals[0].id;
    this.articlePaging.webPortalId = this.webPortalId;
    this.sectionPaging.webPortalId = this.webPortalId;
  }

  async getAllArticles() {
    this.loader.startLoader('l-1');
    await this.articleService.getAllArticles(this.articlePaging).then(async data => {
      this.articlesView = await data;
      this.tableLength = this.articlesView.numOfArticles;
    }).catch(err => { });
    if (this.articlesView.articles != null) {
      this.articlesView.articles.forEach(a => {
        a.articleStatus = a.articleStatus.charAt(0).toUpperCase() + a.articleStatus.slice(1);
        if (a.lockedBy !== null) {
          a.lockedDetails = a.lockedBy + ' @ ' + new DatePipe('en-US').transform(a.lockedDate, 'dd-MM-yyyy HH:mm');
        }
      });
    } else {
      this.snackBar.open('There are no articles in this category', 'OK', { duration: 3500 });
    }
    this.articles = this.articlesView.articles;
    this.articleDataSource = new MatTableDataSource<ArticleView>(this.articles);
    this.observable = this.articleDataSource.connect();
    this.loader.stopAllLoader('l-1');
  }

  async getAllSectionArticles() {
    this.sectionPaging.sectionId = this.sectionId;
    this.sectionArticles = await this.sectionService.getAllSectionArticles(this.sectionPaging);
    if (this.sectionArticles != null) {
      this.sectionArticles.forEach(sa => {
        sa.webPortalName = this.webPortals.find(w => w.id === sa.webPortalId).name;
      });
      this.sectionArticles.sort((a, b) => a.order - b.order);
    } else {
      this.sectionArticles = new Array<ArticleSectionGet>();
    }
    this.sectionDataSource = new MatTableDataSource<ArticleSectionGet>(this.sectionArticles);
    if (this.sectionArticles != null) {
      this.sectionDataSource.paginator = this.sectionPaginator;
      this.sectionDataSource.sort = this.sectionSort;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (this.sectionDataSource.data != null) {
      this.sectionDataSource.filter = filterValue.trim().toLowerCase();
    }

    if (this.sectionDataSource.paginator) {
      this.sectionDataSource.paginator.firstPage();
    }
  }

  async onSectionChange() {
    this.sectionPaging.sectionId = this.sectionId;
    this.getAllSectionArticles();
  }

  async onWebPortalChange() {
    this.loader.startLoader('l-1');
    this.articlePaging.webPortalId = this.webPortalId;
    this.sectionPaging.webPortalId = this.webPortalId;
    await this.getAllSectionArticles();
    await this.getAllArticles();
    this.loader.stopLoader('l-1');
  }

  async addArticleToSection(id) {
    this.modalService.confirmDialogOpen({
      title: `Add Article To ${this.sections.find(s => s.id === this.sectionId).name}?`,
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(async data => {
      this.loader.startLoader('l-1');
      if (data === true) {
        const addArticle = new ArticleSection();
        addArticle.articleId = id;
        if (this.sectionArticles != null) {
          addArticle.order = this.sectionArticles.length + 1;
        } else {
          addArticle.order = 1;
        }
        addArticle.webPortalId = this.webPortalId;
        await this.sectionService.addArticleToSection(this.sectionId, addArticle).then(data => {
          this.getAllSectionArticles();
        }).catch(err => {
        });
        this.loader.stopLoader('l-1');
      }
    });
  }

  removeArticleFromSection(id) {
    this.modalService.confirmDialogOpen({
      title: `Remove Article From ` + this.sections.find(s => s.id === this.sectionId).name + '?',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(confirm => {
      this.loader.startLoader('l-1');
      if (confirm === true) {
        this.sectionService.removeArticleFromSection(this.sectionId, this.webPortalId, id).then(async resp => {
          const data = await resp;
          if (data) {
            setTimeout(() => {
              this.sectionArticles.splice(this.sectionArticles.indexOf(this.sectionArticles.find(s => s.articleId === id)), 1);
              if (this.sectionArticles.length > 0) {
                for (let i = 0; i < this.sectionArticles.length; i++) {
                  const article = this.sectionArticles[i];
                  article.order = i + 1;
                }
                this.sectionService.sortSectionArticles(this.sectionId, this.sectionArticles);
              }
              this.getAllSectionArticles();
              this.loader.stopLoader('l-1');
            }, 500);
          }
        }).catch();
      }
    });
  }

  articlePageChange(event) {
    this.articlePaging.fromArticle = (event.pageIndex * event.pageSize);
    this.articlePaging.numArticles = event.pageSize;
    this.getAllArticles();
  }

  searchArticles() {
    this.articlePaging.search = this.articlePaging.search.trim();
    this.getAllArticles();
  }

  dropArticle(event: CdkDragDrop<Array<ArticleSectionGet>>) {
    moveItemInArray(this.sectionArticles, event.previousIndex, event.currentIndex);
    this.sectionDataSource = new MatTableDataSource<ArticleSectionGet>(this.sectionArticles);
  }

  sortSectionArticles() {
    this.loader.startLoader('l-1');
    for (let i = 0; i < this.sectionArticles.length; i++) {
      const article = this.sectionArticles[i];
      article.order = i + 1;
    }
    this.sectionService.sortSectionArticles(this.sectionId, this.sectionArticles).then(data => {
      this.snackBar.open('Section Sort Saved Successfully', 'OK', { duration: 3500 });
    });
    this.sectionArticles.sort((a, b) => a.order - b.order);
    this.sectionDataSource = new MatTableDataSource<ArticleSectionGet>(this.sectionArticles);
    this.sectionDataSource.paginator = this.sectionPaginator;
    this.sectionDataSource.sort = this.sectionSort;
    this.loader.stopLoader('l-1');
  }

  openArticleToSectionModal(id) {
    const article = this.articles.find(a => a.id === id);
    this.modalService.addArticleToSectionOpen({ article: article, sections: this.sections });
    this.modalService.addArticleToSetionConfirmed().subscribe(async (sections: Array<number>) => {
      if (sections !== null && sections !== undefined) {
        const addArticle = new ArticleSection();
        addArticle.articleId = id;
        addArticle.order = 1;
        addArticle.webPortalId = this.webPortalId;
        await sections.forEach(async s => {
          await this.sectionService.addArticleToSection(s, addArticle);
        });
        setTimeout(async () => {
          await this.getAllSectionArticles();
          this.sortSectionArticles();
        }, 1000);
      }
    });
  }

}
