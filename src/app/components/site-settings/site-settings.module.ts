import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuerySettingsComponent } from './query-settings/query-settings.component';
import { SectionComponent } from './section/section.component';
import { FooterSettingsComponent } from './footer-settings/footer-settings.component';
import { WebPortalComponent } from './web-portal/web-portal.component';
import { SiteSettingsRoutingModule } from './site-settings-routing.module';
import { SynonymComponent } from './synonym/synonym.component';
import { AngularMaterialModule } from 'src/app/modules/angular-material/angular-material.module';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ArticleModule } from '../article/article.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { SafeHtmlPipe } from 'src/app/base/safe-html-pipe';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { EditWebPortalComponent } from './web-portal/edit-web-portal/edit-web-portal.component';

@NgModule({
  declarations: [QuerySettingsComponent, SectionComponent, FooterSettingsComponent, WebPortalComponent, SynonymComponent, SafeHtmlPipe, EditWebPortalComponent
  ],
  imports: [
    SharedModule,
    ArticleModule,
    MaterialFileInputModule,
    CommonModule,
    SiteSettingsRoutingModule,
    AngularMaterialModule,
    FormsModule,
    DragDropModule,
    TieredMenuModule,
  ], exports: [SafeHtmlPipe]
})
export class SiteSettingsModule { }
