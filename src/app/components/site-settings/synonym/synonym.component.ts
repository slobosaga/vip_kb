import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable } from 'rxjs';
import { ModalService } from 'src/app/services/modal.service';
import { SynonymService } from 'src/app/services/synonym.service';

@Component({
  selector: 'app-synonym',
  templateUrl: './synonym.component.html',
  styleUrls: ['./synonym.component.scss']
})
export class SynonymComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('synonymForm') synonymForm: NgForm;

  synonymRequest = new Array<string>();
  synonymsList = new Array<{ value: string[], id: number }>();
  dataSource: MatTableDataSource<{ value: string[], id: number }>;
  observable: Observable<any>;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(private synonymService: SynonymService, private snackBar: MatSnackBar, private loader: NgxUiLoaderService, private modalService: ModalService) {
    this.dataSource = new MatTableDataSource<{ value: string[], id: number }>(this.synonymsList);
  }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    this.loader.startLoader('l-1');
    let counter = 0;
    this.synonymsList = new Array<{ value: string[], id: number }>();
    this.synonymRequest = [];
    this.synonymRequest = await this.synonymService.getAllSynonyms();
    this.synonymRequest.forEach(s => {
      const synonym = s.split(',');
      this.synonymsList.push({ value: synonym, id: counter });
      counter++;
    });
    this.dataSource = new MatTableDataSource<{ value: string[], id: number }>(this.synonymsList);
    this.dataSource.data = this.synonymsList;
    this.dataSource.paginator = this.paginator;
    this.observable = this.dataSource.connect();
    this.loader.stopLoader('l-1');
  }

  saveSynonyms() {
    this.synonymForm.form.markAllAsTouched();
    this.loader.startLoader('l-1');
    if (this.synonymForm.invalid === false) {
      this.synonymRequest = new Array<string>();
      this.synonymsList.forEach(sl => {
        if (sl.value.length > 0) {
          this.synonymRequest.push(sl.value.join(','));
        }
      });
      this.synonymService.createSynonym(this.synonymRequest).then(async data => {
        await data;
        this.snackBar.open('Synonyms saved successfully', 'OK', { duration: 3500 });
        this.getData();
        const search = document.getElementById('search') as HTMLInputElement;
        search.value = '';
      });
    } else {
      this.loader.stopLoader('l-1');
    }
  }

  addSynonym() {
    this.synonymForm.form.markAllAsTouched();
    if (this.synonymForm.invalid === false) {
      const synonym = { value: [], id: null };
      this.synonymRequest = new Array<string>();
      this.synonymsList.forEach(sl => {
        this.synonymRequest.push(sl.value.join(','));
      });
      this.synonymsList = new Array<{ value: string[], id: number }>();
      this.synonymRequest.forEach(s => {
        let counter = 0;
        this.synonymsList.push({ value: s.split(','), id: counter });
        counter++;
      });
      this.synonymsList.unshift(synonym);
      this.dataSource = new MatTableDataSource<{ value: string[], id: number }>(this.synonymsList);
      this.dataSource.data = this.synonymsList;
      this.dataSource.paginator = this.paginator;
      this.observable = this.dataSource.connect();
    }
  }

  removeSynonym(i) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Synonym?',
      message: 'Synonyms will be deleted after you click on SAVE SYNONYMS!',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.synonymsList.splice(i, 1);
        this.dataSource.data = this.synonymsList;
        this.dataSource.paginator = this.paginator;
        this.observable = this.dataSource.connect();
      }
    });
  }

  addWord(event: MatChipInputEvent, id: number) {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.synonymsList.find(s => s.id === id).value.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  removeWord(word: string, id: number) {
    const index = this.synonymsList.find(s => s.id === id).value.indexOf(word);
    this.synonymsList.find(s => s.id === id).value.splice(index, 1);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
