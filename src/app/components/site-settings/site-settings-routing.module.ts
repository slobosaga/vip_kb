import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Role } from 'src/app/models/enums/rolesEnum';
import { AuthGuardService as AuthGuard } from 'src/app/services/auth-guard.service';
import { FooterSettingsComponent } from './footer-settings/footer-settings.component';
import { QuerySettingsComponent } from './query-settings/query-settings.component';
import { SectionComponent } from './section/section.component';
import { SynonymComponent } from './synonym/synonym.component';
import { EditWebPortalComponent } from './web-portal/edit-web-portal/edit-web-portal.component';
import { WebPortalComponent } from './web-portal/web-portal.component';


const routes: Routes = [
  {
    path: 'query-management',
    component: QuerySettingsComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'synonyms',
    component: SynonymComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'sections',
    component: SectionComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin] }
  },
  {
    path: 'footer-settings',
    component: FooterSettingsComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin] }
  },
  {
    path: 'web-portals',
    component: WebPortalComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin] }
  },
  {
    path: 'web-portals/edit-web-portal/:id',
    component: EditWebPortalComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin] }
  },
  {
    path: 'web-portals/create-web-portal',
    component: EditWebPortalComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin] }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SiteSettingsRoutingModule {
}
