import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable, ReplaySubject } from 'rxjs';
import { Category } from 'src/app/models/category';
import { Role } from 'src/app/models/role';
import { CreateWebPortalRequest, UpdateWebPortalRequest, WebPortal } from 'src/app/models/webPortal';
import { ArticleService } from 'src/app/services/article.service';
import { ConfigService } from 'src/app/services/config.service';
import { MediaService } from 'src/app/services/media.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-web-portal',
  templateUrl: './web-portal.component.html',
  styleUrls: ['./web-portal.component.scss']
})
export class WebPortalComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('multiSelect', { static: true }) multiSelect: MatSelect;

  webPortals: Array<WebPortal>;
  dataSource: MatTableDataSource<WebPortal>;
  observable: Observable<any>;
  portalType = new Array<{ id: number, name: string }>();
  categories = new Array<Category>();
  filteredCategories: ReplaySubject<Category[]> = new ReplaySubject<Category[]>(1);
  categoriesCount = this.categories.length;
  roles: Array<Role>;
  selectedRoles;
  featuredImageName = 'Select an Image';
  formData = new FormData();
  displayedColumns = ['name', 'portalTypeName', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'action'];

  constructor(private articleService: ArticleService, private cofigService: ConfigService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private modalService: ModalService, private mediaService: MediaService, private router: Router) {
  }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    this.loader.startLoader('l-1');
    await this.getPortalTypes();
    // await this.getAllRoles();
    await this.getAllWebPortals();
    // await this.getAllCategories();
    this.loader.stopLoader('l-1');
  }

  async getAllWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
    this.webPortals.forEach(p => {
      p.portalTypeName = this.portalType.find(t => t.id === p.type).name;
    });
    this.dataSource = new MatTableDataSource<WebPortal>(this.webPortals);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.observable = this.dataSource.connect();
  }

  async getPortalTypes() {
    this.portalType = await this.articleService.getAllProtalTypes();
  }

  async getAllRoles() {
    this.roles = await this.articleService.getAllRoles('false');
  }

  async getAllCategories() {
    this.categories = await this.articleService.getAllCategories('');
    // this.categories = this.categories.sort((a, b) => a.sort - b.sort);
  }

  onRoleSelectionChange(event) {
  }

  addWebPortal() {
    // let enable = true;
    // this.webPortals.forEach(w => {
    //   if (w.id === null) {
    //     enable = false;
    //   }
    // });
    // if (enable === true) {
    //   const portal = new WebPortal();
    //   portal.expanded = true;
    //   this.webPortals.unshift(portal);
    //   this.dataSource = new MatTableDataSource<WebPortal>(this.webPortals);
    //   this.dataSource.paginator = this.paginator;
    //   this.dataSource.sort = this.sort;
    //   this.observable = this.dataSource.connect();
    // }
    this.router.navigate(['site-settings/web-portals/create-web-portal/']);
  }

  cancel(i) {
    this.modalService.confirmDialogOpen({
      title: 'Cancel Web Portal Creation?',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.webPortals.splice(i, 1);
        this.dataSource = new MatTableDataSource<WebPortal>(this.webPortals);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.observable = this.dataSource.connect();
      }
    });
  }

  saveWebPortal(id) {
    const webPortal = this.webPortals.find(w => w.id === id);
    if (webPortal.name && webPortal.type) {
      this.loader.startLoader('l-1');
      if (webPortal.id === null) {
        const portal = new CreateWebPortalRequest();
        portal.logoHref = webPortal.logoHref;
        portal.logoUrl = webPortal.logoUrl;
        portal.name = webPortal.name;
        portal.roles = webPortal.roles;
        portal.webPortalTypeId = webPortal.type;
        portal.categories = webPortal.categories;
        portal.numOfPopularArticles = webPortal.numOfPopularArticles;
        this.cofigService.createWebPortal(portal).then(data => {
          this.loader.stopLoader('l-1');
          this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
          this.getAllWebPortals();
        });
      } else {
        const portal = new UpdateWebPortalRequest();
        portal.name = webPortal.name;
        portal.logoHref = webPortal.logoHref;
        portal.logoUrl = webPortal.logoUrl;
        portal.roles = webPortal.roles;
        portal.categories = webPortal.categories;
        portal.numOfPopularArticles = webPortal.numOfPopularArticles;
        this.cofigService.updateWebPortal(webPortal.id, portal).then(data => {
          this.loader.stopLoader('l-1');
          this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
          this.getAllWebPortals();
        });
      }
    }
  }

  deletePortal() {
    // this.modalService.confirmDialogOpen({
    //   title: 'Delete Web Portal?',
    //   message: 'Are you sure?',
    //   cancelText: 'Cancel',
    //   confirmText: 'Yes'
    // });
    // this.modalService.confirmDialogConfirmed().subscribe(data => {
    //   if (data === true) {
    //     this.cofigService.deleteWebPortal(this.webPortals.find(w => w.id === id).id).then(data => {
    //       this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
    //       this.getAllWebPortals();
    //     });
    //   }
    // });
    this.modalService.deletePortalModalOpen({ webPortals: this.webPortals });
    this.modalService.deletePortalModalConfirmed().subscribe(data => {
      if (data === true) {
        this.getData();
      }
    });
  }

  async setFeaturedImage(event, i) {
    const file = event.target.files[0];
    if (file) {
      this.featuredImageName = file.filename;
      this.formData = new FormData();
      this.formData.append('file', file, file.filename);
      this.mediaService.uploadImage(this.formData).then(data => {
        this.webPortals[i].logoHref = file.filename;
        this.webPortals[i].logoUrl = data.location;
      });
    }
  }

  setPortalCategories(event, index) {
    this.webPortals[index].categories = [];
    event.forEach(e => {
      this.webPortals[index].categories.push(e.id);
    });
  }

  editWebPortal(id) {
    this.router.navigate(['site-settings/web-portals/edit-web-portal/' + id]);
  }

}
