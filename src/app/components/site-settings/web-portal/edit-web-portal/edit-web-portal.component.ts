import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ReplaySubject } from 'rxjs';
import { Category } from 'src/app/models/category';
import { Role } from 'src/app/models/role';
import { CreateWebPortalRequest, UpdateWebPortalRequest, WebPortal } from 'src/app/models/webPortal';
import { ArticleService } from 'src/app/services/article.service';
import { ConfigService } from 'src/app/services/config.service';
import { MediaService } from 'src/app/services/media.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-edit-web-portal',
  templateUrl: './edit-web-portal.component.html',
  styleUrls: ['./edit-web-portal.component.scss']
})
export class EditWebPortalComponent implements OnInit {
  @ViewChild('multiSelect', { static: true }) multiSelect: MatSelect;
  @ViewChild('portalForm') portalForm: NgForm;

  categories = new Array<Category>();
  filteredCategories: ReplaySubject<Category[]> = new ReplaySubject<Category[]>(1);
  categoriesCount = this.categories.length;
  roles = new Array<Role>();
  portalType = new Array<{ id: number, name: string }>();
  selectedRoles;
  featuredImageName = 'Select an Image';
  formData = new FormData();
  webPortal = new WebPortal();
  webPortalId = null;

  constructor(private articleService: ArticleService, private route: ActivatedRoute, private router: Router, private cofigService: ConfigService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private modalService: ModalService, private mediaService: MediaService) {
    if (this.route.snapshot.paramMap.get('id')) {
      this.webPortalId = this.route.snapshot.paramMap.get('id');
    }
  }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    this.loader.startLoader('l-1');
    if (this.webPortalId != null) {
      await this.getWebPortal();
    }
    await this.getAllRoles();
    await this.getPortalTypes();
    this.loader.stopLoader('l-1');
  }

  async getWebPortal() {
    this.webPortal = await this.cofigService.getWebPortal(this.webPortalId);
  }

  async getAllRoles() {
    this.roles = await this.articleService.getAllRoles('false');
  }

  async getAllCategories() {
    this.categories = await this.articleService.getAllCategories('');
  }

  async getPortalTypes() {
    this.portalType = await this.articleService.getAllProtalTypes();
  }

  async setFeaturedImage(event) {
    const file = event.target.files[0];
    if (file) {
      this.featuredImageName = file.filename;
      this.formData = new FormData();
      this.formData.append('file', file, file.filename);
      this.mediaService.uploadImage(this.formData).then(data => {
        this.webPortal.logoHref = file.filename;
        this.webPortal.logoUrl = data.location;
      });
    }
  }

  setPortalCategories(event) {
    this.webPortal.categories = [];
    event.forEach(e => {
      this.webPortal.categories.push(e.id);
    });
  }

  saveWebPortal() {
    if (this.portalForm.form.invalid === false) {
      this.loader.startLoader('l-1');
      if (this.webPortal.id === null) {
        const portal = new CreateWebPortalRequest();
        portal.logoHref = this.webPortal.logoHref;
        portal.logoUrl = this.webPortal.logoUrl;
        portal.name = this.webPortal.name;
        portal.roles = this.webPortal.roles;
        portal.webPortalTypeId = this.webPortal.type;
        portal.categories = this.webPortal.categories;
        portal.numOfPopularArticles = this.webPortal.numOfPopularArticles;
        portal.numOfListedArticles = this.webPortal.numOfListedArticles;
        this.cofigService.createWebPortal(portal).then(data => {
          this.loader.stopLoader('l-1');
          this.router.navigate(['/site-settings/web-portals/edit-web-portal/' + data]);
          this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
        });
      } else {
        const portal = new UpdateWebPortalRequest();
        portal.name = this.webPortal.name;
        portal.logoHref = this.webPortal.logoHref;
        portal.logoUrl = this.webPortal.logoUrl;
        portal.roles = this.webPortal.roles;
        portal.categories = this.webPortal.categories;
        portal.numOfPopularArticles = this.webPortal.numOfPopularArticles;
        portal.numOfListedArticles = this.webPortal.numOfListedArticles;
        this.cofigService.updateWebPortal(this.webPortal.id, portal).then(data => {
          this.loader.stopLoader('l-1');
          this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
        });
      }
    }
  }

  cancel() {
    this.modalService.confirmDialogOpen({
      title: 'Cancel Web Portal Creation?',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.router.navigate(['/site-settings/web-portals']);
      }
    });
  }

}
