import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatMenu } from '@angular/material/menu';
import { TreeNode } from 'src/app/base/models/treeNode';

@Component({
  selector: 'app-category-menu',
  templateUrl: './category-menu.component.html',
  styleUrls: ['./category-menu.component.scss']
})
export class CategoryMenuComponent implements OnInit {
  // tslint:disable: no-output-native
  @ViewChild('menu', { static: true }) menu: MatMenu;
  @Input() items: TreeNode;
  @Output() categoryEvent: EventEmitter<number> = new EventEmitter<number>();
  @Output() mouseenter: EventEmitter<boolean> = new EventEmitter<boolean>();

  isMatMenuOpen: boolean;
  isMatMenu2Open: any;
  timedOutCloser: any;

  constructor() { }

  ngOnInit(): void {
  }

  selectCategory(id) {
    this.categoryEvent.emit(id);
  }

  mouseEnter(trigger) {
    if (this.timedOutCloser) {
      clearTimeout(this.timedOutCloser);
    }
    trigger.openMenu();
  }

  mouseLeave(trigger) {
    this.timedOutCloser = setTimeout(() => {
      trigger.closeMenu();
    }, 400);
  }

}
