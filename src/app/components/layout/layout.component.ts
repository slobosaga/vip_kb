import { AfterViewInit, ChangeDetectorRef, Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { CookieService } from 'ngx-cookie-service';
import { Role } from 'src/app/models/enums/rolesEnum';
import { WebPortal } from 'src/app/models/webPortal';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReportingService } from '../../services/reporting.service';
import { ReportTypesResponse } from '../../models/reports';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {
  @ViewChild('langLocal') langLocal;

  userFullName: string;
  languages;
  language;
  enteredButton = false;
  isMatMenuOpen = false;
  isMatMenu2Open = false;
  prevButtonTrigger;
  showLanguage = true;
  systemAdmin = [Role.SystemAdmin];
  admin = [Role.SystemAdmin, Role.Admin];
  supervisor = [Role.SystemAdmin, Role.Admin, Role.Supervisor];
  writer = [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer];
  reader = [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer, Role.Reader];
  webPortals = new Array<WebPortal>();
  defaultLanguage = JSON.parse(localStorage.getItem('A1_Admin_defaultLanguage'));
  defaultWebPortal = JSON.parse(localStorage.getItem('A1_Admin_defaultWebPortal'));
  isSystemAdmin = JSON.parse(localStorage.getItem('A1_Admin_isSystemAdmin'));
  reportMenus = new Array<ReportTypesResponse>();

  constructor(private router: Router, private cookieService: CookieService, private cdRef: ChangeDetectorRef, private snackBar: MatSnackBar, private articleService: ArticleService, private userService: UserService, private eventEmitter: EventEmitterService, private ren: Renderer2, private reportService: ReportingService) {
    this.userFullName = localStorage.getItem('A1_Admin_Username');
    this.languages = JSON.parse(localStorage.getItem('A1_Admin_languages'));
    if (!localStorage.getItem('A1_Admin_language')) {
      this.language = this.defaultLanguage;
      if (this.language === null) {
        this.language = this.languages[0].id;
      }
      localStorage.setItem('A1_Admin_language', JSON.stringify(this.language));
    } else {
      this.language = JSON.parse(localStorage.getItem('A1_Admin_language'));
    }

  }
  ngAfterViewInit(): void {
    this.cdRef.checkNoChanges();
  }

  async ngOnInit() {
    if (this.isSystemAdmin === false) {
      this.getWebPortals();
    }
    this.showHideLanguage();
    this.reportMenus = await this.reportService.getReportTypes();
  }

  private showHideLanguage() {
    if (this.router.url === '/application-settings/create-tenant'
      || this.router.url === '/site-settings/web-portals'
      || this.router.url === '/application-settings/roles'
      || this.router.url === '/site-settings/footer-settings'
      || this.router.url === '/application-settings/register-user'
      || this.router.url.match('/articles/categories')
      || this.router.url.match('/articles/templates')
      || this.router.url === '/site-settings/synonyms'
      || this.router.url === '/articles/comments'
      || this.router.url.match('/site-settings/web-portals/edit-web-portal')
      || this.router.url.match('site-settings/web-portals/create-web-portal')
      || this.router.url.match('/articles/create-article')) {
      this.showLanguage = false;
    } else {
      this.showLanguage = true;
    }
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event.url === '/application-settings/create-tenant'
          || event.url === '/site-settings/web-portals'
          || event.url === '/application-settings/roles'
          || event.url === '/site-settings/footer-settings'
          || event.url === '/application-settings/register-user'
          || event.url.match('/articles/categories')
          || event.url.match('/articles/templates')
          || event.url === '/site-settings/synonyms'
          || event.url === '/articles/comments'
          || event.url.match('/articles/create-article')) {
          this.showLanguage = false;
        } else {
          this.showLanguage = true;
        }
      }
    });
    if (this.isSystemAdmin === true) {
      this.showLanguage = false;
    }
  }

  async getWebPortals() {
    this.webPortals = await this.articleService.getAllWebPortals();
    const defaultPortal = this.webPortals.find(p => p.id === this.defaultWebPortal);
    if (!defaultPortal) {
      localStorage.setItem('A1_Admin_defaultWebPortal', null);
      this.defaultWebPortal = null;
    }
  }

  setLanguage(event) {
    this.language = event;
    localStorage.setItem('A1_Admin_language', this.language);
    const ele = document.getElementById(event);

    this.eventEmitter.emitLanguageId.emit(this.language);
  }

  setDefaultLanguage(id) {
    this.defaultLanguage = id;
    this.userService.updateUserProfile(this.defaultWebPortal, this.defaultLanguage).then(data => {
      if (data) {
        localStorage.setItem('A1_Admin_defaultLanguage', JSON.stringify(this.defaultLanguage));
        this.snackBar.open('Default Language Updated. Login again to see changes.', 'OK');
      }
    });
  }

  setDefaultWebPortal(id) {
    this.defaultWebPortal = id;
    this.userService.updateUserProfile(this.defaultWebPortal, this.defaultLanguage).then(data => {
      if (data) {
        localStorage.setItem('A1_Admin_defaultWebPortal', JSON.stringify(this.defaultWebPortal));
        this.snackBar.open('Default Web Portal Updated. Login again to see changes.', 'OK');
      }
    });
  }

  menuenter() {
    this.isMatMenuOpen = true;
    if (this.isMatMenu2Open) {
      this.isMatMenu2Open = false;
    }
  }

  menuLeave(trigger, button) {
    setTimeout(() => {
      if (!this.isMatMenu2Open && !this.enteredButton) {
        this.isMatMenuOpen = false;
        trigger.closeMenu();
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-program-focused');
      } else {
        this.isMatMenuOpen = false;
      }
    }, 80);
  }

  menu2enter() {
    this.isMatMenu2Open = true;
  }

  menu2Leave(trigger1, trigger2, button) {
    setTimeout(() => {
      if (this.isMatMenu2Open) {
        trigger1.closeMenu();
        this.isMatMenuOpen = false;
        this.isMatMenu2Open = false;
        this.enteredButton = false;
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-program-focused');
      } else {
        this.isMatMenu2Open = false;
        trigger2.closeMenu();
      }
    }, 100);
  }

  buttonEnter(trigger) {
    setTimeout(() => {
      if (this.prevButtonTrigger && this.prevButtonTrigger != trigger) {
        this.prevButtonTrigger.closeMenu();
        this.prevButtonTrigger = trigger;
        this.isMatMenuOpen = false;
        this.isMatMenu2Open = false;
        trigger.openMenu();
        this.ren.removeClass(trigger.menu.items.first['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(trigger.menu.items.first['_elementRef'].nativeElement, 'cdk-program-focused');
      } else if (!this.isMatMenuOpen) {
        this.enteredButton = true;
        this.prevButtonTrigger = trigger;
        trigger.openMenu();
        this.ren.removeClass(trigger.menu.items.first['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(trigger.menu.items.first['_elementRef'].nativeElement, 'cdk-program-focused');
      } else {
        this.enteredButton = true;
        this.prevButtonTrigger = trigger;
      }
    });
  }

  buttonLeave(trigger, button) {
    setTimeout(() => {
      if (this.enteredButton && !this.isMatMenuOpen) {
        trigger.closeMenu();
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-program-focused');
      }
      if (!this.isMatMenuOpen) {
        trigger.closeMenu();
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-program-focused');
      } else {
        this.enteredButton = false;
      }
    }, 100);
  }

  logout() {
    this.router.navigate(['/login']);
    setTimeout(() => {
      localStorage.removeItem('A1_Admin_accessToken');
      localStorage.removeItem('A1_Admin_defaultLanguage');
      localStorage.removeItem('A1_Admin_defaultWebPortal');
      localStorage.removeItem('A1_Admin_expiresIn');
      localStorage.removeItem('A1_Admin_isSystemAdmin');
      localStorage.removeItem('A1_Admin_a1kbRoles');
      localStorage.removeItem('A1_Admin_languages');
      this.cookieService.deleteAll();
    }, 500);
  }

}
