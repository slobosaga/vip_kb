import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-relevance-tuning',
  templateUrl: './relevance-tuning.component.html',
  styleUrls: ['./relevance-tuning.component.scss']
})
export class RelevanceTuningComponent implements OnInit {
  bodyKeywords: Array<string>;
  subjectKeywords: Array<string>;
  constructor() {
    this.bodyKeywords = new Array<string>();
    this.subjectKeywords = new Array<string>();
  }

  ngOnInit(): void {
  }

  addBodyBoostBtn() {
    const keyword = '';
    this.bodyKeywords.push(keyword);
  }

  removeBodyKeyword(keyword) {
    this.bodyKeywords.splice(this.bodyKeywords.indexOf(keyword), 1);
  }

  removeSubjectKeyword(keyword) {
    this.subjectKeywords.splice(this.subjectKeywords.indexOf(keyword), 1);
  }

  addSubjectBoostBtn() {
    const keyword = '';
    this.subjectKeywords.push(keyword);
  }

  reset() {
    this.subjectKeywords = new Array<string>();
    this.bodyKeywords = new Array<string>();
  }

}
