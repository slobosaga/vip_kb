import { AfterViewInit, Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { jsPlumb } from 'jsplumb';
import { GuidedHelpAnswerType, GuidedHelpQuestion, GuidedHelpQuestionType } from 'src/app/models/guided-help.model';
import { ModalService } from 'src/app/services/modal.service';
declare var $: any;


@Component({
  selector: 'app-guided-help',
  templateUrl: './guided-help.component.html',
  styleUrls: ['./guided-help.component.scss']
})
export class GuidedHelpComponent implements OnInit, AfterViewInit {
  @ViewChild('questions', { read: ViewContainerRef, static: true }) viewContainerRef: ViewContainerRef;

  exampleDragElements: Array<{ question: string, answers: Array<{ answer: string, id: string }> }> = new Array<{ question: string, answers: Array<{ answer: string, id: string }> }>();
  jsPlumbInstance = jsPlumb.getInstance({ Container: 'container', });
  zoom = 1.00;
  static newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      // tslint:disable-next-line: no-bitwise
      const r = Math.random() * 16 | 0;
      // tslint:disable-next-line: no-bitwise
      const v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  constructor(private modalService: ModalService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.jsPlumbInstance.setContainer(document.getElementById('container'));
  }

  addNewEle() {
    this.modalService.questionModalOpen({
      question: new GuidedHelpQuestion(),
      questionTypes: new Array<GuidedHelpQuestionType>(),
      answerTypes: new Array<GuidedHelpAnswerType>()
    });
    // const answers = [
    //   { id: GuidedHelpComponent.newGuid(), answer: 'Molim te odgovor 1' },
    //   { id: GuidedHelpComponent.newGuid(), answer: 'Molim te odgovor 2' },
    //   { id: GuidedHelpComponent.newGuid(), answer: 'Molim te odgovor 3' }
    // ];
    // this.exampleDragElements.unshift({ question: GuidedHelpComponent.newGuid(), answers: answers });
    // const exampleDropOptions = {
    //   tolerance: 'touch',
    //   hoverClass: 'dropHover',
    //   activeClass: 'dragActive'
    // };
    // const exampleGreyEndpointOptionsSource = {
    //   paintStyle: { fill: '#99cb3a' },
    //   connectorStyle: { stroke: '#99cb3a', strokeWidth: 3 },
    //   maxConnections: 30,
    //   isSource: true,
    //   isTarget: false,
    //   dropOptions: exampleDropOptions,
    //   listStyle: {
    //     endpoint: ['Rectangle', { width: 30, height: 30 }]
    //   }
    // };
    // const exampleGreyEndpointOptionsTarget = {
    //   paintStyle: { width: 25, height: 16, fill: '#ccc' },
    //   connectorStyle: { stroke: '#ccc' },
    //   maxConnections: 30,
    //   isSource: false,
    //   isTarget: true,
    //   dropOptions: exampleDropOptions,
    //   listStyle: {
    //     endpoint: ['Rectangle', { width: 30, height: 30 }]
    //   }
    // };
    // setTimeout(() => {
    //   this.jsPlumbInstance.addEndpoint(guid, {
    //     anchor: 'Bottom',
    //     endpoint: ['Dot', { radius: 7 }],
    //     maxConnections: 30,
    //     connectorOverlays: [['Arrow', { location: 1 }]],
    //     connector: ['Bezier', { curviness: 63 }],
    //     uuid: guid + '_bottom',
    //   }, exampleGreyEndpointOptionsSource);
    //   this.jsPlumbInstance.addEndpoint(guid, {
    //     anchor: 'TopCenter',
    //     endpoint: 'Dot',
    //     maxConnections: 30,
    //     connectorOverlays: [['Arrow', { location: 1 }]],
    //     uuid: guid + '_top'
    //   }, exampleGreyEndpointOptionsTarget);
    //   this.jsPlumbInstance.draggable(guid);
    // }, 0);
  }

  saveAsJSON() {
    const container = this.viewContainerRef.element.nativeElement.parentElement as Document;
    const nodes = Array.from(container.querySelectorAll('.questions')).map((node: HTMLDivElement) => {
      return {
        id: node.id,
        top: node.offsetTop,
        left: node.offsetLeft,
      };
    });
    const connections = (this.jsPlumbInstance.getAllConnections() as any[]).map((conn) => ({ id: conn.getUuids() }));
    const json = JSON.stringify({ nodes, connections });
  }

  zoomInOut(event: WheelEvent) {
    if (event.deltaY > 0 && event.ctrlKey === true) {
      event.preventDefault();
      this.zoom = this.zoom - 0.01;
    } else if (event.deltaY < 0 && event.ctrlKey === true) {
      event.preventDefault();
      this.zoom = this.zoom + 0.01;
    }
    this.jsPlumbInstance.setZoom(this.zoom);
    $('.drag-zone').css({
      '-webkit-transform': `scale(${this.zoom})`,
      '-moz-transform': `scale(${this.zoom})`,
      '-ms-transform': `scale(${this.zoom})`,
      '-o-transform': `scale(${this.zoom})`,
      transform: `scale(${this.zoom})`
    });
  }

}
