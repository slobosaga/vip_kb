import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable } from 'rxjs';
import { Role } from 'src/app/models/role';
import { User } from 'src/app/models/user';
import { ConfigService } from 'src/app/services/config.service';
import { LoginSerivce } from 'src/app/services/login.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  users: Array<User>;
  user = new User();
  roles = new Array<Role>();
  dataSource: MatTableDataSource<User>;
  cols = ['firstName', 'lastName', 'email', 'phone', 'isAdUser', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'lastLoginTime', 'action'];

  constructor(private configService: ConfigService, private snackBar: MatSnackBar, private loginService: LoginSerivce, private modalService: ModalService, private loader: NgxUiLoaderService, private router: Router,) { }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    this.loader.startLoader('l-1');
    this.users = await this.loginService.getAllUsers();
    this.dataSource = new MatTableDataSource<User>(this.users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loader.stopLoader('l-1');
  }

  async getAllUsers() {
    this.loader.startLoader('l-1');
    this.users = await this.loginService.getAllUsers();
    this.dataSource = new MatTableDataSource<User>(this.users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loader.stopLoader('l-1');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async getRoles() {
    this.roles = await this.configService.getAllRoles();
  }


  editUser(id, event) {
    if (event.target.id !== 'delete') {
      this.router.navigate(['/application-settings/edit-user/' + id]);
    }
  }

  registerUser() {
    this.router.navigate(['/application-settings/register-user/']);
  }

  deleteUser(id) {
    this.modalService.confirmDialogOpen({
      title: 'Cancel',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.loader.startLoader('l-1');
        this.loginService.deleteUser(id).then(data => {
          this.snackBar.open('User deleted successfully', 'OK', { duration: 3500 });
          this.getAllUsers();
          this.loader.stopLoader('l-1');
        }).catch(err => {
          this.loader.stopLoader('l-1');
        });
      }
    });
  }
}
