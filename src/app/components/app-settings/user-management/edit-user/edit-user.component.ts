import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Role } from 'src/app/models/role';
import { User } from 'src/app/models/user';
import { ConfigService } from 'src/app/services/config.service';
import { LoginSerivce } from 'src/app/services/login.service';
import { Location } from '@angular/common';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  @ViewChild('userForm') userForm: NgForm;

  userId = null;
  user: User;
  roles = new Array<Role>();

  constructor(private route: ActivatedRoute, private modalService: ModalService, private location: Location, private snackBar: MatSnackBar, private configService: ConfigService, private loader: NgxUiLoaderService, private loginService: LoginSerivce) {
    if (this.route.snapshot.paramMap.get('id')) {
      this.userId = this.route.snapshot.paramMap.get('id');
    }
    this.user = new User();
  }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    if (this.userId !== null) {
      this.getUser();
    }
    this.getRoles();
  }

  async getUser() {
    this.user = await this.loginService.getUser(this.userId);
  }

  async getRoles() {
    this.roles = await this.configService.getAllRoles('false');
  }

  async save() {
    if (this.userForm.form.valid) {
      if (this.userId !== null) {
        this.updateUser();
      } else {
        this.registerUser();
      }
    }
  }

  registerUser() {
    this.loader.startLoader('l-1');
    this.loginService.registerUser(this.user).then(data => {
      if (data) {
        this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
        this.location.back();
        this.loader.stopLoader('l-1');
      }
      this.loader.stopLoader('l-1');
    }
    );
  }

  updateUser() {
    this.loader.startLoader('l-1');
    this.loginService.updateUser(this.user).then(data => {
      if (data) {
        this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
        this.location.back();
        this.loader.stopLoader('l-1');
      } else {
        this.loader.stopLoader('l-1');
      }
    }
    );
  }

  cancel() {
    if (this.user.isAdUser === false) {
      this.modalService.confirmDialogOpen({
        title: 'Cancel',
        message: 'Are you sure?',
        cancelText: 'Cancel',
        confirmText: 'Yes'
      });
      this.modalService.confirmDialogConfirmed().subscribe(data => {
        if (data === true) {
          this.location.back();
        }
      });
    } else {
      this.location.back();
    }
  }


}
