import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppSettingsComponent } from './app-settings.component';
import { AuthGuardService as AuthGuard } from '../../services/auth-guard.service';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { RoleComponent } from './role/role.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { FileShareConfigComponent } from './file-share-config/file-share-config.component';
import { EditAppSettingsComponent } from './edit-app-settings/edit-app-settings.component';
import { EditEmailTemplateComponent } from './email-template/edit-email-template/edit-email-template.component';
import { EditUserComponent } from './user-management/edit-user/edit-user.component';
import { Role } from 'src/app/models/enums/rolesEnum';

const routes: Routes = [
  {
    path: 'tenant',
    component: AppSettingsComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin], isSystemAdmin: true }
  },
  {
    path: 'create-tenant',
    component: EditAppSettingsComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin], isSystemAdmin: true }
  },
  {
    path: 'edit-tenant/:id',
    component: EditAppSettingsComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin], isSystemAdmin: true }
  },
  {
    path: 'view-tenant/:id',
    component: EditAppSettingsComponent,
    canActivate: [AuthGuard],
    data: { readonly: true, role: [Role.SystemAdmin, Role.Admin], isSystemAdmin: true },
  },
  {
    path: 'email-templates',
    component: EmailTemplateComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'create-email-template',
    component: EditEmailTemplateComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'edit-email-template/:id',
    component: EditEmailTemplateComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'view-email-template/:id',
    component: EditEmailTemplateComponent,
    canActivate: [AuthGuard],
    data: { readonly: true, role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'roles',
    component: RoleComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'user-management',
    component: UserManagementComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'edit-user/:id',
    component: EditUserComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'register-user',
    component: EditUserComponent,
    canActivate: [AuthGuard],
    data: { role: [Role.SystemAdmin, Role.Admin, Role.Supervisor] }
  },
  {
    path: 'media-management',
    component: FileShareConfigComponent,
    canActivate: [AuthGuard],
    data: { isSystemAdmin: false }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})

export class AppSettingsRoutingModule { }
