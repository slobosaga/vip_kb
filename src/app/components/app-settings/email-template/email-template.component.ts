import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subscription } from 'rxjs';
import { EmailTemplate } from 'src/app/models/emailTemplate';
import { ConfigService } from 'src/app/services/config.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.scss']
})
export class EmailTemplateComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  emailTemplates: Array<EmailTemplate>;
  dataSource: MatTableDataSource<EmailTemplate>;
  emailTypes: any;
  displayedColumns = ['subject', 'createdBy', 'createdDate', 'updatedBy', 'updatedDate', 'action'];
  languageSubscriber: Subscription;

  constructor(private loader: NgxUiLoaderService, private eventEmitter: EventEmitterService, private router: Router, private configService: ConfigService, private snackBar: MatSnackBar, private modalService: ModalService) { }

  ngOnDestroy(): void {
    this.languageSubscriber.unsubscribe();
  }

  ngOnInit(): void {
    this.getData();
    this.languageSubscriber = this.eventEmitter.emitLanguageId.subscribe(() => {
      this.getData();
    });
  }

  async getData() {
    this.loader.startLoader('l-1');
    await this.getAllEmailTemplates();
    this.loader.stopLoader('l-1');
  }

  async getAllEmailTemplates() {
    this.emailTemplates = await this.configService.getAllEmailTemplates();
    this.dataSource = new MatTableDataSource<EmailTemplate>(this.emailTemplates);
    if (this.emailTemplates !== null) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async getAllEmailTypes() {
    this.emailTypes = await this.configService.getAllEmailTypes();
  }

  editTemplate(id) {
    this.router.navigate(['/application-settings/edit-email-template/' + id]);
  }

  createTemplate() {
    this.router.navigate(['/application-settings/create-email-template/']);
  }

  viewTemplate(id, event) {
    if (event.target.id !== 'delete') {
      this.router.navigate(['/application-settings/view-email-template/' + id]);
    }
  }

  addTemplate() {
    const template = new EmailTemplate();
    this.emailTemplates.push(template);
  }

  async createEmailTemplate(i) {
    this.configService.createEmailTemplate(this.emailTemplates[i]).then(data => {
      if (data) {
        this.snackBar.open('Saved Successfully', 'OK', { duration: 3500 });
        this.getData();
      }
    });
  }

  async updateTemplate(id) {
    this.configService.createEmailTemplate(this.emailTemplates.find(et => et.id === id)).then(data => {
      if (data) {
        this.snackBar.open('Saved Successfully', 'OK', { duration: 3500 });
        this.getData();
      }
    });
  }

  saveEmailTemplate(i, id) {
    if (id === null) {
      this.createEmailTemplate(i);
    } else {
      this.updateTemplate(id);
    }
  }

  async deleteTemplate(id) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Email Template?',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.configService.deleteEmailTemplate(id).then(res => {
          if (res) {
            this.snackBar.open('Deleted Successfully', 'OK', { duration: 3500 });
            this.getData();
          }
        });
      }
    });
  }

  cancel(i) {
    this.modalService.confirmDialogOpen({
      title: 'Cancel creation?',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.emailTemplates.splice(i, 1);
        this.snackBar.open('Deleted Successfully', 'OK', { duration: 3500 });
      }
    });
  }

}
