import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { EmailTemplate, EmailTemplatePlaceholder } from 'src/app/models/emailTemplate';
import { ConfigService } from 'src/app/services/config.service';
import { ModalService } from 'src/app/services/modal.service';
import { Location } from '@angular/common';
import { AppConfigService } from 'src/app/base/configuration.service';
import { environment } from './../../../../../environments/environment';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EventEmitterService } from 'src/app/services/event-emitter.service';


@Component({
  selector: 'app-edit-email-template',
  templateUrl: './edit-email-template.component.html',
  styleUrls: ['./edit-email-template.component.scss']
})
export class EditEmailTemplateComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('templates') templates: NgForm;

  emailTemplateId = null;
  readonly = false;
  emailTemplateTypes: any;
  emailTemplate: EmailTemplate = null;
  emailTemplatePlaceholders = new Array<EmailTemplatePlaceholder>();
  ckEditorUrl: string = null;
  languageSub: Subscription;

  constructor(private loader: NgxUiLoaderService, private route: ActivatedRoute, private location: Location, private configService: ConfigService, private snackBar: MatSnackBar, private modalService: ModalService, private appConfigService: AppConfigService, private eventEmitterService: EventEmitterService) {
    if (this.route.snapshot.paramMap.get('id')) {
      this.emailTemplateId = this.route.snapshot.paramMap.get('id');
    }
    if (this.route.snapshot.data?.readonly) {
      this.readonly = this.route.snapshot.data?.readonly;
    }
    this.emailTemplate = new EmailTemplate();
    if (environment.production === true) {
      this.ckEditorUrl = appConfigService.ckEditorUrlProd;
    } else {
      this.ckEditorUrl = appConfigService.ckEditorUrlDev;
    }
  }

  ngOnDestroy(): void {
    localStorage.removeItem('A1_Admin_placeholders');
    this.languageSub.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.loader.startLoader('l-1');
    setTimeout(() => {
      this.getData();
      this.loader.stopLoader('l-1');
    }, 0);
  }

  ngOnInit(): void {
    this.languageSub = this.eventEmitterService.emitLanguageId.subscribe(lang => {
      if (lang) {
        this.getData();
      }
    });
  }

  async getData() {
    if (this.emailTemplateId !== null) {
      await this.getEmailTemplate();
    }
    await this.getAllEmailTypes();
    await this.getAllEmailTemplatesPlaceholders();
  }

  async getEmailTemplate() {
    this.emailTemplate = await this.configService.getEmailTemplate(this.emailTemplateId);
  }

  async getAllEmailTypes() {
    this.emailTemplateTypes = await this.configService.getAllEmailTypes();
    if (this.emailTemplateTypes.length === 0) {
      this.snackBar.open('You have created every TEMPLATE TYPE for this LANGUAGE.', 'OK', { duration: 4500 });
      this.location.back();
    }
  }

  async getAllEmailTemplatesPlaceholders() {
    this.emailTemplatePlaceholders = await this.configService.getAllEmailTemplatePlaceholders();
    const tinyPlaceholders = new Array<{ text: string, value: string }>();
    this.emailTemplatePlaceholders.forEach(p => {
      tinyPlaceholders.push({ text: p.name, value: p.value });
    });
    localStorage.setItem('A1_Admin_placeholders', JSON.stringify(tinyPlaceholders));
  }

  async createEmailTemplate() {
    if (this.emailTemplate.body && this.templates.form.valid === true) {
      this.configService.createEmailTemplate(this.emailTemplate).then(data => {
        if (data) {
          this.snackBar.open('Saved Successfully', 'OK', { duration: 3500 });
          this.location.back();
        }
      });
    } else {
      this.snackBar.open('Template Editor cannot be empty', 'OK', { duration: 3500 });
    }
  }

  async updateTemplate() {
    if (this.emailTemplate.body && this.templates.form.valid === true) {
      this.configService.updateEmailTemplate(this.emailTemplate, this.emailTemplateId).then(data => {
        if (data) {
          this.snackBar.open('Saved Successfully', 'OK', { duration: 3500 });
          this.location.back();
        }
      });
    } else {
      this.snackBar.open('Template Editor cannot be empty', 'OK', { duration: 3500 });
    }
  }

  save() {
    if (this.emailTemplateId !== null) {
      this.updateTemplate();
    } else {
      this.createEmailTemplate();
    }
  }

  async ckFileUpload(event) {
    const fileLoader = event.data.fileLoader;
    const formData = new FormData();
    const xhr = fileLoader.xhr;
    xhr.open('POST', fileLoader.uploadUrl, true);
    xhr.setRequestHeader('Content-Disposition', 'miltipart/form-data');
    xhr.setRequestHeader('X-File-Name', fileLoader.fileName);
    xhr.setRequestHeader('X-File-Size', fileLoader.total);
    xhr.setRequestHeader('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken'));
    formData.append('file', fileLoader.file, fileLoader.fileName);
    xhr.send(formData);
    event.stop();
  }

  ckFileResponse(event) {
    event.stop();
    const data = event.data;
    const xhr = data.fileLoader.xhr;
    let response = xhr.responseText;
    response = JSON.parse(response);
    data.url = location.origin + response.url;
  }


}
