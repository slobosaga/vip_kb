import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Location } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { AutocompleteConfig, ElasticConfig, EmailConfig, EmailTemplateConfig, FileShareConfig, GeneralConfig, LdapConfigRequest, ReportConfig, ScheduleConfig, SectionConfig, SshConfig } from 'src/app/models/configurations';
import { Language } from 'src/app/models/enums/language';
import { TenantRequest, TenantView } from 'src/app/models/tenant';
import { ArticleService } from 'src/app/services/article.service';
import { ConfigService } from 'src/app/services/config.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-edit-app-settings',
  templateUrl: './edit-app-settings.component.html',
  styleUrls: ['./edit-app-settings.component.scss']
})
export class EditAppSettingsComponent implements OnInit {
  @ViewChild('tenantForm') tenantForm: NgForm;

  tenantId = null;
  readonly = false;
  tenant: TenantView;
  languages;
  selectedLanguages: number[] = [];
  emailCategoryTypes;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(private route: ActivatedRoute, private el: ElementRef, private articleService: ArticleService, private location: Location, private configService: ConfigService, private modalService: ModalService, private snackBar: MatSnackBar) {
    this.tenantId = this.route.snapshot.paramMap.get('id');
    if (this.route.snapshot.data?.readonly) {
      this.readonly = this.route.snapshot.data?.readonly;
    }
    this.tenant = new TenantView();
  }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    if (this.tenantId != null) {
      await this.getTenant();
    }
    this.getLanguages();
    this.getAllEmailCategoryViewTypes();
  }

  async getTenant() {
    this.tenant = await this.configService.getTenant(this.tenantId);
    this.tenant.tenantConfig.languages.forEach(l => {
      this.selectedLanguages.push(l.id);
    });
    if (this.tenant.tenantConfig.emailTemplateConfig.categoryView !== null) {
      this.tenant.tenantConfig.emailTemplateConfig.categoryView = this.tenant.tenantConfig.emailTemplateConfig.categoryView.toUpperCase();
    }
    if (this.tenant.elasticConfig === null) {
      this.tenant.elasticConfig = new ElasticConfig();
    }
    if (this.tenant.tenantConfig.autocompleteConfig === null) {
      this.tenant.tenantConfig.autocompleteConfig = new AutocompleteConfig();
    }
    if (this.tenant.tenantConfig.emailConfig === null) {
      this.tenant.tenantConfig.emailConfig = new EmailConfig();
    }
    if (this.tenant.tenantConfig.sshConfig === null) {
      this.tenant.tenantConfig.sshConfig = new SshConfig();
    }
    if (this.tenant.tenantConfig.scheduleConfig === null) {
      this.tenant.tenantConfig.scheduleConfig = new ScheduleConfig();
    }
    if (this.tenant.tenantConfig.emailTemplateConfig === null) {
      this.tenant.tenantConfig.emailTemplateConfig = new EmailTemplateConfig();
    }
    if (this.tenant.tenantConfig.sectionConfig === null) {
      this.tenant.tenantConfig.sectionConfig = new SectionConfig();
    }
    if (this.tenant.tenantConfig.generalConfig === null) {
      this.tenant.tenantConfig.generalConfig = new GeneralConfig();
    }
    if (this.tenant.tenantConfig.ldapConfigs === null) {
      this.tenant.tenantConfig.ldapConfigs = new Array<LdapConfigRequest>();
    }
    if (this.tenant.tenantConfig.languages === null) {
      this.tenant.tenantConfig.languages = new Array<Language>();
    }
    if (this.tenant.tenantConfig.reportConfig == null) {
      this.tenant.tenantConfig.reportConfig = new ReportConfig();
    }
    if (this.tenant.tenantConfig.reportConfig.emailList === null) {
      this.tenant.tenantConfig.reportConfig.emailList = new Array<string>();
    }
  }

  async getLanguages() {
    this.languages = await this.articleService.getAllLanguages('true');
  }

  async getAllEmailCategoryViewTypes() {
    this.emailCategoryTypes = await this.configService.getAllEmailCategoryViewTypes();
  }

  addLdapConfgis() {
    const ldap = new LdapConfigRequest();
    this.tenant.tenantConfig.ldapConfigs.push(ldap);
  }

  removeLdapConfigs(i) {
    this.modalService.confirmDialogOpen({
      title: 'Remove LDAP Configuration',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.tenant.tenantConfig.ldapConfigs.splice(i, 1);
      }
    });
  }

  addLanguages() {
    const lang = new Language();
    this.tenant.tenantConfig.languages.push(lang);
  }

  removeLanguage(i) {
    this.modalService.confirmDialogOpen({
      title: 'Remove Translation',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.tenant.tenantConfig.languages.splice(i, 1);
      }
    });
  }

  addLanguagesTenantConfig() {
    const lang = new Language();
    this.tenant.tenantConfig.languages.push(lang);
  }

  removeLanguageTenantConfig(i) {
    this.modalService.confirmDialogOpen({
      title: 'Remove Translation',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.tenant.tenantConfig.languages.splice(i, 1);
      }
    });
  }

  saveTenant() {
    if (this.tenantForm.form.valid === true) {
      if (this.tenant.tenantConfig.sendSuggestionConfig.emailTo.length > 0) {
        this.setTenantLanguages();
        if (this.tenantId != null) {
          this.configService.updateTenat(this.tenant, this.tenantId).then(data => {
            if (data !== undefined) {
              this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
              this.location.back();
            }
          }).catch(err => {
          });
        } else {
          this.configService.createTenant(this.tenant).then(data => {
            if (data !== undefined) {
              this.snackBar.open('Saved Successfully', 'OK', { duration: 3000 });
              this.location.back();
            }
          }).catch(err => {
          });
        }
      } else {
        const ele = document.getElementById('sendSuggestionToListInput');
        ele.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
        ele.focus();
        this.snackBar.open('At least one email is required in the Email To list', 'OK', {duration: 4000});
      }
    } else {
      for (const key of Object.keys(this.tenantForm.controls)) {
        if (this.tenantForm.controls[key].invalid) {
          const invalidControl = this.el.nativeElement.querySelector('[name="' + key + '"]');
          invalidControl.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
        }
      }
    }
  }

  setTenantLanguages() {
    this.tenant.tenantConfig.languages = new Array<Language>();
    this.selectedLanguages.forEach(s => {
      const lang = new Language();
      lang.id = s;
      lang.name = this.languages.find(l => l.id === s).name;
      this.tenant.tenantConfig.languages.push(lang);
    });
  }

  addReportEmail(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      if (this.tenant.tenantConfig.reportConfig.emailList === null) {
        this.tenant.tenantConfig.reportConfig.emailList = [];
      }
      this.tenant.tenantConfig.reportConfig.emailList.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }


  removeReportEmail(keyword: string): void {
    const index = this.tenant.tenantConfig.reportConfig.emailList.indexOf(keyword);
    if (index >= 0) {
      this.tenant.tenantConfig.reportConfig.emailList.splice(index, 1);
    }
  }

  pasteReportEmail(event: ClipboardEvent): void {
    event.preventDefault();
    event.clipboardData.getData('Text').split(/;|,|\n/).forEach(value => {
      if (value.trim()) {
        this.tenant.tenantConfig.reportConfig.emailList.push(value.trim());
      }
    });
  }

  addSuggestionEmailTo(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      if (this.tenant.tenantConfig.sendSuggestionConfig.emailTo === null) {
        this.tenant.tenantConfig.sendSuggestionConfig.emailTo = [];
      }
      this.tenant.tenantConfig.sendSuggestionConfig.emailTo.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  addSuggestionEmailCc(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      if (this.tenant.tenantConfig.sendSuggestionConfig.emailCc === null) {
        this.tenant.tenantConfig.sendSuggestionConfig.emailCc = [];
      }
      this.tenant.tenantConfig.sendSuggestionConfig.emailCc.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }


  removeSuggestionEmailTo(keyword: string): void {
    const index = this.tenant.tenantConfig.sendSuggestionConfig.emailTo.indexOf(keyword);
    if (index >= 0) {
      this.tenant.tenantConfig.sendSuggestionConfig.emailTo.splice(index, 1);
    }
  }

  removeSuggestionEmailCc(keyword: string): void {
    const index = this.tenant.tenantConfig.sendSuggestionConfig.emailCc.indexOf(keyword);
    if (index >= 0) {
      this.tenant.tenantConfig.sendSuggestionConfig.emailCc.splice(index, 1);
    }
  }

  pasteSuggestionEmailTo(event: ClipboardEvent): void {
    event.preventDefault();
    event.clipboardData.getData('Text').split(/;|,|\n/).forEach(value => {
      if (value.trim()) {
        this.tenant.tenantConfig.sendSuggestionConfig.emailTo.push(value.trim());
      }
    });
  }

  pasteSuggestionEmailCc(event: ClipboardEvent): void {
    event.preventDefault();
    event.clipboardData.getData('Text').split(/;|,|\n/).forEach(value => {
      if (value.trim()) {
        this.tenant.tenantConfig.sendSuggestionConfig.emailCc.push(value.trim());
      }
    });
  }

}
