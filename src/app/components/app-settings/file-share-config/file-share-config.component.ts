import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable } from 'rxjs';
import { Media, MediaFilter, MediaResponse } from 'src/app/models/media';
import { MediaService } from 'src/app/services/media.service';
import { ModalService } from 'src/app/services/modal.service';
import { saveAs } from 'file-saver';
import { Clipboard } from '@angular/cdk/clipboard';
import { Role } from 'src/app/models/enums/rolesEnum';

@Component({
  selector: 'app-file-share-config',
  templateUrl: './file-share-config.component.html',
  styleUrls: ['./file-share-config.component.scss']
})
export class FileShareConfigComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  filter = new MediaFilter();
  mediaData = new MediaResponse();
  allMedia = new Array<Media>();
  dataSource: MatTableDataSource<Media>;
  observable: Observable<any>;
  formData = new FormData();
  tableLength = 0;
  writer = [Role.SystemAdmin, Role.Admin, Role.Supervisor, Role.Writer];
  imgExtenList = ['jpg', 'jpeg', 'jfif', 'apng', 'avif', 'gif', 'pjpeg', 'pjp', 'png', 'svg', 'webp'];
  videoExtenList = ['mp4', 'webm', 'ogg'];

  constructor(private mediaService: MediaService, private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private modalService: ModalService, private clipboard: Clipboard) { }

  ngOnInit(): void {
    this.getAllMedia();
  }

  async getAllMedia() {
    this.loader.startLoader('l-1');
    this.mediaData = await this.mediaService.getAllMedia(this.filter);
    this.allMedia = this.mediaData.mediaFiles;
    this.tableLength = this.mediaData.totalNum;
    if (this.allMedia != null) {
      this.allMedia.forEach(m => {
        const ext = m.originalName.split('.');
        const e = ext[ext.length - 1];
        if (this.videoExtenList.find(v => v === e)) {
          m.isVideo = true;
        } else {
          m.isVideo = false;
        }
      });
    }
    this.dataSource = new MatTableDataSource<Media>(this.allMedia);
    this.observable = this.dataSource.connect();
    this.loader.stopLoader('l-1');
  }

  searchMedia() {
    this.paginator.pageIndex = 0;
    this.getAllMedia();
  }

  private scrollIntoView() {
    const topOfPage = document.getElementById('topOfPage');
    if (topOfPage !== null) {
      topOfPage.scrollIntoView();
    }
  }

  async uploadImage(event) {
    this.loader.startLoader('l-1');
    const file = event.target.files[0];
    if (file) {
      this.formData.append('file', file, file.filename);
      this.mediaService.uploadImage(this.formData).then(async data => {
        const result = await data;
        if (result) {
          this.snackBar.open('Uploaded Successfully', 'OK', { duration: 3500 });
        }
        this.filter = new MediaFilter();
        this.getAllMedia();
        this.loader.stopLoader('l-1');
      });
    }
  }

  async multipleUpload(event, mediaInput) {
    this.loader.startLoader('l-1');
    for (let i = 0; i < event.target.files.length; i++) {
      const file = event.target.files[i];
      if (file) {
        this.formData = new FormData();
        this.formData.append('file', file, file.filename);
        this.mediaService.uploadImage(this.formData).then(async data => {
          const result = await data;
          if (result) {
          }
        });
      }
    }
    this.snackBar.open('Uploaded Successfully', 'OK', { duration: 3500 });
    mediaInput.clear();
    this.filter = new MediaFilter();
    this.getAllMedia();
    this.loader.stopLoader('l-1');
  }

  pageChange(event) {
    this.filter.fromIndex = (event.pageIndex * event.pageSize);
    this.filter.numOfRows = event.pageSize;
    this.getAllMedia();
    this.scrollIntoView();
  }

  deleteFile(fileName) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Media File',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.mediaService.deleteMedia(fileName).then(resp => {
          if (resp) {
            this.snackBar.open('Deleted Successfully', 'OK', { duration: 3000 });
            this.getAllMedia();
          }
        });
      }
    });
  }

  checked(event) {
    if (event.checked === true) {
      this.getAllMedia();
    } else {
      this.getAllMedia();
    }
  }

  copyUrl(url) {
    this.clipboard.copy(url);
    this.snackBar.open('Copied to Clipboard', 'OK', { duration: 2500 });
  }

  downloadMedia(filename) {
    this.mediaService.getImage(filename).subscribe(data => {
      saveAs(data, filename);
    });
  }
}
