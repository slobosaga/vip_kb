import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Role as Roles } from 'src/app/models/enums/rolesEnum';
import { Role } from 'src/app/models/role';
import { ConfigService } from 'src/app/services/config.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  roles = new Array<Role>();
  role: string;
  dataSource: MatTableDataSource<Role>;
  supervisor = [Roles.SystemAdmin, Roles.Admin, Roles.Supervisor];

  cols = ['name', 'isAdRole', '2FA', 'createdBy', 'createdDate', 'editorColor', 'action'];

  constructor(private loader: NgxUiLoaderService, private snackBar: MatSnackBar, private configService: ConfigService, private modalService: ModalService) {
    this.dataSource = new MatTableDataSource<Role>(this.roles);
  }

  ngOnInit(): void {
    this.getRoles();
  }

  async getRoles() {
    this.loader.startLoader('l-1');
    this.roles = await this.configService.getAllRoles();
    this.roles.forEach(r => {
      if (r.name.match('A1KB_AGENT')) {
        r.isAgentRole = true;
      }
    });
    this.dataSource = new MatTableDataSource<Role>(this.roles);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loader.stopLoader('l-1');
  }

  async createRole() {
    this.modalService.roleDialogOpen({
      title: 'Create New Role',
      message: 'Enter Role Name',
      cancelText: 'Cancel',
      confirmText: 'Save',
      role: { name: null, color: null, is2FA: false }
    });
    this.modalService.roleDialogConfirmed().subscribe(data => {
      this.loader.startLoader('l-1');
      if (data !== false) {
        if (data !== null && data !== undefined) {
          this.configService.createRole(data).then(res => {
            this.snackBar.open('Role Created Successfully', 'OK', { duration: 3500 });
            this.getRoles();
          });
        }
      } else {
        this.loader.stopLoader('l-1');
      }
    });
  }

  // table filter
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  updateRoleColor(event, id) {
    this.modalService.confirmDialogOpen({
      title: 'Update Editor Color',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.loader.startLoader('l-1');
        const role = this.roles.find(r => r.id === id);
        role.color = event;
        this.configService.updateRole(id, { color: role.color, is2FA: role.is2FA, name: role.name });
        setTimeout(() => {
          this.getRoles();
        }, 500);
        this.loader.stopLoader('l-1');
      } else {
        this.dataSource = new MatTableDataSource<Role>(this.roles);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  update2fa(id) {
    this.modalService.confirmDialogOpen({
      title: 'Update Two-Factor Auth',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.loader.startLoader('l-1');
        const role = this.roles.find(r => r.id === id);
        role.is2FA = !role.is2FA;
        this.configService.updateRole(id, { color: role.color, is2FA: role.is2FA, name: role.name });
        setTimeout(() => {
          this.getRoles();
        }, 500);
        this.loader.stopLoader('l-1');
      } else {
        this.dataSource = new MatTableDataSource<Role>(this.roles);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  deleteRole(id) {
    this.modalService.confirmDialogOpen({
      title: 'Delete Role',
      message: 'Are you sure?',
      cancelText: 'Cancel',
      confirmText: 'Yes'
    });
    this.modalService.confirmDialogConfirmed().subscribe(data => {
      if (data === true) {
        this.configService.deleteRole(id);
        this.getRoles();
        this.snackBar.open('Role Deleted Successfully', 'OK', { duration: 3500 });
      }
    });
  }

}
