import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Tenant, TenantView } from 'src/app/models/tenant';
import { ConfigService } from 'src/app/services/config.service';

@Component({
  selector: 'app-app-settings',
  templateUrl: './app-settings.component.html',
  styleUrls: ['./app-settings.component.scss']
})
export class AppSettingsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  tenants = new Array<Tenant>();
  dataSource: MatTableDataSource<Tenant>;
  displayedColumns = ['name', 'companyName', 'createdBy', 'createdDate', 'udpatedBy', 'updatedDate', 'action'];

  constructor(private configService: ConfigService, private router: Router, private loader: NgxUiLoaderService) { }

  ngOnInit(): void {
    this.loader.startLoader('l-1');
    this.getTenantData();
    this.loader.stopLoader('l-1');
  }

  getTenantData() {
    this.configService.getAllTenants().then(async data => {
      this.tenants = await data;
      this.dataSource = new MatTableDataSource<Tenant>(this.tenants);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (this.tenants != null) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  editTenant(id) {
    this.router.navigate(['/application-settings/edit-tenant/' + id]);
  }

  createTenant() {
    this.router.navigate(['/application-settings/create-tenant/']);
  }

  viewTenant(id) {
    this.router.navigate(['/application-settings/view-tenant/' + id]);
  }

}
