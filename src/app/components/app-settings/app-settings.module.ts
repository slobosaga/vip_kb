import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { RoleComponent } from './role/role.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { FileShareConfigComponent } from './file-share-config/file-share-config.component';
import { AppSettingsRoutingModule } from './app-settings-routing.module';
import { FormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/modules/angular-material/angular-material.module';
import { EditAppSettingsComponent } from './edit-app-settings/edit-app-settings.component';
import { EditEmailTemplateComponent } from './email-template/edit-email-template/edit-email-template.component';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { EditUserComponent } from './user-management/edit-user/edit-user.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { PipeSharedModule } from 'src/app/modules/pipe-shared/pipe-shared.module';
import { CKEditorModule } from 'ckeditor4-angular';



@NgModule({
  declarations: [EmailTemplateComponent, RoleComponent, UserManagementComponent, FileShareConfigComponent, EditAppSettingsComponent, EditEmailTemplateComponent, EditUserComponent],
  imports: [
    CommonModule,
    AppSettingsRoutingModule,
    AngularMaterialModule,
    MaterialFileInputModule,
    FormsModule,
    ColorPickerModule,
    PipeSharedModule,
    CKEditorModule
  ]
})
export class AppSettingsModule { }
