import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subscription } from 'rxjs';
import { TreeFlatNode, TreeNode } from 'src/app/base/models/treeNode';
import { CategorySortRequest } from 'src/app/models/category';
import { ArticleService } from 'src/app/services/article.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss']
})
export class TreeViewComponent implements OnInit, OnChanges, OnDestroy {
  // tslint:disable-next-line: no-input-rename
  @Input('treeNodes') treeNodes = new Array<TreeNode>();
  @Input() webPortalId: string;
  @Input() filter: string;
  @Output() sortOrderEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  treeControl: FlatTreeControl<TreeFlatNode>;
  treeFlattener: MatTreeFlattener<TreeNode, TreeFlatNode>;
  dataSource: MatTreeFlatDataSource<TreeNode, TreeFlatNode>;
  expandedNodeSet = new Set<number>();
  dragging = false;
  expandTimeout: any;
  expandDelay = 1000;
  subscirber: Subscription;
  sort = 0;
  categoriesSortRequest: Array<CategorySortRequest>;
  changedData;

  constructor(private eventEmitter: EventEmitterService, private snackBar: MatSnackBar, private articleService: ArticleService, private modalService: ModalService, private loader: NgxUiLoaderService) {
  }

  ngOnInit(): void {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<TreeFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    this.dataSource.data = this.treeNodes;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.subscirber = this.eventEmitter.emmitTreeNodes.subscribe(data => {
      this.dataSource.data = data;
      if (this.filter !== '') {
        this.treeControl.expandAll();
      }
    });
  }

  ngOnDestroy(): void {
    this.subscirber.unsubscribe();
  }

  transformer = (node: TreeNode, level: number) => {
    return new TreeFlatNode(!!node.hasChildren, node.name, level, node.parentId, node.id);
  }
  private getLevel = (node: TreeFlatNode) => node.level;
  private isExpandable = (node: TreeFlatNode) => node.hasChildren;
  private getChildren = (node: TreeNode): TreeNode[] => node.children;
  hasChild = (_: number, nodeData: TreeFlatNode) => nodeData.hasChildren;

  visibleNodes(): TreeNode[] {
    this.forgetMissingExpandedNodes(this.treeControl, this.expandedNodeSet);
    this.rememberExpandedTreeNodes(this.treeControl, this.expandedNodeSet);
    const result = [];

    function addExpandedChildren(node: TreeNode, expanded: Set<number>) {
      result.push(node);
      if (expanded.has(node.id)) {
        node.children.map(child => addExpandedChildren(child, expanded));
      }
    }
    this.dataSource.data.forEach(node => {
      addExpandedChildren(node, this.expandedNodeSet);
    });
    return result;
  }

  drop(event: CdkDragDrop<string[]>) {
    const visibleNodes = this.visibleNodes();
    const changedData = JSON.parse(JSON.stringify(this.dataSource.data));
    if (!event.isPointerOverContainer) { return; }

    // determine where to insert the node
    const nodeAtDest: TreeNode = visibleNodes[event.currentIndex];
    const newSiblings = this.findNodeSiblings(changedData, nodeAtDest.id);
    if (!newSiblings) { return; }
    let insertIndex = newSiblings.findIndex(n => n.id === nodeAtDest.id);

    // remove the node from its old place
    const node = event.item.data;
    const siblings = this.findNodeSiblings(changedData, node.id);
    const siblingIndex = siblings.findIndex(n => n.id === node.id);
    const nodeToInsert: TreeNode = siblings.splice(siblingIndex, 1)[0];

    if (this.webPortalId !== null && (nodeToInsert.parentId !== null || nodeAtDest.parentId !== null)) {
      if (nodeAtDest.parentId !== nodeToInsert.parentId) {
        this.snackBar.open('Categories cannot change parents while a Web Portal is selected', 'OK', { duration: 3000 });
        return;
      }
    }

    // index offset
    if (nodeToInsert.parentId !== null && event.previousIndex < event.currentIndex && nodeToInsert.parentId !== nodeAtDest.parentId) {
      insertIndex = insertIndex + 1;
    }

    // set parentId
    if (nodeAtDest.parentId !== null) {
      nodeToInsert.parentId = nodeAtDest.parentId;
    } else {
      nodeToInsert.parentId = null;
    }

    if (nodeAtDest.id === nodeToInsert.id) { return; }
    // insert node
    newSiblings.splice(insertIndex, 0, nodeToInsert);
    // rebuild tree with mutated data
    this.rebuildTreeForData(changedData);
  }

  findNodeSiblings(arr: Array<TreeNode>, id: number): Array<TreeNode> {
    let result;
    let subResult;
    arr.forEach((item: TreeNode, i) => {
      if (item.id === id) {
        result = arr;
        // if (this.expandedNodeSet.has(item.id)) {
        //   subResult = item.children;
        //   if (subResult) { result = subResult; }
        // }
      } else if (item.children) {
        subResult = this.findNodeSiblings(item.children, id);
        if (subResult) { result = subResult; }
      }
    });
    return result;
  }

  dragStart() {
    this.dragging = true;
  }

  dragEnd() {
    this.dragging = false;
  }

  dragHover(node: TreeFlatNode) {
    if (this.dragging) {
      clearTimeout(this.expandTimeout);
      this.expandTimeout = setTimeout(() => {
        this.treeControl.expand(node);
      }, this.expandDelay);
    }
  }

  dragHoverEnd() {
    if (this.dragging) {
      clearTimeout(this.expandTimeout);
    }
  }

  rebuildTreeForData(data: any) {
    this.forgetMissingExpandedNodes(this.treeControl, this.expandedNodeSet);
    this.rememberExpandedTreeNodes(this.treeControl, this.expandedNodeSet);
    this.dataSource.data = data;
    this.eventEmitter.emmitTreeNodes.emit(data);
    this.expandNodesById(this.treeControl.dataNodes, Array.from(this.expandedNodeSet));
  }

  private rememberExpandedTreeNodes(treeControl: FlatTreeControl<TreeFlatNode>, expandedNodeSet: Set<number>) {
    if (treeControl.dataNodes) {
      treeControl.dataNodes.forEach((node) => {
        if (treeControl.isExpandable(node) && treeControl.isExpanded(node)) {
          expandedNodeSet.add(node.id);
        }
      });
    }
  }

  private forgetMissingExpandedNodes(treeControl: FlatTreeControl<TreeFlatNode>, expandedNodeSet: Set<number>) {
    if (treeControl.dataNodes) {
      expandedNodeSet.forEach((nodeId) => {
        // maintain expanded node state
        const node = treeControl.dataNodes.find((n) => n.id === nodeId);
        if (!node) {
          // if the tree doesn't have the previous node, remove it from the expanded list
          expandedNodeSet.delete(nodeId);
        } else {
          if (treeControl.isExpanded(node) === false) {
            expandedNodeSet.delete(nodeId);
          }
        }
      });
    }
  }

  private expandNodesById(flatNodes: TreeFlatNode[], ids: number[]) {
    if (!flatNodes || flatNodes.length === 0) {
      return;
    }
    const idSet = new Set(ids);
    return flatNodes.forEach((node) => {
      if (idSet.has(node.id)) {
        this.treeControl.expand(node);
        let parent = this.getParentNode(node);
        while (parent) {
          this.treeControl.expand(parent);
          parent = this.getParentNode(parent);
        }
      }
    });
  }

  private getParentNode(node: TreeFlatNode): TreeFlatNode | null {
    const currentLevel = node.level;
    if (currentLevel < 1) {
      return null;
    }
    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];
      if (currentNode.level < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  sortCategories(nodes: Array<TreeNode> = null) {
    if (nodes === null) {
      nodes = this.dataSource.data;
    }
    nodes.forEach(n => {
      this.sort++;
      this.categoriesSortRequest.push({ id: n.id, parentId: n.parentId, order: this.sort });
      if (n.children.length > 0) {
        this.sortCategories(n.children);
      }
    });
  }

  async saveSortOrder() {
    const options = {
      title: 'Are you sure?',
      message: 'Sort Categories',
      cancelText: 'No',
      confirmText: 'Yes'
    };
    this.modalService.confirmDialogOpen(options);
    this.modalService.confirmDialogConfirmed().subscribe(confirm => {
      if (confirm === true) {
        this.loader.startLoader('l-1');
        this.sort = 0;
        this.categoriesSortRequest = new Array<CategorySortRequest>();
        this.sortCategories();
        this.categoriesSortRequest.sort((a: CategorySortRequest, b: CategorySortRequest) => a.order - b.order);
        if (this.categoriesSortRequest.length > 0) {
          if (this.webPortalId === null) {
            this.articleService.sortCategories(this.categoriesSortRequest).then(data => {
              this.snackBar.open('Categories Sort Order Saved Successfully', 'OK', { duration: 3500 });
              this.sortOrderEvent.emit(true);
            }).catch(err => {
              this.loader.stopLoader('l-1');
            });
          } else {
            this.articleService.sortCategoriesByPortal(this.categoriesSortRequest, this.webPortalId).then(data => {
              this.snackBar.open('Categories Sort Order Saved Successfully', 'OK', { duration: 3500 });
              this.sortOrderEvent.emit(true);
            }).catch(err => {
              this.loader.stopLoader('l-1');
            });
          }
        }
      }
    });
  }

}
