import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './modules/angular-material/angular-material.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ArticleService } from './services/article.service';
import { HttpClientHelper } from './base/httpHelper';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './services/auth.service';
import { LoginSerivce } from './services/login.service';
import { NgxUiLoaderModule, NgxUiLoaderConfig } from 'ngx-ui-loader';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { DatePipe } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { AuthGuardService } from './services/auth-guard.service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS, } from '@angular/material/core';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { ArticleModule } from './components/article/article.module';
import { MatTreeModule } from '@angular/material/tree';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { EventEmitterService } from './services/event-emitter.service';
import { ConfirmDialogComponent } from './modals/confirm-dialog/confirm-dialog.component';
import { ModalService } from './services/modal.service';
import { InputModalComponent } from './modals/input-modal/input-modal.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SortModalComponent } from './modals/sort-modal/sort-modal.component';
import { RelatedArticlesModalComponent } from './modals/related-articles-modal/related-articles-modal.component';
import { AppSettingsComponent } from './components/app-settings/app-settings.component';
import { SiteSettingsModule } from './components/site-settings/site-settings.module';
import { AppSettingsModule } from './components/app-settings/app-settings.module';
import { CommentModalComponent } from './modals/comment-modal/comment-modal.component';
import { QueryService } from './services/query.service';
import { ForgotpassModalComponent } from './modals/forgotpass-modal/forgotpass-modal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PreviewImgModalComponent } from './modals/preview-img-modal/preview-img-modal.component';
import { CategoryModalComponent } from './modals/category-modal/category-modal.component';
import { ArticlePreviewModalComponent } from './modals/article-preview-modal/article-preview-modal.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { PipeSharedModule } from './modules/pipe-shared/pipe-shared.module';
import { ArticleNewLanguageModalComponent } from './modals/article-new-language-modal/article-new-language-modal.component';
import { RoleInputModalComponent } from './modals/role-input-modal/role-input-modal.component';
import { AddQueryconfigModalComponent } from './modals/add-queryconfig-modal/add-queryconfig-modal.component';
import { AppConfigService } from './base/configuration.service';
import { ColorPickerModule } from 'ngx-color-picker';
import { UserService } from './services/user.service';
import { AddArticleSectionModalComponent } from './modals/add-article-section-modal/add-article-section-modal.component';
import { DeletePortalModalComponent } from './modals/delete-portal-modal/delete-portal-modal.component';
import { RollbackModalComponent } from './modals/rollback-modal/rollback-modal.component';
import { GuidedHelpComponent } from './components/guided-help/guided-help.component';
import { QuestionModalComponent } from './modals/question-modal/question-modal.component';
import { DevicePreviewModalComponent } from './modals/device-preview-modal/device-preview-modal.component';
import { ReportModule } from './components/reports/report.module';
import { ReportingService } from './services/reporting.service';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: '#f77474',
  fgsColor: '#f77474',
  hasProgressBar: false
};

export const MY_FORMATS = {
  parse: {
    dateInput: 'dd-MM-yyyy',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM',
  },
};

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    ConfirmDialogComponent,
    InputModalComponent,
    SortModalComponent,
    RelatedArticlesModalComponent,
    AppSettingsComponent,
    CommentModalComponent,
    ForgotpassModalComponent,
    PreviewImgModalComponent,
    CategoryModalComponent,
    ArticlePreviewModalComponent,
    ArticleNewLanguageModalComponent,
    RoleInputModalComponent,
    AddQueryconfigModalComponent,
    AddArticleSectionModalComponent,
    DeletePortalModalComponent,
    RollbackModalComponent,
    GuidedHelpComponent,
    QuestionModalComponent,
    DevicePreviewModalComponent,
  ],
  imports: [
    BrowserModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    MaterialFileInputModule,
    NgxMatSelectSearchModule,
    HttpClientModule,
    ReactiveFormsModule,
    DragDropModule,
    FormsModule,
    MatTreeModule,
    ArticleModule,
    AppSettingsModule,
    SiteSettingsModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    SimpleNotificationsModule.forRoot({
      timeOut: 5000,
      showProgressBar: true,
      pauseOnHover: true,
      clickToClose: true,
      clickIconToClose: true,
      position: ['top', 'right']
    }),
    FlexLayoutModule,
    PipeSharedModule,
    ColorPickerModule,
    ReportModule,
  ],
  providers: [
    HttpClient,
    HttpClientHelper,
    ArticleService,
    AuthService,
    AuthGuardService,
    NotificationsService,
    LoginSerivce,
    DatePipe,
    { provide: MAT_DATE_LOCALE, useValue: 'en-RS' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    CookieService,
    ModalService,
    EventEmitterService,
    QueryService,
    UserService,
    ReportingService,
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [AppConfigService],
      useFactory: (appConfigService: AppConfigService) => {
        return () => {
          return appConfigService.loadAppConfig();
        };
      }
    }

  ],
  entryComponents: [
    ConfirmDialogComponent,
    InputModalComponent,
    SortModalComponent,
    RelatedArticlesModalComponent,
    CommentModalComponent,
    ForgotpassModalComponent,
    PreviewImgModalComponent,
    CategoryModalComponent,
    ArticlePreviewModalComponent,
    ArticleNewLanguageModalComponent,
    RoleInputModalComponent,
    AddQueryconfigModalComponent,
    AddArticleSectionModalComponent,
    DeletePortalModalComponent,
    RollbackModalComponent,
    QuestionModalComponent,
    DevicePreviewModalComponent
  ],
  exports: [ConfirmDialogComponent],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
