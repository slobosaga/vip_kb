import { Pipe, PipeTransform } from '@angular/core';
import { DecimalHelper } from 'src/app/base/decimal-helper';

const Type = {
  price: 'price' as 'price',
  minutes: 'minutes' as 'minutes',
  quantity: 'quantity' as 'quantity'
};
type Type = (typeof Type)[keyof typeof Type];
export { Type };
@Pipe({ name: 'mynumber' })

export class TransformNumberPipe implements PipeTransform {
  transform(value: number, type: Type, _decimalPlaces = null) {
    if (!isNaN(value)) {
      switch (type) {
        case 'price': {
          const decimalPlaces = _decimalPlaces != null ? _decimalPlaces : + localStorage.getItem('A1_Admin_MoneyAmountPrecisionDigits');
          value = DecimalHelper.roundDecimals(value, decimalPlaces);
          return new Intl.NumberFormat('en-US',
            {
              minimumFractionDigits: decimalPlaces,
              maximumFractionDigits: decimalPlaces,
              useGrouping: true
            })
            .format(value);
        }
        case 'minutes': {
          const decimalPlaces = _decimalPlaces != null ? _decimalPlaces : + localStorage.getItem('A1_Admin_MinutesAmountPrecisionDigits');
          return new Intl.NumberFormat('en-US',
            {
              minimumIntegerDigits: 1,
              minimumFractionDigits: decimalPlaces,
              maximumFractionDigits: decimalPlaces,
              useGrouping: true
            })
            .format(value);
        }
        case 'quantity': {
          const decimalPlaces = _decimalPlaces != null ? _decimalPlaces : 0;
          return new Intl.NumberFormat('en-US',
            {
              minimumIntegerDigits: 1,
              minimumFractionDigits: decimalPlaces,
              maximumFractionDigits: decimalPlaces,
              useGrouping: true
            })
            .format(value);
        }
      }
    }
  }
}

