export class DecimalHelper {
  static roundDecimals(value: number, decimalPlaces: number) {
      let sign = value >= 0 ? 1 : -1;
      return +(Math.round((value * Math.pow(10, decimalPlaces)) + (sign * 0.0001)) / Math.pow(10, decimalPlaces)).toFixed(decimalPlaces);
  }
}
