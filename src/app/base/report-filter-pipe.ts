import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reportFilter'
})

export class ReportFilterPipe implements PipeTransform {
  transform(type: string, field: string): boolean {
    const fields = [];

    switch(type) {
      case 'articleopenings':
        fields.push('date', 'webPortal', 'role');
        break;
      case 'articlegrades':
        fields.push('date', 'webPortal', 'role');
        break;
      case 'searchphrases':
        fields.push('date');
        break;
      case 'statuses':
        fields.push('date', 'webPortal');
        break;
      case 'categories':
        fields.push('webPortal');
        break;
      case 'bookmarks':
        fields.push('date', 'webPortal');
        break;
    }

    return !fields.includes(field);
  }
}
