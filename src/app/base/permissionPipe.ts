import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'permission'
})

export class PermissionPipe implements PipeTransform {
  permissions: string[];
  constructor() {
    this.permissions = JSON.parse(localStorage.getItem('A1_Admin_a1kbRoles'));
  }

  transform(permissionCode: string[]): boolean {
    let result = false;
    permissionCode.forEach(p => {
      if (this.permissions.find(pc => pc === p)) {
        result = true;
      }
    });
    return result;
  }
}
