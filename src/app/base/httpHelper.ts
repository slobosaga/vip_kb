import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AppConfigService } from '../base/configuration.service';
// tslint:disable: max-line-length


@Injectable()
export class HttpClientHelper {

  headers: HttpHeaders;
  blobHeaders: HttpHeaders;
  arrayBufferHeaders: HttpHeaders;
  reportHeaders: HttpHeaders;
  fileHeadres: HttpHeaders;
  apiurl;

  constructor(private http: HttpClient, private router: Router, private appConfigService: AppConfigService, private notificationService: NotificationsService, private loader: NgxUiLoaderService) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
    });
    this.blobHeaders = new HttpHeaders({
      'Content-Dispo': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    this.arrayBufferHeaders = new HttpHeaders({
      'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    });
    this.fileHeadres = new HttpHeaders({
      'Content-Disposition': 'multipart/form-data'
    });
    this.apiurl = appConfigService.apiBaseUrl;
    sessionStorage.setItem('A1_Admin_api', this.apiurl);
  }

  get(url: string, httpParams: HttpParams = null): any {
    return this.http.get(this.apiurl + url, {
      headers: this.headers.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')),
      withCredentials: false, params: httpParams
    }).toPromise().catch(err => {
      this.handleError(err); throw err.error;
    });
  }

  post(url: string, data: any, httpParams: HttpParams = null): any {
    return this.http.post(this.apiurl + url, data, { headers: this.headers.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), params: httpParams, withCredentials: false }).toPromise().catch(err => {
      if (url !== 'Auth/authenticate') {
        this.handleError(err);
      }
      throw err.error;
    });
  }

  put(url: string, data: any, httpParams: HttpParams = null): any {
    return this.http.put(this.apiurl + url, data, { headers: this.headers.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), withCredentials: false, params: httpParams }).toPromise().catch(err => {
      this.handleError(err);
    });
  }

  putObsv(url: string, data: any, httpParams: HttpParams = null): Observable<any> {
    return this.http.put(this.apiurl + url, data, { headers: this.headers.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), withCredentials: false, params: httpParams });
  }

  delete(url: string, httpParams: HttpParams = null): any {
    return this.http.delete(this.apiurl + url, { headers: this.headers.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), withCredentials: false, params: httpParams }).toPromise().catch(err => {
      this.handleError(err);
    });
  }

  public blob(url: string, httpParams: HttpParams = null) {
    return this.http.get(this.apiurl + url, { headers: this.blobHeaders.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), withCredentials: false, responseType: 'blob', params: httpParams });
  }

  postBlob(url: string, data: any, httpParams: HttpParams = null): any {
    return this.http.post(this.apiurl + url, data, { headers: this.blobHeaders.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), withCredentials: false, params: httpParams });
  }

  arrayBufferPost(url: string, data: any, httpParams: HttpParams = null): any {
    return this.http.post(this.apiurl + url, data, { headers: this.arrayBufferHeaders.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), params: httpParams, withCredentials: false }).toPromise().catch(err => {
      this.handleError(err); throw err.error;
    });
  }

  file(url: string, data: FormData, httpParams: HttpParams = null): any {
    return this.http.post(this.apiurl + url, data, { headers: this.fileHeadres.append('Authorization', 'bearer ' + localStorage.getItem('A1_Admin_accessToken')), withCredentials: false, params: httpParams }).toPromise();
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.message);
    } else {
      console.log(error);
      if (error.status === 400) {
        let errorMsg = '';
        if (error.error.errors) {
          for (const key in error.error.errors) {
            if (Object.prototype.hasOwnProperty.call(error.error.errors, key)) {
              const element = error.error.errors[key];
              errorMsg += element.toString() + '\n';
            }
          }
        } else {
          errorMsg = error.error;
        }
        this.notificationService.error('Error', errorMsg);
      } else if (error.status === 401) {
        this.notificationService.error('Error', error.error);
        this.router.navigate(['/login']);
      } else if (error.status === 403) {
        this.notificationService.error('Error', error.error);
      } else if (error.status === 0) {
        this.notificationService.error('Error', 'Service Unavailable');
      } else if (error.status === 500) {
        this.notificationService.error('Error', error.error);
      } else if (error.status === 404) {
        this.notificationService.error('404', 'Not Found');
      } else if (error) {
        this.notificationService.error('Error', error.error);
      }
      this.loader.stop();
      this.loader.stopAll();
      this.loader.stopLoader('l-1');
      this.loader.stopLoader('login');
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
