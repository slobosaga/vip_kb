export interface TreeNode {
  id: number;
  name: string;
  parentId: number;
  hasChildren: boolean;
  children: Array<TreeNode>;
}

export interface TreeNodeSort {
  id: number;
  name: string;
  parentId: number;
  hasChildren: boolean;
  children: Array<TreeNode>;
  expanded: boolean;
}


export class TreeFlatNode {
  constructor(
    public hasChildren: boolean,
    public name: string,
    public level: number,
    public parentId: number,
    public id: number
  ) { }
}
