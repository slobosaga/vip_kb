import { NgModule } from '@angular/core';
import { TransformNumberPipe } from './numberpipe';
import { PermissionPipe } from './permissionPipe';

@NgModule({
  imports: [

  ],
  declarations: [
    TransformNumberPipe,
    PermissionPipe
  ],
  exports: [
    TransformNumberPipe,
    PermissionPipe
  ]
})
export class PipesSharedModule { }
