import { Translations } from "./link-group";

export class Category {
  id: number;
  parentId: number;
  name: string;
  sort: number;
  tenantId: number;
  translations: Array<Translations>;
  createdBy: number;
  createdDate: Date;
  updatedBy: number;
  updatedDate: Date;

  constructor() {
    this.id = null;
    this.parentId = null;
    this.sort = null;
    this.tenantId = null;
    this.createdBy = null;
    this.translations = new Array<Translations>();
    this.tenantId = null;
    this.createdBy = null;
    this.createdDate = new Date();
    this.updatedBy = null;
    this.updatedDate = new Date();
  }
}

export class CategoryRequest {
  parentId: number;
  name: string;
  order: number;
  translations: Array<Translations>;

  constructor() {
    this.parentId = null;
    this.order = null;
    this.translations = new Array<Translations>();
    this.name = null;
  }
}

export class CategorySortRequest {
  id: number;
  order: number;
  parentId: number;

  constructor() {
    this.id = null;
    this.order = null;
    this.parentId = null;
  }
}
