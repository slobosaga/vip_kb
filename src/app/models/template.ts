export class Template {
  id: number;
  text: string;
  name: string;
  tenantId: number;
  createdBy: string;
  createdDate: Date;
  updatedBy: string;
  updatedDate: Date;

  constructor() {
    this.id = null;
    this.tenantId = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
  }
}

export class EditorTemplate {
  title: string;
  description: string;
  content: string;
}
