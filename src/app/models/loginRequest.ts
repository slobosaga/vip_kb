export class LoginRequest {
  email: string;
  password: string;
  appType = 'admin';
}
