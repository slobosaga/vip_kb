export class WebPortal {
  id: string;
  tenantId: number;
  type: number;
  portalTypeName: string;
  numOfListedArticles: number;
  name: string;
  roles: number[];
  rolesId: number[];
  categories: number[];
  logoUrl: string;
  logoHref: string;
  numOfPopularArticles: number;
  createdBy: string;
  createdDate: Date;
  updatedBy: string;
  updatedDate: Date;
  deletedBy: number;
  deletedDate: Date;
  expanded: boolean;

  constructor() {
    this.id = null;
    this.name = null;
    this.tenantId = null;
    this.type = null;
    this.roles = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
    this.deletedBy = null;
    this.deletedDate = null;
    this.logoHref = null;
    this.logoUrl = null;
    this.numOfPopularArticles = null;
    this.numOfListedArticles = null;
  }
}

export class WebPortalView {
  name: string;
  logoUrl: string;
  logoHref: string;
  type: string;
  role: number[];
  categories: number[];
  numOfPopularArticles: number;

  constructor() {
    this.role = null;
    this.categories = null;
  }
}

export class CreateWebPortalRequest {
  name: string;
  webPortalTypeId: number;
  roles: number[];
  categories: number[];
  logoUrl: string;
  logoHref: string;
  numOfPopularArticles: number;
  numOfListedArticles: number;

  constructor() {
    this.webPortalTypeId = null;
    this.roles = null;
    this.categories = null;
    this.numOfListedArticles = null;
  }
}

export class UpdateWebPortalRequest {
  name: string;
  logoUrl: string;
  logoHref: string;
  roles: number[];
  categories: number[];
  numOfPopularArticles: number;
  numOfListedArticles: number;

  constructor() {
    this.numOfListedArticles = null;
  }
}
