export class CkeditorConfig {
  extraPlugins: string;
  width: string;
  extraAllowedContent: string;
  removePlugins: string;
}

export class CkeditorFilter {
  allowedContent = ['snippet'];
}
