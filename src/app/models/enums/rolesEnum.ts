export enum Role {
  SystemAdmin = 'A1KB_SYSTEM_ADMINISTRATOR',
  Admin = 'A1KB_ADMINISTRATOR',
  Supervisor = 'A1KB_SUPERVISOR',
  Writer = 'A1KB_WRITER',
  Reader = 'A1KB_READER'
}
