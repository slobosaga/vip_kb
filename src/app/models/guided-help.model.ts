export class GuidedHelpQuestion {
  questionId: string;
  header: string;
  body: string;
  footer: string;
  answers: Array<GuidedHelpAnswer>;
  type: number;
  answerType: number;
  webPortalId: string;
  languageId: number;
  previousQuestionId: string;

  constructor() {
    this.questionId = null;
    this.header = null;
    this.body = null;
    this.footer = null;
    this.answers = new Array<GuidedHelpAnswer>();
    this.type = null;
    this.webPortalId = null;
    this.languageId = null;
    this.answerType = null;
    this.previousQuestionId = null;
  }
}

export class GuidedHelpAnswer {
  answerId: string;
  questionId: string;
  answer: string;

  constructor() {
    this.answerId = null;
    this.questionId = null;
    this.answer = null;
  }
}

export class GuidedHelpQuestionType {
  id: number;
  name: string;

  constructor() {
    this.id = null;
    this.name = null;
  }
}

export class GuidedHelpAnswerType {
  id: number;
  name: string;

  constructor() {
    this.id = null;
    this.name = null;
  }
}
