import { AutocompleteConfig, ElasticConfig, EmailConfig, EmailTemplateConfig, FileShareConfig, GeneralConfig, LdapConfigRequest, ReportConfig, ScheduleConfig, SectionConfig, SendSuggestionConfig, SshConfig } from "./configurations";
import { Language } from "./enums/language";

export class TenantRequest {
  companyName: string;
  name: string;
  apiJwToken: string;
  emailHost: string;
  emailLanguage: number;
  emailPassword: string;
  emailPort: number;
  emailSentFrom: string;
  emailUsername: string;
  expirationEmailTime: number;
  fileSharePath: string;
  numOfRelatedArticles: number;
  schedulePublishEmailTime: number;
  scheduleUnpublishEmailTime: number;
  sshHostName: string;
  sshPassword: string;
  sshSynonymFileName: string;
  sshUsername: string;
  ldapConfigs: Array<LdapConfigRequest>;
  languages: number[];
  elasticBaseAddress: string;
  elasticPort: number;
  kbPasswordTtl: number;
  latestArticlesDays: number;
  bookmarkArticlesDays: number;
  popularArticlesDays: number;
  autocompleteKeywordsCount: number;
  autocompleteArticlesCount: number;
  emailCategoryViewType: string;

  constructor() {
    this.emailLanguage = null;
    this.emailPort = null;
    this.expirationEmailTime = null;
    this.numOfRelatedArticles = null;
    this.schedulePublishEmailTime = null;
    this.scheduleUnpublishEmailTime = null;
    this.ldapConfigs = null;
    this.languages = null;
    this.elasticPort = null;
    this.kbPasswordTtl = null;
    this.latestArticlesDays = null;
    this.bookmarkArticlesDays = null;
    this.popularArticlesDays = null;
    this.autocompleteKeywordsCount = null;
    this.autocompleteArticlesCount = null;
  }
}

export class TenantConfiguration {
  tenantId: number;
  emailConfig: EmailConfig;
  sshConfig: SshConfig;
  scheduleConfig: ScheduleConfig;
  autocompleteConfig: AutocompleteConfig;
  emailTemplateConfig: EmailTemplateConfig;
  sectionConfig: SectionConfig;
  generalConfig: GeneralConfig;
  reportConfig: ReportConfig;
  sendSuggestionConfig: SendSuggestionConfig;
  ldapConfigs: Array<LdapConfigRequest>;
  languages: Array<Language>;

  constructor() {
    this.tenantId = null;
    this.emailConfig = new EmailConfig();
    this.sshConfig = new SshConfig();
    this.scheduleConfig = new ScheduleConfig();
    this.autocompleteConfig = new AutocompleteConfig();
    this.emailTemplateConfig = new EmailTemplateConfig();
    this.sectionConfig = new SectionConfig();
    this.generalConfig = new GeneralConfig();
    this.reportConfig = new ReportConfig();
    this.sendSuggestionConfig = new SendSuggestionConfig();
    this.ldapConfigs = new Array<LdapConfigRequest>();
    this.languages = new Array<Language>();
    this.reportConfig = new ReportConfig();
  }
}

export class TenantView {
  tenantId: number;
  companyName: string;
  name: string;
  tenantConfig: TenantConfiguration;
  elasticConfig: ElasticConfig;

  constructor() {
    {
      this.tenantId = null;
      this.companyName = null;
      this.name = null;
      this.tenantConfig = new TenantConfiguration();
      this.elasticConfig = new ElasticConfig();
    }
  }
}

export class Tenant {
  id: number;
  name: string;
  companyName: string;
  createdBy: string;
  createdDate: Date;
  updatedBy: string;
  updatedDate: Date;
  deleteBy: number;
  deleteDate: Date;

  constructor() {
    this.id = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
    this.deleteBy = null;
    this.deleteDate = null;
  }
}
