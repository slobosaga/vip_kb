export class Role {
  id: number;
  name: string;
  isAdRole: boolean;
  is2FA: boolean;
  color: string;
  createDate: Date;
  createdBy: string;
  isAgentRole: boolean;

  constructor() {
    this.id = null;
    this.createDate = null;
    this.isAgentRole = false;
    this.is2FA = false;
  }
}

export interface RoleUpdate {
  name: string;
  color: string;
  is2FA: boolean;
}
