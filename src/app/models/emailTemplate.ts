export class EmailTemplate {
  id: number;
  emailTypeId: number;
  subject: string;
  body: string;
  languageId: number;
  language: string;
  createdBy: number;
  createdDate: Date;
  updatedBy: number;
  updatedDate: Date;
  deletedBy: number;
  deletedDate: Date;

  constructor() {
    this.id = null;
    this.emailTypeId = null;
    this.languageId = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
    this.deletedBy = null;
    this.deletedDate = null;
  }
}

export class EmailTemplatePlaceholder {
  id: number;
  name: string;
  value: string;
}
