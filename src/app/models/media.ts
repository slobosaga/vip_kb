export class MediaFilter {
  filter: string;
  fromIndex: number;
  numOfRows: number;
  onlyAuthors: boolean;

  constructor() {
    this.filter = '';
    this.fromIndex = 0;
    this.numOfRows = 10;
    this.onlyAuthors = false;
  }
}

export class Media {
  fileShareName: string;
  originalName: string;
  isVideo: boolean;
  path: string;
  description: string;
  createdBy: string;
  createdDate: Date;
  updatedBy: string;
  updatedDate: Date;

  constructor() {
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
  }
}

export class MediaResponse {
  mediaFiles: Array<Media>;
  totalNum: number;

  constructor() {
    this.mediaFiles = new Array<Media>();
    this.totalNum = 0;
  }
}
