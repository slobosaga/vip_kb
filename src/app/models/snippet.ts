export class Snippet {
  id: number;
  name: string;
  text: string;
  deletedBy: number;
  deletedDate: Date;
  createdBy: string;
  createdDate: Date;
  updatedBy: string;
  updatedDate: Date;

  constructor() {
    this.id = null;
    this.deletedBy = null;
    this.deletedDate = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
  }
}
