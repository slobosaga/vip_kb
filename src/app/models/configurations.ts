export class LdapConfigRequest {
  id: number;
  tenantId: number;
  hostName: string;
  isSecure: boolean;
  port: number;
  baseDn: string;
  fqdn: string;
  domain: string;

  constructor() {
    this.isSecure = false;
    this.domain = null;
  }

}

export class EmailConfig {
  host: string;
  port: number;
  username: string;
  password: string;
  sentFromMail: string;
  language: number;

  constructor() {
    this.port = null;
    this.language = null;
  }
}

export class SshConfig {
  hostname: string;
  username: string;
  password: string;
}

export class FileShareConfig {
  path: string;
}

export class ElasticConfig {
  tenatId: number;
  baseAddress: string;
  port: number;
  id: number;
  username: string;
  password: string;

  constructor() {
    this.tenatId = null;
    this.port = null;
    this.id = null;
    this.baseAddress = null;
    this.username = null;
    this.password = null;
  }
}

export class ScheduleConfig {
  expirationEmailTime: number;
  publishEmailTime: number;
  unpublishEmailTime: number;

  constructor() {
    this.expirationEmailTime = null;
    this.publishEmailTime = null;
    this.unpublishEmailTime = null;
  }
}

export class AutocompleteConfig {
  keywordsCount: number;
  articlesCount: number;
  relatedQuestionsCount: number;

  constructor() {
    this.relatedQuestionsCount = null;
    this.keywordsCount = null;
    this.articlesCount = null;
  }
}

export class EmailTemplateConfig {
  emailLanguage: number;
  categoryView: string;

  constructor() {
    this.emailLanguage = null;
  }
}

export class SectionConfig {
  latestArticlesDays: number;
  popularArticlesDays: number;

  constructor() {
    this.latestArticlesDays = null;
    this.popularArticlesDays = null;
  }
}

export class GeneralConfig {
  apiJwTokenTtl: string;
  numOfRelatedArticles: number;
  kbPasswordTtl: number;
  lookupUserEmail: string;
  lookupUserPassword: string;
  numOfPreviousVersions: number;
  maxOtpAttempts: number;

  constructor() {
    this.apiJwTokenTtl = null;
    this.numOfPreviousVersions = null;
    this.numOfRelatedArticles = null;
    this.kbPasswordTtl = null;
    this.lookupUserEmail = null;
    this.lookupUserPassword = null;
    this.maxOtpAttempts = null;
  }
}

export class ReportConfig {
  emailList: Array<string>;
  dataPeriod: number;

  constructor() {
    this.emailList = new Array<string>();
    this.dataPeriod = null;
  }
}

export class SendSuggestionConfig {
  emailTo: Array<string>;
  emailCc: Array<string>;

  constructor() {
    this.emailTo = new Array<string>();
    this.emailCc = new Array<string>();
  }
}
