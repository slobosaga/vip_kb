export class JwToken {
  access_token: string;
  Expires: Date;
  tokenType: string;
  expires_in: number;
  refreshToken: string;
  isSystemAdmin: boolean;
  scope: string;
  roles: string[];
  message: string;
  isSuccess: boolean;
  userFullName: string;
  userId: number;
  webPortals: string;
  defaultLanguageId: number;
  defaultWebPortal: string;

  constructor() {
    this.Expires = new Date();
    this.expires_in = null;
    this.isSuccess = false;
    this.userId = null;
    this.defaultLanguageId = null;
    this.defaultWebPortal = null;
  }
}
