export class User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  roles: number[];
  isAdUser: boolean;
  phone: string;
  createdBy: number;
  createdDate: Date;
  updatedBy: number;
  updatedDate: Date;
  lastLoginTime: Date;
  deletedBy: number;
  deletedDate: Date;

  constructor() {
    this.id = null;
    this.roles = [];
    this.createdBy = null;
    this.phone = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
    this.deletedBy = null;
    this.deletedDate = null;
    this.isAdUser = false;
  }
}
