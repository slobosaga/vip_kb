import { Role } from "./role";
import { WebPortal } from "./webPortal";

export class ReportTypesResponse {
  displayName: string;
  labels: string;
  id: number;
  name: string;

  constructor() {
    this.displayName = null;
    this.labels = null;
    this.id = null;
    this.name = null;
  }

}

export class ReportFilters {
  reportType: ReportTypesResponse;
  webPortal: WebPortal;
  role: Role;
  fromDate: Date;
  untilDate: Date;
  reportDateFilterType: number | string;
	fromDate_excel: Date;
	untilDate_excel: Date;
  constructor() {
    this.reportType = new ReportTypesResponse();
    this.webPortal = new WebPortal();
    this.role = new Role();
    this.fromDate = new Date();
    this.untilDate = new Date();
    this.reportDateFilterType = 'Year';
  }
}

export class OpenedReadArticlesKpiTableRequest {
  languageId: number;
  reportFilterType: number | string;
  reportDateFilterType: number | string;
  sortColumn: number | string;
  orderType: number | string;
  roleId: number;
  webPortalId: string;
  numRecord: number;
  fromRecord: number;
  fromDate: Date;
  untilDate: Date;
  fromDate_excel: Date;
  untilDate_excel: Date;
  includeAll:boolean; 
  constructor() {
    this.languageId = null;
    this.reportFilterType = null;
    this.reportDateFilterType = null;
    this.sortColumn = null;
    this.orderType = null;
    this.roleId = null;
    this.webPortalId = null;
    this.numRecord = null;
    this.fromRecord = null;
    this.fromDate = null;
    this.untilDate = null;
    this.includeAll = true;
    this.fromDate_excel = null;
    this.untilDate_excel = null;
  }

}

export class OpenedReadArticlesKpiTableArticle {
  articleId: string;
  articleSubject: string;
  searchCount: number;
  treeCount: number;
  linkCount: number;
  total: number;
  breadcumbs: Array<string>;
 
  constructor() {
    this.articleId = null;
    this.articleSubject = null;
    this.searchCount = null;
    this.treeCount = null;
    this.linkCount = null;
    this.total = null;
    this.breadcumbs = new Array<string>();
   
  }
}

export class OpenedReadArticlesKpiTableResponse {
  articles: Array<OpenedReadArticlesKpiTableArticle>;
  total: number;

  constructor() {
    this.articles = new Array<OpenedReadArticlesKpiTableArticle>();
    this.total = null;
  }

}

export class OpenedReadArticlesKpiChartRequest {
  languageId: number;
  reportResponseType: number | string;
  reportFilterType: number | string;
  reportDateFilterType: number | string;
  roleId: number;
  webPortalId: string;
  fromDate: Date;
  untilDate: Date;

  constructor() {
    this.languageId = null;
    this.reportResponseType = null;
    this.reportFilterType = null;
    this.reportDateFilterType = null;
    this.roleId = null;
    this.webPortalId = null;
    this.fromDate = null;
    this.untilDate = null;
  }

}

export class OpenedReadArticlesKpiChartOpeningFilter {
  name: string;
  count: number;

  constructor() {
    this.name = null;
    this.count = null;
  }
}

export class OpenedReadArticlesKpiChartOpening {
  date: Date;
  total: number;
  filters: Array<OpenedReadArticlesKpiChartOpeningFilter>;

  constructor() {
    this.date = null;
    this.total = null;
    this.filters = new Array<OpenedReadArticlesKpiChartOpeningFilter>();
  }
}

export class OpenedReadArticlesKpiChartResponse {
  openings: Array<OpenedReadArticlesKpiChartOpening>;
  total: number;

  constructor() {
    this.openings = new Array<OpenedReadArticlesKpiChartOpening>();
    this.total = null;
  }
}

export class ArticleRatingsKpiTableRequest {
  languageId: number;
  reportFilterType: number | string;
  reportDateFilterType: number | string;
  reportResponseType: number | string;
  sortColumn: number | string;
  orderType: number | string;
  roleId: number;
  webPortalId: string;
  numRecord: number;
  fromRecord: number;
  fromDate: Date;
  untilDate: Date;
	fromDate_excel: Date;
	untilDate_excel: Date;

  constructor() {
    this.languageId = null;
    this.reportFilterType = null;
    this.reportDateFilterType = null;
    this.reportResponseType = null;
    this.sortColumn = null;
    this.orderType = null;
    this.roleId = null;
    this.webPortalId = null;
    this.numRecord = null;
    this.fromRecord = null;
    this.fromDate = null;
    this.untilDate = null;
    this.fromDate_excel = null;
    this.untilDate_excel = null;
  }
}

export class ArticleRatingsKpiTableArticle {
  articleId: string;
  articleSubject: string;
  yesCount: number;
  noCount: number;
  total: number;
  breadcumbs: Array<string>;

  constructor() {
    this.articleId = null;
    this.articleSubject = null;
    this.yesCount = null;
    this.noCount = null;
    this.total = null;
    this.breadcumbs = new Array<string>();
  }

}

export class ArticleRatingsKpiTableResponse {
  articles: Array<ArticleRatingsKpiTableArticle>;
  total: number;

  constructor() {
    this.articles = new Array<ArticleRatingsKpiTableArticle>();
    this.total = null;
  }
}

export class ArticleStatusesKpiTableRequest {
  languageId: number;
  reportDateFilterType: number | string;
  sortColumn: number | string;
  orderType: number | string;
  fromDate: Date;
  untilDate: Date;
  webPortalId: string;
  numRecord: number;
  fromRecord: number;
	fromDate_excel: Date;
	untilDate_excel: Date;
  constructor() {
    this.languageId = null;
    this.reportDateFilterType = null;
    this.sortColumn = null;
    this.orderType = null;
    this.fromDate = null;
    this.untilDate = null;
    this.webPortalId = null;
    this.numRecord = null;
    this.fromRecord = null;
  }

}

export class ArticleStatusesKpiTableStatus {
  draftCount: number;
  submittedCount: number;
  rejectedCount: number;
  needChangesCount: number;
  approvedCount: number;
  scheduledCount: number;
  publishedCount: number;
  unpublishedCount: number;
  total: number;
  webPortalName: string;

  constructor() {
    this.draftCount = null;
    this.submittedCount = null;
    this.rejectedCount = null;
    this.needChangesCount = null;
    this.approvedCount = null;
    this.scheduledCount = null;
    this.publishedCount = null;
    this.unpublishedCount = null;
    this.total = null;
    this.webPortalName = null;
  }

}

export class ArticleStatusesKpiTableResponse {
  statuses: Array<ArticleStatusesKpiTableStatus>;
  total: number;

  constructor() {
    this.statuses = Array<ArticleStatusesKpiTableStatus>();
    this.total = null;
  }
}

export class BookmarkStatisticsKpiTableRequest {
  languageId: number;
  reportFilterType: number | string;
  reportDateFilterType: number | string;
  reportResponseType: number | string;
  orderType: number | string;
  numRecord: number;
  fromRecord: number;
  roleId: number;
  webPortalId: string;
  fromDate: Date;
  untilDate: Date;
	fromDate_excel: Date;
	untilDate_excel: Date;
  constructor() {
    this.languageId = null;
    this.reportFilterType = null;
    this.reportDateFilterType = null;
    this.reportResponseType = null;
    this.orderType = null;
    this.numRecord = null;
    this.fromRecord = null;
    this.roleId = null;
    this.webPortalId = null;
    this.fromDate = null;
    this.untilDate = null;
  }

}

export class BookmarkStatisticsKpiTableBookmark {
  articleId: string;
  articleSubject: string;
  total: number;
  breadcrumbs: Array<string>;

  constructor() {
    this.articleId = null;
    this.articleSubject = null;
    this.total = null;
    this.breadcrumbs = new Array<string>();
  }

}

export class BookmarkStatisticsKpiTableResponse {
  bookmarks: Array<BookmarkStatisticsKpiTableBookmark>;
  total: number;

  constructor() {
    this.bookmarks = new Array<BookmarkStatisticsKpiTableBookmark>();
    this.total = null;
  }
}

export class KeywordsAndPhrasesKpiTableRequest {
  languageId: number;
  reportDateFilterType: number | string;
  orderType: number | string;
  numRecord: number;
  fromRecord: number;
  fromDate: Date;
  untilDate: Date;
  webPortalId: string;
  roleId: number;
  fromDate_excel: Date;
	untilDate_excel: Date;
  constructor() {
    this.languageId = null;
    this.reportDateFilterType = null;
    this.orderType = null;
    this.numRecord = null;
    this.fromRecord = null;
    this.fromDate = null;
    this.untilDate = null;
    this.webPortalId = null;
    this.roleId = null;
  }

}

export class KeywordsAndPhrasesKpiTableSearchPhrase {
  searchPhrase: string;
  total: number;

  constructor() {
    this.searchPhrase = null;
    this.total = null;
  }

}

export class KeywordsAndPhrasesKpiTableResponse {
  searchPhrases: Array<KeywordsAndPhrasesKpiTableSearchPhrase>;
  total: number;

  constructor() {
    this.searchPhrases = new Array<KeywordsAndPhrasesKpiTableSearchPhrase>();
    this.total = null;
  }

}

export class ArticleCategoryKpiTableRequest {
  languageId: number;
  webPortalId: string;
  numRecord: number;
  fromRecord: number;
  sortColumn: string | string;
  orderType: number | string;
	fromDate_excel: Date;
	untilDate_excel: Date;

  constructor() {
    this.languageId = null;
    this.webPortalId = null;
    this.numRecord = null;
    this.fromRecord = null;
    this.sortColumn = null;
    this.orderType = null;
    this.fromDate_excel = null;
    this.untilDate_excel = null;
  }

}

export class ArticleCategoryKpiTableCategory {
  webPortalName: string;
  total: string;
  level1: string;
  level2: string;
  level3: string;

  constructor() {
    this.webPortalName = null;
    this.total = null;
    this.level1 = null;
    this.level2 = null;
    this.level3 = null;
  }
}

export class ArticleCategoryKpiTableResponse {
  categories: Array<ArticleCategoryKpiTableCategory>;
  total: number;

  constructor() {
    this.categories = new Array<ArticleCategoryKpiTableCategory>();
    this.total = null;
  }

}
