import { CommentStatus, CommentType } from './comment';

export class ElasticComment {
  id: string;
  articleId: string;
  body: string;
  status: CommentStatus;
  type: CommentType;
  language: number;
  portalType: number;
  createdBy: number;
  createdDate: Date;

  constructor() {
    this.status = null;
    this.type = null;
    this.language = null;
    this.portalType = null;
    this.createdBy = null;
    this.createdDate = null;
  }
}

export class ElasticCommentType {
  id: number;
  name: string;

  constructor() {
    this.id = null;
  }
}

export class ElasticCommentStatus {
  id: number;
  name: string;

  constructor() {
    this.id = null;
  }
}
