export class CommentResponse {
  comments: Array<Comment>;
  total: number;

  constructor() {
    this.comments = new Array<Comment>();
    this.total = null;
  }
}

export class Comment {
  id: string;
  webPortalId: string;
  webPortalName: string;
  articleId: string;
  body: string;
  status: string;
  subject: string;
  type: string;
  languageId: number;
  language: string;
  portalType: string;
  userEmail: string;
  createdBy: number;
  createdDate: Date;
  updatedBy: number;
  updatedDate: Date;
  createdByFullName: string;
  updatedByFullName: string;

  constructor() {
    this.status = null;
    this.type = null;
    this.languageId = null;
    this.portalType = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
    this.webPortalName = null;
  }
}

export class CommentStatus {
  id: number;
  name: string;
  displayName: string;
}

export class CommentType {
  id: number;
  name: string;
  displayName: string;
}

export class InternalComment {
  userId: string;
  userFullName: string;
  text: string;
  commentDate: Date;
  initials: string;

  constructor() {
    this.userId = null;
    this.userFullName = null;
    this.text = null;
    this.commentDate = null;
    this.initials = null;
  }
}

export class ElasticComment {
  id: string;
  articleId: string;
  body: string;
  status: string;
  type: string;
  language: string;
  portalType: string;
  createdBy: number;
  createdDate: Date;
  userFullName: string;

  constructor() {
    this.createdBy = null;
    this.createdDate = null;
  }
}

export class CommentFilter {
  commentType: string;
  commentStatuses: string[];
  articleId: string;
  webPortalId: string;
  fromComment: number;
  numComments: number;

  constructor() {
    this.commentType = null;
    this.commentStatuses = null;
    this.articleId = null;
    this.webPortalId = null;
    this.fromComment = null;
    this.numComments = null;
  }
}
