export class QueryConfig {
  id: number;
  relevance: string;
  queryTypeId: number;
  isActive: boolean;
  boundaryScanner: string;
  boundaryScannerId: number;
  preTags: string;
  postTags: string;
  tenantId: number;
  fuzziness: number;
  queryTypeName: string;
  relevanceSerialized: Array<Relevance>;
  createdBy: number;
  createdDate: Date;
  updatedBy: number;
  updatedDate: Date;
  deletedBy: number;
  deletedDate: Date;

  constructor() {
    this.id = null;
    this.queryTypeId = null;
    this.isActive = null;
    this.boundaryScannerId = null;
    this.tenantId = null;
    this.fuzziness = null;
    this.relevanceSerialized = new Array<Relevance>();
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
    this.deletedBy = null;
    this.deletedDate = null;
  }
}

export class Relevance {
  field: string;
  boost: number;
  isSearchable: boolean;
  boostValues: Array<BoostValue>;

  constructor() {
    this.boost = null;
    this.isSearchable = false;
    this.boostValues = new Array<BoostValue>();
  }

}

export class BoostValue {
  boost: number;
  word: string;

  constructor() {
    this.boost = null;
  }
}

export class QueryArticleView {
  id: string;
  subject: string;
  htmlBody: string;
  excerpt: string;
  publishedDate: Date;
  featuredImage: string;
  hasAttachment: boolean;
  description: string;
  attachments: any;
  relatedArticles: any;
  categories: any;

  constructor() {
    this.publishedDate = null;
    this.hasAttachment = false;
  }
}

export class QueryFilter {
  webPortalId: string;
  languageId: number;
  queryTypeId: number;
  search: string;
  fromArticle: number;
  numArticles: number;
  categoryId: number;

  constructor() {
    this.webPortalId = null;
    this.languageId = null;
    this.queryTypeId = null;
    this.search = null;
    this.fromArticle = null;
    this.numArticles = null;
    this.categoryId = null;
  }
}

export class QueryRequest {
  relevance: Array<Relevance>;
  queryTypeId: number;
  preTags: string;
  postTags: string;
  fuzziness: number;

  constructor() {
    this.relevance = new Array<Relevance>();
    this.queryTypeId = null;
    this.fuzziness = null;
  }
}
