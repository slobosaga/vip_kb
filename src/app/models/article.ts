import { Breadcrumbs } from './breadcrumps';
import { Category } from './category';
import { ElasticComment, InternalComment } from './comment';
import { Role } from './role';

export class Article {

}

export class ArticleRequest {
  articleStatus: string;
  categories: Array<number>;
  scheduledDate: Date;
  subject: string;
  htmlBodyExternal: string;
  htmlBodyInternal: string;
  keywords: string[];
  excerpt: string;
  featuredImage: string | ArrayBuffer;
  scheduledUnpublishedDate: Date;
  sendSchedulePublishEmail: boolean;
  expirationDate: Date;
  expirationCategories: Array<number>;
  attachments: Array<ArticleAttachment>;
  seo: string;
  relatedArticles: Array<RelatedArticle>;
  templates: Array<number>;
  snippets: Array<number>;
  userRoles: Array<number>;
  description: string;
  emailTo: Array<string>;
  emailCc: Array<string>;
  comments: Array<InternalComment>;
  sendExpirationEmail: boolean;
  sendScheduleUnpublishEmail: boolean;
  relatedQuestions: string[];

  constructor() {
    this.articleStatus = null;
    this.categories = new Array<number>();
    this.scheduledDate = null;
    this.scheduledUnpublishedDate = null;
    this.sendSchedulePublishEmail = false;
    this.sendExpirationEmail = false;
    this.sendScheduleUnpublishEmail = false;
    this.expirationDate = null;
    this.expirationCategories = new Array<number>();
    this.attachments = new Array<ArticleAttachment>();
    this.relatedArticles = new Array<RelatedArticle>();
    this.templates = new Array<number>();
    this.snippets = new Array<number>();
    this.userRoles = new Array<number>();
    this.seo = null;
    this.categories = new Array<number>();
    this.comments = new Array<InternalComment>();
    this.htmlBodyExternal = null;
    this.htmlBodyInternal = null;
    this.relatedQuestions = [];
  }
}

export class ArticleGet {
  language: number;
  subject: string;
  htmlBodyExternal: string;
  htmlBodyInternal: string;
  keywords: Array<string>;
  excerpt: string;
  featuredImage: string | ArrayBuffer;
  scheduledUnpublishedDate: Date;
  attachments: Array<ArticleAttachment>;
  expirationDate: Date;
  seo: string;
  description: string;
  categories: Array<{ id: number, name: string }>;
  expirationCategories: Array<{ id: number, name: string }>;
  relatedArticles: Array<RelatedArticle>;
  comments: Array<InternalComment>;
  snippets: Array<{ id: number, name: string }>;
  templates: Array<{ id: number, name: string }>;
  breadcrumbs: Array<Category>;
  articleRevisions: Array<ArticleRevision>;
  elasticComments: Array<ElasticComment>;
  webPortals: Array<string>;
  scheduledDate: Date;
  sendExpirationEmail: boolean;
  sendScheduleUnpublishEmail: boolean;
  sendSchedulePublishEmail: boolean;
  articleStatus: string;
  emailTo: Array<string>;
  emailCc: Array<string>;
  userRoles = new Array<Role>();
  isLocked: boolean;
  relatedQuestions: string[];

  constructor() {
    this.language = null;
    this.htmlBodyExternal = null;
    this.htmlBodyInternal = null;
    this.scheduledUnpublishedDate = null;
    this.attachments = null;
    this.expirationDate = null;
    this.categories = new Array<{ id: number, name: string }>();
    this.expirationCategories = new Array<{ id: number, name: string }>();
    this.relatedArticles = null;
    this.comments = null;
    this.snippets = new Array<{ id: number, name: string }>();
    this.templates = new Array<{ id: number, name: string }>();
    this.breadcrumbs = null;
    this.articleRevisions = null;
    this.elasticComments = null;
    this.scheduledDate = null;
    this.sendExpirationEmail = false;
    this.sendSchedulePublishEmail = false;
    this.sendScheduleUnpublishEmail = false;
    this.featuredImage = null;
    this.articleStatus = '';
    this.seo = null;
    this.keywords = new Array<string>();
    this.relatedQuestions = [];
  }
}

export class ArticleView {
  id: string;
  subject: string;
  featuredImage: string;
  excerpt: string;
  isLocked: boolean;
  lockedBy: string;
  lockedDate: Date;
  isPublishedExternally: boolean;
  articleStatus: string;
  hasAttachment: boolean;
  description: string;
  articleBreadcrumbs: Array<Breadcrumbs>;
  createdByFullName: string;
  createdBy: number;
  createdDate: Date;
  updatedByFullName: string;
  updatedBy: number;
  updatedDate: Date;
  lockedDetails: string;
  order: number;

  constructor() {
    this.isLocked = false;
    this.lockedDate = null;
    this.isPublishedExternally = false;
    this.hasAttachment = false;
    this.articleBreadcrumbs = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
    this.lockedDetails = '';
    this.order = null;
  }
}

export enum ArticleStatus {
  Draft = 'draft',
  Submitted = 'submitted',
  NeedChanges = 'need_changes',
  Rejected = 'reject',
  Approved = 'approved',
  Scheduled = 'scheduled',
  Published = 'published',
  Unpublished = 'unpublished'
}

export class ArticleViewResponse {
  breadcrumbs: Array<Breadcrumbs>;
  articles: Array<ArticleView>;
  numOfArticles: number;

  constructor() {
    this.breadcrumbs = null;
    this.articles = null;
    this.numOfArticles = null;
  }
}

export class ArticleAttachment {
  attachment: string | ArrayBuffer;
  name: string;
  order: number;
  id: number;

  constructor() {
    this.order = 0;
    this.id = null;
  }
}

export class RelatedArticle {
  id: string;
  subject: string;
  order: number;

  constructor() {
    this.order = null;
  }
}

export class RelatedArticleObj {
  articles: Array<RelatedArticle>;
  total: number;

  constructor() {
    this.articles = new Array<RelatedArticle>();
    this.total = null;
  }
}

export class RelatedArticlePaging {
  categories: Array<number>;
  languageId: number;
  keyword: string;
  filterByCategory: boolean;

  constructor() {
    this.filterByCategory = false;
    this.categories = null;
    this.languageId = null;
    this.keyword = null;
  }
}

export class GenerateRelatedArticles {
  categories: Array<number>;
  keyword: string;
  alreadyRelatedArticles: Array<string>;
  languageId: number;
  articleId: string;

  constructor() {
    this.languageId = null;
    this.alreadyRelatedArticles = [];
    this.articleId = null;
  }
}

export class ArticleRevision {
  articleStatus: string;
  subject: string;
  htmlBodyInternal: string;
  htmlBodyExternal: string;
  keywords: Array<string>;
  excerpt: string;
  featuredImage: string | ArrayBuffer;
  seo;
  description: string;
  relatedQuestions: Array<string>;
  snippets = new Array<{ id: number, name: string }>();
  templates = new Array<{ id: number, name: string }>();


  constructor() {
    this.articleStatus = null;
  }
}

export class ArticleRevisions {
  id: number;
  subject: string;
  articleStatus: string;
  updatedBy: string;
  updatedDate: string;

  constructor() {
    this.id = null;
    this.subject = null;
    this.articleStatus = null;
    this.updatedBy = null;
    this.updatedDate = null;
  }
}

export class ArticlePaging {
  search: string;
  webPortalId: string;
  fromArticle: number;
  numArticles: number;
  onlyMine: boolean;
  categoryId: number;
  articleStatusId: number;
  dateCreated: Date;
  dateFromCreated: Date;
  dateToCreated: Date;
  datePublished: Date;
  dateFromPublished: Date;
  dateToPublished: Date;
  columnName: string;
  orderType: string;

  constructor() {
    this.fromArticle = 0;
    this.numArticles = 10;
    this.onlyMine = false;
    this.categoryId = null;
    this.articleStatusId = null;
    this.dateCreated = null;
    this.dateFromCreated = null;
    this.dateToCreated = null;
    this.dateFromPublished = null;
    this.datePublished = null;
    this.dateToPublished = null;
    this.webPortalId = null;
    this.orderType = 'asc';
    this.columnName = null;
  }
}

export class ArticleSort {
  categoryId: number;
  articles: Array<{ id: string, order: number }>;

  constructor() {
    this.categoryId = null;
    this.articles = new Array<{ id: string, order: number }>();
  }
}

export class FindReplaceArticleResult {
  locked: number;
  unlocked: number;
  articles: Array<FindAndReplaceArticles>;

  constructor() {
    this.locked = null;
    this.unlocked = null;
    this.articles = new Array<FindAndReplaceArticles>();
  }
}

export class FindAndReplaceArticles {
  articleId: string;
  subject: string;
  excerpt: string;
  articleStatus: string;
  webPortals: string;
  lockedDate: Date;
  lockedBy: string;
  lockedDetails: string;

  constructor() {
    this.articleId = null;
    this.subject = null;
    this.excerpt = null;
    this.articleStatus = null;
    this.webPortals = null;
    this.lockedDate = null;
  }
}

export class FindAndReplaceSnippetReslut {
  locked: number;
  unlocked: number;
  snippetCount: number;
  snippets: Array<FindAndRepalceSnippets>;

  constructor() {
    this.locked = null;
    this.unlocked = null;
    this.snippetCount = null;
    this.snippets = new Array<FindAndRepalceSnippets>();
  }
}

export class FindAndRepalceSnippets {
  id: number;
  name: string;
  text: string;
  createdBy: string;
  createdDate: Date;
  updatedBy: string;
  updatedDate: Date;

  constructor() {
    this.id = null;
    this.name = null;
    this.text = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updatedBy = null;
    this.updatedDate = null;
  }
}

export class Devices {
  deviceName: string;
  deviceDetails: DeviceDetails;

  constructor() {
    this.deviceDetails = new DeviceDetails();
  }
}

export class DeviceDetails {
  width: string;
  height: string;

  constructor() {
    this.width = null;
    this.height = null;
  }
}
