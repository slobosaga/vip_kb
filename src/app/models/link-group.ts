export class LinkGroupResponse {
  id: number;
  name: string;
  order: number;
  translations: Array<Translations>;
  links: Array<LinkResponse>;
  webPortalId: string;
  expanded: boolean;
  isSideGroup: boolean;

  constructor() {
    this.id = null;
    this.translations = new Array<Translations>();
    this.links = new Array<LinkResponse>();
    this.expanded = false;
  }
}

export class LinkGroupRequest {
  linkGroup: LinkGroupResponse;
  webPortalId: string;

  constructor() {
    this.linkGroup = null;
  }
}

export class LinkResponse {
  id: number;
  name: string;
  href: string;
  order: number;
  translations: Array<Translations>;
  expanded: boolean;

  constructor() {
    this.id = null;
    this.order = null;
    this.translations = new Array<Translations>();
    this.expanded = false;
  }
}

export class Translations {
  languageId: number;
  language: string;
  name: string;

  constructor() {
    this.languageId = null;
  }
}

export class LinkGroupSort {
  webPortalId: string;
  linkGroups: Array<{id: number, order: number}>;

  constructor() {
    this.webPortalId = null;
    this.linkGroups = new Array<{id: number, order: number}>();
  }
}

