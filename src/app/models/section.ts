import { ArrayType } from "@angular/compiler";
import { Category } from "./category";

export class Section {
  id: number;
  name: string;
  description: string;
  type: number;
  createdBy: number;
  createdDate: Date;

  constructor() {
    this.id = null;
    this.type = null;
    this.createdBy = null;
    this.createdDate = null;
  }
}


export class SectionPaging {
  sectionId: number;
  webPortalId: string;
  languageId: number;
  fromIndex: number;
  numOfRows: number;

  constructor() {
    this.sectionId = null;
    this.webPortalId = null;
    this.languageId = null;
    this.fromIndex = null;
    this.numOfRows = null;
  }
}

export class ArticleSection {
  articleId: string;
  webPortalId: string;
  order: number;

  constructor() {
    this.order = null;
  }
}

export class ArticleSectionGet {
  articleId: string;
  language: string;
  sectionId: number;
  webPortalId: string;
  webPortalName: string;
  subjec: string;
  description: string;
  excerpt: string;
  featuredImage: string;
  hasAttachment: boolean;
  isExternal: boolean;
  userRoles: Array<string>;
  order: number;
  publishedDate: Date;
  categories: Array<{ id: number, name: string }>;

  constructor() {
    this.sectionId = null;
    this.hasAttachment = false;
    this.isExternal = false;
    this.userRoles = new Array<string>();
    this.publishedDate = null;
    this.categories = new Array<{ id: number, name: string }>();
  }
}
