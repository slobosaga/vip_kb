export class Breadcrumbs {
  id: number;
  parentId: number;
  name: string;
  sort: number;
  tenantId: number;
  translations: string;
  isOnExternalPortal: boolean;
  createdBy: number;
  createdDate: Date;
  updateBy: number;
  updatedDate: Date;
  deletedBy: number;
  deletedDate: Date;

  constructor() {
    this.id = null;
    this.parentId = null;
    this.sort = null;
    this.tenantId = null;
    this.createdBy = null;
    this.createdDate = null;
    this.updateBy = null;
    this.updatedDate = null;
    this.deletedBy = null;
    this.deletedDate = null;
  }
}
