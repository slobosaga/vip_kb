tinymce.PluginManager.add("addRoles", function (editor, url) {
  var openDialog = function openDialog() {
    if (editor.dom.getParents(editor.selection.getNode()).find(function (ele) {
      return ele.localName.includes("role_");
    })) {
      return editor.windowManager.alert("Role Already Exists!");
    }

    for (var i = 0; i < editor.dom.getParents(editor.selection.getNode()).length; i++) {
      var element = editor.dom.getParents(editor.selection.getNode())[i];
      if (element.id.includes("role_")) {
        return editor.windowManager.alert("Role Already Exists!");
      }
    }

    var rolesColor = JSON.parse(localStorage.getItem("A1_Admin_articleBodyRoles"));
    var roles = [];
    rolesColor.forEach(function (r) {
      roles.push({
        value: r.value,
        text: r.text
      });
    });
    var bodyRoles = JSON.parse(localStorage.getItem("A1_Admin_bodyRole"));

    if (roles === null) {
      return editor.windowManager.alert("Please Select a Role!");
    } else if (roles.length === 0) {
      return editor.windowManager.alert("Please Select a Role!");
    }

    if (!editor.selection.getContent()) {
      return editor.windowManager.alert("Please Select a Paragraf of text!");
    }

    return editor.windowManager.open({
      title: "Article Roles",
      body: {
        type: "panel",
        items: [
          {
            type: "selectbox",
            name: "role",
            label: "Select Role",
            disabled: false,
            size: 1,
            items: roles
          }
        ]
      },
      buttons: [
        {
          primary: true,
          type: "submit",
          name: "submit",
          text: "Add"
        },
        {
          type: "cancel",
          name: "cancel",
          text: "Cancel"
        }
      ],
      onSubmit: function onSubmit(data) {
        var role = data.getData();
        var newBodyRole;
        var selectionEle = editor.selection.getNode();
        var selectionRange = editor.selection.getContent();
        var childEleCount = selectionEle.childElementCount;
        var counter = 1;

        if (selectionEle != role.role) {
          if (bodyRoles !== null) {
            var filteredBodyRole = bodyRoles.filter(function (b) {
              return b.role === role.role;
            });
            filteredBodyRole = filteredBodyRole.sort(function (a, b) {
              return a.id - b.id;
            });

            if (filteredBodyRole.length > 0) {
              counter = filteredBodyRole[filteredBodyRole.length - 1].id + 1;
            } else {
              counter = 1;
            }
          } else {
            bodyRoles = [];
          }

          newBodyRole = {
            role: role.role,
            id: counter,
            content: selectionRange
          };

          bodyRoles.push(newBodyRole);
          localStorage.setItem("A1_Admin_bodyRole", JSON.stringify(bodyRoles));
          var color = rolesColor.find(function (r) {
            return r.value === newBodyRole.role;
          }).color;

          var roleHtml = "";
          if (childEleCount === 0) {
            roleHtml = "<"
              .concat(role.role, ' id="')
              .concat(counter, '" style="background-color: ')
              .concat(color, '">')
              .concat(selectionRange, "</")
              .concat(role.role, ">");
          } else if (childEleCount > 0) {
            roleHtml = "<div id="
              .concat(role.role)
              .concat(counter, ' style="background-color: ')
              .concat(color, '"><')
              .concat(role.role, ' id="')
              .concat(counter, '">')
              .concat(selectionRange, "</")
              .concat(role.role, "></div>");
          }
          editor.execCommand("mceInsertContent", false, roleHtml);
          counter++;
        }
        data.close();
      }
    });
  };

  editor.ui.registry.addMenuButton("addRoles", {
    text: "Add Roles",
    onAction: function onAction() {
      openDialog();
    }
  });

  editor.ui.registry.addButton("addRoles", {
    text: "Add Roles",
    onAction: function onAction() {
      openDialog();
    }
  });
});
