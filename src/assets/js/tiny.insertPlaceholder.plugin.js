tinymce.PluginManager.add("insertPlaceholder", function (editor, url) {

  var openDialog = function openDialog() {
    var placeholders = [];
    placeholders = JSON.parse(localStorage.getItem("A1_Admin_placeholders"));
    if (placeholders === null) {
      return editor.windowManager.alert("There are no placeholders");
    }

    var placeholder = placeholders[0];
    return editor.windowManager.open({
      title: "Insert Placeholder",
      size: "large",
      body: {
        type: "panel",
        items: [
          {
            type: "listbox",
            name: "placeholders",
            label: "Select Placeholder",
            disabled: false,
            items: placeholders
          },
          {
            type: "iframe",
            name: "iframe",
            label: "Placeholder Preview",
            sandboxed: true
          }
        ]
      },
      buttons: [
        {
          primary: true,
          type: "submit",
          name: "submit",
          text: "Insert"
        },
        {
          type: "cancel",
          name: "cancel",
          text: "Cancel"
        }
      ],
      initialData: {
        iframe: placeholder.value
      },
      onChange: function onChange(dialogApi, details) {
        var data = dialogApi.getData();

        if (details.name === "placeholders") {
          dialogApi.setData({
            iframe: data.placeholders
          });
        }
      },
      onSubmit: function onSubmit(dialogApi, details) {
        var data = dialogApi.getData();
        editor.execCommand("mceInsertContent", false, data.placeholders);
        dialogApi.close();
      },
      onAction: function onAction(dialogApi, details) {
        // var dialogData = dialogApi.getData();
        // if (details.name === 'searchBtn') {
        //   if (dialogData.filter.length > 0) {
        //     snippets = snippets.filter(s => s.text.includes(dialogData.filter))
        //   } else {
        //     snippets = JSON.parse(localStorage.getItem('A1_Admin_snippets'));
        //   }
        // }
      }
    });
  };

  editor.ui.registry.addMenuButton("insertPlaceholder", {
    text: "Insert Placeholder",
    onAction: function onAction() {
      openDialog();
    }
  });
  editor.ui.registry.addButton("insertPlaceholder", {
    text: "Insert Placeholder",
    onAction: function onAction() {
      openDialog();
    }
  });
});
