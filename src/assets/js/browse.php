<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example: Browsing Files</title>
    <script>
        // Helper function to get parameters from the query string.
        function getUrlParam( paramName ) {
            var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
            var match = window.location.search.match( reParam );

            return ( match && match.length > 1 ) ? match[1] : null;
        }
        // Simulate user action of selecting a file to be returned to CKEditor.
        function returnFileUrl() {

            var funcNum = getUrlParam( 'CKEditorFuncNum' );
            var fileUrl = '/path/to/file.txt';
            window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
            window.close();
        }
    </script>
</head>
<body>
    <button onclick="returnFileUrl()">Select File</button>
</body>
</html> -->
<?php
header("Content-Type: text/html; charset=utf-8\n");
header("Cache-Control: no-cache, must-revalidate\n");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

// e-z params
$dim = 150;         /* image displays proportionally within this square dimension ) */
$cols = 4;          /* thumbnails per row */
$thumIndicator = '_th'; /* e.g., *image123_th.jpg*) -> if not using thumbNails then use empty string */
?>
<!DOCTYPE html>
<html>

<head>
  <title>browse file</title>
  <meta charset="utf-8">

  <style>
    html,
    body {
      padding: 0;
      margin: 0;
      background: black;
    }

    table {
      width: 100%;
      border-spacing: 15px;
    }

    td {
      text-align: center;
      padding: 5px;
      background: #181818;
    }

    img {
      border: 5px solid #303030;
      padding: 0;
      verticle-align: middle;
    }

    img:hover {
      border-color: blue;
      cursor: pointer;
    }
  </style>

</head>


<body>

  <table>

    <?php
