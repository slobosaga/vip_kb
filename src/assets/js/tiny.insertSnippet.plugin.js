tinymce.PluginManager.add("insertSnippet", function (editor, url) {
  var openDialog = function openDialog() {
    var snippets = [];
    snippets = JSON.parse(localStorage.getItem("A1_Admin_snippets"));

    if (snippets === null) {
      return editor.windowManager.alert("There are no snippets");
    }

    var snippet = snippets[0];
    return editor.windowManager.open({
      title: "Insert Snippet",
      size: "large",
      body: {
        type: "panel",
        items: [
          {
            type: "listbox",
            name: "snippets",
            label: "Select Snippet",
            disabled: false,
            items: snippets
          },
          {
            type: "iframe",
            name: "iframe",
            label: "Snippet Preview",
            sandboxed: true
          }
        ]
      },
      buttons: [
        {
          primary: true,
          type: "submit",
          name: "submit",
          text: "Insert"
        },
        {
          type: "cancel",
          name: "cancel",
          text: "cancel"
        }
      ],
      initialData: {
        iframe: snippet.value
      },
      onChange: function onChange(dialogApi, details) {
        var data = dialogApi.getData();

        if (details.name === "snippets") {
          console.log(data);
          dialogApi.setData({
            iframe: data.snippets
          });
        }
      },
      onSubmit: function onSubmit(dialogApi, details) {
        var data = dialogApi.getData();
        console.log(data.snippets);
        editor.insertContent(data.snippets);
        dialogApi.close();
      },
      onAction: function onAction(dialogApi, details) {
        var dialogData = dialogApi.getData();

        if (details.name === "searchBtn") {
          if (dialogData.filter.length > 0) {
            snippets = snippets.filter(function (s) {
              return s.text.includes(dialogData.filter);
            });
          } else {
            snippets = JSON.parse(localStorage.getItem("A1_Admin_snippets"));
          }
        }
      }
    });
  };

  editor.ui.registry.addMenuButton("insertSnippet", {
    text: "Insert Snippet",
    onAction: function onAction() {
      openDialog();
    }
  });
  editor.ui.registry.addButton("insertSnippet", {
    text: "Insert Snippet",
    onAction: function onAction() {
      openDialog();
    }
  });
});
