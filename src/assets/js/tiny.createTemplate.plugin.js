"use strict";

tinymce.PluginManager.add("createTemplates", function (editor, url) {
  var openDialog = function openDialog() {
    var initialData = "";

    if (editor.selection.getContent()) {
      initialData = editor.selection.getContent();
    }

    return editor.windowManager.open({
      initialData: {
        templateBody: initialData
      },
      size: "large",
      title: "Create Template",
      body: {
        type: "panel",
        items: [
          {
            type: "input",
            name: "templateTitle",
            label: "Template Title"
          },
          {
            type: "textarea",
            name: "templateBody",
            label: "Template Body",
            maximized: true
          }
        ]
      },
      buttons: [
        {
          type: "cancel",
          name: "close",
          text: "Cancel"
        },
        {
          type: "submit",
          name: "submit",
          text: "Save",
          primary: true
        }
      ],
      onSubmit: function onSubmit(data) {
        var template = data.getData();

        if (template.templateTitle === "" || template.templateBody === "") {
          editor.windowManager.alert(
            "Template Title and/or Body cannot be empty."
          );
          localStorage.setItem("A1_Admin_saveTemplate", "false");
        } else {
          localStorage.setItem("A1_Admin_saveTemplate", "true");
          localStorage.setItem("A1_Admin_templateTitle", template.templateTitle);
          localStorage.setItem("A1_Admin_templateBody", template.templateBody);
        }

        data.close();
      },
      onCancel: function onCancel(data) {
        localStorage.setItem("A1_Admin_saveTemplate", "false");
        localStorage.setItem("A1_Admin_templateTitle", "");
        localStorage.setItem("A1_Admin_templateBody", "");
      }
    });
  };

  editor.ui.registry.addButton("createTemplates", {
    text: "Create Template",
    onAction: function onAction() {
      openDialog();
    }
  });
  editor.ui.registry.addMenuItem("createTemplates", {
    text: "Create Template",
    onAction: function onAction() {
      openDialog();
    }
  });
});
