tinymce.PluginManager.add("insertTemplate", function (editor, url) {
  var openDialog = function openDialog() {
    var templates = [];
    templates = JSON.parse(localStorage.getItem("A1_Admin_templates"));

    if (templates === null) {
      return editor.windowManager.alert("There are no templates");
    }

    var template = templates[0];
    return editor.windowManager.open({
      title: "Insert Template",
      size: "large",
      body: {
        type: "panel",
        items: [
          {
            type: "listbox",
            name: "templates",
            label: "Select Template",
            disabled: false,
            items: templates
          },
          {
            type: "iframe",
            name: "iframe",
            label: "Template Preview",
            sandboxed: true
          }
        ]
      },
      buttons: [
        {
          primary: true,
          type: "submit",
          name: "submit",
          text: "Insert"
        },
        {
          type: "cancel",
          name: "cancel",
          text: "cancel"
        }
      ],
      initialData: {
        iframe: template.value
      },
      onChange: function onChange(dialogApi, details) {
        var data = dialogApi.getData();

        if (details.name === "templates") {
          dialogApi.setData({
            iframe: data.templates
          });
        }
      },
      onSubmit: function onSubmit(dialogApi, details) {
        var data = dialogApi.getData();
        editor.execCommand("mceInsertContent", false, data.templates);
        dialogApi.close();
      },
      onAction: function onAction(dialogApi, details) {
        var dialogData = dialogApi.getData();

        if (details.name === "searchBtn") {
          if (dialogData.filter.length > 0) {
            templates = templates.filter(function (s) {
              return s.text.includes(dialogData.filter);
            });
          } else {
            templates = JSON.parse(localStorage.getItem("A1_Admin_templates"));
          }
        }
      }
    });
  };

  editor.ui.registry.addMenuButton("insertTemplate", {
    text: "Insert Template",
    onAction: function onAction() {
      openDialog();
    }
  });
  editor.ui.registry.addButton("insertTemplate", {
    text: "Insert Template",
    onAction: function onAction() {
      openDialog();
    }
  });
});
