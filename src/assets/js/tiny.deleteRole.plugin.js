tinymce.PluginManager.add("deleteRole", function (editor, url) {
  var openDialog = function openDialog() {
    var bodyRoles = JSON.parse(localStorage.getItem("A1_Admin_bodyRole"));

    if (!editor.selection.getContent()) {
      return editor.windowManager.alert("Please Select a Paragraf of text");
    }

    var selectionInner = editor.selection.getNode().innerHTML;
    var selectionEle = editor.selection.getNode();
    var childEleCount = editor.selection.getNode().childElementCount;

    if (childEleCount > 0) {
      if (selectionEle.localName.includes("role_")) {
        editor.dom.getParents(selectionEle).forEach(function (element) {
          if (element.id === selectionEle.localName + selectionEle.id) {
            editor.dom.remove(element);
          }
        });
        editor.execCommand("mceInsertContent", false, selectionInner);
      } else {
        return editor.windowManager.alert(
          "Please Select a Paragraf of text with a ROLE"
        );
      }
    } else {
      if (selectionEle.localName.includes("role_")) {
        editor.execCommand("mceReplaceContent", false, selectionInner);
      } else {
        return editor.windowManager.alert(
          "Please Select a Paragraf of text with a ROLE"
        );
      }
    }
  };

  editor.ui.registry.addMenuButton("deleteRole", {
    text: "Delete Role",
    onAction: function onAction() {
      openDialog();
    }
  });
  editor.ui.registry.addButton("deleteRole", {
    text: "Delete Role",
    onAction: function onAction() {
      openDialog();
    }
  });
});
