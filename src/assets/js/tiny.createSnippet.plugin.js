"use strict";

tinymce.PluginManager.add("createSnippets", function (editor, url) {
  var openDialog = function openDialog() {
    var initialData = "";

    if (editor.selection.getContent()) {
      initialData = editor.selection.getContent();
    }

    return editor.windowManager.open({
      initialData: {
        snippetBody: initialData
      },
      size: "large",
      title: "Create Snippet",
      body: {
        type: "panel",
        items: [
          {
            type: "input",
            name: "snippetTitle",
            label: "Snippet Title"
          },
          {
            type: "textarea",
            name: "snippetBody",
            label: "Snippet Body",
            maximized: true
          }
        ]
      },
      buttons: [
        {
          type: "cancel",
          name: "close",
          text: "Cancel"
        },
        {
          type: "submit",
          name: "submit",
          text: "Save",
          primary: true
        }
      ],
      onSubmit: function onSubmit(data) {
        var snippet = data.getData();
        console.log(snippet);

        if (snippet.snippetTitle === "" || snippet.snippetBody === "") {
          editor.windowManager.alert(
            "Snippet Title and/or Body cannot be empty."
          );
          localStorage.setItem("A1_Admin_saveSnippet", "false");
        } else {
          localStorage.setItem("A1_Admin_saveSnippet", "true");
          localStorage.setItem("A1_Admin_snippetTitle", snippet.snippetTitle);
          localStorage.setItem("A1_Admin_snippetBody", snippet.snippetBody);
        }

        data.close();
      },
      onCancel: function onCancel(data) {
        localStorage.setItem("A1_Admin_saveSnippet", "false");
        localStorage.setItem("A1_Admin_snippetTitle", "");
        localStorage.setItem("A1_Admin_snippetBody", "");
      }
    });
  };

  editor.ui.registry.addButton("createSnippets", {
    text: "Create Snippet",
    onAction: function onAction() {
      openDialog();
    }
  });
  editor.ui.registry.addMenuItem("createSnippets", {
    text: "Create Snippet",
    onAction: function onAction() {
      openDialog();
    }
  });
});
