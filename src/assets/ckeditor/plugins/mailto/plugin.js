CKEDITOR.plugins.add('mailto', {
  icons: 'mailto',
  init: function (editor) {
      editor.addCommand('mailto', new CKEDITOR.dialogCommand('mailtoDialog'));
      editor.ui.addButton('mailto', {
          label: 'Mail To',
          command: 'mailto',
          toolbar: 'tools,5',
          icons: 'mailto',
      });
      CKEDITOR.dialog.add('mailtoDialog', this.path + 'dialogs/mailto.js');
  }
});
