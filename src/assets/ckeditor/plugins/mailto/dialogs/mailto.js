CKEDITOR.dialog.add('mailtoDialog', function(editor) {
    const mailToPattern = /^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*;\s*|\s*$))+$/;
    const mailPattern = /^(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*;\s*|\s*$))*$/;
    return {
        title: 'Create Mail To Link',
        resizable: CKEDITOR.DIALOG_RESIZE_BOTH,
        minWidth: 600,
        minHeight: 500,
        contents: [{
            id: 'tab1',
            label: 'Create Mail To Link',
            title: 'Create Mail To Link',
            elements: [{
                    type: 'text',
                    id: 'mailTo',
                    label: 'To (separate with ;)',
                    validate: function() {
                        let regex = new RegExp(mailToPattern);
                        var val = this.getValue().trim();
                        if (val.length === 0) {
                            alert('Fields cannot be empty');
                            return false;
                        }
                        if (!regex.test(val)) {
                            alert('TO must be an valid email');
                            return false;
                        }
                    },
                },
                {
                    type: 'text',
                    id: 'mailCC',
                    label: 'CC (separate with ; or ,)',
                    validate: function() {
                        let regex = new RegExp(mailPattern);
                        var val = this.getValue().trim();
                        if (!regex.test(val)) {
                            alert('CC must be an valid email');
                            return false;
                        }
                    },
                },
                {
                    type: 'text',
                    id: 'mailBcc',
                    label: 'BCC',
                    validate: function() {
                        let regex = new RegExp(mailPattern);
                        var val = this.getValue().trim();
                        if (!regex.test(val)) {
                            alert('BCC must be an valid email');
                            return false;
                        }
                    },
                },
                {
                    type: 'text',
                    id: 'mailSubject',
                    label: 'Subject',
                    validate: function() {
                        var val = this.getValue().trim();
                        if (val.length < 1) {
                            alert('Fields cannot be empty');
                            return false;
                        }
                    },
                },
                {
                    type: 'textarea',
                    id: 'mailBody',
                    label: 'Body',
                    validate: function() {
                        var val = this.getValue().trim();
                        if (val.length < 1) {
                            alert('Fields cannot be empty');
                            return false;
                        }
                    },
                }
            ]
        }],
        onShow: function() {
            var dialog = this;
            var to, cc, bcc, subject, body;
            // check if text is selected
            var selection = editor.getSelection();
            var content = selection.getRanges()[0];
            var newEle = editor.document.createElement('div');
            newEle.append(content.cloneContents());
            if (newEle.getHtml() === '') {
                alert('Please select an element or text in the editor');
                dialog.hide();
            }
            // check for href tag
            var selection = editor.getSelection().getStartElement();
            var href = selection.getAttribute('href');
            const params = new URLSearchParams(href)
            for (const param of params) {
                if (param[0].includes('mailto')) {
                    to = param[0].split(':');
                    to = to[1];
                }
                if (param[0] === 'cc') {
                    cc = param[1];
                }
                if (param[0] === 'bcc') {
                    bcc = param[1];
                }
                if (param[0] === 'subject') {
                    subject = param[1];
                }
                if (param[0] === 'body') {
                    body = param[1];
                }
            }
            if (to !== '')
                dialog.setValueOf('tab1', 'mailTo', to);
            if (cc !== '')
                dialog.setValueOf('tab1', 'mailCC', cc);
            if (bcc !== '')
                dialog.setValueOf('tab1', 'mailBcc', bcc);
            if (subject !== '')
                dialog.setValueOf('tab1', 'mailSubject', subject);
            if (body !== '')
                dialog.setValueOf('tab1', 'mailBody', body);
        },
        onOk: function() {
            var dialog = this;
            var selection = editor.getSelection().getSelectedText();
            var to = dialog.getValueOf('tab1', 'mailTo');
            var cc = dialog.getValueOf('tab1', 'mailCC');
            var bcc = dialog.getValueOf('tab1', 'mailBcc');
            var subject = dialog.getValueOf('tab1', 'mailSubject');
            var body = dialog.getValueOf('tab1', 'mailBody');
            var mail = `<a href="mailto:${to}`
            if (cc !== '')
                mail = mail + `&cc=${cc}`;
            if (bcc !== '')
                mail = mail + `&bcc=${bcc}`;
            mail = mail + `&subject=${subject}&body=${body}">${selection}</a>`;
            // var mailTo = `<a href="mailto:${to}&cc=${cc}&bcc=${bcc}&subject=${subject}&body=${body}">${selection}</a>`;
            // mailTo.setAttribute('href', `mailto:${to}?cc=${cc}&bcc=${bcc}&subject=${subject}&body=${body}`);
            // mailTo.innerHtml = selection;
            console.log(mail);
            var mailtoEle = CKEDITOR.dom.element.createFromHtml(mail);
            console.log(mailtoEle);
            editor.insertElement(mailtoEle);
        }
    };
});