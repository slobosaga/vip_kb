CKEDITOR.plugins.add('snippets', {
  icons: 'snippet',
  init: function (editor) {
    editor.addCommand('snippets', new CKEDITOR.dialogCommand('snippetsDialog'));
    editor.ui.addButton('snippet', {
      label: 'Insert Snippet',
      command: 'snippets',
      toolbar: 'tools,2',
    });
    CKEDITOR.dialog.add('snippetsDialog', this.path + 'dialogs/snippets.js');
  }
});
