
CKEDITOR.dialog.add('snippetsDialog', function (editor) {
  var snippets = [];
  snippets = JSON.parse(localStorage.getItem('A1_Admin_snippets'));
  var snippetArray = setItems(snippets);
  return {
    title: 'Insert Snippet',
    resizable: CKEDITOR.DIALOG_RESIZE_NONE,
    minWidth: 600,
    minHeight: 500,
    contents: [
      {
        id: 'tab1',
        label: 'Select Snippet',
        title: 'Select Snippet Title',
        elements: [
          {
            type: 'text',
            id: 'search',
            label: 'Insert Snippet Title',
          },
          {
            type: 'button',
            id: 'buttonId',
            label: 'Filter',
            title: 'My title',
            onClick: function () {
              var dialog = this.getDialog();
              var filter = dialog.getValueOf('tab1', 'search');
              var selectEle = dialog.getContentElement('tab1', 'snippets');

              filter = filter.toString().trim();
              var filteredSnippets = snippets.filter(function (t) { return t.text.toUpperCase().includes(filter.toUpperCase()) });
              selectEle.clear();
              filteredSnippets.forEach(function (t) {
                selectEle.add(t.text, [t.value]);
              })
            }
          },
          {
            type: 'select',
            id: 'snippets',
            label: 'Select a snippet',
            'default': '',
            items: snippetArray,
            validate: function (api) {
              if (this.getValue().length < 1) {
                alert('Please Select a Snippet');
                return false;
              }
            },
            onChange: function (api) {
              var dialog = this.getDialog();
              var selectEle = dialog.getContentElement('tab1', 'snippets');
              var snippetHtml = dialog.getContentElement('tab1', 'snippetHtml').domId;
              document.getElementById(snippetHtml).innerHTML = selectEle.getValue();
              var snippEle = document.getElementById(snippetHtml);
              console.log((dialog.getContentElement('tab1', 'snippets')));
            }
          },
          {
            type: 'html',
            id: 'snippetHtml',
            html: 'Select a Snippet',
            style: 'white-space: normal'
          }
        ]
      }
    ],
    onOk: function () {
      var dialog = this;
      var snipEle = CKEDITOR.dom.element.createFromHtml(dialog.getValueOf('tab1', 'snippets'));
      console.log(snipEle);
      sessionStorage.setItem('addedSnippetId', snipEle.getAttribute('data-snippet'));
      editor.insertElement(snipEle);
      dialog.reset();
    }
  };
});

function setItems(snippets) {
  var items = [];
  if (snippets !== null) {
    snippets.forEach(function (snippet) {
      var item = [];
      item.push(snippet.text);
      item.push(snippet.value);
      items.push(item);
    });
  }
  return items;
}
