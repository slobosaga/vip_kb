CKEDITOR.dialog.add('rolesDialog', function (editor) {
  var selectedRoles = JSON.parse(localStorage.getItem("A1_Admin_articleBodyRoles"));
  var selectedRolesArray = setItems(selectedRoles);
  console.log(selectedRolesArray);
  return {
    title: 'Add Role',
    resizable: CKEDITOR.DIALOG_RESIZE_BOTH,
    minWidth: 600,
    minHeight: 200,
    contents: [
      {
        id: 'tab1',
        label: 'Add Role',
        title: 'Add Role',
        elements: [
          {
            type: 'text',
            id: 'search',
            label: 'Insert Role Title',
          },
          {
            type: 'button',
            id: 'buttonId',
            label: 'Filter',
            title: 'Filter',
            onClick: function () {
              var dialog = this.getDialog();
              var filter = dialog.getValueOf('tab1', 'search');
              var selectEle = dialog.getContentElement('tab1', 'roles');
              filter = filter.toString().trim();
              var filteredRoles = roles.filter(function (t) {
                return t.text.includes(filter)
              });
              selectEle.clear();
              filteredRoles.forEach(function (t) {
                selectEle.add(t.text, [t.value]);
              });
            }
          },
          {
            type: 'select',
            id: 'roles',
            label: 'Select Role',
            'default': '',
            items: selectedRolesArray,
            validate: function (api) {
              if (this.getValue().length < 1) {
                alert('Please Select a Placeholder');
                return false;
              }
            },
            onChange: function (api) {
              var dialog = this.getDialog();
              var selectedEle = dialog.getContentElement('tab1', 'roles');
              var role = selectedEle.getValue();
              var selection = editor.getSelection();
              var content = selection.getRanges()[0].cloneContents();
            },

          }
        ]
      }
    ],
    // TO-DO da li se brani vise rola na isti tekst?
    onShow: function () {
      var dialog = this;
      selectedRoles = JSON.parse(localStorage.getItem("A1_Admin_articleBodyRoles"));
      selectedRolesArray = setItems(selectedRoles);
      //set role dropdown
      var selectEle = dialog.getContentElement('tab1', 'roles');
      selectEle.clear();
      selectedRoles.forEach(r => {
        selectEle.add(r.text, [r.value]);
      });

      var container = editor.getSelection().getStartElement();
      container = container.clone(false);
      container.appendHtml(editor.getSelectedHtml().getHtml());

      if (container.$.attributes['data-role']) {
        // container.$.attributes['data-role'].nodeValue;
      }
      if (selectedRoles.length === 0) {
        alert('Please Select a Role');
        dialog.reset();
        dialog.hide();
      } else {
        var selection = editor.getSelection();
        var content = selection.getRanges()[0].cloneContents();
        var newEle = editor.document.createElement('div');
        newEle.append(content);
        var selection = container.getOuterHtml();
        if (newEle.getHtml() === '') {
          alert('Text must be selected');
          dialog.reset();
          dialog.hide();
        }
      }
    },
    onOk: function () {
      var dialog = this;
      // var sel = editor.getSelection();
      // var ranges = sel.getRanges();
      // var el = new CKEDITOR.dom.element("div");
      // for (var i = 0, len = ranges.length; i < len; ++i) {
      //   el.append(ranges[i].cloneContents());
      //   console.log(ranges[i].cloneContents(false).getHtml());
      //   console.log(editor.elementPath().block.getOuterHtml());
      // }
      // console.log(el.getHtml());
      var rolesInArticle = JSON.parse(localStorage.getItem('A1_Admin_bodyRoles'));
      var role = dialog.getValueOf('tab1', 'roles');
      var roleColor = selectedRoles.find(r => r.value === role).color;
      var container = editor.getSelection().getStartElement();
      container = container.clone(false);
      container.appendHtml(editor.getSelectedHtml().getHtml());
      if (container.$.attributes['data-role']) {
        container.$.attributes['data-role'].nodeValue = role;
        alert('Selected Text Already has the specifited role: ' + role.toUpperCase());
        dialog.reset();
        dialog.hide();
      }
      console.log(container);
      if (editor.getSelection().getRanges()[0].cloneContents().$.childNodes.length >= 2) {
        var newEle = editor.document.createElement('div');
        newEle.setAttribute(`data-role`, role);
        newEle.setAttribute('id', rolesInArticle.length);
        newEle.$.style.backgroundColor = roleColor;
        newEle.append(container);
        editor.insertElement(newEle);
        rolesInArticle.push({role: role, id: rolesInArticle.length, content: newEle.innerHtml})
      } else {
        container.setAttribute(`data-role`, role);
        container.setAttribute('id', 1);
        container.$.style.backgroundColor = roleColor;
        editor.insertElement(container);
        rolesInArticle.push({role: role, id: rolesInArticle.length, content: container.innerHtml})
      }
      dialog.reset();
    }
  }
});

function setItems(roles) {
  var items = [];
  if (roles !== null) {
    roles.forEach(role => {
      var item = [];
      item.push(role.text);
      item.push(role.value);
      items.push(item);
    });
  }
  return items;
}
