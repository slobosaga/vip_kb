CKEDITOR.plugins.add('roles', {
  icons: 'roles',
  init: function (editor) {
    editor.addCommand('roles', new CKEDITOR.dialogCommand('rolesDialog'));
    editor.ui.addButton('roles', {
      label: 'Add Role',
      command: 'roles',
      toolbar: 'tools,6',
      icons: 'roles',
    });
    CKEDITOR.dialog.add('rolesDialog', this.path + 'dialogs/roles.js');
  }
});
