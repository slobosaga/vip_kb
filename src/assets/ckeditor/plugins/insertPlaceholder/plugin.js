CKEDITOR.plugins.add('insertPlaceholder', {
  icons: 'insertPlaceholder',
  init: function (editor) {
    editor.addCommand('insertPlaceholder', new CKEDITOR.dialogCommand('insertPlaceholderDialog'));
    editor.ui.addButton('insertPlaceholder', {
      label: 'Insert Placeholder',
      command: 'insertPlaceholder',
      toolbar: 'tools,4',
      icons: 'insertPlaceholder',
    });
    CKEDITOR.dialog.add('insertPlaceholderDialog', this.path + 'dialogs/insertPlaceholder.js');
  }
});
