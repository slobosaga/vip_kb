CKEDITOR.dialog.add('insertPlaceholderDialog', function (editor) {
  var placeholders = [];
  placeholders = JSON.parse(localStorage.getItem('A1_Admin_placeholders'));
  var placeholderArray = setItems(placeholders);
  return {
    title: 'Insert Placeholder',
    resizable: CKEDITOR.DIALOG_RESIZE_BOTH,
    minWidth: 600,
    minHeight: 500,
    contents: [
      {
        id: 'tab1',
        label: 'Select Placeholder',
        title: 'Select Placeholder Title',
        elements: [
          {
            type: 'text',
            id: 'search',
            label: 'Insert Placeholder Title',
          },
          {
            type: 'button',
            id: 'buttonId',
            label: 'Filter',
            title: 'Filter',
            onClick: function () {
              var dialog = this.getDialog();
              var filter = dialog.getValueOf('tab1', 'search');
              var selectEle = dialog.getContentElement('tab1', 'placeholders');
              filter = filter.toString().trim().toUpperCase();
              var filteredPlaceholders = placeholders.filter(function (t) {
                return t.text.toUpperCase().includes(filter)
              });
              selectEle.clear();
              filteredPlaceholders.forEach(function (t) {
                selectEle.add(t.text, [t.value]);
              });
            }
          },
          {
            type: 'select',
            id: 'placeholders',
            label: 'Select Placeholder',
            'default': '',
            items: placeholderArray,
            validate: function (api) {
              if (this.getValue().length < 1) {
                alert('Please Select a Placeholder');
                return false;
              }
            },
            onChange: function (api) {
              var dialog = this.getDialog();
              var selectEle = dialog.getContentElement('tab1', 'placeholders');
              var placeholder = dialog.getContentElement('tab1', 'palceholderHtml');
              document.getElementById(placeholder).innerHTML = selectEle.getValue();
            }
          },
          {
            type: 'html',
            id: 'placeholderHtml',
            html: 'Select a Placeholder'
          }
        ]
      }
    ],
    onOk: function () {
      var dialog = this;
      editor.insertHtml(dialog.getValueOf('tab1', 'placeholders'));
    }
  }
});

function setItems(placeholders) {
  var items = [];
  if (placeholders !== null) {
    placeholders.forEach(function (placeholder) {
      var item = [];
      item.push(placeholder.text);
      item.push(placeholder.value);
      items.push(item);
    });
  }
  return items;
}
