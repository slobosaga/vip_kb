
CKEDITOR.dialog.add('templatesDialog', function (editor) {
  var templates = [];
  templates = JSON.parse(localStorage.getItem('A1_Admin_templates'));
  var templatesArray = setItems(templates);
  return {
    title: 'Insert Template',
    resizable: CKEDITOR.DIALOG_RESIZE_BOTH,
    minWidth: 600,
    minHeight: 500,
    contents: [
      {
        id: 'tab1',
        label: 'Select Template',
        title: 'Select Template Title',
        elements: [
          {
            type: 'text',
            id: 'search',
            label: 'Insert Template Title',
          },
          {
            type: 'button',
            id: 'buttonId',
            label: 'Filter',
            title: 'My title',
            onClick: function () {
              var dialog = this.getDialog();
              var filter = dialog.getValueOf('tab1', 'search');
              var selectEle = dialog.getContentElement('tab1', 'templates');

              filter = filter.toString().trim();
              var filteredTemplates = templates.filter(function (t) { return t.text.toUpperCase().includes(filter.toUpperCase()) });
              selectEle.clear();
              filteredTemplates.forEach(function (t) {
                selectEle.add(t.text, [t.value]);
              })
            }
          },
          {
            type: 'select',
            id: 'templates',
            label: 'Select a template',
            items: templatesArray,
            validate: function (api) {
              if (this.getValue().length < 1) {
                alert('Please Select a Template');
                return false;
              }
            },
            onChange: function (api) {
              var dialog = this.getDialog();
              var selectEle = dialog.getContentElement('tab1', 'templates');
              var templateHtml = dialog.getContentElement('tab1', 'templateHtml').domId;
              document.getElementById(templateHtml).innerHTML = selectEle.getValue();
            }
          },
          {
            type: 'html',
            id: 'templateHtml',
            html: 'Select a Template'
          }
        ]
      }
    ],
    onOk: function () {
      var dialog = this;
      editor.insertHtml(dialog.getValueOf('tab1', 'templates'));
    }
  };
});


function setItems(templates) {
  var items = [];
  if (templates !== null) {
    templates.forEach(function (template) {
      var item = [];
      item.push(template.text);
      item.push(template.value);
      items.push(item);
    });
  }
  return items;
}
