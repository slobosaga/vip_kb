CKEDITOR.plugins.add('templates', {
    icons: 'template',
    init: function (editor) {
        editor.addCommand('templates', new CKEDITOR.dialogCommand('templatesDialog'));
        editor.ui.addButton('template', {
            label: 'Insert Template',
            command: 'templates',
            toolbar: 'tools,3'
        });
        CKEDITOR.dialog.add('templatesDialog', this.path + 'dialogs/templates.js');
    }
});
