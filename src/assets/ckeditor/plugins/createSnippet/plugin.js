CKEDITOR.plugins.add('createSnippet', {
  icons: 'createSnippet',
  init: function (editor) {
    editor.addCommand('createSnippet', new CKEDITOR.dialogCommand('createSnippetsDialog'));
    editor.ui.addButton('createSnippet', {
      label: 'Create Snippet',
      command: 'createSnippet',
      toolbar: 'tools,0',
    });
    CKEDITOR.dialog.add('createSnippetsDialog', this.path + 'dialogs/createSnippets.js');
  }
});
