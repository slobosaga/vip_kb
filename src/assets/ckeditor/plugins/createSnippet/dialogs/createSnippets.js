CKEDITOR.dialog.add('createSnippetsDialog', function (editor) {
  return {
    title: 'Create Snippet',
    resizable: CKEDITOR.DIALOG_RESIZE_BOTH,
    minWidth: 600,
    minHeight: 500,
    contents: [
      {
        id: 'tab1',
        label: 'Create Snippet',
        title: 'Create Snippet',
        elements: [
          {
            type: 'text',
            id: 'title',
            label: 'Snippet Title',
            validate: function (api) {
              var val = this.getValue().trim();
              if (val.length < 1) {
                alert('Fields cannot be empty');
                return false;
              }
            },
          },
          {
            type: 'textarea',
            id: 'body',
            label: 'Snippet Body',
            cols: 30,
            rows: 30,
            validate: function () {
              var val = this.getValue().trim();
              if (val.length < 1) {
                alert('Fields cannot be empty');
                return false;
              }
            }
          }
        ]
      }
    ],
    onShow: function () {
      var dialog = this;
      var container = editor.getSelection().getStartElement();
      container = container.clone(false);
      container.appendHtml(editor.getSelectedHtml().getHtml());
      console.log(container.getOuterHtml());
      var selection = container.getOuterHtml();
      dialog.setValueOf('tab1', 'body', selection);
    },
    onOk: function () {
      var dialog = this;
      var http = new XMLHttpRequest();
      var url = sessionStorage.getItem('A1_Admin_api');
      var body = { name: dialog.getValueOf('tab1', 'title'), text: dialog.getValueOf('tab1', 'body') };
      http.open('POST', url + 'Article/snippets', true);
      http.setRequestHeader('Content-type', 'application/json');
      http.setRequestHeader('Authorization', `bearer ${localStorage.getItem('A1_Admin_accessToken')}`);

      http.onreadystatechange = function () {
        if (http.readyState == 4 && http.status == 200) {
          var snippets = JSON.parse(localStorage.getItem('A1_Admin_snippets'));
          snippets.push({ text: dialog.getValueOf('tab1', 'title'), value: dialog.getValueOf('tab1', 'body') });
          localStorage.setItem('A1_Admin_snippets', JSON.stringify(snippets));
          alert(http.responseText);
        }
      }
      http.send(JSON.stringify(body));
    }
  }
});
