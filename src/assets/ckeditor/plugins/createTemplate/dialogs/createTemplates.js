CKEDITOR.dialog.add('createTemplatesDialog', function (editor) {
  return {
    title: 'Create Template',
    resizable: CKEDITOR.DIALOG_RESIZE_BOTH,
    minWidth: 600,
    minHeight: 500,
    contents: [
      {
        id: 'tab1',
        label: 'Create Template',
        title: 'Create Template',
        elements: [
          {
            type: 'text',
            id: 'title',
            label: 'Template Title',
            validate: function (api) {
              var val = this.getValue().trim();
              if (val.length < 1) {
                alert('Fields cannot be empty');
                return false;
              }
            },
          },
          {
            type: 'textarea',
            id: 'body',
            label: 'Template Body',
            cols: 30,
            rows: 30,
            validate: function () {
              var val = this.getValue().trim();
              if (val.length < 1) {
                alert('Fields cannot be empty');
                return false;
              }
            }
          }
        ]
      }
    ],
    onShow: function () {
      var dialog = this;
      var selection = editor.getSelection();
      var content = selection.getRanges()[0];
      var newEle = editor.document.createElement('div');
      newEle.append(content.cloneContents());
      dialog.setValueOf('tab1', 'body', newEle.getHtml());
    },
    onOk: function () {
      var dialog = this;
      var http = new XMLHttpRequest();
      var url = sessionStorage.getItem('A1_Admin_api');
      var body = { name: dialog.getValueOf('tab1', 'title'), text: dialog.getValueOf('tab1', 'body') };
      http.open('POST', url + 'Article/templates', true);
      http.setRequestHeader('Content-type', 'application/json');
      http.setRequestHeader('Authorization', `bearer ${localStorage.getItem('A1_Admin_accessToken')}`)

      http.onreadystatechange = function () {
        if (http.readyState == 4 && http.status == 200) {
          templates = JSON.parse(localStorage.getItem('A1_Admin_templates'));
          templates.push({ name: dialog.getValueOf('tab1', 'title'), value: dialog.getValueOf('tab1', 'body') });
          localStorage.setItem('A1_Admin_templates', JSON.stringify(templates));
          alert('Template successfully created.');
        }
      }
      http.send(JSON.stringify(body));
    }
  }
})
