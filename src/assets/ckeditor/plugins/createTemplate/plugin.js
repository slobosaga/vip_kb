CKEDITOR.plugins.add('createTemplate', {
  icons: 'createTemplate',
  init: function (editor) {
    editor.addCommand('createTemplate', new CKEDITOR.dialogCommand('createTemplatesDialog'));
    editor.ui.addButton('createTemplate', {
      label: 'Create Template',
      command: 'createTemplate',
      toolbar: 'tools,1',
    });
    CKEDITOR.dialog.add('createTemplatesDialog', this.path + 'dialogs/createTemplates.js');
  }
});
//Test