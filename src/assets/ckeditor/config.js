﻿/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function(config) {
    config.uploadUrl = '/knowledgebaseapi/api/Media/ck';
    config.imageUploadUrl = '/knowledgebaseapi/api/Media/ck';
    config.filebrowserUploadUrl = '/knowledgebaseapi/api/Media/ck';
    config.width = '99.8%';
    config.height = '560px';
    config.removePlugins = 'image2';
    config.extraPlugins = 'image,templates,snippets,html5video,widgetselection,lineutils,uploadimage,uploadwidget,filebrowser,accordionList,collapsibleItem,iframedialog,iframe,mailto,createSnippet,createTemplate,dialog,insertPlaceholder,tableresize';
    config.extraAllowedContent = 'div[data-*]; p[data-*]; span[data-*]; div(*); p(*); span(*)';
    config.skin = 'n1theme';
    config.font_names = 'A1Serif-Bold;A1Serif-Regular;A1Sans-Bold;A1Sans-Regular;Verdana;' + config.font_names;
    config.font_defaultLabel = "Verdana";
    config.fontSize_defaultLabel = "16";
    config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}';
    config.entities_latin = false;
    config.allowedContent = true;
    config.basicEntities = false;
    config.entities = false;
    config.forceSimpleAmpersand = true;
    config.entities_additional = '#8364;'
};